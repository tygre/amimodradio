#
#	 AmiModRadio
#	 All of Aminet modules at your fingertips
#
#
#
#	 Copyright 2015-2024 Tygre <tygre@chingu.asia>
#
#	 This file is part of AmiModRadio.
#
#	 AmiModRadio is free software: you can redistribute it and/or modify
#	 it under the terms of the GNU General Public License as published by
#	 the Free Software Foundation, either version 3 of the License, or
#	 (at your option) any later version.
#
#	 AmiModRadio is distributed in the hope fthat it will be useful,
#	 but WITHOUT ANY WARRANTY; without even the implied warranty of
#	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#	 GNU General Public License for more details.
#
#	 You should have received a copy of the GNU General Public License
#	 along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.
#
#
#
#	 Entirely developed on an Amiga!
#	 (Except for source code versioning...)
#	 tygre@chingu.asia
#

# Main paths
EXEDIR          = bin/debug/
OBJECTSDIR      = o/debug/
EXEDIR_DIST     = bin/release/AmiModRadio/
OBJECTSDIR_DIST = o/release/
ifeq "$(strip $(filter dist, $(MAKECMDGOALS)))" "dist"
	# Main paths for release (target "dist") ONLY
	EXEDIR      = $(EXEDIR_DIST)
	OBJECTSDIR  = $(OBJECTSDIR_DIST)
endif

# Secondary paths
CACHEDIR   = $(EXEDIR)cache/
CATSDIR    = $(EXEDIR)catalogs/
DATADIR    = $(EXEDIR)data/
DOCDIR     = $(EXEDIR)doc/
ICONSDIR   = $(EXEDIR)icons/
IMAGESDIR  = $(EXEDIR)images/

# (Re)Source files
NON_UI_FILES = arexx.c blacklists.c controls.c controls_workers.c ftp.c httpx.c httpx_http.c httpx_https.c kvs.c locale.c log.c players.c players_workers.c prefs.c ratings.c ringhio.c sources.c utils.c utils_guis.c utils_socketbase.c utils_tasks.c version.c xad.c z_dictionary.c z_fortify.c z_ftpparse.c z_iniparser.c

UI_ACT_FILES = controls_act.c gui_act.c
SOURCES_ACT  = $(NON_UI_FILES) $(UI_ACT_FILES)
OBJECTS_ACT  = $(addprefix $(OBJECTSDIR), $(SOURCES_ACT:.c=.o))
FLAGS_ACT    = -DUI_ACT
EXE_ACT      = $(EXEDIR)AmiModRadio_ACT

UI_CLI_FILES = controls_cli.c
SOURCES_CLI  = $(NON_UI_FILES) $(UI_CLI_FILES)
OBJECTS_CLI  = $(addprefix $(OBJECTSDIR), $(SOURCES_CLI:.c=.o))
FLAGS_CLI    = -DUI_CLI
EXE_CLI      = $(EXEDIR)AmiModRadio_CLI

UI_MUI_FILES = controls_mui.c gui_mui.c
SOURCES_MUI  = $(NON_UI_FILES) $(UI_MUI_FILES)
OBJECTS_MUI  = $(addprefix $(OBJECTSDIR), $(SOURCES_MUI:.c=.o))
FLAGS_MUI    = -DUI_MUI
EXE_MUI      = $(EXEDIR)AmiModRadio_MUI

UI_REA_FILES = controls_rea.c gui_rea.c
SOURCES_REA  = $(NON_UI_FILES) $(UI_REA_FILES)
OBJECTS_REA  = $(addprefix $(OBJECTSDIR), $(SOURCES_REA:.c=.o))
FLAGS_REA    = -DUI_REA
EXE_REA      = $(EXEDIR)AmiModRadio_REA

SOURCES_AIO  = $(NON_UI_FILES) $(UI_ACT_FILES) $(UI_CLI_FILES) $(UI_MUI_FILES) $(UI_REA_FILES)
OBJECTS_AIO  = $(addprefix $(OBJECTSDIR), $(SOURCES_AIO:.c=.o))
FLAGS_AIO    = $(FLAGS_ACT) $(FLAGS_CLI) $(FLAGS_MUI) $(FLAGS_REA)
EXE_AIO      = $(EXEDIR)AmiModRadio

DATA	     = $(addprefix $(EXEDIR),     $(shell find data/     -mindepth 1 -maxdepth 1         | sed 's/ /?/g'))
CATSLOCALES	 = $(addprefix $(EXEDIR),     $(shell find catalogs/ -mindepth 1 -maxdepth 1 -type d | sed 's/ /?/g'))
DOCLOCALES   = $(addprefix $(EXEDIR),     $(shell find doc/      -mindepth 1 -maxdepth 1 -type d | sed 's/ /?/g'))
ICONS        = $(addprefix $(EXEDIR),     $(shell find icons/    -mindepth 1 -maxdepth 1         | sed 's/ /?/g'))
IMAGESSETS   = $(addprefix $(EXEDIR),     $(shell find images/   -mindepth 1 -maxdepth 1 -type d | sed 's/ /?/g'))

# Compiler and linker
CC     = vbccm68k -quiet -cpp-comments -DAMISSL_NO_STATIC_FUNCTIONS \
		 -IDevKits:sdk/classic/AmiSSL/Developer/include             \
		 -IDevKits:sdk/classic/ClassAct2/include_h/                 \
		 -Idevkits:sdk/classic/MUI/Developer/C/Include/             \
		 -Idevkits:sdk/classic/NDK_32R4/Include_H/                  \
		 -Idevkits:sdk/classic/Roadshow/netinclude/                 \
		 -IDevKits:sdk/classic/XAD/Include/C/                       \
		 -IVBCC:targets/m68k-amigaos/NDK_32/include/

AS     = vasmm68k_mot -quiet -Fhunk

LD     = vlink -bamigahunk -Cvbcc -Bstatic VLibOS3:startup.o
LDLIBS = -LVBCC:targets/m68k-amigaos/NDK_32/lib/ -lamiga -LVLibOS3: -lvc

# Because Make does not support spaces properly...
space :=
space +=
replaceQuestionBySpace = $(subst ?,$(space),$1)
replaceSpaceByQuestion = $(subst $(space),?,$1)



# ----------------------------------------

# target 'all' (default target, for debug)
all :  COFLAGS += -O=1 -g -hunkdebug -DFORTIFY -DLOG_UNRECOGNISED_MODULES
all :  ASFLAGS +=
all :  LDFLAGS += 
all :  $(EXEDIR)     \
	   $(OBJECTSDIR) \
	   $(EXE_ACT)    \
	   $(EXE_CLI)    \
	   $(EXE_MUI)    \
	   $(EXE_REA)    \
	   $(EXE_AIO)    \
	   catalogs      \
	   data          \
	   docs          \
	   images

# target 'dist' for release
dist : COFLAGS += -O=3 -size -final
dist : ASFLAGS += -opt-size
dist : LDFLAGS += -S -s -sc -sd -x
dist : $(EXEDIR)     \
	   $(OBJECTSDIR) \
	   $(EXE_ACT)    \
	   $(EXE_CLI)    \
	   $(EXE_MUI)    \
	   $(EXE_REA)    \
	   $(EXE_AIO)    \
	   catalogs      \
	   data          \
	   docs          \
	   icons         \
	   images        \
	   readmes       \
	   $(EXEDIR:/=).lha

# target 'clean'
clean : mostlyclean
	-rm -R $(CACHEDIR)
	-rm -R $(DATADIR)
	-rm -R $(DOCDIR)
	-rm -R $(ICONSDIR)
	-rm -R $(IMAGESDIR)

# target 'mostlyclean'
mostlyclean :
	-rm -R $(OBJECTSDIR)
	-rm -R $(EXEDIR_DIST)
	-rm -R $(OBJECTSDIR_DIST)
	-rm -R $(EXEDIR_DIST:/=).info

# target 'maintainer-clean'
maintainer-clean :
	-rm $(EXE_ACT) $(EXE_CLI) $(EXE_MUI) $(EXE_REA) $(EXE_AIO)
	-rm -R $(OBJECTSDIR)

# ----------------------------------------



# Directories
$(EXEDIR) :
	mkdir -p "$@"
	
$(OBJECTSDIR) :
	mkdir -p "$@"



# Objects and executable
$(OBJECTS_AIO) : $(OBJECTSDIR)%.o : %.c
	$(CC) $(COFLAGS) $< -o=T:vbcc.asm
	$(AS) $(ASFLAGS) T:vbcc.asm -o $@
	rm T:vbcc.asm

$(OBJECTSDIR)main_act.o : main.c
	$(CC) $(COFLAGS) $(FLAGS_ACT) $< -o=T:vbcc.asm
	$(AS) $(ASFLAGS) T:vbcc.asm -o $@
	rm T:vbcc.asm
$(EXE_ACT) : $(OBJECTS_ACT) $(OBJECTSDIR)main_act.o
	$(LD) $(LDFLAGS) $^ $(LDLIBS) -o $(EXE_ACT)

$(OBJECTSDIR)main_cli.o : main.c
	$(CC) $(COFLAGS) $(FLAGS_CLI) $< -o=T:vbcc.asm
	$(AS) $(ASFLAGS) T:vbcc.asm -o $@
	rm T:vbcc.asm
$(EXE_CLI) : $(OBJECTS_CLI) $(OBJECTSDIR)main_cli.o
	$(LD) $(LDFLAGS) $^ $(LDLIBS) -o $(EXE_CLI)

$(OBJECTSDIR)main_mui.o : main.c
	$(CC) $(COFLAGS) $(FLAGS_MUI) $< -o=T:vbcc.asm
	$(AS) $(ASFLAGS) T:vbcc.asm -o $@
	rm T:vbcc.asm
$(EXE_MUI) : $(OBJECTS_MUI) $(OBJECTSDIR)main_mui.o
	$(LD) $(LDFLAGS) $^ $(LDLIBS) -o $(EXE_MUI)

$(OBJECTSDIR)main_rea.o : main.c
	$(CC) $(COFLAGS) $(FLAGS_REA) $< -o=T:vbcc.asm
	$(AS) $(ASFLAGS) T:vbcc.asm -o $@
	rm T:vbcc.asm
$(EXE_REA) : $(OBJECTS_REA) $(OBJECTSDIR)main_rea.o
	$(LD) $(LDFLAGS) $^ $(LDLIBS) -o $(EXE_REA)

$(OBJECTSDIR)main_aio.o : main.c
	$(CC) $(COFLAGS) $(FLAGS_AIO) $< -o=T:vbcc.asm
	$(AS) $(ASFLAGS) T:vbcc.asm -o $@
	rm T:vbcc.asm
$(EXE_AIO) : $(OBJECTS_AIO) $(OBJECTSDIR)main_aio.o
	$(LD) $(LDFLAGS) $^ $(LDLIBS) -o $(EXE_AIO)



# Data
data : $(DATADIR) $(DATA) $(EXEDIR)AmiAutoUpdater

$(DATADIR) :
	mkdir -p "$@"

$(DATA) : $(DATADIR)% : $(wildcard data/%)
	cp -f -R "$(call replaceQuestionBySpace,data/$(@F))" "$(call replaceQuestionBySpace,$@)"

$(EXEDIR)AmiAutoUpdater : $(DATADIR)AmiAutoUpdater
	cp -f "$(call replaceQuestionBySpace,data/AmiAutoUpdater)"      "$(call replaceQuestionBySpace,$(EXEDIR))"
	cp -f "$(call replaceQuestionBySpace,data/Update and Run)"      "$(call replaceQuestionBySpace,$(EXEDIR))"
	cp -f "$(call replaceQuestionBySpace,data/Update and Run.info)" "$(call replaceQuestionBySpace,$(EXEDIR))"



# Catalogs
catalogs : $(CATSDIR) $(CATSLOCALES)

$(CATSDIR) :
	mkdir -p "$@"

# I cannot use just "mkdir" here because if the subdirectory does
# not exist the following rule "$ (CATS)" will not be invoked.
# So, if the subdirectory does not exist, I also copy its content.
#   mkdir "$(call replaceQuestionBySpace,$@)"
$(CATSLOCALES) :
	cp -f -R "catalogs/$(notdir $(call replaceQuestionBySpace,catalogs/$@))" "$(call replaceQuestionBySpace,$@)"



# Doc
docs : $(DOCDIR) $(DOCLOCALES)

$(DOCDIR) :
	mkdir -p "$@"

# I cannot use just "mkdir" here because if the subdirectory does
# not exist the following rule "$ (DOC)" will not be invoked.
# So, if the subdirectory does not exist, I also copy its content.
#   mkdir "$(call replaceQuestionBySpace,$@)"
$(DOCLOCALES) :
	cp -f -R "doc/$(notdir $(call replaceQuestionBySpace,doc/$@))" "$(call replaceQuestionBySpace,$@)"



# Icons
icons : $(ICONSDIR) $(ICONS) $(EXE).info $(EXEDIR:/=).info $(DOCDIR:/=).info

$(ICONSDIR) :
	mkdir -p "$@"

$(ICONS) : $(ICONSDIR)% : $(wildcard icons/%)
	cp -f -R "$(call replaceQuestionBySpace,icons/$*)" "$(call replaceQuestionBySpace,$@)"

$(EXE).info :
	cp -f "$(ICONSDIR)AmiModRadio.tool2.info" "$@"

$(EXEDIR:/=).info :
	cp -f "$(ICONSDIR)AmiModRadio.drawer2.info" "$@"

$(DOCDIR:/=).info :
	cp -f "$(ICONSDIR)Doc.info" "$@"



# Images
images : $(IMAGESDIR) $(IMAGESSETS)

$(IMAGESDIR) :
	mkdir -p "$@"

# I cannot use just "mkdir" here because if the subdirectory does
# not exist the following rule "$ (IMAGES)" will not be invoked.
# So, if the subdirectory does not exist, I also copy its content.
#   mkdir "$(call replaceQuestionBySpace,$@)"
$(IMAGESSETS) :
	cp -f -R "images/$(notdir $(call replaceQuestionBySpace,images/$@))" "$(call replaceQuestionBySpace,$@)"



# Read-me files
README_FILES     = 0?-?README             \
				   0?-?README.info        \
				   1?-?THANKS             \
				   1?-?THANKS.info        \
				   2?-?GPL?v3             \
				   2?-?GPL?v3.info        \
				   LICENSE?dictionary     \
				   LICENSE?fortify        \
				   LICENSE?ftpparse       \
				   LICENSE?iniparser      \
				   LICENSE?tasksocketbase
README_FILES_DST = $(addprefix $(EXEDIR), $(README_FILES))

$(README_FILES_DST) :
	cp -f "$(call replaceQuestionBySpace,$(notdir $@))" $(EXEDIR)

readmes	: $(README_FILES_DST)



# LHA archive of 'dist'
$(EXEDIR:/=).lha : PARENTDIR = $(dir $(patsubst %/,%,$(EXEDIR)))
$(EXEDIR:/=).lha : EXENAME   = $(notdir $(patsubst %/,%,$(EXEDIR)))
$(EXEDIR:/=).lha :
	rm -f          RAM:$(EXENAME).lha
	/C/LHA -r -e a RAM:$(EXENAME).lha $(PARENTDIR) $(EXENAME).info $(EXENAME)/*



# Debugging...
debug :
	@echo $(dir $(patsubst %/,%,$(EXEDIR)))
	@echo $(notdir $(patsubst %/,%,$(EXEDIR)))
	@echo $(EXEDIR:/=).info
	@echo $(MAKECMDGOALS)
	@echo $(EXEDIR)
	@echo $(OBJECTS_ALL)
	@echo $(DATA)
	@echo $(DATADIR)
	@echo $(addprefix $(DATADIR), $(DATA))
	@echo $(IMAGESDIR)
	@echo $(IMAGESSRC)
	@echo $(IMAGES)
	@echo $(call replace, $(IMAGES))
	@echo $(README_FILES_DST)

