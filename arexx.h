/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



#ifndef AREXX_H
#define AREXX_H 1

#include "utils.h"      // For IN and OUT

#include <exec/ports.h> // For struct MsgPort

typedef struct MsgPort *(*port_getter)(void);

int  arexx_setup(
	 void);

void arexx_cleanup(
	 void);

int  arexx_send_command(
	 IN  port_getter   port_getter_fp,
	 IN  char         *command,
	 IN  char         *message,
	 /*@null@*/
	 OUT char        **result);

#endif
