/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



#ifndef LOG_H
#define LOG_H 1

#include "utils.h"

typedef void (*log_print_callback_fp)(IN char *);

int  log_setup(
	 IN log_print_callback_fp debug_callback,
	 IN log_print_callback_fp information_callback,
	 IN log_print_callback_fp warning_callback,
	 IN log_print_callback_fp error_callback);

void log_cleanup(
	 void);

void log_print_debug(
	 IN char *message, ...);

void log_print_information(
	 IN char *message, ...);

void log_print_warning(
	 IN char *message, ...);

void log_print_error(
	 IN char *message, ...);

#endif

