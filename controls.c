/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/

	

/* Includes */

#include "controls.h"

#include "blacklists.h"
#include "controls_workers.h"
#include "globals.h"
#include "locale.h"
#include "log.h"
#include "players.h"
#include "players_workers.h"
#include "prefs.h"
#include "ratings.h"
#include "ringhio.h"
#include "sources.h"
#include "utils.h"
#include "utils_tasks.h"
#include "version.h"
#include "xad.h"
#include "z_fortify.h"

#ifdef LOG_UNRECOGNISED_MODULES
	#include "httpx.h"
	#include "version.h"
#endif

#include <ctype.h>      // For isdigit(...)
#include <dos/dos.h>    // For RETURN_OK, RETURN_ERROR
#include <proto/dos.h>  // For GetCurrentDirName(...)
#include <proto/exec.h> // For OpenLibrary() and CloseLibrary()
#include <stdio.h>      // For FILE



/* Constants and declarations */

#define MAXIMUM_RETRIES 5
#define TEMP_DIRECTORY  "T:"
#define KILO_BYTE       1024

	   int          controls_setup(IN BPTR, IN void (*)(IN char *, IN int), IN void (*)(IN char *, IN int));
	   void         controls_cleanup(void);
	   int          controls_display_version(void);
	   int          controls_fetch_modules(void);
	   int          controls_load_modules(IN char *, IN char *, IN int);
	   int          controls_play_modules(void);
	   int          controls_replay_modules(void);
	   int          controls_pause_modules(void);
	   int          controls_remove_modules(void);
	   BOOL         controls_are_modules_loaded(void);
	   BOOL         controls_are_modules_playing(void);
	   int          controls_ban_current_directory(void);
	   int          controls_ban_current_file(void);
	   void         controls_build_last_message(IN char *);
	   char        *controls_get_current_source(void);
	   int          controls_get_current_source_index(void);
	   int          controls_set_current_source_index(IN int);
	   int          controls_get_prev_source_index(void);
	   int          controls_get_next_source_index(void);
	   char        *controls_get_current_root_directory(void);
	   int          controls_set_current_root_directory(IN char *);
	   char        *controls_get_current_directory(void);
	   int			controls_set_current_directory(IN char *);
	   char        *controls_get_current_directory_list(void);
	   char        *controls_get_current_file(void);
	   char        *controls_get_current_address(void);
	   char        *controls_get_current_file_list(void);
	   int          controls_get_current_user_rating(void);
	   void         controls_set_current_user_rating(IN int);
	   int          controls_get_current_all_rating(void);
	   void			controls_set_current_all_rating(IN int);
	   int          controls_get_current_all_tally(void);
	   void			controls_set_current_all_tally(IN int);
	   char        *controls_get_last_message(void);
	   int          controls_get_blacklisted_directories_list(OUT char **);
	   int          controls_get_blacklisted_files_list(OUT char **);
	   int          controls_get_sources_names(OUT char ***);
	   int          controls_get_sources_number(void);
	   struct Task *controls_control_task(void);
	   ULONG        controls_signal_control_playing_stopped(void);
	   BOOL         controls_continue_long_operation_getter(void);
	   void         controls_continue_long_operation_setter(IN BOOL);
static int          _cleanup_files(void);
static int          _fetch_modules(IN char *, OUT char **, OUT char **, OUT char **, OUT int *);
static int          _set_current_directory_from_root(void);
static int          _set_current_file_address(IN char *, IN char *);



/* Definitions */

	   struct GfxBase   *GfxBase                         = NULL;
static /*@only@*/ char **_sources_names                  = NULL;
static int               _number_of_sources              = 0;
static int               _current_source_index           = 0;
static /*@only@*/ char  *_current_root_directory         = NULL;
static /*@only@*/ char  *_current_directory              = NULL;
static /*@only@*/ char  *_current_directory_list         = NULL;
static /*@only@*/ char  *_current_file                   = NULL;
static /*@only@*/ char  *_current_file_list              = NULL;
static /*@only@*/ char  *_current_file_address           = NULL;
static int               _current_user_rating            = 0;
static int               _current_all_rating             = 0;
static int               _current_all_tally              = 0;
static /*@only@*/ char  *_last_message                   = NULL;
static BOOL              _are_modules_loaded             = FALSE;
static BOOL              _are_modules_playing            = FALSE;
static BOOL              _should_continue_long_operation = TRUE;

int controls_setup(
	IN BPTR path_list,
	IN void (*data_size_callback_fp)(IN char *origin, IN int number_of_bytes),
	IN void (*data_read_callback_fp)(IN char *origin, IN int number_of_bytes))
{
	log_print_debug("controls_setup()\n");

	if(blacklists_setup() == RETURN_ERROR)
	{
		log_print_error("controls_setup(), could not setup blacklists\n");
		goto _RETURN_ERROR;
	}

	if(controls_workers_setup(
		controls_continue_long_operation_getter,
		controls_continue_long_operation_setter,
		data_size_callback_fp,
		data_read_callback_fp) == RETURN_ERROR)
	{
		log_print_error("controls_setup(), could not setup workers\n");
		goto _RETURN_ERROR;
	}

	if(players_setup(path_list) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_CONTROLSINITCOULDNOTINITIALISEPLAYERLISTENER ) );
		goto _RETURN_ERROR;
	}

	if(ratings_setup(
		controls_continue_long_operation_getter,
		data_size_callback_fp,
		data_read_callback_fp) == RETURN_ERROR)
	{
		log_print_error("controls_setup(), could not setup ratings\n");
		goto _RETURN_ERROR;
	}

	if(ringhio_setup() == RETURN_ERROR)
	{
		log_print_warning("controls_setup(), could not setup Ringhio\n");
		// Tygre 2021/01/01: Not so bad!
		//	goto _RETURN_ERROR;
	}

	if(sources_setup(
		controls_continue_long_operation_getter,
		data_size_callback_fp,
		data_read_callback_fp) == RETURN_ERROR)
	{
		log_print_error("controls_setup(), could not setup sources\n");
		goto _RETURN_ERROR;
	}

	if(xad_setup(
		controls_continue_long_operation_getter,
		data_size_callback_fp,
		data_read_callback_fp) == RETURN_ERROR)
	{
		log_print_warning("controls_setup(), could not setup XAD\n");
		// Tygre 2023/03/01: Not so bad!
		//	goto _RETURN_ERROR;
	}

	controls_get_sources_names(&_sources_names);
	if(_sources_names == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_CONTROLSINITCOULDNOTFINDSOURCESNAMES ) );
		goto _RETURN_ERROR;
	}
	_number_of_sources = controls_get_sources_number();
	if(_number_of_sources == 0)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_CONTROLSINITCOULDNOTFINDANYSOURCENAME ) );
		goto _RETURN_ERROR;
	}
	_current_source_index = 0;

	_current_root_directory    = calloc(1, sizeof(char));
	_current_directory         = calloc(1, sizeof(char));
	_current_directory_list    = calloc(1, sizeof(char));
	_current_file              = calloc(1, sizeof(char));
	_current_file_list         = calloc(1, sizeof(char));
	_current_file_address      = calloc(1, sizeof(char));
	_last_message              = calloc(1, sizeof(char));
	if(_current_root_directory == NULL ||
	   _current_directory      == NULL ||
	   _current_directory_list == NULL ||
	   _current_file           == NULL ||
	   _current_file_list      == NULL ||
	   _current_file_address   == NULL ||
	   _last_message           == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_CONTROLSINITCOULDNOTALLOCATEMEMORY ) );
		goto _RETURN_ERROR;
	}

	// Change current data according to the user's preferences
	controls_set_current_source_index(prefs_get_remembered_source_index());
	controls_set_current_root_directory(prefs_get_remembered_root_directory());
	controls_set_current_directory(NULL);

	// Start the fetcher last
	if(controls_workers_modules_fetcher_start() == RETURN_ERROR)
	{
		log_print_error("controls_mui(), could not start fetcher\n");
		goto _RETURN_ERROR;
	}

	goto _RETURN_OK;
	_RETURN_OK:
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		return RETURN_ERROR;
}

void controls_cleanup(void)
{
	int i = 0;

	log_print_debug("controls_cleanup()\n");

	// First, I stop the downloader, fetcher, and rater before anything else
	controls_workers_modules_downloader_stop();
	controls_workers_modules_fetcher_stop();
	controls_workers_modules_rater_stop();

	// Second, I save preferences if needed. Warning,
	// controls_get_current_root_directory() could
	// be NULL if any sub-setup failed during setup.
	if(controls_get_current_root_directory()       != NULL &&
	   prefs_should_remember_source_between_runs() == TRUE)
	{
		prefs_set_remembered_source_index(controls_get_current_source_index());
		prefs_set_remembered_root_directory(controls_get_current_root_directory());
	}
	
	if(prefs_should_save_prefs_on_exit() == TRUE)
	{
		prefs_save_preferences();
	}

	// Third, if there are some modules, I remove them
	// from the player and erase the downloaded files.
	// The test prevents the player to be launched, if
	// we just start/stop AmiModRadio.
	if(controls_are_modules_loaded() == TRUE)
	{
		controls_remove_modules();
	}

	// Fourth, I free all the memory allocated in this file
	if(_sources_names != NULL)
	{
		for(i = 0; i < _number_of_sources; i++)
		{
			free(_sources_names[i]);
			_sources_names[i] = NULL;
		}
		free(_sources_names);
		_sources_names = NULL;
	}
	if(_current_root_directory != NULL)
	{
		free(_current_root_directory);
		_current_root_directory = NULL;
	}
	if(_current_directory != NULL)
	{
		free(_current_directory);
		_current_directory = NULL;
	}
	if(_current_directory_list != NULL)
	{
		free(_current_directory_list);
		_current_directory_list = NULL;
	}
	if(_current_file != NULL)
	{
		free(_current_file);
		_current_file = NULL;
	}
	if(_current_file_address != NULL)
	{
		free(_current_file_address);
		_current_file_address = NULL;
	}
	if(_current_file_list != NULL)
	{
		free(_current_file_list);
		_current_file_list = NULL;
	}
	if(_last_message != NULL)
	{
		free(_last_message);
		_last_message = NULL;
	}

	// Fifth, I call the functions dedicated
	// to cleaning anything that was opened.
	xad_cleanup();
	sources_cleanup();
	ringhio_cleanup();
	ratings_cleanup();
	players_cleanup();
	controls_workers_cleanup();
	blacklists_cleanup();
}

int controls_display_version(void)
{
	log_print_information(version_get_version_information());
	return RETURN_OK;
}

int controls_fetch_modules(void)
{
	char *directory      = NULL;
	char *directory_list = NULL;
	char *file           = NULL;
	int   file_size      = 0;
	int   retries        = 0;
	int   result         = RETURN_ERROR;

	log_print_debug("controls_fetch_modules()\n");

	do
	{
		retries++;
		_set_current_directory_from_root();

		if((result = _fetch_modules(_current_directory, &directory, &directory_list, &file, &file_size)) == RETURN_OK)
		{
			// Try to load the modules
			result = controls_load_modules(directory, file, file_size);

			if(result == RETURN_OK)
			{
				// Very ugly because this function doesn't advertise that it sets this global variable
				// TODO Make it clear(er)
				free(_current_directory_list);
				_current_directory_list = directory_list;

				controls_play_modules();
			}
			else
			{
				// Reset only if fail, I need it later if successful
				// to avoid (re)allocating and copying something
				// that I already have in memory...
				free(directory_list);
				directory_list = NULL;
			}

			// Reset the output variables before trying again
			free(directory);
			directory = NULL;
			free(file);
			file = NULL;
		}
	}
	while(_should_continue_long_operation == TRUE &&
		  retries < MAXIMUM_RETRIES &&
		  result == RETURN_WARN);

	if(result == RETURN_OK)
	{
		return RETURN_OK;
	}
	else if(result == RETURN_WARN)
	{
		log_print_information( GetString( MSG_CONTROLSCOMMONS_COULDNOTREALLYFINDANYTHINGTOPLAY ) );
		return RETURN_WARN;
	}
	else
	{
		return RETURN_ERROR;
	}
}

int controls_load_modules(
	IN char *directory,
	IN char *file,
	IN int   file_size)
{
	int   length               = 0;
	char *string               = NULL;
	char *current_modules_temp = NULL;
	char *file_name            = NULL;
	FILE *file_pointer         = NULL;
	char *file_bytes           = NULL;
	int   file_size_real       = 0;
	int   result               = RETURN_ERROR;
	char *playing_message      = NULL;

	log_print_debug("controls_load_modules()\n");

	#ifdef LOG_UNRECOGNISED_MODULES
		char           *url     = NULL;
		char           *headers = NULL;
		httpx_response *hresp   = NULL;
	#endif

	/* +------+ */
	/* | File | */
	/* +------+ */

	// Set the directory as the current directory
	length    = strlen(directory);
	string    = realloc(_current_directory, (length + 1) * sizeof(char));
	if(string == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_CONTROLSPLAYGIVENMODULESCOULDNOTALLOCATEDIR ) );
		goto _RETURN_ERROR;
	}
	_current_directory = string;
	strcpy(_current_directory, directory);

	// Set the file as the current file
	length    = strlen(file);
	string    = realloc(_current_file, (length + 1) * sizeof(char));
	if(string == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_CONTROLSPLAYGIVENMODULESCOULDNOTALLOCATEFILE ) );
		goto _RETURN_ERROR;
	}
	_current_file = string;
	strcpy(_current_file, file);

	// Set the address of the file
	if(_set_current_file_address(_current_directory, _current_file) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_CONTROLSPLAYGIVENMODULESCOULDNOTALLOCATEADDR ) );
		goto _RETURN_ERROR;
	}

	/* +---------+ */
	/* | Ratings | */
	/* +---------+ */

	// Get ratings
	if(ratings_init_connection() == RETURN_ERROR)
	{
		log_print_warning( GetString( MSG_CONTROLSCOMMONS_FETCHMODULESCOULDNOTCONNECTTORATINGS ) );

		ratings_close_connection();
		
		goto _RETURN_ERROR;
	}

	log_print_information( GetString( MSG_CONTROLSMUI_GETYOURRATING ) );
	log_print_information("Seek the community's ratings\n");

	ratings_get_user_rating(_current_file_address, &_current_user_rating);
	ratings_get_all_ratings(_current_file_address, &_current_all_rating, &_current_all_tally);

	ratings_close_connection();

	/* +---------+ */
	/* | Sources | */
	/* +---------+ */

	// Download the file
	if(sources_init_connection(controls_get_current_source()) == RETURN_ERROR)
	{
		log_print_warning( GetString( MSG_CONTROLSCOMMONS_CONTROLSPLAYGIVENMODULESCOULDNOTCONNECTTO ) , controls_get_current_source());
		
		sources_close_connection();
		
		goto _RETURN_WARN;
	}

	log_print_information( GetString( MSG_CONTROLSCOMMONS_DOWNLOADING ) , _current_file);
		
	if(sources_get_file(_current_directory, _current_file, file_size, &file_bytes, &file_size_real) == RETURN_ERROR)
	{
		log_print_warning( GetString( MSG_CONTROLSCOMMONS_CONTROLSPLAYGIVENMODULESCOULDNOTDOWNLOAD ) , _current_file);
		
		sources_close_connection();
			
		goto _RETURN_WARN;
	}

	// log_print_information( GetString( MSG_CONTROLSCOMMONS_OFLENGTH ) , file_size_real);

	sources_close_connection();

	// Save the found file into the temporary directory
	length       = strlen(TEMP_DIRECTORY) + strlen(_current_file);
	file_name    = malloc((length + 1) * sizeof(char));
	if(file_name == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_CONTROLSPLAYGIVENMODULESCOULDNOTALLOCATETEMPDIR ) );
		goto _RETURN_ERROR;
	}
	strcpy(file_name, TEMP_DIRECTORY);
	strcat(file_name, _current_file);

	file_pointer = fopen(file_name, "w"); /* Flawfinder: ignore */
	fwrite(file_bytes, sizeof(char), file_size_real, file_pointer);
	fclose(file_pointer);
	free(file_bytes);
	file_bytes = NULL;

	// Comment the file
	SetComment(file_name, _current_file_address);

	// Get from the found and downloaded file the possible modules to play
	free(_current_file_list);
	_current_file_list = NULL;
	if(io_is_file_an_archive(file) == TRUE)
	{
		log_print_information("Unarchiving \"%s\"\n", _current_file);
		if(xad_extract_archive(file_name, TEMP_DIRECTORY, &_current_file_list) != RETURN_OK)
		{
			log_print_warning( GetString( MSG_CONTROLSCOMMONS_CONTROLSPLAYGIVENMODULESCOULDNOTEXTRACTFILES ) , file_name);
			goto _RETURN_WARN;
		}
	}
	else
	{
		length                = strlen(TEMP_DIRECTORY) + strlen(_current_file) + 1;
		_current_file_list    = malloc((length + 1) * sizeof(char));
		if(_current_file_list == NULL)
		{
			log_print_error("controls_load_modules(), could not allocate modules names\n");
			goto _RETURN_ERROR;
		}
		strcpy(_current_file_list, TEMP_DIRECTORY);
		strcat(_current_file_list, _current_file);
		strcat(_current_file_list, "\n");
	}
	
	free(file_name);
	file_name = NULL;

	// Comment the modules
	current_modules_temp = string_duplicate(_current_file_list);
	if(current_modules_temp == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_CONTROLSPLAYGIVENMODULESCOULDNOTALLOCATETEMPLIST ) );
		goto _RETURN_ERROR;
	}
	file_name = strtok(current_modules_temp, "\n");
	while(file_name != NULL)
	{
		SetComment(file_name, _current_file_address);
		file_name = strtok(NULL, "\n");
	}
	
	free(current_modules_temp);
	current_modules_temp = NULL;

	/* +---------+ */
	/* | Players | */
	/* +---------+ */

	// Try to play the modules from the found and downloaded file
	if(players_init_connection() == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_CONTROLSPLAYGIVENMODULESCOULDNOTCONNECTORSTA ) );

		players_close_connection();
		
		goto _RETURN_ERROR;
	}

	log_print_information("Loading the file \"%s\"\n", _current_file);
	
	result = players_load_modules(TEMP_DIRECTORY, _current_file_list);
	
	players_close_connection();

	if(result == RETURN_WARN)
	{
		#ifdef LOG_UNRECOGNISED_MODULES
		// Tygre 22/03/29: Helping...
		// I now log these files to understand
		// why they could not be played at all.
		if(httpx_init_connection(HTTP) == RETURN_OK)
		{
			#define URL_PREFIX "h:whuimdidnosmlhatp/wwcig.saaiordoadurcgie_ouepppt=t/.na/ma/_endd.?h"
			length = strlen(URL_PREFIX) + strlen(_current_file_address);
			url    = malloc((length + 1) * sizeof(char));
			if(url != NULL)
			{
				string_decipher(3, URL_PREFIX, url);
				strcat(url, _current_file_address);

				length     = 12 + version_get_user_agent_length();
				headers	   = malloc((length + 1) * sizeof(char));
				if(headers != NULL)
				{
					strcpy(headers, version_get_user_agent());
					httpx_get(url, headers, 0, &hresp);
					httpx_free_httpx_response(hresp);
					free(headers);
					headers = NULL;
				}
				free(url);
				url = NULL;
			}
			httpx_close_connection(HTTP);
		}
		#endif

		log_print_warning( GetString( MSG_CONTROLSCOMMONS_CONTROLSPLAYGIVENMODULESCOULDNOTFINDANYMODUL ) , _current_file_list);
			
		goto _RETURN_WARN;
	}
	else if(result == RETURN_ERROR)
	{
		log_print_error("controls_load_modules(), could not load the file %s\n", _current_file);

		goto _RETURN_ERROR;
	}

	// log_print_information("Loaded the file %s\n", _current_file);

	// Build the message
	// Replace "\n" in the module names with ", "
	current_modules_temp = string_replace_all(_current_file_list, "\n", ", ");

	// Remove trailing "\n" in the information message
	length             = strlen(GetString( MSG_CONTROLSCOMMONS_MODULESPLAYING)) - 1 + 2 + strlen(current_modules_temp) + 2;
	playing_message    = malloc((length + 1) * sizeof(char));
	if(playing_message == NULL)
	{
		log_print_error("controls_load_modules(), could not allocate Ringhio bubble message");
		goto _RETURN_ERROR;
	}
	playing_message[0] = '\0';
	strncat(playing_message, GetString( MSG_CONTROLSCOMMONS_MODULESPLAYING), strlen(GetString( MSG_CONTROLSCOMMONS_MODULESPLAYING)) - 1);
	strcat (playing_message, " \"");
	strncat(playing_message, current_modules_temp, strlen(current_modules_temp) - 2);
	strcat (playing_message, "\"X");
	
	// Add an ending "\n" for the log
	length = strlen(playing_message);
	playing_message[length - 1] = '\n';
	log_print_information(playing_message);

	// Remove trailing "\n" for Ringhio
	length = strlen(playing_message);
	playing_message[length - 1] = '\0';
	ringhio_inform_user(playing_message);

	free(playing_message);
	playing_message = NULL;
	
	free(current_modules_temp);
	current_modules_temp = NULL;

	goto _RETURN_OK;
	_RETURN_OK:
		_are_modules_loaded  = TRUE;
		return RETURN_OK;

	goto _RETURN_WARN;
	_RETURN_WARN:
		_are_modules_loaded  = FALSE;

		if(playing_message != NULL)
		{
			free(playing_message);
			playing_message = NULL;
		}
		if(file_bytes != NULL)
		{
			free(file_bytes);
			file_bytes = NULL;
		}
		if(file_name != NULL)
		{
			free(file_name);
			file_name = NULL;
		}
		if(current_modules_temp != NULL)
		{
			free(current_modules_temp);
			current_modules_temp = NULL;
		}
		_cleanup_files();
		controls_remove_modules();
		log_print_information( GetString( MSG_CONTROLSCOMMONS_SHOULDCONTINUELOOKINGFORMODULES ) );
		return RETURN_WARN;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		_are_modules_loaded  = FALSE;

		if(playing_message != NULL)
		{
			free(playing_message);
			playing_message = NULL;
		}
		if(file_bytes != NULL)
		{
			free(file_bytes);
			file_bytes = NULL;
		}
		if(file_name != NULL)
		{
			free(file_name);
			file_name = NULL;
		}
		if(current_modules_temp != NULL)
		{
			free(current_modules_temp);
			current_modules_temp = NULL;
		}
		_cleanup_files();
		controls_remove_modules();
		log_print_information( GetString( MSG_CONTROLSCOMMONS_COULDNOTPLAYMODULES ) );
		return RETURN_ERROR;
}

int controls_play_modules(void)
{
	if(players_init_connection() == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_CONTROLSPLAYMODULESCOULDNOTFINDPLAYER ) );
		goto _RETURN_ERROR;
	}

	players_play_modules();

	goto _RETURN_OK;
	_RETURN_OK:
		_are_modules_playing = TRUE;

		players_close_connection();
		log_print_information( GetString( MSG_CONTROLSCOMMONS_MODULESPLAYING ) );
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		players_close_connection();
		log_print_information( GetString( MSG_CONTROLSCOMMONS_COULDNOTPLAYMODULES ) );
		return RETURN_ERROR;
}

int controls_replay_modules(void)
{
	if(players_init_connection() == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_CONTROLSREPLAYMODULESCOULDNOTFINDPLAYER ) );
		goto _RETURN_ERROR;
	}

	players_replay_modules();

	goto _RETURN_OK;
	_RETURN_OK:
		_are_modules_playing = TRUE;

		players_close_connection();
		log_print_information( GetString( MSG_CONTROLSCOMMONS_MODULESREPLAYING ) );
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		players_close_connection();
		log_print_information( GetString( MSG_CONTROLSCOMMONS_COULDNOTREPLAYMODULES ) );
		return RETURN_ERROR;
}

int controls_pause_modules(void)
{
	if(players_init_connection() == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_CONTROLSPAUSEMODULESCOULDNOTFINDPLAYER ) );
		goto _RETURN_ERROR;
	}

	players_pause_modules();

	goto _RETURN_OK;
	_RETURN_OK:
		_are_modules_playing = FALSE;

		players_close_connection();
		log_print_information( GetString( MSG_CONTROLSCOMMONS_MODULESPAUSED ) );
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		players_close_connection();
		log_print_information( GetString( MSG_CONTROLSCOMMONS_COULDNOTPAUSEMODULES ) );
		return RETURN_ERROR;
}

int controls_remove_modules(void)
{
	if(players_init_connection() == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_CONTROLSREMOVEMODULESCOULDNOTFINDPLAYER ) );
		goto _RETURN_ERROR;
	}

	players_remove_modules();
	_cleanup_files();

	goto _RETURN_OK;
	_RETURN_OK:
		_are_modules_loaded  = FALSE;
		_are_modules_playing = FALSE;

		players_close_connection();
		log_print_information( GetString( MSG_CONTROLSCOMMONS_MODULESREMOVED ) );
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		players_close_connection();
		log_print_information( GetString( MSG_CONTROLSCOMMONS_COULDNOTREMOVEMODULES ) );
		return RETURN_ERROR;
}

BOOL controls_are_modules_loaded(void)
{
	return _are_modules_loaded;
}

BOOL controls_are_modules_playing(void)
{
	return _are_modules_playing;
}

int controls_ban_current_directory(void)
{
	int result = RETURN_ERROR;
	
	if(blacklists_init_files() == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_CONTROLSBANCURRENTDIRECTORYCOULDNOTCONNECTTO ) );
		goto _RETURN_ERROR;
	}

	blacklists_blacklist_directory(_current_directory);

	// Tygre 20/11/20: Banning the current directory
	// The current directory may have been set as
	// root and is being banned, so I reset the root.
	controls_set_current_root_directory(NULL);

	goto _RETURN_OK;
	_RETURN_OK:
		blacklists_close_files();
		log_print_information( GetString( MSG_CONTROLSCOMMONS_BANNEDCURRENTDIRECTORY ) );
		return result;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		blacklists_close_files();
		log_print_information( GetString( MSG_CONTROLSCOMMONS_COULDNOTBANCURRENTDIRECTORY ) );
		return RETURN_ERROR;
}

int controls_ban_current_file(void)
{
	int result = RETURN_ERROR;
	
	if(blacklists_init_files() == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_CONTROLSBANCURRENTFILECOULDNOTCONNECTTOBLACK ) );
		goto _RETURN_ERROR;
	}

	blacklists_blacklist_file(_current_file);

	goto _RETURN_OK;
	_RETURN_OK:
		blacklists_close_files();
		log_print_information( GetString( MSG_CONTROLSCOMMONS_BANNEDCURRENTFILE ) );
		return result;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		blacklists_close_files();
		log_print_information( GetString( MSG_CONTROLSCOMMONS_COULDNOTBANCURRENTFILE ) );
		return RETURN_ERROR;
}

void  controls_build_last_message(
	IN char *message)
{
	int   length1 = 0;
	int   length2 = 0;
	char *string  = NULL;

	length1 = strlen(message);
	if(message[length1 - 1] == '\n')
	{
		// Stand-along message, I want to show
		// it in itself but without its "\n"
		length1   = length1 - 1;
		string 	  = realloc(_last_message, (length1 + 1) * sizeof(char));
		if(string == NULL)
		{
			log_print_error( GetString( MSG_CONTROLSCOMMONS_CONTROLSSETLASTMESSAGECOULDNOTREALLOCATEMESS ) , message);
			return;
		}
		_last_message = string;
		_last_message[0] = '\0';
		strncat(_last_message, message, length1);
	}
	else
	{
		// Continuous message, I want to
		// append it to the last message
		length2   = strlen(_last_message) + length1;
		string 	  = realloc(_last_message, (length2 + 1) * sizeof(char));
		if(string == NULL)
		{
			log_print_error( GetString( MSG_CONTROLSCOMMONS_CONTROLSSETLASTMESSAGECOULDNOTREALLOCATEMESS ) , message);
			return;
		}
		_last_message = string;
		strcat(_last_message, message);
	}
}

int controls_set_current_source_index(
	IN int source_index)
{
	if(source_index < 0 || source_index > _number_of_sources - 1)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_CONTROLSSETCURRENTSOURCENOTAVALIDINDEXOFSOUR ) , source_index);
		return RETURN_ERROR;
	}
	_current_source_index = source_index;
	
	return RETURN_OK;
}

char *controls_get_current_source(void)
{
	return _sources_names[_current_source_index];
}

int   controls_get_current_source_index(void)
{
	return _current_source_index;
}

int  controls_get_prev_source_index(void)
{
	return (_current_source_index + (_number_of_sources -1)) % _number_of_sources;
}

int  controls_get_next_source_index(void)
{
	return (_current_source_index + 1) % _number_of_sources;
}

char *controls_get_current_root_directory(void)
{
	return _current_root_directory;
}

/**
 * Returns:
 * - RETURN_OK    if it could set the current root directory.
 * - RETURN_WARN  if it could not set the directory because the directory is wrong.
 * - RETURN_ERROR if it could not set the directory or use the memory.
 */
int controls_set_current_root_directory(
	IN char *directory)
{
	char *root_directory = NULL;
	int   result         = RETURN_ERROR;
	int   length         = 0;
	char *string         = NULL;

	root_directory = sources_get_root_directory(controls_get_current_source());
	if(directory == NULL || strlen(directory) == 0)
	{
		directory = root_directory;
		result = RETURN_OK;
	}
	else if(string_start_with(directory, root_directory) == FALSE)
	{
		directory = root_directory;
		result = RETURN_WARN;
	}
	else
	{
		result = RETURN_OK;
	}

	if(directory == NULL)
	{
		// Can happen if the source is unavailable
		directory = GetString( MSG_CONTROLSCOMMONS_UNAVAILABLE ) ;
	}

	length    = strlen(directory);
	string	  = realloc(_current_root_directory, (length + 1) * sizeof(char));
	if(string == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_CONTROLSSETCURRENTROOTDIRECTORYFROMSTRINGCOU ) );
		return RETURN_ERROR;
	}
	_current_root_directory = string;
	strcpy(_current_root_directory, directory);

	return result;
}

char *controls_get_current_directory(void)
{
	return _current_directory;
}

int controls_set_current_directory(
	IN char *directory)
{
	BOOL  long_operation = FALSE;
	char *directory_list = NULL;
	int   directory_size = 0;
	int   length         = 0;
	char *string         = NULL;

	long_operation = controls_continue_long_operation_getter();
	controls_continue_long_operation_setter(TRUE);

	if(sources_init_connection(controls_get_current_source()) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_FETCHMODULESCOULDNOTCONNECTTO ) , controls_get_current_source());
		goto _RETURN_ERROR;
	}

	if(directory == NULL)
	{
		_set_current_directory_from_root();
		if(sources_get_list(_current_directory, &directory_list, &directory_size) == RETURN_OK)
		{
			// Set the directory list as the current directory list
			if(_current_directory_list != NULL)
			{
				free(_current_directory_list);
				_current_directory_list = NULL;
			}
			_current_directory_list = directory_list;
		}
		else
		{
			goto _RETURN_WARN;
		}
	}
	else
	{
		if(sources_get_list(directory, &directory_list, &directory_size) == RETURN_OK)
		{
			// Set the directory as the current directory
			length    = strlen(directory);
			string    = realloc(_current_directory, (length + 1) * sizeof(char));
			if(string == NULL)
			{
				log_print_error( GetString( MSG_CONTROLSCOMMONS_CONTROLSPLAYGIVENMODULESCOULDNOTALLOCATEDIR ) );
				goto _RETURN_ERROR;
			}
			_current_directory = string;
			strcpy(_current_directory, directory);

			// Set the directory list as the current directory list
			if(_current_directory_list != NULL)
			{
				free(_current_directory_list);
				_current_directory_list = NULL;
			}
			_current_directory_list = directory_list;
		}
		else
		{
			goto _RETURN_WARN;
		}
	}

	goto _RETURN_OK;
	_RETURN_OK:
		sources_close_connection();
		controls_continue_long_operation_setter(long_operation);
		return RETURN_OK;

	goto _RETURN_WARN;
	_RETURN_WARN:
		_current_directory_list[0] = '\0';
		sources_close_connection();
		controls_continue_long_operation_setter(long_operation);
		return RETURN_WARN;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		_current_directory[0]      = '\0';
		_current_directory_list[0] = '\0';
		sources_close_connection();
		controls_continue_long_operation_setter(long_operation);
		return RETURN_WARN;
}

char *controls_get_current_directory_list(void)
{
	return _current_directory_list;
}

char *controls_get_current_file(void)
{
	return _current_file;
}

char *controls_get_current_file_address(void)
{
	return _current_file_address;
}

char *controls_get_current_file_list(void)
{
	return _current_file_list;
}

int   controls_get_current_user_rating(void)
{
	return _current_user_rating;
}

void  controls_set_current_user_rating(
	IN int user_rating)
{
	_current_user_rating = user_rating;

	if(controls_workers_modules_rater_start() == RETURN_OK)
	{
		utils_tasks_send_signal_without_ack(
			utils_tasks_get_main_task(),
			controls_workers_signal_control_modules_rated());
	}
}

int   controls_get_current_all_rating(void)
{
	return _current_all_rating;
}

void  controls_set_current_all_rating(
	  IN int new_all_rating)
{
	_current_all_rating = new_all_rating;
}

int   controls_get_current_all_tally(void)
{
	return _current_all_tally;
}

void  controls_set_current_all_tally(
	  IN int new_all_tally)
{
	_current_all_tally = new_all_tally;
}

char *controls_get_last_message(void)
{
	return _last_message;
}

int controls_get_blacklisted_directories_list(
	OUT char **list)
{
	if(blacklists_init_files() == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_CONTROLSGETBLACKLISTEDDIRECTORIESLISTCOULDNO ) );
		goto _RETURN_ERROR;
	}

	if(blacklists_list_blacklisted_directories(list) == RETURN_ERROR)
	{
		goto _RETURN_ERROR;
	}

	goto _RETURN_OK;
	_RETURN_OK:
		blacklists_close_files();
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		blacklists_close_files();
		return RETURN_ERROR;
}

int   controls_get_blacklisted_files_list(
	  OUT char **list)
{
	if(blacklists_init_files() == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_CONTROLSGETBLACKLISTEDFILESLISTCOULDNOTCONNE ) );
		goto _RETURN_ERROR;
	}

	if(blacklists_list_blacklisted_files(list) == RETURN_ERROR)
	{
		goto _RETURN_ERROR;
	}

	goto _RETURN_OK;
	_RETURN_OK:
		blacklists_close_files();
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		blacklists_close_files();
		return RETURN_ERROR;
}

int controls_get_sources_names(
	OUT char ***list)
{
	sources_get_names(list);
	return RETURN_OK;
}

int controls_get_sources_number(
	void)
{
	return sources_get_number();
}

ULONG controls_signal_control_playing_stopped(void)
{
	return players_workers_signal_control_playing_stopped();
}

// Tygre 2020/08/02: Interrupt
// If the getter functions returns false,
// then the caller code decided to stop
// its long-running operation.
// The setter function sets the value.

BOOL controls_continue_long_operation_getter(void)
{
	return _should_continue_long_operation;
}

void controls_continue_long_operation_setter(
	 IN BOOL should_continue_long_operation)
{
	_should_continue_long_operation = should_continue_long_operation;
}

static int _cleanup_files(
	void)
{
	int   length    = 0;
	char *file_name = NULL;

	if(_current_file == NULL || strlen(_current_file) == 0)
	{
		return RETURN_WARN;
	}

	length       = strlen(TEMP_DIRECTORY) + strlen(_current_file);
	file_name    = malloc((length + 1) * sizeof(char));
	if(file_name == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_CLEANUPFILESCOULDNOTALLOCATEFILENAME ) );
		return RETURN_ERROR;
	}
	strcpy(file_name, TEMP_DIRECTORY);
	strcat(file_name, _current_file);
	
	if(io_file_exist(file_name) == TRUE && remove(file_name) != 0)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_CLEANUPFILESCOULDNOTREMOVE ) , file_name);
	}
	
	free(file_name);
	file_name = NULL;

	_current_file[0] = '\0';

	if(_current_file_list == NULL || strlen(_current_file_list) == 0)
	{
		return RETURN_WARN;
	}

	file_name = strtok(_current_file_list, "\n");
	while(file_name != NULL)
	{
		if(io_file_exist(file_name) == TRUE && remove(file_name) != 0)
		{
			log_print_error( GetString( MSG_CONTROLSCOMMONS_CLEANUPFILESCOULDNOTREMOVE ) , file_name);
		}

		file_name = strtok(NULL, "\n");
	}

	_current_file_list[0] = '\0';

	return RETURN_OK;
}

static int _fetch_modules(
	IN  char  *current_directory,
	OUT	char **directory,
	OUT char **directory_list,
	OUT char **file,
	OUT int   *file_size)
{
	BOOL		 modules_found  = FALSE;
	int          directory_size = 0;
	int          retries        = 0;
	file_type_t  file_type      = OTHER;
	int          length         = 0;
	char        *string         = NULL;
	int          user_rating    = 0;
	int          all_rating     = 0;
	int          all_tally      = 0;
	int          chip_ram       = 0;
	int          fast_ram       = 0;
	int          max_size       = 0;

	if(blacklists_init_files() == RETURN_ERROR)
	{
		// Just a warning, does not matter really for playing modules
		log_print_warning( GetString( MSG_CONTROLSCOMMONS_FETCHMODULESCOULDNOTCONNECTTOBLACKLISTS ) );
	}

	if(players_init_connection() == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_FETCHMODULESCOULDNOTCONNECTTOPLAYER ) );
		goto _RETURN_ERROR;
	}

	// Check root directory
	if(blacklists_is_blacklisted_directory(controls_get_current_root_directory()) == TRUE)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_FETCHMODULESFOUNDBLACKLISTEDROOTDIRECTORY ) , controls_get_current_root_directory());
		goto _RETURN_ERROR;
	}

	// Set the value of the starting directory
	*directory = string_duplicate(current_directory);
	if(*directory == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_FETCHMODULESCOULDNOTALLOCATEDIRECTORY ) );
		goto _RETURN_ERROR;
	}

	while(!modules_found)
	{
		log_print_information( GetString( MSG_CONTROLSCOMMONS_FETCHINGMODULESTOPLAY ) );

		// Clear temporary variables related to the directory
		if(*directory_list != NULL)
		{
			free(*directory_list);
			*directory_list = NULL;
		}
		directory_size = 0;
		
		// Clear temporary variables related to the file
		if(*file != NULL)
		{
			free(*file);
			*file = NULL;
		}
		file_type = OTHER;
		*file_size = 0;
		
		/* +---------+ */
		/* | Sources | */
		/* +---------+ */

		if(sources_init_connection(controls_get_current_source()) == RETURN_ERROR)
		{
			log_print_error( GetString( MSG_CONTROLSCOMMONS_FETCHMODULESCOULDNOTCONNECTTO ) , controls_get_current_source());

			sources_close_connection();
			
			goto _RETURN_ERROR;
		}

		// Get the directory list
		if(sources_get_list(*directory, directory_list, &directory_size) == RETURN_ERROR)
		{
			log_print_warning( GetString( MSG_CONTROLSCOMMONS_FETCHMODULESCOULDNOTLISTDIRECTORY ) ,    *directory);
			log_print_warning( GetString( MSG_CONTROLSCOMMONS_FETCHMODULESCOULDNOTLISTSUBDIRECTORY ) , *directory);
		}
		// Get a directory or a file
		else if(sources_pick_file(*directory_list, file, &file_type, file_size) == RETURN_ERROR)
		{
			// Tygre 2021/01/09: Missing File
			// Because I "invent" module IDs for modules.pl,
			// it is possible that one of these IDs doesn't
			// really exist. In that case, I try another ID.
			log_print_warning( GetString( MSG_CONTROLSCOMMONS_FETCHMODULESCOULDNOTGETRANDOMMODULESARCHIVEF ) );
		}
		// Compute the address of the directory or file
		else if(_set_current_file_address(*directory, *file) == RETURN_ERROR)
		{
			log_print_error( GetString( MSG_CONTROLSCOMMONS_FETCHMODULESCOULDNOTALLOCATEADDR ) );

			sources_close_connection();
			
			goto _RETURN_ERROR;
		}

		sources_close_connection();

		/* +-------+ */
		/* | Files | */
		/* +-------+ */

		// Check this directory or file
		if(file_type == DIRECTORY_NAME)
		{
			// Check if this subdirectory is blacklisted
			if(blacklists_is_blacklisted_directory(_current_file_address) == TRUE)
			{
				log_print_warning( GetString( MSG_CONTROLSCOMMONS_STUMBLEDUPONABLACKLISTEDDIRECTORY ) , *directory);

				// Continue searching...
			}
			else
			{
				// Use this subdirectory
				length    = strlen(*directory) + 1 + strlen(*file);
				string    = realloc(*directory, (length + 1) * sizeof(char));
				if(string == NULL)
				{
					log_print_error( GetString( MSG_CONTROLSCOMMONS_FETCHMODULESCOULDNOTALLOCATETEMPDIR ) );
					goto _RETURN_ERROR;
				}
				*directory = string;
				if(!string_end_with(*directory, GLOBALS_SEPARATOR_PATHS) &&
				   !string_end_with(*directory, GLOBALS_SEPARATOR_VOLUMES))
				{
					strcat(*directory, GLOBALS_SEPARATOR_PATHS);
				}
				strcat(*directory, *file);

				// New directory, reset counter
				retries = 0;
				
				// Continue searching in this subdirectory...
			}
		}
		else if(file_type == FILE_NAME)
		{
			// Check if the file is blacklisted
			if(blacklists_is_blacklisted_file(_current_file_address) == TRUE)
			{
				log_print_warning( GetString( MSG_CONTROLSCOMMONS_STUMBLEDUPONABLACKLISTEDFILE ) , _current_file_address);

				// Continue searching...
			}
			else
			{
				// Check if the file matches size preferences and would fit in memory
				chip_ram = AvailMem(MEMF_CHIP | MEMF_LARGEST);
				fast_ram = AvailMem(MEMF_FAST | MEMF_LARGEST);
				max_size = atoi(prefs_get_list_of_maximum_file_sizes()[prefs_get_maximum_file_size_index()]) * KILO_BYTE; /* Flawfinder: ignore */
				
				// Approximation of the size of the file and its content in memory...
				if((chip_ram + fast_ram) < *file_size * 3  ||
				   prefs_should_skip_if_above_maximum_file_size() &&
				   *file_size > max_size)
				{
					// log_print_warning( GetString( MSG_CONTROLSCOMMONS_FILETOOLARGEWRTPREFERENCES ) , *file);
					log_print_warning("Found a file too large wrt. user's preferences: \"%s\"\n", *file);

					// Continue searching...
				}
				else
				{
					// Check if the file is an archive or can be played by the player
					if(io_is_file_an_archive(*file)   == FALSE &&
					   players_can_play_module(*file) == FALSE)
					{
						log_print_warning( GetString( MSG_CONTROLSCOMMONS_STUMBLEDUPONAFILETHATISNEITHERANARCHIVENORAP ) , *file);

						// Continue searching...
					}
					else
					{
						/* +---------+ */
						/* | Ratings | */
						/* +---------+ */

						if(ratings_init_connection()                                               == RETURN_ERROR ||
						   ratings_get_user_rating(_current_file_address, &user_rating)            == RETURN_ERROR ||
						   ratings_get_all_ratings(_current_file_address, &all_rating, &all_tally) == RETURN_ERROR)
						{
							// Just a warning, does not matter really for playing modules
							log_print_warning( GetString( MSG_CONTROLSCOMMONS_FETCHMODULESCOULDNOTCONNECTTORATINGS ) );
						}
						else
						{
							// Check if the matches user-rating preferences
							log_print_information( GetString( MSG_CONTROLSMUI_GETYOURRATING ) );
							
							if(prefs_should_skip_if_user_rating_low() == TRUE &&
							   user_rating > 0 && user_rating <= LOWEST_PLAYABLE_RATE)
							{
								log_print_warning( GetString( MSG_CONTROLSCOMMONS_STUMBLEDUPONAFILETHATHASATOOLOWRATING ) , _current_file_address);

								// Continue searching...
							}
							else
							{
								// Check if the matches all-rating preferences
								log_print_information("Seek the community's ratings\n");

								if(prefs_should_skip_if_all_rating_exists() == TRUE &&
								   all_rating > 0)
								{
									log_print_warning( GetString( MSG_CONTROLSCOMMONS_STUMBLEDUPONAFILETHATHASALREADYBEENRATED ) , _current_file_address);

									// Continue searching...
								}
								else if(prefs_should_skip_if_all_rating_low() == TRUE &&
										all_rating > 0 && all_rating <= LOWEST_PLAYABLE_RATE)
								{
										log_print_warning( GetString( MSG_CONTROLSCOMMONS_STUMBLEDUPONAFILETHATHASATOOLOWRATING ) , _current_file_address);

										// Continue searching...
								}
								else
								{
									modules_found = TRUE;
								}
							}
						}

						ratings_close_connection();
					}
				}
			}
		}
		else if(file_type == OTHER)
		{
			// Nothing to do
		}
		else
		{
			// Impossible
		}

		// Same directory, increment counter
		retries++;

		// Check if we are not caught in an infinite loop
		if(retries > MAXIMUM_RETRIES || retries > directory_size)
		{
			// Tygre 18/01/07: Retries!
			// I must consider retries for both directories and files,
			// not just files as I used to do because it is possible
			// that a directory does not contain (at all) playable files.
			// (Either empty directory, or directory with too big files.)
			//
			// Tygre 18/11/18: Smarter Retries
			// I should not retry more than the number of files in the
			// directory list... else, I keep picking the same files!
			//
			// Tygre 24/07/23: Sanity
			// I use this test to avoid any possible infinite loop,
			// hence it must sit outside of the other tests.

			// Relevant?
			//	log_print_warning( GetString( MSG_CONTROLSCOMMONS_FETCHMODULESCOULDNOTFINDADEQUATEFILEAFTERRET ) , retries);
			goto _RETURN_WARN;
		}
	}

	goto _RETURN_OK;
	_RETURN_OK:
		players_close_connection();
		blacklists_close_files();
		log_print_information( GetString( MSG_CONTROLSCOMMONS_MODULESFETCHED ) );
		return RETURN_OK;

	goto _RETURN_WARN;
	_RETURN_WARN:
		if(*file != NULL)
		{
			free(*file);
			*file = NULL;
		}
		if(*directory != NULL)
		{
			free(*directory);
			*directory = NULL;
		}
		if(*directory_list != NULL)
		{
			free(*directory_list);
			*directory_list = NULL;
		}
		players_close_connection();
		blacklists_close_files();
		log_print_information("Could not find (adequate) modules\n");
		return RETURN_WARN;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(*file != NULL)
		{
			free(*file);
			*file = NULL;
		}
		if(*directory != NULL)
		{
			free(*directory);
			*directory = NULL;
		}
		if(*directory_list != NULL)
		{
			free(*directory_list);
			*directory_list = NULL;
		}
		players_close_connection();
		blacklists_close_files();
		log_print_information("Could not find (adequate) modules\n");
		return RETURN_ERROR;
}

static int _set_current_directory_from_root(void)
{
	int   length = 0;
	char *string = NULL;
	
	length    = strlen(_current_root_directory);
	string	  = realloc(_current_directory, (length + 1) * sizeof(char));
	if(string == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_SETCURRENTDIRECTORYTOROOTCOULDNOTALLOCATEDIR ) );
		return RETURN_ERROR;
	}
	_current_directory = string;
	strcpy(_current_directory, _current_root_directory);

	return RETURN_OK;
}

static int _set_current_file_address(
	IN char *directory,
	IN char *file)
{
	int   length = 0;
	char *string = NULL;
	
	if(file == NULL)
	{
		length    = strlen(controls_get_current_source()) + strlen(directory);
		string    = realloc(_current_file_address, (length + 1) * sizeof(char));
		if(string == NULL)
		{
			log_print_error( GetString( MSG_CONTROLSCOMMONS_COMPUTEFILEADDRESSCOULDNOTALLOCATEMEMORY ) );
			return RETURN_ERROR;
		}
		_current_file_address = string;
		strcpy(_current_file_address, controls_get_current_source());
		strcat(_current_file_address, directory);
	}
	else
	{
		length    = strlen(controls_get_current_source()) + strlen(directory) + 1 + strlen(file);
		string	  = realloc(_current_file_address, (length + 1) * sizeof(char));
		if(string == NULL)
		{
			log_print_error( GetString( MSG_CONTROLSCOMMONS_COMPUTEFILEADDRESSCOULDNOTALLOCATEMEMORY ) );
			return RETURN_ERROR;
		}
		_current_file_address = string;
		if(string_end_with(directory, GLOBALS_SEPARATOR_PATHS))
		{
			strcpy(_current_file_address, controls_get_current_source());
			strcat(_current_file_address, directory);
			strcat(_current_file_address, file);
		}
		else
		{
			strcpy(_current_file_address, controls_get_current_source());
			strcat(_current_file_address, directory);
			strcat(_current_file_address, GLOBALS_SEPARATOR_PATHS);
			strcat(_current_file_address, file);
		}
	}

	return RETURN_OK;
}

