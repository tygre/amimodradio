/*
	Copyright (c) 2012-2013  Swen Kooij
	Parts copyright (c) 2015-2024 Tygre <tygre@chingu.asia>

	This file used to be part of http-client-c under the names:
		- http-client-c.h
		- stringx.h
		- urlparser.h
	This file combines the three files above and is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope fthat it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.

	Warning:
	This library does not tend to work that stable nor does it fully implent the
	standards described by IETF. For more information on the precise implentation
	of the Hyper Text Transfer Protocol: http://www.ietf.org/rfc/rfc2616.txt
*/



/* Includes */

#include "httpx_http.h"

#include "globals.h"
#include "httpx.h"
#include "locale.h"
#include "log.h"
#include "prefs.h"
#include "utils.h"
#include "utils_socketbase.h" // Sockets and such from Roadshow SDK, with task-specific SocketBase and data
#include "z_fortify.h"



/* Constants and declarations */

int             httpx_http_setup(IN BOOL (*)(), IN void (*)(IN char *, IN int), IN void (*)(IN char *, IN int));
void            httpx_http_cleanup(void);
int             httpx_http_init_connection(void);
void            httpx_http_close_connection(void);
httpx_response *httpx_http_request(IN char *, IN parsed_url *, IN int);



/* Definitions */

static BOOL (*_continue_long_operation_fp)() = NULL; // Tygre 20/08/02: To stop long-running HTTP operations
static void (*_httpx_size_callback_fp)(
				IN char *origin,
				IN int number_of_bytes)      = NULL; // Tygre 22/12/19: To update a progress bar when necessary
static void (*_httpx_read_callback_fp)(
				IN char *origin,
				IN int number_of_bytes)      = NULL; // Tygre 22/12/19: To update a progress bar when necessary



int httpx_http_setup(
	IN BOOL (*continue_long_operation_fp)(),
	IN void (*httpx_size_callback_fp)(IN char *origin, IN int number_of_bytes),
	IN void (*httpx_read_callback_fp)(IN char *origin, IN int number_of_bytes))
{
	log_print_debug("httpx_http_setup()\n");

	_continue_long_operation_fp = continue_long_operation_fp;
	_httpx_size_callback_fp     = httpx_size_callback_fp;
	_httpx_read_callback_fp     = httpx_read_callback_fp;

	return RETURN_OK;
}

void httpx_http_cleanup(
	 void)
{
	log_print_debug("httpx_http_cleanup()\n");

	// Nothing to do
}

int httpx_http_init_connection(
	void)
{
	// printf("%s: \thttpx_http_init_connection()\n", FindTask(NULL)->tc_Node.ln_Name);
	log_print_debug("httpx_http_init_connection()\n");

	if(_httpx_size_callback_fp)
	{
		_httpx_size_callback_fp("httpx_http_init_connection()", GLOBALS_MAX_LINE_LENGTH);
	}
	if(_httpx_read_callback_fp)
	{
		_httpx_read_callback_fp("httpx_http_init_connection()", 0);
	}

	return RETURN_OK;
}

void httpx_http_close_connection(
	 void)
{
	// printf("%s: \thttpx_http_close_connection()\n", FindTask(NULL)->tc_Node.ln_Name);
	log_print_debug("httpx_http_close_connection()\n");

	if(_httpx_size_callback_fp)
	{
		_httpx_size_callback_fp("httpx_http_init_connection()", GLOBALS_MAX_LINE_LENGTH);
	}
	if(_httpx_read_callback_fp)
	{
		_httpx_read_callback_fp("httpx_http_init_connection()", GLOBALS_MAX_LINE_LENGTH);
	}
}

httpx_response *httpx_http_request(
	IN char       *http_headers,
	IN parsed_url *purl,
	IN  int        bytes_limit)
{
	char                *hreq                  = NULL;
	int                  hreq_size             = 0;
	httpx_response      *hresp                 = NULL;
	int                  sock                  = -1;
	struct timeval       tv                    = { 0 };
	struct hostent      *he                    = NULL;
	struct sockaddr_in   remote                = { 0 };
	int                  sent                  = 0;
	int                  sent_temp             = 0;
	char                *response              = NULL;
	int                  response_size         = 0;
	char                *response_temp         = NULL;
	int                  number_of_bytes       = 0;
	char                *received_data         = NULL;
	int                  size_of_received_data = 0;
	char                *temp_string           = NULL;
	char                *status_line           = NULL;

	// Parsed URL
	if(purl == NULL)
	{
		log_print_error( GetString( MSG_HTTP_HTTPREQUESTCOULDNOTPARSEURL ) );
		goto _RETURN_ERROR;
	}

	// Create socket
	if((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
	{
		log_print_error( GetString( MSG_HTTP_HTTPREQUESTCOULDNOTCREATESOCKET ) );
		goto _RETURN_ERROR;
	}

	// Get host name
	he = gethostbyname((char *)purl->host);
	if(he == NULL)
	{
		log_print_error("httpx_http_request(), could not get IP address of %s\n" , purl->host);
		goto _RETURN_ERROR;
	}

	// Zero out the remote address
	memset(&remote, 0, sizeof(remote));

	// Set up remote address for connect
	// Tygre 22/02/05: Guru on 68000
	// The code used to be:
	//	temp_int = inet_pton(AF_INET, purl->ip, (void *)(&(remote->sin_addr.s_addr)));
	// which would Guru 8000 0003 on a 68000
	// because of misalignment of the address.
	// I replaced it by the simpler code:
	memcpy(&remote.sin_addr, he->h_addr, he->h_length);
	remote.sin_family = AF_INET;
	remote.sin_port   = htons(atoi(purl->port)); /* Flawfinder: ignore */

	// Connect to server
	if(connect(sock, (struct sockaddr *)&remote, sizeof(struct sockaddr)) < 0)
	{
		log_print_error( GetString( MSG_HTTP_HTTPREQUESTCOULDNOTCONNECTTOSERVER ) );
		goto _RETURN_ERROR;
	}

	// Set a timeout
	tv.tv_secs  = GLOBALS_MAX_WAIT_IN_SECONDS;
	tv.tv_micro = 0;

	if(setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv))) {
		log_print_warning("httpx_http_request(), could not set timeout (send)\n");
		// Not so important?
		//	goto _RETURN_ERROR;
	}
	if(setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv))) {
		log_print_warning("httpx_http_request(), could not set timeout (recv)\n");
		// Not so important?
		//	goto _RETURN_ERROR;
	}

	// Send headers to server
	// Tygre 2016/05/15: Format
	// Somehow, the header sent to the Web server was broken
	// because of the missing mandatory trailing "\r\n". The
	// server was timeouting instead of sending a response.
	hreq_size = strlen(http_headers) + 2;
	hreq      = malloc((hreq_size + 1) * sizeof(char));
	if(hreq   == NULL)
	{
		log_print_error("httpx_http_request(), could not allocate HTTP request\n");
		goto _RETURN_ERROR;
	}
	hreq[0] = '\0';
	strcat(hreq, http_headers);
	strcat(hreq, "\r\n");

	while(sent < hreq_size)
	{
		if(_continue_long_operation_fp != NULL && !_continue_long_operation_fp())
		{
			// Tygre 2020/08/02: Interrupt
			// If the callback function returns false,
			// then the caller code decided to stop
			// this long-running operation.
			log_print_warning("httpx_http_request(), sending data interrupted by user\n");
			goto _RETURN_ERROR;
		}

		sent_temp = send(sock, hreq + sent, hreq_size - sent, 0);
		if(sent_temp == -1)
		{
			log_print_error( GetString( MSG_HTTP_HTTPREQUESTCOULDNOTSENDHEADERS ) );
			goto _RETURN_ERROR;
		}
		sent += sent_temp;

		if(_httpx_read_callback_fp)
		{
			_httpx_read_callback_fp("httpx_http_request() (sending)", sent_temp);
		}
	 }

	// Receive response
	received_data    = malloc((GLOBALS_MAX_BUFFER_LENGTH + 1) * sizeof(char));
	if(received_data == NULL)
	{
		log_print_error( GetString( MSG_HTTP_HTTPREQUESTCOULDNOTALLOCATEBUFFER ) );
		goto _RETURN_ERROR;
	}

	response    = malloc((GLOBALS_MAX_BUFFER_LENGTH + 1) * sizeof(char));
	if(response == NULL)
	{
		log_print_error( GetString( MSG_HTTP_HTTPREQUESTCOULDNOTALLOCATEBUFFER ) );
		goto _RETURN_ERROR;
	}

	response_size = GLOBALS_MAX_BUFFER_LENGTH;

	while((size_of_received_data = recv(sock, received_data, GLOBALS_MAX_BUFFER_LENGTH, 0)) > 0)
	{
		if(_continue_long_operation_fp != NULL && !_continue_long_operation_fp())
		{
			// Tygre 2020/08/02: Interrupt
			// If the callback function returns false,
			// then the caller code decided to stop
			// this long-running operation.
			log_print_warning("httpx_http_request(), receiving data interrupted by user\n");
			goto _RETURN_ERROR;
		}

		if(_httpx_read_callback_fp)
		{
			_httpx_read_callback_fp("httpx_http_request() (receiving)", size_of_received_data);
		}

		if(response_size < number_of_bytes + size_of_received_data)
		{
			response_size = response_size + 3 * GLOBALS_MAX_BUFFER_LENGTH;
			response_temp = realloc(response, response_size);
			if(response_temp == NULL)
			{
				log_print_error( GetString( MSG_HTTP_HTTPREQUESTCOULDNOTALLOCATEBUFFER ) );
				goto _RETURN_ERROR;
			}
			response      = response_temp;
		}
		memcpy(response + number_of_bytes, received_data, size_of_received_data);
		number_of_bytes += size_of_received_data;

		if(bytes_limit > 0 && number_of_bytes > bytes_limit)
		{
			log_print_error("Stopped downloading a file too large wrt. user's preferences\n");
			goto _RETURN_ERROR;
		}
	}
	
	free(received_data);
	received_data = NULL;
	
	if(size_of_received_data < 0)
	{
		log_print_error( GetString( MSG_HTTP_HTTPREQUESTCOULDNOTRECEIVEDATAFROMTHESERVER ) );
		log_print_error("httpx_http_request(), recv() failed with error %d\n", Errno());
		goto _RETURN_ERROR;
	}

	// Allocate memeory for response
	hresp = malloc(sizeof(httpx_response));
	if(hresp == NULL)
	{
		log_print_error( GetString( MSG_HTTP_HTTPREQUESTCOULDNOTALLOCATEHTTPRESPONSE ) );
		goto _RETURN_ERROR;
	}
	         hresp->request_uri        = purl;
	         hresp->body               = NULL;
	*(int *)&hresp->body_size_in_bytes = 0;
	         hresp->status_code        = NULL;
	*(int *)&hresp->status_code_value  = 0;
	         hresp->status_text        = NULL;
	         hresp->request_headers    = http_headers;
	         hresp->response_headers   = NULL;

	// Parse body
	temp_string    = strstr(response, "\r\n\r\n");
	if(temp_string == NULL)
	{
		log_print_error("httpx_http_request(), could not find response delimiter\n");
		goto _RETURN_ERROR;
	}
	number_of_bytes    = number_of_bytes - (temp_string + 4 - response) * sizeof(char);
	if(number_of_bytes > 0)
	{
		// Tygre 23/01/29: With different optimisation
		// malloc(0) either returns non-NULL or NULL.
		// I protect myself by checking the number of
		// bytes to allocate first. I must continue
		// even if the body is NULL because it could
		// be legitimate with, e.g., a 302 status code.
		hresp->body    = malloc(number_of_bytes);
		if(hresp->body == NULL)
		{
			temp_string = NULL;
			log_print_error("httpx_http_request(), could not allocate response body\n");
			goto _RETURN_ERROR;
		}
		memcpy((void *)hresp->body, temp_string + 4, number_of_bytes);
		*(int *)&hresp->body_size_in_bytes = number_of_bytes;
	}

	// Parse status code and text
	temp_string = get_until(response, "\r\n");
	status_line = strrpl("HTTP/1.1 ", "", temp_string);
	free(temp_string);
	temp_string = NULL;
	
	temp_string        = string_duplicate_n(status_line, 4);
	hresp->status_code = strrpl(" ", "", temp_string);
	free(temp_string);
	temp_string = NULL;
	
	temp_string        = strrpl(hresp->status_code, "", status_line);
	hresp->status_text = strrpl(" ", "", temp_string);
	free(temp_string);
	temp_string = NULL;
	
	*(int *)&hresp->status_code_value = atoi(hresp->status_code); /* Flawfinder: ignore */

	// Parse response headers
	hresp->response_headers	= get_until(response, "\r\n\r\n");

	goto _RETURN_OK;
	_RETURN_OK:
		free(status_line);
		free(hreq);
		CloseSocket(sock);
		free(response);
		return hresp;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(status_line != NULL)
		{
			free(status_line);
		}
		if(temp_string != NULL)
		{
			free(temp_string);
		}
		if(received_data != NULL)
		{
			free(received_data);
		}
		if(hreq != NULL)
		{
			free(hreq);
		}
		if(sock > -1)
		{
			CloseSocket(sock);
		}
		if(hresp != NULL)
		{
			// Don't touch the request URL,
			// headers, they come from the
			// caller, which handles them.
			hresp->request_uri     = NULL;
			hresp->request_headers = NULL;
			httpx_free_httpx_response(hresp);
		}
		if(response != NULL)
		{
			free(response);
		}
		return NULL;
}

