/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include "prefs.h"

#include "globals.h"
#include "kvs.h"
#include "locale.h"
#include "log.h"
#include "utils.h"         // For IN, OUT...
#include "z_fortify.h"

#include <exec/execbase.h>
#include <exec/types.h>    // For BOOL...
#include <stdio.h>
#include <stdlib.h>
#include <proto/dos.h>
#include <proto/exec.h>



/* Constants and declarations */

#define PREFS_FILE                             "ENVARC:AmiModRadio"
#define IMAGES_DIRECTORY_ROOT                  "images/"

#define SAVED_MODULE_DIRECTORY                 "saved_modules_directory"
#define SHOULD_PLAY_ON_STARTUP                 "should_play_on_startup"
#define SHOULD_SHOW_LOG_PAGE_ON_FETCH          "should_show_log_page_on_fetch"
#define MAXIMUM_LOG_SIZE_INDEX                 "maximum_log_size_index"
#define NUMBER_OF_MAXIMUM_LOG_SIZES            5
#define MAXIMUM_LOG_SIZES                      {"1", "2", "5", "10", "20"}
#define SHOULD_REMEMBER_SOURCE_BETWEEN_RUNS    "should_remember_source_between_runs"
#define REMEMBERED_SOURCE_INDEX                "remembered_source_index"
#define REMEMBERED_ROOT_DIRECTORY              "remembered_root_directory"
#define SHOULD_CACHE_LIST_DURING_RUN           "should_cache_list_during_run"
#define SHOULD_CACHE_LIST_BETWEEN_RUNS         "should_cache_list_between_runs"
#define CACHE_DIRECTORY                        "cache_directory"
#define SHOULD_SAVE_PREFS_ON_EXIT              "should_save_prefs_on_exit"
#define IMAGES_DIRECTORY                       "images_directory"
#define SHOULD_SKIP_IF_BANNED                  "should_skip_if_banned"
#define SHOULD_SKIP_IF_USER_RATING_LOW         "should_skip_if_user_rating_low"
#define SHOULD_SKIP_IF_ALL_RATING_EXISTS       "should_skip_if_all_rating_exists"
#define SHOULD_SKIP_IF_ALL_RATING_LOW          "should_skip_if_all_rating_low"
#define SHOULD_SKIP_IF_ABOVE_MAXIMUM_FILE_SIZE "should_skip_if_above_maximum_file_size"
#define MAXIMUM_FILE_SIZE_INDEX                "maximum_file_size_index"
#define NUMBER_OF_MAXIMUM_FILE_SIZES           10
#define MAXIMUM_FILE_SIZES                     {"5", "10", "20", "40", "80", "160", "320", "640", "1280", "Unlimited"}
#define MAIN_WINDOW_X                          "main_window_X"
#define MAIN_WINDOW_Y                          "main_window_Y"

	   int    prefs_setup(void);
	   void   prefs_cleanup(void);
	   char  *prefs_get_saved_modules_directory(void);
	   void   prefs_set_saved_modules_directory(IN char *);
	   BOOL   prefs_should_play_on_startup(void);
	   void   prefs_set_play_on_startup(IN BOOL);
	   BOOL   prefs_should_show_log_page_on_fetch(void);
	   void   prefs_set_show_log_page_on_fetch(IN BOOL);
	   int    prefs_get_maximum_log_size_index(void);
	   void   prefs_set_maximum_log_size_index(IN int);
	   char **prefs_get_list_of_maximum_log_sizes(void);
	   int    prefs_get_number_of_maximum_log_sizes(void);
	   BOOL   prefs_should_remember_source_between_runs(void);
	   void   prefs_set_remember_source_between_runs(IN BOOL);
	   int    prefs_get_remembered_source_index(void);
	   void   prefs_set_remembered_source_index(IN int);
	   char  *prefs_get_remembered_root_directory(void);
	   void   prefs_set_remembered_root_directory(IN char *);
	   BOOL   prefs_should_cache_list_during_run(void);
	   void   prefs_set_cache_list_during_run(IN BOOL);
	   BOOL   prefs_should_cache_list_between_runs(void);
	   void   prefs_set_cache_list_between_runs(IN BOOL);
	   char  *prefs_get_cache_directory(void);
	   void   prefs_set_cache_directory(IN char *);
	   BOOL   prefs_should_save_prefs_on_exit(void);
	   void   prefs_set_save_prefs_on_exit(IN BOOL);
	   char  *prefs_get_images_directory(void);
	   void   prefs_set_images_directory(IN char *);
	   char **prefs_get_list_of_images_sets(void);
	   int    prefs_get_number_of_images_sets(void);
	   int    prefs_get_current_images_set_index(void);
	   void	  prefs_set_current_images_set_index(IN int);
	   BOOL   prefs_should_skip_if_banned(void);
	   void   prefs_set_skip_if_banned(IN BOOL);
	   BOOL   prefs_should_skip_if_user_rating_low(void);
	   void   prefs_set_skip_if_user_rating_low(IN BOOL);
	   BOOL   prefs_should_skip_if_all_rating_exists(void);
	   void   prefs_set_skip_if_all_rating_exists(IN BOOL);
	   BOOL   prefs_should_skip_if_all_rating_low(void);
	   void   prefs_set_skip_if_all_rating_low(IN BOOL);
	   BOOL   prefs_should_skip_if_above_maximum_file_size(void);
	   void   prefs_set_skip_if_above_maximum_file_size(IN BOOL);
	   int    prefs_get_maximum_file_size_index(void);
	   void   prefs_set_maximum_file_size_index(IN int);
	   char **prefs_get_list_of_maximum_file_sizes(void);
	   int    prefs_get_number_of_maximum_file_sizes(void);
	   int    prefs_get_main_window_X(void);
	   void   prefs_set_main_window_X(IN int);
	   int    prefs_get_main_window_Y(void);
	   void   prefs_set_main_window_Y(IN int);
	   void   prefs_save_preferences(void);
	   void   prefs_reset_preferences(void);
	   void   prefs_default_preferences(void);
static void   _build_list_of_images_sets(void);
static void   _recompute_list_of_images_sets_index(void);
static void   _free_list_of_images_sets(void);
static void   _clear_preferences(void);



/* Definitions */

static KVSstore  *_store_as_default         = NULL;
static KVSstore  *_store_on_file            = NULL;
static KVSstore  *_store                    = NULL;
static /*@only@*/ char     **_images_sets              = NULL;
static int        _number_of_images_sets    = 0;
static int        _current_images_set_index = 0;

int prefs_setup(
	void)
{
	FILE *file        = NULL;
	int   r           = 0;
	char  key[100]    = { 0 };
	char  value[100]  = { 0 };
	
	log_print_debug("prefs_setup()\n");

	_store_as_default = kvs_create(kvs_compare_strings);
	_store_on_file    = kvs_create(kvs_compare_strings);
	_store            = kvs_create(kvs_compare_strings);

	file = fopen(PREFS_FILE, "r"); /* Flawfinder: ignore */
	if(file == NULL)
	{
		log_print_warning( GetString( MSG_PREFS_PREFSINITPREFERENCESCOULDNOTOPENPREFERENCES ) );
	}
	else
	{
		// Tygre 2017/05/20: StormC
		// I cannot:
		//	while((r = fscanf(file, "%99[^= ] = %99[^\n]\n", key, value)) != EOF)
		// as with VBCC because it leads to an infinite loop with StormC
		// I must:
		//	while((r = fscanf(file, "%99[^= ] = %99[^\n]\n", key, value)) == 2)
		while((r = fscanf(file, "%99[^= ] = %99[^\n]\n", key, value)) != EOF)
		{
			// If the file is empty, then
			//	key = { '\n' }
			// and I do not consider it.
			if(strlen(key) > 1)
			{
				// Removing trailing '\n'
				value[strlen(value)] = '\0';
				
				kvs_put(_store_on_file, string_duplicate(key), string_duplicate(value));
				kvs_put(_store        , string_duplicate(key), string_duplicate(value));
			}
		}
		fclose(file);
	}

	// Defaults
	kvs_put(_store_as_default,  string_duplicate(SAVED_MODULE_DIRECTORY), string_duplicate("modules/"));
	if(kvs_get_value(_store,                     SAVED_MODULE_DIRECTORY) == NULL)
	{
		kvs_put(_store_on_file, string_duplicate(SAVED_MODULE_DIRECTORY), string_duplicate("modules/"));
		kvs_put(_store,         string_duplicate(SAVED_MODULE_DIRECTORY), string_duplicate("modules/"));
	}
	
	kvs_put(_store_as_default,  string_duplicate(SHOULD_PLAY_ON_STARTUP), string_btoa(FALSE));
	if(kvs_get_value(_store,                     SHOULD_PLAY_ON_STARTUP) == NULL)
	{
		kvs_put(_store_on_file, string_duplicate(SHOULD_PLAY_ON_STARTUP), string_btoa(FALSE));
		kvs_put(_store,         string_duplicate(SHOULD_PLAY_ON_STARTUP), string_btoa(FALSE));
	}

	kvs_put(_store_as_default,  string_duplicate(SHOULD_SHOW_LOG_PAGE_ON_FETCH), string_btoa(FALSE));
	if(kvs_get_value(_store,                     SHOULD_SHOW_LOG_PAGE_ON_FETCH) == NULL)
	{
		kvs_put(_store_on_file, string_duplicate(SHOULD_SHOW_LOG_PAGE_ON_FETCH), string_btoa(FALSE));
		kvs_put(_store,         string_duplicate(SHOULD_SHOW_LOG_PAGE_ON_FETCH), string_btoa(FALSE));
	}

	kvs_put(_store_as_default,  string_duplicate(MAXIMUM_LOG_SIZE_INDEX), string_duplicate("1"));
	if(kvs_get_value(_store,                     MAXIMUM_LOG_SIZE_INDEX) == NULL)
	{
		kvs_put(_store_on_file, string_duplicate(MAXIMUM_LOG_SIZE_INDEX), string_duplicate("1"));
		kvs_put(_store,         string_duplicate(MAXIMUM_LOG_SIZE_INDEX), string_duplicate("1"));
	}

	kvs_put(_store_as_default,  string_duplicate(SHOULD_CACHE_LIST_DURING_RUN), string_btoa(TRUE));
	if(kvs_get_value(_store,                     SHOULD_CACHE_LIST_DURING_RUN) == NULL)
	{
		kvs_put(_store_on_file, string_duplicate(SHOULD_CACHE_LIST_DURING_RUN), string_btoa(TRUE));
		kvs_put(_store,         string_duplicate(SHOULD_CACHE_LIST_DURING_RUN), string_btoa(TRUE));
	}

	kvs_put(_store_as_default,  string_duplicate(SHOULD_CACHE_LIST_BETWEEN_RUNS), string_btoa(TRUE));
	if(kvs_get_value(_store,                     SHOULD_CACHE_LIST_BETWEEN_RUNS) == NULL)
	{
		kvs_put(_store_on_file, string_duplicate(SHOULD_CACHE_LIST_BETWEEN_RUNS), string_btoa(TRUE));
		kvs_put(_store,         string_duplicate(SHOULD_CACHE_LIST_BETWEEN_RUNS), string_btoa(TRUE));
	}

	kvs_put(_store_as_default,  string_duplicate(CACHE_DIRECTORY), string_duplicate("cache/"));
	if(kvs_get_value(_store,                     CACHE_DIRECTORY) == NULL)
	{
		kvs_put(_store_on_file, string_duplicate(CACHE_DIRECTORY), string_duplicate("cache/"));
		kvs_put(_store,         string_duplicate(CACHE_DIRECTORY), string_duplicate("cache/"));
	}

	kvs_put(_store_as_default,  string_duplicate(SHOULD_SAVE_PREFS_ON_EXIT), string_btoa(TRUE));
	if(kvs_get_value(_store,                     SHOULD_SAVE_PREFS_ON_EXIT) == NULL)
	{
		kvs_put(_store_on_file, string_duplicate(SHOULD_SAVE_PREFS_ON_EXIT), string_btoa(TRUE));
		kvs_put(_store,         string_duplicate(SHOULD_SAVE_PREFS_ON_EXIT), string_btoa(TRUE));
	}

	kvs_put(_store_as_default,  string_duplicate(SHOULD_SKIP_IF_BANNED), string_btoa(TRUE));
	if(kvs_get_value(_store,                     SHOULD_SKIP_IF_BANNED) == NULL)
	{
		kvs_put(_store_on_file, string_duplicate(SHOULD_SKIP_IF_BANNED), string_btoa(TRUE));
		kvs_put(_store,         string_duplicate(SHOULD_SKIP_IF_BANNED), string_btoa(TRUE));
	}

	kvs_put(_store_as_default,  string_duplicate(IMAGES_DIRECTORY), string_duplicate("Default/"));
	if(kvs_get_value(_store,                     IMAGES_DIRECTORY) == NULL)
	{
		kvs_put(_store_on_file, string_duplicate(IMAGES_DIRECTORY), string_duplicate("Default/"));
		kvs_put(_store,         string_duplicate(IMAGES_DIRECTORY), string_duplicate("Default/"));
	}

	// Tygre 2020/05/21: Bigger size
	// I set the default module size
	// to be 20Kb or less instead of
	// 5Kb or less for more modules.
	kvs_put(_store_as_default,  string_duplicate(MAXIMUM_FILE_SIZE_INDEX), string_duplicate("3"));
	if(kvs_get_value(_store,                     MAXIMUM_FILE_SIZE_INDEX) == NULL)
	{
		kvs_put(_store_on_file, string_duplicate(MAXIMUM_FILE_SIZE_INDEX), string_duplicate("3"));
		kvs_put(_store,         string_duplicate(MAXIMUM_FILE_SIZE_INDEX), string_duplicate("3"));
	}

	kvs_put(_store_as_default,  string_duplicate(MAIN_WINDOW_X), string_duplicate("0"));
	if(kvs_get_value(_store,                     MAIN_WINDOW_X) == NULL)
	{
		kvs_put(_store_on_file, string_duplicate(MAIN_WINDOW_X), string_duplicate("0"));
		kvs_put(_store,         string_duplicate(MAIN_WINDOW_X), string_duplicate("0"));
	}

	kvs_put(_store_as_default,  string_duplicate(MAIN_WINDOW_Y), string_duplicate("11"));
	if(kvs_get_value(_store,                     MAIN_WINDOW_Y) == NULL)
	{
		kvs_put(_store_on_file, string_duplicate(MAIN_WINDOW_Y), string_duplicate("11"));
		kvs_put(_store,         string_duplicate(MAIN_WINDOW_Y), string_duplicate("11"));
	}

	// Images sets
	_build_list_of_images_sets();

	return RETURN_OK;
}

void prefs_cleanup(
	 void)
{
	log_print_debug("prefs_cleanup()\n");

	kvs_destroy(_store_as_default);
	kvs_destroy(_store_on_file);
	kvs_destroy(_store);

	// Images sets
	_free_list_of_images_sets();
}

char *prefs_get_saved_modules_directory(
	  void)
{
	return (char *)kvs_get_value(_store, SAVED_MODULE_DIRECTORY);
}

void prefs_set_saved_modules_directory(
	 IN char *new_saved_modules_directory)
{
	kvs_put(_store, string_duplicate(SAVED_MODULE_DIRECTORY), string_duplicate(new_saved_modules_directory));
}

BOOL prefs_should_play_on_startup(
	 void)
{
	return string_atoi(kvs_get_value(_store, SHOULD_PLAY_ON_STARTUP));
}

void prefs_set_play_on_startup(
	 IN BOOL should_play_on_startup)
{
	kvs_put(_store, string_duplicate(SHOULD_PLAY_ON_STARTUP), string_btoa(should_play_on_startup));
}

BOOL prefs_should_show_log_page_on_fetch(
	 void)
{
	return string_atoi(kvs_get_value(_store, SHOULD_SHOW_LOG_PAGE_ON_FETCH));
}

void prefs_set_show_log_page_on_fetch(
	 IN BOOL should_show_log_page_on_fetch)
{
	kvs_put(_store, string_duplicate(SHOULD_SHOW_LOG_PAGE_ON_FETCH), string_btoa(should_show_log_page_on_fetch));
}

int  prefs_get_maximum_log_size_index(
	   void)
{
	return string_atoi(kvs_get_value(_store, MAXIMUM_LOG_SIZE_INDEX));
}

void prefs_set_maximum_log_size_index(
	 IN int new_maximum_log_size_index)
{
	char string[GLOBALS_MAX_LINE_LENGTH] = { 0 };

	string_itoa(new_maximum_log_size_index, 10, string);
	kvs_put(_store, string_duplicate(MAXIMUM_LOG_SIZE_INDEX), string_duplicate(string));
}

char **prefs_get_list_of_maximum_log_sizes(
	   void)
{
	static /*@only@*/ char *sizes[] = MAXIMUM_LOG_SIZES;
	
	return sizes;
}

int prefs_get_number_of_maximum_log_sizes(
	void)
{
	return NUMBER_OF_MAXIMUM_LOG_SIZES;
}

BOOL prefs_should_remember_source_between_runs(
	 void)
{
	return string_atoi(kvs_get_value(_store, SHOULD_REMEMBER_SOURCE_BETWEEN_RUNS));
}

void prefs_set_remember_source_between_runs(
	 IN BOOL should_show_log_page_on_fetch)
{
	kvs_put(_store, string_duplicate(SHOULD_REMEMBER_SOURCE_BETWEEN_RUNS), string_btoa(should_show_log_page_on_fetch));
}

int prefs_get_remembered_source_index(
	void)
{
	return string_atoi(kvs_get_value(_store, REMEMBERED_SOURCE_INDEX));
}

void prefs_set_remembered_source_index(
	 IN int new_remembered_source_index)
{
	char string[GLOBALS_MAX_LINE_LENGTH] = { 0 };

	string_itoa(new_remembered_source_index, 10, string);
	kvs_put(_store, string_duplicate(REMEMBERED_SOURCE_INDEX), string_duplicate(string));
}

char *prefs_get_remembered_root_directory(
	  void)
{
	return (char *)kvs_get_value(_store, REMEMBERED_ROOT_DIRECTORY);
}

void prefs_set_remembered_root_directory(
	 IN char *new_remembered_root_directory)
{
	kvs_put(_store, string_duplicate(REMEMBERED_ROOT_DIRECTORY), string_duplicate(new_remembered_root_directory));
}

BOOL prefs_should_cache_list_during_run(
	 void)
{
	return string_atoi(kvs_get_value(_store, SHOULD_CACHE_LIST_DURING_RUN));
}

void prefs_set_cache_list_during_run(
	 IN BOOL should_cache_list_during_run)
{
	kvs_put(_store, string_duplicate(SHOULD_CACHE_LIST_DURING_RUN), string_btoa(should_cache_list_during_run));
}

BOOL prefs_should_cache_list_between_runs(
	 void)
{
	return string_atoi(kvs_get_value(_store, SHOULD_CACHE_LIST_BETWEEN_RUNS));
}

void prefs_set_cache_list_between_runs(
	 IN BOOL should_cache_list_between_runs)
{
	kvs_put(_store, string_duplicate(SHOULD_CACHE_LIST_BETWEEN_RUNS), string_btoa(should_cache_list_between_runs));
}

char *prefs_get_cache_directory(
	  void)
{
	return (char *)kvs_get_value(_store, CACHE_DIRECTORY);
}


void prefs_set_cache_directory(
	 IN char *new_cache_directory)
{
	kvs_put(_store, string_duplicate(CACHE_DIRECTORY), string_duplicate(new_cache_directory));
}

BOOL prefs_should_save_prefs_on_exit(
	 void)
{
	return string_atoi(kvs_get_value(_store, SHOULD_SAVE_PREFS_ON_EXIT));
}

void prefs_set_save_prefs_on_exit(
	 IN BOOL should_save_prefs_on_exit)
{
	kvs_put(_store, string_duplicate(SHOULD_SAVE_PREFS_ON_EXIT), string_btoa(should_save_prefs_on_exit));
}

char *prefs_get_images_directory(
	  void)
{
	return (char *)kvs_get_value(_store, IMAGES_DIRECTORY);
}

void prefs_set_images_directory(
	 IN char *new_images_directory)
{
	kvs_put(_store, string_duplicate(IMAGES_DIRECTORY), string_duplicate(new_images_directory));
}

char **prefs_get_list_of_images_sets(
	   void)
{
	return _images_sets;
}

int prefs_get_number_of_images_sets(
	void)
{
	return _number_of_images_sets;
}

int prefs_get_current_images_set_index(
 	void)
{
	return _current_images_set_index;
}

void prefs_set_current_images_set_index(
	 IN int new_current_image_set_index)
{
	_current_images_set_index = new_current_image_set_index;
}

BOOL prefs_should_skip_if_banned(
	 void)
{
	return string_atoi(kvs_get_value(_store, SHOULD_SKIP_IF_BANNED));
}

void prefs_set_skip_if_banned(
	 IN BOOL should_skip_if_banned)
{
	kvs_put(_store, string_duplicate(SHOULD_SKIP_IF_BANNED), string_btoa(should_skip_if_banned));
}

BOOL prefs_should_skip_if_user_rating_low(
	 void)
{
	return string_atoi(kvs_get_value(_store, SHOULD_SKIP_IF_USER_RATING_LOW));
}

void prefs_set_skip_if_user_rating_low(
	 IN BOOL should_skip_if_user_rating_low)
{
	kvs_put(_store, string_duplicate(SHOULD_SKIP_IF_USER_RATING_LOW), string_btoa(should_skip_if_user_rating_low));
}

BOOL prefs_should_skip_if_all_rating_exists(
	 void)
{
	return string_atoi(kvs_get_value(_store, SHOULD_SKIP_IF_ALL_RATING_EXISTS));
}

void prefs_set_skip_if_all_rating_exists(
	 IN BOOL should_skip_if_all_rating_exists)
{
	kvs_put(_store, string_duplicate(SHOULD_SKIP_IF_ALL_RATING_EXISTS), string_btoa(should_skip_if_all_rating_exists));
}

BOOL prefs_should_skip_if_all_rating_low(
	 void)
{
	return string_atoi(kvs_get_value(_store, SHOULD_SKIP_IF_ALL_RATING_LOW));
}

void prefs_set_skip_if_all_rating_low(
	 IN BOOL should_skip_if_all_rating_low)
{
	kvs_put(_store, string_duplicate(SHOULD_SKIP_IF_ALL_RATING_LOW), string_btoa(should_skip_if_all_rating_low));
}

BOOL prefs_should_skip_if_above_maximum_file_size(
	 void)
{
	return string_atoi(kvs_get_value(_store, SHOULD_SKIP_IF_ABOVE_MAXIMUM_FILE_SIZE));
}

void prefs_set_skip_if_above_maximum_file_size(
	 IN BOOL should_skip_if_above_maximum_file_size)
{
	kvs_put(_store, string_duplicate(SHOULD_SKIP_IF_ABOVE_MAXIMUM_FILE_SIZE), string_btoa(should_skip_if_above_maximum_file_size));
}

int prefs_get_maximum_file_size_index(
	void)
{
	return string_atoi(kvs_get_value(_store, MAXIMUM_FILE_SIZE_INDEX));
}

void prefs_set_maximum_file_size_index(
	 IN int new_maximum_file_size_index)
{
	char string[GLOBALS_MAX_LINE_LENGTH] = { 0 };

	string_itoa(new_maximum_file_size_index, 10, string);
	kvs_put(_store, string_duplicate(MAXIMUM_FILE_SIZE_INDEX), string_duplicate(string));
}

char **prefs_get_list_of_maximum_file_sizes(
	   void)
{
	static /*@only@*/ char *sizes[] = MAXIMUM_FILE_SIZES;
	
	return sizes;
}

int prefs_get_number_of_maximum_file_sizes(
	void)
{
	return NUMBER_OF_MAXIMUM_FILE_SIZES;
}

int prefs_get_main_window_X(
	void)
{
	return string_atoi(kvs_get_value(_store, MAIN_WINDOW_X));
}

void prefs_set_main_window_X(
	 IN int x_pos)
{
	char string[GLOBALS_MAX_LINE_LENGTH] = { 0 };

	string_itoa(x_pos, 10, string);
	kvs_put(_store, string_duplicate(MAIN_WINDOW_X), string_duplicate(string));
}

int prefs_get_main_window_Y(
	void)
{
	return string_atoi(kvs_get_value(_store, MAIN_WINDOW_Y));
}

void prefs_set_main_window_Y(
	 IN int y_pos)
{
	char string[GLOBALS_MAX_LINE_LENGTH] = { 0 };

	string_itoa(y_pos, 10, string);
	kvs_put(_store, string_duplicate(MAIN_WINDOW_Y), string_duplicate(string));
}

void prefs_save_preferences(
	 void)
{
	size_t  length = -1;
	char   *key    = NULL;
	char   *value  = NULL;
	FILE   *file   = NULL;

	file = fopen(PREFS_FILE, "w"); /* Flawfinder: ignore */
	if(file == NULL)
	{
		log_print_error( GetString( MSG_PREFS_PREFSSAVEPREFERENCESCOULDNOTOPENPREFERENCES ) );
		return;
	}

	length = kvs_length(_store);
	while(length > 0)
	{
		length--;
		key	  = (char *)kvs_get_key(_store, length);
		value = (char *)kvs_get_value(_store, key);
		fprintf(file, "%s = %s\n", (char *)key, (char *)value);
	}
	fclose(file);
}

void prefs_reset_preferences(
	 void)
{
	size_t  length = -1;
	char   *key    = NULL;
	char   *value  = NULL;

	_clear_preferences();

	length = kvs_length(_store_on_file);
	while(length > 0)
	{
		length--;
		key	  = (char *)kvs_get_key(_store_on_file, length);
		value = (char *)kvs_get_value(_store_on_file, key);
		
		kvs_put(_store, string_duplicate(key), string_duplicate(value));
	}

	// Images sets
	_recompute_list_of_images_sets_index();
}

void prefs_default_preferences(
	 void)
{
	size_t  length = -1;
	char   *key    = NULL;
	char   *value  = NULL;

	_clear_preferences();

	length = kvs_length(_store_as_default);
	while(length > 0)
	{
		length--;
		key	  = (char *)kvs_get_key(_store_as_default, length);
		value = (char *)kvs_get_value(_store_as_default, key);

		kvs_put(_store, string_duplicate(key), string_duplicate(value));
	}

	// Images sets
	_recompute_list_of_images_sets_index();
}

static void _build_list_of_images_sets(
	        void)
{
	io_dir *images_dir   = NULL;
	char   *name         = NULL;
	int     name_length  = 0;
	int     i            = 0;

	images_dir    = calloc(1, sizeof(io_dir));
	if(images_dir == NULL)
	{
		log_print_error("_build_list_of_images_sets(), could not allocate directory\n");
		goto _RETURN;
	}

	images_dir->lock    = Lock(IMAGES_DIRECTORY_ROOT, ACCESS_READ);
	if(images_dir->lock == BPTR_ZERO)
	{
		log_print_error("_build_list_of_images_sets(), could not lock directory\n");
		goto _RETURN;
	}

	if(Examine(images_dir->lock, &images_dir->fib) == 0)
	{
		log_print_error("_build_list_of_images_sets(), could not examine directory\n");
		goto _RETURN;
	}

	// First, I count the number of possible images sets
	while(ExNext(images_dir->lock, &images_dir->fib) != 0)
	{
		_number_of_images_sets++;
	}

	_images_sets    = malloc((_number_of_images_sets + 1) * sizeof(char *));
	if(_images_sets == NULL)
	{
		log_print_error("_build_list_of_images_sets(), could not allocate images sets\n");
		goto _RETURN;
	}

	// Second, I iterate through the images sets.
	// If one or more were removed, Examine() will
	// return 0. If one or more were added, they
	// will be ignored, i < _number_of_images_sets.
	if(Examine(images_dir->lock, &images_dir->fib) == 0)
	{
		log_print_error("_build_list_of_images_sets(), could not examine directory\n");
		goto _RETURN;
	}
	
	i = 0;
	while(ExNext(images_dir->lock, &images_dir->fib) != 0 &&
	      i < _number_of_images_sets)
	{
		name               = images_dir->fib.fib_FileName;
		name_length        = strlen(name);
		_images_sets[i]    = malloc((name_length + 1 + 1) * sizeof(char));
		if(_images_sets[i] == NULL)
		{
			log_print_error("_build_list_of_images_sets(), could not allocate images set\n");
			goto _RETURN;
		}
		strcpy(_images_sets[i], name);
		_images_sets[i][name_length + 0] = GLOBALS_SEPARATOR_PATHS_CHAR;
		_images_sets[i][name_length + 1] = '\0';

		if(string_equal(_images_sets[i], (char *)kvs_get_value(_store, IMAGES_DIRECTORY)))
		{
			prefs_set_current_images_set_index(i);
		}

		i++;
	}

	// I add an extra NULL entry to _images_sets
	// to accommodate the need of MUI KeyCycle.
	_images_sets[i] = NULL;

	goto _RETURN;
	_RETURN:
		if(images_dir->lock != BPTR_ZERO)
		{
			UnLock(images_dir->lock);
		}
		if(images_dir != NULL)
		{
			free(images_dir);
		}
		return;
}

static void _recompute_list_of_images_sets_index(
	        void)
{
	int i = 0;
	
	for(i = 0; i < _number_of_images_sets; i++)
	{
		if(string_equal(_images_sets[i], (char *)kvs_get_value(_store, IMAGES_DIRECTORY)))
		{
			prefs_set_current_images_set_index(i);
			break;
		}
	}
}

static void _free_list_of_images_sets(
	        void)
{
	int i = 0;
	
	for(i = 0; i < _number_of_images_sets; i++)
	{
		free(_images_sets[i]);
	}
	free(_images_sets);
}

static void _clear_preferences(
	        void)
{
	size_t  length = -1;
	char   *key    = NULL;

	length = kvs_length(_store);
	while(length > 0)
	{
		length--;
		key	= (char *)kvs_get_key(_store, length);
		kvs_remove(_store, key);
	}
}

