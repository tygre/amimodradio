/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



#ifndef RATINGS_H
#define RATINGS_H 1

#include "utils.h"

int  ratings_setup(
	 IN  BOOL  (*continue_long_operation_fp)(),
	 IN  void  (*data_size_callback_fp)(IN char *origin, IN int number_of_bytes),
	 IN  void  (*data_read_callback_fp)(IN char *origin, IN int number_of_bytes)
);

void ratings_cleanup(
	 void);

int  ratings_init_connection(
	 void);

void ratings_close_connection(
	 void);

int  ratings_get_user_rating(
	 IN  char *path,
	 OUT int  *rating);

int  ratings_get_all_ratings(
	 IN  char *path,
	 OUT int  *ratings,
	 OUT int  *tally);

int  ratings_add_user_rating(
	 IN  char *path,
	 IN  int   rating,
	 OUT int  *ratings,
	 OUT int  *tally);

#endif

