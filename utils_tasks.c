/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include "utils_tasks.h"

#include "globals.h"
#include "kvs.h"
#include "locale.h"
#include "log.h"
#include "utils.h"
#include "z_fortify.h"

#include <stdio.h>
#include <stdlib.h>
#include <dos/dos.h>       // For RETURN_OK, RETURN_ERROR...
#include <dos/dosextens.h> // For struct Task
#include <exec/types.h>    // For ULONG
#include <proto/exec.h>	   // For FindTask()



/* Constants and declarations */

	   int    utils_tasks_setup(void);
	   void   utils_tasks_cleanup(void);
	   int    utils_tasks_allocate_signal(OUT ULONG *, OUT ULONG *, IN char *);
	   void   utils_tasks_free_signal(IN ULONG);
	   void   utils_tasks_send_signal_without_ack(IN struct Task *, IN ULONG);
	   void   utils_tasks_send_signal_and_wait_ack(IN struct Task *, IN ULONG, IN ULONG);
	   void   utils_tasks_send_signal_and_wait_ack_from_last_sender(IN ULONG, IN ULONG);
	   void   utils_tasks_send_back_ack(void);
	   char  *utils_tasks_get_task_name(IN struct Task *);
	   char  *utils_tasks_get_current_task_name(void);
	   char  *utils_tasks_get_signalset_name(IN ULONG);
	   ULONG  utils_tasks_signal_task_to_do(void);
	   ULONG  utils_tasks_signal_task_to_wait(void);
	   ULONG  utils_tasks_signal_task_to_stop(void);
	   ULONG  utils_tasks_signal_control_task_stopped(void);
static int    _compare_task_ptrs(IN KVSkey *, IN KVSkey *);

static /*@only@*/ struct Task *_main_task                         = NULL;
static KVSstore               *_store_signals_names               = NULL;
static KVSstore               *_store_last_senders                = NULL;
static KVSstore               *_store_last_signals_ack            = NULL;
static ULONG                   _SIGNAL_TO_CONTROL_TASK_STOPPED    = -1;
static ULONG                   _SIGNAL_TO_TASK_DO                 = -1;
static ULONG                   _SIGNAL_TO_TASK_WAIT               = -1;
static ULONG                   _SIGNAL_TO_TASK_STOP               = -1;
static ULONG                   _SIGNALSET_TO_CONTROL_TASK_STOPPED = -1;
static ULONG                   _SIGNALSET_TO_TASK_DO              = -1;
static ULONG                   _SIGNALSET_TO_TASK_WAIT            = -1;
static ULONG                   _SIGNALSET_TO_TASK_STOP            = -1;



/* Definitions */

int	utils_tasks_setup(
	void)
{
	log_print_debug("utils_tasks_setup()\n");

	_main_task = FindTask(NULL);

	_store_signals_names       = kvs_create(kvs_compare_strings);
	_store_last_senders        = kvs_create(_compare_task_ptrs);
	_store_last_signals_ack    = kvs_create(_compare_task_ptrs);
	if(_store_signals_names    == NULL ||
	   _store_last_senders     == NULL ||
	   _store_last_signals_ack == NULL)
	{
		goto _RETURN_ERROR;
	}

	if(utils_tasks_allocate_signal(&_SIGNAL_TO_CONTROL_TASK_STOPPED, &_SIGNALSET_TO_CONTROL_TASK_STOPPED, "_SIGNAL_TO_CONTROL_TASK_STOPPED") == RETURN_ERROR ||
	   utils_tasks_allocate_signal(&_SIGNAL_TO_TASK_DO,              &_SIGNALSET_TO_TASK_DO,              "_SIGNAL_TO_TASK_DO")              == RETURN_ERROR ||
	   utils_tasks_allocate_signal(&_SIGNAL_TO_TASK_WAIT,            &_SIGNALSET_TO_TASK_WAIT,            "_SIGNAL_TO_TASK_WAIT")            == RETURN_ERROR ||
	   utils_tasks_allocate_signal(&_SIGNAL_TO_TASK_STOP,            &_SIGNALSET_TO_TASK_STOP,            "_SIGNAL_TO_TASK_STOP")            == RETURN_ERROR)
	{
		goto _RETURN_ERROR;
	}

	goto _RETURN_OK;
	_RETURN_OK:
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		utils_tasks_cleanup();
		return RETURN_ERROR;
}

void utils_tasks_cleanup(
	void)
{
	log_print_debug("utils_tasks_cleanup()\n");

	utils_tasks_free_signal(_SIGNAL_TO_CONTROL_TASK_STOPPED);
	utils_tasks_free_signal(_SIGNAL_TO_TASK_DO);
	utils_tasks_free_signal(_SIGNAL_TO_TASK_WAIT);
	utils_tasks_free_signal(_SIGNAL_TO_TASK_STOP);

	kvs_destroy(_store_signals_names);
				_store_signals_names    = NULL;
	kvs_destroy(_store_last_senders);
				_store_last_senders     = NULL;
	kvs_destroy(_store_last_signals_ack);
				_store_last_signals_ack = NULL;
}

int utils_tasks_allocate_signal(
 	OUT ULONG *signal,
	OUT ULONG *signalset,
 	IN  char  *signal_name)
{
	static int   signal_number    = 16;
		   char *signalset_string = NULL;

	// TODO: Understand why using -1L
	// to let the system assign the
	// signal numbers lead to the signal
	// _SIGNAL_TO_TASK_DO being sent
	// twice to the Fetcher only when
	// looking for a player for a source
	// that uses HTTPS.
	*signal = AllocSignal(signal_number);
	signal_number++;
	if(*signal == -1)
	{
		log_print_error( GetString( MSG_PLAYERS_ALLOCATESIGNALCOULDNOTALLOCATESIGNAL ) , signal_name);
		return RETURN_ERROR;
	}
	*signalset = 1 << *signal;

	// TODO: Understand why using ULONG keys
	// leads to some signal_name to be freed
	// twice and others not at all, above, in:
	//      kvs_destroy(_store_signals_names);
	// 	signalset_ptr = (ULONG *)malloc(sizeof(ULONG));
	// *signalset_ptr = *signalset;
	// kvs_put(_store_signals_names, signalset_ptr, string_duplicate(signal_name));
	signalset_string    = malloc((GLOBALS_MAX_NUMBER_DIGITS_FOR_SIZE + 1) * sizeof(char));
	if(signalset_string == NULL)
	{
		log_print_error("utils_tasks_allocate_signal(), could not allocate memory\n");
		return RETURN_ERROR;
	}
	string_snprintf(signalset_string, GLOBALS_MAX_NUMBER_DIGITS_FOR_SIZE + 1, "%lu", (ULONG)*signalset);
	kvs_put(_store_signals_names, signalset_string, string_duplicate(signal_name));

	return RETURN_OK;
}

void utils_tasks_free_signal(
	 IN ULONG  signal)
{
	FreeSignal(signal);
}

void utils_tasks_send_signal_without_ack(
	 IN struct Task *receiver,
	 IN ULONG        signalset)
{
	struct Task *sender = FindTask(NULL);

	if(receiver == NULL ||
	   sender   == NULL)
	{
		return;
	}

	// Signaling
	Signal((struct Task *)receiver, signalset);
}

void utils_tasks_send_signal_and_wait_ack(
	 IN struct Task *receiver,
	 IN ULONG        signalset,
	 IN ULONG        signalset_ack)
{
	struct Task	**receiver_ptr1     = NULL;
	struct Task	**receiver_ptr2     = NULL;
	struct Task **sender_ptr        = NULL;
	ULONG        *signalset_ack_ptr = NULL;
	ULONG         received_signals  = 0L;

	if(receiver      == NULL ||
	   signalset     == -1   ||
	   signalset_ack == -1)
	{
		return;
	}

	// To send the ack., I associate the receiver task
	// and the ack. signalset that the sender expects
	// with the sender that sent it to this receiver.
	// Because KVS handles its (key, value) pairs and
	// may have to free either key, value, or a pair,
	// I do not store the Task* but Task**.

	// (Receiver, sender)
	receiver_ptr1    = (struct Task **)malloc(sizeof(struct Task *));
	if(receiver_ptr1 == NULL)
	{
		return;
	}
	*receiver_ptr1 = (struct Task *)receiver;

	sender_ptr    = (struct Task **)malloc(sizeof(struct Task *));
	if(sender_ptr == NULL)
	{
		free(receiver_ptr1);
		return;
	}
	*sender_ptr  = FindTask(NULL);

	kvs_put(_store_last_senders, receiver_ptr1, sender_ptr);

	// (Receiver, Signalset)
	receiver_ptr2    = (struct Task **)malloc(sizeof(struct Task *));
	if(receiver_ptr2 == NULL)
	{
		return;
	}
	*receiver_ptr2 = (struct Task *)receiver;

	signalset_ack_ptr    = (ULONG *)malloc(sizeof(ULONG));
	if(signalset_ack_ptr == NULL)
	{
		free(receiver_ptr2);
		return;
	}
	*signalset_ack_ptr = signalset_ack;

	kvs_put(_store_last_signals_ack, receiver_ptr2, signalset_ack_ptr);

	// Signaling
	Signal((struct Task *)receiver, signalset);

	// Understanding why, when quitting AMR,
	// this statement would Guru 8000 0003 (memory):
	//      utils_tasks_get_signalset_name(signalset_ack)
	//
	// The reason was, of course, a race condition.
	// The listener would receivew STOP and would
	// acknowledge with STOPPED. AMR would wait
	// for the STOPPED message, would receive it,
	// acknowledge it, and continue. Between the
	// Signal() above and the Wait() below, AMR
	// would already have utils_tasks_cleanup(),
	// destroying the three KVSes, leading to a
	// NULL in utils_tasks_get_signalset_name().
	// The solutions are to wait for the Listener
	// task to disappear entirely OR comment
	// out the log_print_debug() statements below.
	//	log_print_debug(DEBUG, "utils_tasks_send_signal_and_wait_ack(), %s is waiting from %s for %s", utils_tasks_get_task_name(*sender_ptr), utils_tasks_get_task_name(receiver), utils_tasks_get_signalset_name(signalset_ack));
	received_signals = Wait(
		signalset_ack |
		SIGBREAKF_CTRL_C);
}

void utils_tasks_send_signal_and_wait_ack_from_last_sender(
	 IN ULONG signalset,
	 IN ULONG signalset_ack)
{
	struct Task  *current_task    = NULL;
	struct Task **last_sender_ptr = NULL;
	struct Task  *last_sender     = NULL;

	current_task = FindTask(NULL);

	// Getting the sender
	last_sender_ptr = (struct Task **)kvs_get_value(_store_last_senders, &current_task);
	if( last_sender_ptr == NULL ||
	   *last_sender_ptr == NULL)
	{
		log_print_warning("utils_tasks_send_signal_and_wait_ack_from_last_sender(), receiver is NULL");
		return;
	}
	last_sender = *last_sender_ptr;

	// Signaling
	utils_tasks_send_signal_and_wait_ack(last_sender, signalset, signalset_ack);
}

void utils_tasks_send_back_ack(
	 void)
{
	struct Task  *current_task        = NULL;
	struct Task **last_sender_ptr     = NULL;
	struct Task  *last_sender         = NULL;
	ULONG        *last_signal_ack_ptr = NULL;
	ULONG         last_signal_ack     = -1;

	current_task = FindTask(NULL);

	// Getting the sender
	last_sender_ptr = (struct Task **)kvs_get_value(_store_last_senders, &current_task);
	if( last_sender_ptr == NULL ||
	   *last_sender_ptr == NULL)
	{
		log_print_warning("utils_tasks_send_back_ack(), current task wasn't signalled by another task");
		return;
	}
	last_sender = *last_sender_ptr;

	// Getting the signalset
	last_signal_ack_ptr = (ULONG *)kvs_get_value(_store_last_signals_ack, &current_task);
	if(last_signal_ack_ptr == NULL)
	{
		log_print_warning("utils_tasks_send_back_ack(), current task doesn't know the ack. signalset");
		return;
	}
	last_signal_ack	= *last_signal_ack_ptr;

	// Signaling
	Signal(last_sender, last_signal_ack);
}

struct Task *utils_tasks_get_main_task(
			 void)
{
	return _main_task;
}

char *utils_tasks_get_task_name(
	  IN struct Task *task)
{
	return task->tc_Node.ln_Name;
}

char *utils_tasks_get_current_task_name(
	  void)
{
	return utils_tasks_get_task_name(FindTask(NULL));
}

char *utils_tasks_get_signalset_name(
	  IN ULONG signalset)
{
	char *signalset_string = NULL;
	char *signalset_names  = NULL;
	
	// Tygre 20/11/19: Many signals...
	// I pretend for the moment that there is only one signal in a signal set...

	signalset_string    = malloc((GLOBALS_MAX_NUMBER_DIGITS_FOR_SIZE + 1) * sizeof(char));
	if(signalset_string == NULL)
	{
		log_print_error("utils_tasks_get_signalset_name(), could not allocate memory");
		return NULL;
	}
	string_snprintf(signalset_string, GLOBALS_MAX_NUMBER_DIGITS_FOR_SIZE + 1, "%lu", signalset);
	signalset_names = (char *)kvs_get_value(_store_signals_names, signalset_string);
	free(signalset_string);

	return signalset_names;
}

ULONG  utils_tasks_signal_task_to_do(
	   void)
{
	return _SIGNALSET_TO_TASK_DO;
}

ULONG  utils_tasks_signal_task_to_wait(
	   void)
{
	return _SIGNALSET_TO_TASK_WAIT;
}

ULONG  utils_tasks_signal_task_to_stop(
	   void)
{
	return _SIGNALSET_TO_TASK_STOP;
}

ULONG  utils_tasks_signal_control_task_stopped(
	   void)
{
	return _SIGNALSET_TO_CONTROL_TASK_STOPPED;
}

static int _compare_task_ptrs(
		   IN KVSkey *a,
		   IN KVSkey *b)
{
	// I know that the two keys are pointers on pointers of Tasks.
	struct Task *x = *((struct Task **)a);
	struct Task *y = *((struct Task **)b);

	return x - y;
}

