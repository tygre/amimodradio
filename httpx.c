/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include "httpx.h"

#include "globals.h"
#include "httpx_http.h"
#include "httpx_https.h"
#include "locale.h"
#include "log.h"
#include "utils.h"
#include "utils_socketbase.h" // Sockets and such from Roadshow SDK, with task-specific SocketBase and data
#include "version.h"
#include "z_fortify.h"

#include <ctype.h>            // For tolower() and isalpha()
#include <proto/exec.h>       // For OpenLibrary() and CloseLibrary()



/* Constants and declarations */

#define MAXIMUM_HEADER_LENGTH 1024
#define SCHEME_HTTP           "http"
#define SCHEME_HTTPS          "https"

		int             httpx_setup(IN BOOL (*)(), IN void (*)(IN char *, IN int), IN void (*)(IN char *, IN int));
		void            httpx_cleanup(void);
		int             httpx_init_connection(IN scheme_type_t);
		void            httpx_close_connection(IN scheme_type_t);
		int             httpx_head(IN char *, IN char *, OUT httpx_response **);
		int             httpx_get (IN char *, IN char *, IN int, OUT httpx_response **);
		int             httpx_post(IN char *, IN char *, IN char *, IN int, OUT httpx_response **);
		void            httpx_free_httpx_response(IN httpx_response *);
		char           *get_until(IN char *, IN char *);
		char           *strrpl(IN char *, IN char *, IN char *);
static  httpx_response *_httpx_head(IN char *, IN char *, IN char *, IN int);
static  httpx_response *_httpx_get (IN char *, IN char *, IN char *, IN int);
static  httpx_response *_httpx_post(IN char *, IN char *, IN char *, IN int);
typedef httpx_response *(*caller)(IN char *, IN char *, IN char *, IN int);
static  httpx_response *_handle_response(IN caller, IN httpx_response *, IN char *, IN char *, IN int);
static  parsed_url     *_parse_url(IN char *);
static  void            _free_parsed_url(IN parsed_url *);
static  int             _is_scheme_char(IN int);
static  char           *_hostname_to_ip(IN char *);
static  char           *_inet_ntoa(struct in_addr);
static  char           *_base64_encode(IN char *);
static  void            _encode_block(IN unsigned char *, OUT char *, IN int);
static  int             _strstr(IN char *, IN char *);
static  int			    _stridx(IN char *, IN char *);



/* Definitions */

typedef httpx_response *(*httpx_request)(
	IN char       *http_headers,
	IN parsed_url *parsed_url,
	IN  int        bytes_limit);

static BOOL   _httpx_https_setup = FALSE;
static USHORT _number_redirects  = 0;

// Tygre 24/08/08: Multi-tasking
// These variables should not be shared between tasks,
// I must use tc_UserData_t from utils_socketbase.h.
//	static BOOL _httpx_http_inited  = FALSE;
//	static BOOL _httpx_https_inited = FALSE;



int  httpx_setup(
	 IN BOOL (*continue_long_operation_fp)(),
	 IN void (*httpx_size_callback_fp)(IN char *origin, IN int number_of_bytes),
	 IN void (*httpx_read_callback_fp)(IN char *origin, IN int number_of_bytes))
{
	log_print_debug("httpx_setup()\n");

	// If I cannot setup HTTP, there's a major problem, with SocketBase or such.
	if(httpx_http_setup(
		continue_long_operation_fp,
		httpx_size_callback_fp,
		httpx_read_callback_fp) == RETURN_ERROR)
	{
		return RETURN_ERROR;
	}

	// If I cannot setup HTTPs, it's okay (maybe the user didn't install AmiSSL).
	if(httpx_https_setup(
		continue_long_operation_fp,
		httpx_size_callback_fp,
		httpx_read_callback_fp) == RETURN_OK)
	{
		_httpx_https_setup = TRUE;
	}

	return RETURN_OK;
}

void httpx_cleanup(
	 void)
{	 
	log_print_debug("httpx_cleanup()\n");

	httpx_http_cleanup();
	httpx_https_cleanup();
}

int  httpx_init_connection(
	 IN scheme_type_t required_scheme)
{
	// printf("%s: httpx_init_connection()\n", FindTask(NULL)->tc_Node.ln_Name);
	log_print_debug("httpx_init_connection()\n");

	// If I cannot initialise SocketBase and other task data, all hope is lost!
	if(utils_socketbase_init() == RETURN_ERROR)
	{
		log_print_error("httpx_init_connection(), could not instantiate the task data!\n");
		return RETURN_ERROR;
	}

	// If I cannot open this library, there's a major problem.
	if(SocketBase == NULL)
	{
		if((SocketBase = OpenLibrary("bsdsocket.library", 4)) == NULL)
		{
			log_print_error( GetString( MSG_HTTP_HTTPINITCONNECTIONCOULDNOTOPENBSDSOCKETLIBRARYV4 ) );
			return RETURN_ERROR;
		}
	}
	else
	{
		log_print_error("httpx_init_connection(), another function already initialised SocketBase!\n");
	}

	// Init HTTP if required, it should always be possible
	if((required_scheme              == HTTP  ||
		required_scheme              == BOTH) &&
		httpx_http_init_connection() == RETURN_OK)
	{
		utils_socketbase_httpx_http_inited_set(TRUE);
	}

	// Init HTTPs if require, it's okay (maybe the user didn't install AmiSSL).
	if((required_scheme               == HTTPS ||
		required_scheme               == BOTH) &&
		_httpx_https_setup            == TRUE  &&
		httpx_https_init_connection() == RETURN_OK)
	{
		utils_socketbase_httpx_https_inited_set(TRUE);
	}

	// Init the number of redirects
	_number_redirects = 0;

	// Return the result
	if(utils_socketbase_httpx_http_inited_get()  == TRUE ||
	   utils_socketbase_httpx_https_inited_get() == TRUE)
	{
		return RETURN_OK;
	}
	else
	{
		return RETURN_ERROR;
	}
}

void httpx_close_connection(
	 IN scheme_type_t required_scheme)
{
	// printf("%s: httpx_close_connection()\n", FindTask(NULL)->tc_Node.ln_Name);
	log_print_debug("httpx_close_connection()\n");

	if((required_scheme                          == HTTP  ||
		required_scheme                          == BOTH) &&
		utils_socketbase_httpx_http_inited_get() == TRUE)
	{
		httpx_http_close_connection();
		utils_socketbase_httpx_http_inited_set(FALSE);
	}

	if((required_scheme                           == HTTPS ||
		required_scheme                           == BOTH) &&
		utils_socketbase_httpx_https_inited_get() == TRUE)
	{
		httpx_https_close_connection();
		utils_socketbase_httpx_https_inited_set(FALSE);
	}

	if(SocketBase != NULL)
	{
		CloseLibrary(SocketBase);
		SocketBase = NULL;
	}
	else
	{
		log_print_error("httpx_close_connection(), another function already closed SocketBase!\n");
	}

	utils_socketbase_close();
}

int httpx_head(
	IN  char            *url,
	IN  char            *custom_headers,
	OUT httpx_response **hresp)
{
	int status_code = 0;

	*hresp = _httpx_head(url, custom_headers, NULL, 0);
	if(*hresp == NULL)
	{
		return RETURN_ERROR;
	}
	else
	{
		status_code = (*hresp)->status_code_value;

		if(status_code == 200)
		{
			return RETURN_OK;
		}
		else
		{
			log_print_error("httpx_head(), HEAD failed with HTTP error response %d (%s)\n", status_code, url);
			httpx_free_httpx_response(*hresp);
			*hresp = NULL;
			return RETURN_ERROR;
		}
	}
}

int httpx_get(
	IN  char            *url,
	IN  char            *custom_headers,
	IN  int              bytes_limit,
	OUT httpx_response **hresp)
{
	int status_code = 0;

	*hresp = _httpx_get(url, custom_headers, NULL, bytes_limit);
	if(*hresp == NULL)
	{
		return RETURN_ERROR;
	}
	else
	{
		status_code = (*hresp)->status_code_value;

		if(status_code == 200)
		{
			return RETURN_OK;
		}
		else
		{
			log_print_error("httpx_get(), GET failed with HTTP error response %d (%s)\n", status_code, url);
			httpx_free_httpx_response(*hresp);
			*hresp = NULL;
			return RETURN_ERROR;
		}
	}
}

int http_post(
	IN  char            *url,
	IN  char            *custom_headers,
	IN  char            *post_data,
	IN  int              bytes_limit,
	OUT httpx_response **hresp)
{
	int status_code = 0;

	*hresp = _httpx_post(url, custom_headers, post_data, bytes_limit);
	if(*hresp == NULL)
	{
		return RETURN_ERROR;
	}
	else
	{
		status_code = (*hresp)->status_code_value;

		if(status_code == 200)
		{
			return RETURN_OK;
		}
		else
		{
			log_print_error("httpx_post(), POST failed with HTTP error response %d (%s)\n", status_code, url);
			httpx_free_httpx_response(*hresp);
			*hresp = NULL;
			return RETURN_ERROR;
		}
	}
}

void httpx_free_httpx_response(
	IN httpx_response *hresp)
{
	if(hresp != NULL)
	{
		if(hresp->request_uri != NULL)
		{
			_free_parsed_url(hresp->request_uri);
			hresp->request_uri = NULL;
		}
		if(hresp->body != NULL)
		{
			free((void *)hresp->body);
			hresp->body = NULL;
		}
		if(hresp->status_code != NULL)
		{
			free((void *)hresp->status_code);
			hresp->status_code = NULL;
		}
		if(hresp->status_text != NULL)
		{
			free((void *)hresp->status_text);
			hresp->status_text = NULL;
		}
		if(hresp->request_headers != NULL)
		{
			free((void *)hresp->request_headers);
			hresp->request_headers = NULL;
		}
		if(hresp->response_headers != NULL)
		{
			free((void *)hresp->response_headers);
			hresp->response_headers = NULL;
		}
		free((void *)hresp);
	}
}

// Get all characters until '*until' has been found
char *get_until(
	IN char *haystack,
	IN char *until)
{
	int offset = _stridx(haystack, until);
	return string_duplicate_n(haystack, offset);
}

// Search and new_needle a string with another string, in a string
char *strrpl(
	IN char *needle,
	IN char *new_needle,
	IN char *haystack)
{
	char *p           = NULL;
	char *old         = NULL;
	char *result      = NULL;
	int   counter     = 0;
	int   needle_size = 0;

	needle_size = strlen(needle);
	for(p = strstr(haystack, needle); p != NULL; p = strstr(p + needle_size, needle))
	{
		counter++;
	}
	counter = (strlen(new_needle) - needle_size) * counter + strlen(haystack) + 1;
	result  = (char *)calloc(counter, sizeof(char));

	old     = (char *)haystack;
	for(p = strstr(haystack, needle); p != NULL; p = strstr(p + needle_size , needle))
	{
		strncpy(result + strlen(result), old, p - old);
		strncpy(result + strlen(result), new_needle, strlen(new_needle) + 1);
		old = p + needle_size;
	}
	strncpy(result + strlen(result), old, strlen(old) + 1);

	return result;
}

static httpx_response *_httpx_head(
	IN char *url,
	IN char *custom_headers,
	IN char *post_data,
	IN int   bytes_limit)
{
	parsed_url     *purl                = NULL;
	char           *http_headers        = NULL;
	int             http_headers_length = 0;
	char           *upwd                = NULL;
	char           *base64              = NULL;
	char           *auth_header         = NULL;
	char           *string              = NULL;
	httpx_response *hresp               = NULL;

	// Parse URL
	purl = _parse_url(url);
	if(purl == NULL)
	{
		log_print_error( GetString( MSG_HTTP_HTTPHEADCHARCOULDNOTPARSEURL ) );
		goto _RETURN_ERROR;
	}

	// Declare header
	http_headers_length = MAXIMUM_HEADER_LENGTH;
	http_headers        = malloc(http_headers_length * sizeof(char));
	if(http_headers     == NULL)
	{
		log_print_error("httpx_head(), could not allocate HTTP headers\n");
		goto _RETURN_ERROR;
	}

	// Build query/headers
	if(purl->path != NULL)
	{
		if(purl->query != NULL)
		{
			string_snprintf(http_headers, http_headers_length, "HEAD /%s?%s HTTP/1.1\r\nHost:%s\r\nConnection:close\r\n", purl->path, purl->query, purl->host);
		}
		else
		{
			string_snprintf(http_headers, http_headers_length, "HEAD /%s HTTP/1.1\r\nHost:%s\r\nConnection:close\r\n", purl->path, purl->host);
		}
	}
	else
	{
		if(purl->query != NULL)
		{
			string_snprintf(http_headers, http_headers_length, "HEAD /?%s HTTP/1.1\r\nHost:%s\r\nConnection:close\r\n", purl->query, purl->host);
		}
		else
		{
			string_snprintf(http_headers, http_headers_length, "HEAD / HTTP/1.1\r\nHost:%s\r\nConnection:close\r\n", purl->host);
		}
	}

	// Handle authorisation if needed
	if(purl->username != NULL)
	{
		// Format username:password pair
		upwd    = malloc(MAXIMUM_HEADER_LENGTH * sizeof(char));
		if(upwd == NULL)
		{
			log_print_error("httpx_head(), could not allocate user password\n");
			goto _RETURN_ERROR;
		}
		string_snprintf(upwd, MAXIMUM_HEADER_LENGTH, "%s:%s", purl->username, purl->password);

		// Encode in Base64
		base64    = _base64_encode(upwd);
		if(base64 == NULL)
		{
			log_print_error("httpx_head(), could not allocate encoded password\n");
			goto _RETURN_ERROR;
		}

		// Form header
		auth_header    = malloc(MAXIMUM_HEADER_LENGTH * sizeof(char));
		if(auth_header == NULL)
		{
			log_print_error("httpx_head(), could not allocate authentication header\n");
			goto _RETURN_ERROR;
		}
		string_snprintf(auth_header, MAXIMUM_HEADER_LENGTH, "Authorization: Basic %s\r\n", base64);

		// Add to header
		http_headers_length = strlen(http_headers) + strlen(auth_header) + 1;
		string		        = realloc(http_headers, http_headers_length);
		if(string           == NULL)
		{
			log_print_error("httpx_head(), could not reallocate HTTP headers\n");
			goto _RETURN_ERROR;
		}
		http_headers = string;
		string_snprintf(http_headers, http_headers_length, "%s%s", http_headers, auth_header);

		free(upwd);
		upwd = NULL;
		free(base64);
		base64 = NULL;
		free(auth_header);
		auth_header = NULL;
	}

	// Add custom headers and close
	if(custom_headers != NULL)
	{
		string_snprintf(http_headers, http_headers_length, "%s%s\r\n", http_headers, custom_headers);
	}
	else
	{
		string_snprintf(http_headers, http_headers_length, "%s%s\r\n", http_headers, version_get_user_agent());
	}

	// Make request and return response
	if(string_equal(purl->scheme, SCHEME_HTTP) == TRUE)
	{
		if(utils_socketbase_httpx_http_inited_get() == TRUE)
		{
			hresp = httpx_http_request(http_headers, purl, bytes_limit);
		}
		else
		{
			log_print_warning("httpx_head(), HTTP wasn't initialised\n");
		}
	}
	else if(string_equal(purl->scheme, SCHEME_HTTPS) == TRUE)
	{
		if(utils_socketbase_httpx_https_inited_get() == TRUE)
		{
			hresp = httpx_https_request(http_headers, purl, bytes_limit);
		}
		else
		{
			log_print_warning("httpx_head(), HTTPs wasn't initialised\n");
		}
	}
	else
	{
		log_print_error("httpx_head(), unknown scheme!\n");
		goto _RETURN_ERROR;
	}

	if(hresp == NULL)
	{
		goto _RETURN_ERROR;
	}
	else
	{
		hresp = _handle_response(_httpx_head, hresp, custom_headers, post_data, bytes_limit);
	}

	goto _RETURN_OK;
	_RETURN_OK:
		return hresp;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(upwd != NULL)
		{
			free(upwd);
			upwd = NULL;
		}
		if(base64 != NULL)
		{
			free(base64);
			base64 = NULL;
		}
		if(auth_header != NULL)
		{
			free(auth_header);
			auth_header = NULL;
		}
		if(http_headers != NULL)
		{
			free(http_headers);
			http_headers = NULL;
		}
		if(purl != NULL)
		{
			_free_parsed_url(purl);
			purl = NULL;
		}
		return NULL;
}

static httpx_response *_httpx_get(
	IN char *url,
	IN char *custom_headers,
	IN char *post_data,
	IN int   bytes_limit)
{
	parsed_url     *purl                = NULL;
	char           *http_headers        = NULL;
	int             http_headers_length = 0;
	char           *upwd                = NULL;
	char           *base64              = NULL;
	char           *auth_header         = NULL;
	char           *string              = NULL;
	httpx_response *hresp               = NULL;

	// Parse URL
	purl = _parse_url(url);
	if(purl == NULL)
	{
		log_print_error( GetString( MSG_HTTP_HTTPHEADCHARCOULDNOTPARSEURL ) );
		goto _RETURN_ERROR;
	}

	// Declare variable
	http_headers_length = MAXIMUM_HEADER_LENGTH;
	http_headers        = malloc(http_headers_length * sizeof(char));

	// Build query/headers
	if(purl->path != NULL)
	{
		if(purl->query != NULL)
		{
			string_snprintf(http_headers, http_headers_length, "GET /%s?%s HTTP/1.1\r\nHost:%s\r\nConnection:close\r\n", purl->path, purl->query, purl->host);
		}
		else
		{
			string_snprintf(http_headers, http_headers_length, "GET /%s HTTP/1.1\r\nHost:%s\r\nConnection:close\r\n", purl->path, purl->host);
		}
	}
	else
	{
		if(purl->query != NULL)
		{
			string_snprintf(http_headers, http_headers_length, "GET /?%s HTTP/1.1\r\nHost:%s\r\nConnection:close\r\n", purl->query, purl->host);
		}
		else
		{
			string_snprintf(http_headers, http_headers_length, "GET / HTTP/1.1\r\nHost:%s\r\nConnection:close\r\n", purl->host);
		}
	}

	// Handle authorisation if needed
	if(purl->username != NULL)
	{
		// Format username:password pair
		upwd    = malloc(MAXIMUM_HEADER_LENGTH * sizeof(char));
		if(upwd == NULL)
		{
			log_print_error("httpx_get(), could not allocate user password\n");
			goto _RETURN_ERROR;
		}
		string_snprintf(upwd, MAXIMUM_HEADER_LENGTH, "%s:%s", purl->username, purl->password);

		// Encode in Base64
		base64    = _base64_encode(upwd);
		if(base64 == NULL)
		{
			log_print_error("httpx_get(), could not allocate encoded password\n");
			goto _RETURN_ERROR;
		}

		// Form header
		auth_header    = malloc(MAXIMUM_HEADER_LENGTH * sizeof(char));
		if(auth_header == NULL)
		{
			log_print_error("httpx_get(), could not allocate authentication header\n");
			goto _RETURN_ERROR;
		}
		string_snprintf(auth_header, MAXIMUM_HEADER_LENGTH, "Authorization: Basic %s\r\n", base64);

		// Add to header
		http_headers_length = strlen(http_headers) + strlen(auth_header) + 2;
		string		        = realloc(http_headers, http_headers_length);
		if(string           == NULL)
		{
			log_print_error("httpx_get(), could not allocate HTTP headers\n");
			goto _RETURN_ERROR;
		}
		http_headers = string;
		string_snprintf(http_headers, http_headers_length, "%s%s", http_headers, auth_header);

		free(upwd);
		free(base64);
		free(auth_header);
	}

	// Add custom headers and close
	if(custom_headers != NULL)
	{
		string_snprintf(http_headers, http_headers_length, "%s%s\r\n", http_headers, custom_headers);
	}
	else
	{
		string_snprintf(http_headers, http_headers_length, "%s%s\r\n", http_headers, version_get_user_agent());
	}

	// Make request and return response
	if(string_equal(purl->scheme, SCHEME_HTTP) == TRUE)
	{
		if(utils_socketbase_httpx_http_inited_get() == TRUE)
		{
			hresp = httpx_http_request(http_headers, purl, bytes_limit);
		}
		else
		{
			log_print_warning("httpx_get(), HTTP wasn't initialised\n");
		}
	}
	else if(string_equal(purl->scheme, SCHEME_HTTPS) == TRUE)
	{
		if(utils_socketbase_httpx_https_inited_get() == TRUE)
		{
			hresp = httpx_https_request(http_headers, purl, bytes_limit);
		}
		else
		{
			log_print_warning("httpx_get(), HTTPs wasn't initialised\n");
		}
	}
	else
	{
		log_print_error("httpx_get(), unknown scheme!\n");
		goto _RETURN_ERROR;
	}

	if(hresp == NULL)
	{
		goto _RETURN_ERROR;
	}
	else
	{
		hresp = _handle_response(_httpx_get, hresp, custom_headers, post_data, bytes_limit);
	}

	goto _RETURN_OK;
	_RETURN_OK:
		return hresp;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(upwd != NULL)
		{
			free(upwd);
		}
		if(base64 != NULL)
		{
			free(base64);
		}
		if(auth_header != NULL)
		{
			free(auth_header);
		}
		if(http_headers != NULL)
		{
			free(http_headers);
		}
		if(purl != NULL)
		{
			_free_parsed_url(purl);
		}
		return NULL;
}

static httpx_response *_httpx_post(
	IN char *url,
	IN char *custom_headers,
	IN char *post_data,
	IN int   bytes_limit)
{
	parsed_url     *purl                = NULL;
	char           *http_headers        = NULL;
	int             http_headers_length = 0;
	char           *upwd                = NULL;
	char           *base64              = NULL;
	char           *auth_header         = NULL;
	char           *string              = NULL;
	httpx_response *hresp               = NULL;

	// Parse URL
	purl = _parse_url(url);
	if(purl == NULL)
	{
		log_print_error( GetString( MSG_HTTP_HTTPHEADCHARCOULDNOTPARSEURL ) );
		goto _RETURN_ERROR;
	}

	// Declare header
	http_headers_length = MAXIMUM_HEADER_LENGTH;
	http_headers        = malloc(http_headers_length * sizeof(char));

	// Build query/headers
	if(purl->path != NULL)
	{
		if(purl->query != NULL)
		{
			string_snprintf(http_headers, http_headers_length, "POST /%s?%s HTTP/1.1\r\nHost:%s\r\nConnection:close\r\nContent-Length:%lu\r\nContent-Type:application/x-www-form-urlencoded\r\n", purl->path, purl->query, purl->host, strlen(post_data));
		}
		else
		{
			string_snprintf(http_headers, http_headers_length, "POST /%s HTTP/1.1\r\nHost:%s\r\nConnection:close\r\nContent-Length:%lu\r\nContent-Type:application/x-www-form-urlencoded\r\n", purl->path, purl->host, strlen(post_data));
		}
	}
	else
	{
		if(purl->query != NULL)
		{
			string_snprintf(http_headers, http_headers_length, "POST /?%s HTTP/1.1\r\nHost:%s\r\nConnection:close\r\nContent-Length:%lu\r\nContent-Type:application/x-www-form-urlencoded\r\n", purl->query, purl->host, strlen(post_data));
		}
		else
		{
			string_snprintf(http_headers, http_headers_length, "POST / HTTP/1.1\r\nHost:%s\r\nConnection:close\r\nContent-Length:%lu\r\nContent-Type:application/x-www-form-urlencoded\r\n", purl->host, strlen(post_data));
		}
	}

	// Handle authorisation if needed
	if(purl->username != NULL)
	{
		// Format username:password pair
		upwd    = malloc(MAXIMUM_HEADER_LENGTH * sizeof(char));
		if(upwd == NULL)
		{
			log_print_error("httpx_post(), could not allocate user password\n");
			goto _RETURN_ERROR;
		}
		string_snprintf(upwd, MAXIMUM_HEADER_LENGTH, "%s:%s", purl->username, purl->password);

		// Encode in Base64
		base64    = _base64_encode(upwd);
		if(base64 == NULL)
		{
			log_print_error("httpx_post(), could not allocate encoded password\n");
			goto _RETURN_ERROR;
		}

		// Form header
		auth_header    = malloc(MAXIMUM_HEADER_LENGTH * sizeof(char));
		if(auth_header == NULL)
		{
			log_print_error("httpx_post(), could not allocate authentication header\n");
			goto _RETURN_ERROR;
		}
		string_snprintf(auth_header, MAXIMUM_HEADER_LENGTH, "Authorization: Basic %s\r\n", base64);

		// Add to header
		http_headers_length = strlen(http_headers) + strlen(auth_header) + 2;
		string		        = realloc(http_headers, http_headers_length);
		if(string           == NULL)
		{
			log_print_error("httpx_post(), could not allocate HTTP headers\n");
			goto _RETURN_ERROR;
		}
		http_headers = string;
		string_snprintf(http_headers, http_headers_length, "%s%s", http_headers, auth_header);

		free(upwd);
		free(base64);
		free(auth_header);
	}

	// Add custom headers and close
	if(custom_headers != NULL)
	{
		string_snprintf(http_headers, http_headers_length, "%s%s\r\n", http_headers, custom_headers);
		string_snprintf(http_headers, http_headers_length, "%s\r\n%s", http_headers, post_data);
	}
	else
	{
		string_snprintf(http_headers, http_headers_length, "%s%s\r\n", http_headers, version_get_user_agent());
		string_snprintf(http_headers, http_headers_length, "%s\r\n%s", http_headers, post_data);
	}

	// Make request and return response
	if(string_equal(purl->scheme, SCHEME_HTTP) == TRUE)
	{
		if(utils_socketbase_httpx_http_inited_get() == TRUE)
		{
			hresp = httpx_http_request(http_headers, purl, bytes_limit);
		}
		else
		{
			log_print_warning("httpx_post(), HTTP wasn't initialised\n");
		}
	}
	else if(string_equal(purl->scheme, SCHEME_HTTPS) == TRUE)
	{
		if(utils_socketbase_httpx_https_inited_get() == TRUE)
		{
			hresp = httpx_https_request(http_headers, purl, bytes_limit);
		}
		else
		{
			log_print_warning("httpx_post(), HTTPs wasn't initialised\n");
		}
	}
	else
	{
		log_print_error("httpx_post(), unknown scheme!\n");
		goto _RETURN_ERROR;
	}

	if(hresp == NULL)
	{
		goto _RETURN_ERROR;
	}
	else
	{
		hresp = _handle_response(_httpx_post, hresp, custom_headers, post_data, bytes_limit);
	}

	goto _RETURN_OK;
	_RETURN_OK:
		return hresp;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(upwd != NULL)
		{
			free(upwd);
		}
		if(base64 != NULL)
		{
			free(base64);
		}
		if(auth_header != NULL)
		{
			free(auth_header);
		}
		if(http_headers != NULL)
		{
			free(http_headers);
		}
		if(purl != NULL)
		{
			_free_parsed_url(purl);
		}
		return NULL;
}

static httpx_response *_handle_response(
	IN caller          caller_fp,
	IN httpx_response *hresp,
	IN char           *custom_headers,
	IN char           *post_data,
	IN int             bytes_limit)
{
	int             status_code = hresp->status_code_value;
	char           *token       = NULL;
	char           *token2      = NULL;
	char           *location1   = NULL;
	int             length      = 0;
	char           *location2   = NULL;
	httpx_response *new_hresp   = NULL;

	// Tygre 23/02/17: Moving
	// Possible redirections are as follows:
	//		->https://amp.dascene.net/downmod.php?index=12345
	//  	--> 302 with Location: http://amp.dascene.net/modules/C/Cutcreator/MOD.eurochart%2020.gz
	//  	---> 301 with Location: http://amp.dascene.net/modules/C/Cutcreator/MOD.eurochart%2020.gz

	// Redirection response
	if(300 <= status_code && status_code < 400 && _number_redirects < GLOBALS_MAX_NUMBER_OF_RETRIES)
	{
		_number_redirects++;

		// Handle redirections
		token = strtok(hresp->response_headers, "\r\n");
		while(token != NULL)
		{
			// Tygre 24/07/14: Capital
			// I don't forget to lower the case
			// of all the characters in case of
			// AMP, whose server returns "location"
			// instead of more standard "Location".
			for(token2 = token; *token2 != '\0' && *token2 != ':'; ++token2)
			{
				*token2 = tolower(*token2);
			}
			if(_strstr(token, "location:"))
			{
				// Extract URL
				location1 = strrpl("location: ", "", token);

				// The location can be "relative"
				if(!strstr(location1, ":"))
				{
					length    = strlen(hresp->request_uri->scheme)
											  + 3
											  + strlen(hresp->request_uri->host)
											  + 1
											  + strlen(location1)
											  + 1;
					location2 = malloc(length * sizeof(char));
					string_snprintf(location2, length, "%s://%s/%s", hresp->request_uri->scheme, hresp->request_uri->host, location1);

					new_hresp =	caller_fp(location2, custom_headers, post_data, bytes_limit);

					httpx_free_httpx_response(hresp);
					free(location2);
					free(location1);

					break;
				}
				else
				{
					new_hresp =	caller_fp(location1, custom_headers, post_data, bytes_limit);

					httpx_free_httpx_response(hresp);
					free(location1);

					break;
				}
			}
			token = strtok(NULL, "\r\n");
		}

		return new_hresp;
	}

	/*
	// Informational response
	else if(100 <= status_code && status_code < 200)
	{
		_number_redirects = 0;
		return (http_response *)hresp;
	}
	// Successful response
	else if(200 <= status_code && status_code < 300)
	{
		_number_redirects = 0;
		return (http_response *)hresp;
	}
	// Client error response
	else if(400 <= status_code && status_code < 500)
	{
		_number_redirects = 0;
		return (http_response *)hresp;
	}
	// Server error reponse
	else if(500 <= status_code && status_code < 600)
	{
		_number_redirects = 0;
		return (http_response *)hresp;
	}
	else
	{
		log_print_error("_handle_response(), met the unknown HTTP status code %d\n", status_code);
		return NULL;
	}
	*/

	_number_redirects = 0;
	return (httpx_response *)hresp;
}

/*
	Parses a specified URL and returns the structure named 'parsed_url'
	Implemented according to:
	RFC 1738 - http://www.ietf.org/rfc/rfc1738.txt
	RFC 3986 -  http://www.ietf.org/rfc/rfc3986.txt
*/
static parsed_url *_parse_url(
	IN char *url)
{
	// Define the variables
	parsed_url *purl;
	const char *tmpstr;
	const char *curstr;
	int         len;
	int         i;
	int         default_port;
	int         userpass_flag;
	BOOL        bracket_flag;

	/*
		char *uri;      // mandatory
		char *scheme;   // mandatory
		char *host;     // mandatory
		char *ip;       // mandatory
		char *port;     // optional
		char *path;     // optional
		char *query;    // optional
		char *fragment; // optional
		char *username; // optional
		char *password; // optional
	*/

	// Allocate the parsed URL
	purl = (parsed_url *)malloc(sizeof(parsed_url));
	if(purl == NULL)
	{
		return NULL;
	}
	purl->uri      = NULL;
	purl->scheme   = NULL;
	purl->host     = NULL;
	purl->ip       = NULL;
	purl->port     = NULL;
	purl->path     = NULL;
	purl->query    = NULL;
	purl->fragment = NULL;
	purl->username = NULL;
	purl->password = NULL;

	curstr = url;

	/*
	 * <scheme>:<scheme-specific-part>
	 * <scheme> := [a-z\+\-\.]+
	 *             upper case = lower case for resiliency
	 */
	// Read scheme
	tmpstr = strchr(curstr, ':');
	if(tmpstr == NULL)
	{
		_free_parsed_url(purl);
		log_print_warning( GetString( MSG_HTTP_PARSEURLCHARNOTAWELLFORMEDURL ) );
		return NULL;
	}

	// Get the scheme length
	len = tmpstr - curstr;

	// Check restrictions
	for (i = 0; i < len; i++)
	{
		if (_is_scheme_char(curstr[i]) == 0)
		{
			// Invalid format
			_free_parsed_url(purl);
			log_print_warning( GetString( MSG_HTTP_PARSEURLCHARNOTAWELLFORMEDSCHEME ) );
			return NULL;
		}
	}

	// Copy the scheme to the storage
	purl->scheme = malloc((len + 1) * sizeof(char));
	if(purl->scheme == NULL)
	{
		_free_parsed_url(purl);
		log_print_error( GetString( MSG_HTTP_PARSEURLCHARCOULDNOTALLOCATEMEMORYFORSCHEME ) );
		return NULL;
	}
	strncpy(purl->scheme, curstr, len);
	(purl->scheme)[len] = '\0';

	// Make the character to lower if it is upper case
	for (i = 0; i < len; i++)
	{
		(purl->scheme)[i] = tolower(purl->scheme[i]);
	}

	if(len > 1 && (purl->scheme)[len - 1] == 's')
	{
		default_port = 443;
	}
	else
	{
		default_port = 80;
	}

	// Skip ':'
	tmpstr++;
	curstr = tmpstr;

	/*
	 * //<user>:<password>@<host>:<port>/<url-path>
	 * Any ":", "@" and "/" must be encoded.
	 */
	// Eat "//"
	for (i = 0; i < 2; i++)
	{
		if('/' != *curstr)
		{
			_free_parsed_url(purl);
			log_print_warning( GetString( MSG_HTTP_PARSEURLCHARNOTAWELLFORMEDURL ) );
			return NULL;
		}
		curstr++;
	}

	// Check if the user (and password) are specified
	userpass_flag = 0;
	tmpstr = curstr;
	while ('\0' != *tmpstr)
	{
		if('@' == *tmpstr)
		{
			userpass_flag = 1;
			break;
		}
		else if('/' == *tmpstr)
		{
			userpass_flag = 0;
			break;
		}
		tmpstr++;
	}

	// User and password specification
	tmpstr = curstr;
	if(userpass_flag)
	{
		// Read username
		while ('\0' != *tmpstr && ':' != *tmpstr && '@' != *tmpstr)
		{
			tmpstr++;
		}
		len = tmpstr - curstr;
		purl->username    = malloc((len + 1) * sizeof(char));
		if(purl->username == NULL)
		{
			_free_parsed_url(purl);
			log_print_warning( GetString( MSG_HTTP_PARSEURLCHARNOTAWELLFORMEDUSERNAMEPASSWORDPAIR ) );
			return NULL;
		}
		strncpy(purl->username, curstr, len);
		(purl->username)[len] = '\0';

		// Proceed current pointer
		curstr = tmpstr;
		if(':' == *curstr)
		{
			/* Skip ':' */
			curstr++;

			// Read password
			tmpstr = curstr;
			while('\0' != *tmpstr && '@' != *tmpstr)
			{
				tmpstr++;
			}
			len = tmpstr - curstr;
			purl->password    = malloc((len + 1) * sizeof(char));
			if(purl->password == NULL)
			{
				_free_parsed_url(purl);
				log_print_error( GetString( MSG_HTTP_PARSEURLCHARCOULDNOTALLOCATEMEMORYFORUSERNAMEPASSWORDP ) );
				return NULL;
			}
			strncpy(purl->password, curstr, len);
			(purl->password)[len] = '\0';
			curstr = tmpstr;
		}
		// Skip '@'
		if('@' != *curstr)
		{
			_free_parsed_url(purl);
			log_print_warning( GetString( MSG_HTTP_PARSEURLCHARNOTAWELLFORMEDUSERNAMEPASSWORDPAIR ) );
			return NULL;
		}
		curstr++;
	}

	if('[' == *curstr)
	{
		bracket_flag = TRUE;
	}
	else
	{
		bracket_flag = FALSE;
	}

	// Proceed on by delimiters with reading host
	tmpstr = curstr;
	while('\0' != *tmpstr) {
		if(bracket_flag && ']' == *tmpstr)
 		{
			// End of IPv6 address
			tmpstr++;
			break;
		}
		else if(!bracket_flag && (':' == *tmpstr || '/' == *tmpstr))
		{
			// Port number is specified
			break;
		}
		tmpstr++;
	}
	len = tmpstr - curstr;
	purl->host    = malloc((len + 1) * sizeof(char));
	if(purl->host == NULL || len <= 0)
	{
		_free_parsed_url(purl);
		log_print_warning( GetString( MSG_HTTP_PARSEURLCHARNOTAWELLFORMEDHOST ) );
		return NULL;
	}
	strncpy(purl->host, curstr, len);
	(purl->host)[len] = '\0';
	curstr = tmpstr;

	// Check if port number specified
	if(':' == *curstr)
	{
		curstr++;
		// Read port number
		tmpstr = curstr;
		while('\0' != *tmpstr && '/' != *tmpstr)
		{
			tmpstr++;
		}
		len = tmpstr - curstr;
		purl->port    = malloc((len + 1) * sizeof(char));
		if(purl->port == NULL)
		{
			_free_parsed_url(purl);
			log_print_error( GetString( MSG_HTTP_PARSEURLCHARCOULDNOTALLOCATEMEMORYFORPORT ) );
			return NULL;
		}
		strncpy(purl->port, curstr, len);
		(purl->port)[len] = '\0';
		curstr = tmpstr;
	}
	else
	{
		if(default_port == 80)
		{
			purl->port    = malloc((2 + 1) * sizeof(char));
			if(purl->port == NULL)
			{
				_free_parsed_url(purl);
				return NULL;
			}
			strncpy(purl->port, "80", 2 + 1);
		}
		else if(default_port == 443)
		{
			purl->port    = malloc((3 + 1) * sizeof(char));
			if(purl->port == NULL)
			{
				_free_parsed_url(purl);
				return NULL;
			}
			strncpy(purl->port, "443", 3 + 1);
		}
		else
		{
			purl->port    = malloc((1 + 1) * sizeof(char));
			if(purl->port == NULL)
			{
				_free_parsed_url(purl);
				return NULL;
			}
			strncpy(purl->port, "0", 1 + 1);
		}
	}

	// Get IP
	purl->ip = _hostname_to_ip(purl->host);

	// Set URI
	purl->uri = string_duplicate(url);

	// End of the string
	if('\0' == *curstr)
	{
		return purl;
	}

	// Skip '/'
	if('/' != *curstr)
	{
		_free_parsed_url(purl);
		log_print_warning( GetString( MSG_HTTP_PARSEURLCHARNOTAWELLFORMEDDOMAINNAME ) );
		return NULL;
	}
	curstr++;

	// Parse path
	tmpstr = curstr;
	while ('\0' != *tmpstr && '#' != *tmpstr  && '?' != *tmpstr)
	{
		tmpstr++;
	}
	len = tmpstr - curstr;
	purl->path    = malloc((len + 1) * sizeof(char));
	if(purl->path == NULL)
	{
		_free_parsed_url(purl);
		log_print_error( GetString( MSG_HTTP_PARSEURLCHARCOULDNOTALLOCATEMEMORYFORPATH ) );
		return NULL;
	}
	strncpy(purl->path, curstr, len);
	(purl->path)[len] = '\0';
	curstr = tmpstr;

	// Check if a query is specified
	if('?' == *curstr)
	{
		// Skip '?'
		curstr++;

		// Read query
		tmpstr = curstr;
		while('\0' != *tmpstr && '#' != *tmpstr)
		{
			tmpstr++;
		}
		len = tmpstr - curstr;
		purl->query    = malloc((len + 1) * sizeof(char));
		if(purl->query == NULL)
		{
			_free_parsed_url(purl);
			log_print_error( GetString( MSG_HTTP_PARSEURLCHARCOULDNOTALLOCATEMEMORYFORQUERY ) );
			return NULL;
		}
		strncpy(purl->query, curstr, len);
		(purl->query)[len] = '\0';
		curstr = tmpstr;
	}

	// Check if fragment is specified
	if('#' == *curstr)
	{
		// Skip '#'
		curstr++;

		// Read fragment
		tmpstr = curstr;
		while('\0' != *tmpstr)
		{
			tmpstr++;
		}
		len = tmpstr - curstr;
		purl->fragment    = malloc((len + 1) * sizeof(char));
		if(purl->fragment == NULL)
 		{
			_free_parsed_url(purl);
			log_print_error( GetString( MSG_HTTP_PARSEURLCHARCOULDNOTALLOCATEMEMORYFORFRAGMENT ) );
			return NULL;
		}
		strncpy(purl->fragment, curstr, len);
		(purl->fragment)[len] = '\0';
		curstr = tmpstr;
	}

	return purl;
}

static void _free_parsed_url(
	IN parsed_url *purl)
{
	if(purl != NULL)
	{
		if(purl->uri != NULL)
		{
			free((void *)purl->uri);
			purl->uri = NULL;
		}
		if(purl->scheme != NULL)
		{
			free((void *)purl->scheme);
			purl->scheme = NULL;
		}
		if(purl->host != NULL)
		{
			free((void *)purl->host);
			purl->host = NULL;
		}
		if(purl->ip != NULL)
		{
			free((void *)purl->ip);
			purl->ip = NULL;
		}
		if(purl->port != NULL)
		{
			free((void *)purl->port);
			purl->port = NULL;
		}
		if(purl->path != NULL)
		{
			free((void *)purl->path);
			purl->path = NULL;
		}
		if(purl->query != NULL)
		{
			free((void *)purl->query);
			purl->query = NULL;
		}
		if(purl->fragment != NULL)
		{
			free((void *)purl->fragment);
			purl->fragment = NULL;
		}
		if(purl->username != NULL)
		{
			free((void *)purl->username);
			purl->username = NULL;
		}
		if(purl->password != NULL)
		{
			free((void *)purl->password);
			purl->password = NULL;
		}
		free((void *)purl);
	}
}

// Check whether the character is permitted in scheme string
static int _is_scheme_char(
	IN int c)
{
	return (!isalpha(c) && '+' != c && '-' != c && '.' != c) ? 0 : 1;
}

// Convert a hostname into a textual IP address
static char *_hostname_to_ip(
	IN char *hostname)
{
	struct hostent *he         = NULL;
	struct sockaddr_in  remote = { 0 };
	char               *ip1    = NULL;
	char               *ip2    = NULL;

	// Get host name
	he = gethostbyname((char *)hostname);
	if(he == NULL)
	{
		log_print_error("_hostname_to_ip(), could not get IP address of %s\n" , hostname);
		return NULL;
	}

	// Zero out the remote address
	memset(&remote, 0, sizeof(remote));

	// Set up remote address for connect
	// Tygre 22/02/05: Guru on 68000
	// The code used to be:
	//	he  = gethostbyname((char *)hostname);
	//	ip1 = _inet_ntoa(*((struct in_addr *)he->h_addr));
	// which would Guru 8000 0003 on a 68000
	// because of misalignment of the address.
	// I replaced it by the simpler code:
	memcpy(&remote.sin_addr, he->h_addr, he->h_length);
	ip1 = _inet_ntoa(remote.sin_addr);

	ip2 = malloc((strlen(ip1) + 1) * sizeof(char));
	if(ip2 == NULL)
	{
		log_print_error("_hostname_to_ip(), could not allocate IP address of %s\n" , hostname);
		return NULL;
	}
	strncpy(ip2, ip1, strlen(ip1) + 1);

	return ip2;
}

/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */
static char *_inet_ntoa(
	struct in_addr in)
{
	static /*@only@*/ char b[18];
	char *p = (char *) &in;
	#define UC(b) (((int)b)&0xff)
	string_snprintf(b, sizeof(b), "%d.%d.%d.%d", UC(p[0]), UC(p[1]), UC(p[2]), UC(p[3]));
	return(b);
}

// Encode a string with Base64
static char *_base64_encode(
	IN char *clrstr)
{
	char *b64dst = malloc(strlen(clrstr) + 50);
	unsigned char in[3];
	int i, len = 0;
	int j = 0;

	if(b64dst == NULL)
	{
		return NULL;
	}

	b64dst[0] = '\0';
	while(clrstr[j] != '\0')
	{
		len = 0;
		for(i=0; i<3; i++)
		{
			in[i] = (unsigned char)clrstr[j];
			if(clrstr[j])
			{
				len++; j++;
			}
			else in[i] = 0;
		}
		if(len)
		{
			_encode_block(in, b64dst, len);
		}
	}
	return b64dst;
}

// Encode 3 8-bit binary bytes as 4 '6-bit' characters
static void _encode_block(
	IN  unsigned char *in,
	OUT char          *b64str,
	IN  int            len)
{
	char b64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	unsigned char out[5];
	out[0] = b64[ in[0] >> 2 ];
	out[1] = b64[ ((in[0] & 0x03) << 4) | ((in[1] & 0xf0) >> 4) ];
	out[2] = (unsigned char) (len > 1 ? b64[ ((in[1] & 0x0f) << 2) |
			 ((in[2] & 0xc0) >> 6) ] : '=');
	out[3] = (unsigned char) (len > 2 ? b64[ in[2] & 0x3f ] : '=');
	out[4] = '\0';
	strncat(b64str, (char *)out, sizeof(out)); /* Flawfinder: ignore */
}

// Check if one string contains another string
static int _strstr(
	IN char *haystack,
	IN char *needle)
{
	char *pos = (char *)strstr(haystack, needle);
	if(pos)
		return 1;
	else
		return 0;
}

// Get the offset of one string in another string
static int _stridx(
	IN char *a,
	IN char *b)
{
	char *offset = (char *)strstr(a, b);
	return offset - a;
}

