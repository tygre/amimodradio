#!/bin/sh
if [ -z "$1" ]; then
	echo "Missing commit comment"
else
	echo "\"$1\""
	while true; do
	read -p "Are you sure to want to commit with this message? " yn
	case $yn in
		[Yy]* )
			echo "Committing to SVN now..."
			# svn status | grep "^\!" | sed 's/^\! *//g' | xargs -IBLA svn rm "BLA"
			svn add --force ./ --auto-props --parents --depth infinity -q
			svn commit -m "$1"
			echo "Committing to Git now..."
			git add -A
			git commit -m "$1"
			git push
			break;;
		[Nn]* ) exit;;
		* ) echo "Please answer yes or no.";;
	esac
	done
fi