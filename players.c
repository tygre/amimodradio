/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre          <tygre@chingu.asia>
	Copyright 2017-2020 Matthew Mondor <mmondor@pulsar-zone.net>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include "players.h"

#include "arexx.h"
#include "locale.h"
#include "log.h"
#include "players_workers.h"
#include "utils.h"
#include "utils_tasks.h"
#include "z_dictionary.h"
#include "z_iniparser.h"
#include "z_fortify.h"

#include <ctype.h>
#include <dos/dostags.h>
#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/rexxsyslib.h>
#include <stdio.h>
#include <string.h>



/* Constants and declarations */

#define FILE_NAME_PLAYERS_DATA  "data/Players"
#define TEMPORARY_PLAYER_OUTPUT "T:AmiModRadio Player Output"
#define OUTPUT_BUFFER           64

#define DISPLAY  "display"
#define COMMAND  "command"
#define PORT     "port"
#define ICONIFY  "iconify"
#define LOAD_1   "load.1"
#define LOAD_2   "load.2"
#define PLAY_1   "play.1"
#define PLAY_2   "play.2"
#define REPLAY_1 "replay.1"
#define REPLAY_2 "replay.2"
#define PAUSE_1  "pause.1"
#define PAUSE_2  "pause.2"
#define REMOVE_1 "remove.1"
#define REMOVE_2 "remove.2"
#define STATUS_1 "status.1"
#define STATUS_2 "status.2"
#define TYPES    "types"

	   int   players_setup(IN BPTR);
	   void  players_cleanup(void);
	   int   players_init_connection(void);
	   void  players_close_connection(void);
	   char *players_get_player_name(void);
	   BOOL  players_can_play_module(IN char *);
	   int   players_load_modules(IN char *, IN char *);
	   int   players_play_modules(void);
	   int   players_replay_modules(void);
	   int   players_pause_modules(void);
	   int   players_remove_modules(void);
	   BOOL  players_check_player_playing(void);
static int   _parse_player_data(void);
static struct MsgPort *_get_player_port(void);
static int   _start_player(void);
static int   _start_player_simply(IN char *, IN char *);

// First implementation of the players-related data
// structs by Matthew Mondor <mmondor@pulsar-zone.net>.
// Current implementation maintained by Tygre.
typedef int player_id_t;
typedef struct _player_data
{
	const char *name;
	const char *arexx_port_name;
	const char *command_cli;
	const char *command_arexx;
	const char *load_modules_cmd1;
	const char *load_modules_cmd2;
	const char *play_modules_cmd1;
	const char *play_modules_cmd2;
	const char *replay_modules_cmd1;
	const char *replay_modules_cmd2;
	const char *pause_modules_cmd1;
	const char *pause_modules_cmd2;
	const char *remove_modules_cmd1;
	const char *remove_modules_cmd2;
	const char *player_listener_cmd;
	const char *player_listener_ret;
	const char *supported_types;
} _player_data_t;



/* Definitions */

static BPTR                       _path_list        = BPTR_ZERO;
static dictionary                *_players_ini_data = NULL;
static int                        _players_number   = 0;
static /*@only@*/ _player_data_t *_player_data      = NULL;
static player_id_t                _player_id        = -1;
static struct MsgPort            *_arexx_port       = NULL;



int players_setup(
	IN BPTR path_list)
{
	log_print_debug("players_setup()\n");

	_path_list = path_list;

	_players_ini_data = iniparser_load(FILE_NAME_PLAYERS_DATA);
	if(_players_ini_data == NULL)
	{
		log_print_error("players_setup(), could not open players data\n");
		return RETURN_ERROR;
	}
	if(_parse_player_data() == RETURN_ERROR)
	{
		log_print_error("players_setup(), could not parse players data\n");
		return RETURN_ERROR;
	}

	if(arexx_setup() == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_PLAYERS_PLAYERSINITCONNECTIONCOULDNOTOPENREXXSYSLIBLIBRARYV0 ) );
		return RETURN_ERROR;
	}

	if(players_workers_setup() == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_PLAYERS_PLAYERSINITCONNECTIONCOULDNOTALLOCATEONESIGNAL ) );
		return RETURN_ERROR;
	}

	return players_workers_player_listener_start();
}

void players_cleanup(
	 void)
{
	log_print_debug("players_cleanup()\n");

	players_workers_player_listener_stop();

	players_workers_cleanup();

	arexx_cleanup();

	if(_players_ini_data != NULL)
	{
		iniparser_freedict(_players_ini_data);
		_players_ini_data = NULL;
	}

	if(_player_data != NULL)
	{
		free(_player_data);
		_player_data = NULL;
	}
}

int players_init_connection(
	void)
{
	log_print_debug("players_init_connection()\n");

	// Tygre 22/12/12: Efficiency
	// I check first if a player was found before,
	// assuming that most users will keep their
	// player opened while AmiModRadio runs ;-)
	if(_player_id == -1)
	{
		if(_get_player_port() == NULL)
		{
			return _start_player();
		}
	}

	return RETURN_OK;
}

void players_close_connection(
	 void)
{
	log_print_debug("players_close_connection()\n");

	// I keep the player ID, assuming that
	// the same player will still run next.
	//	_player_id = -1;
}

char *players_get_player_name(
	  void)
{
	if(_player_id == -1)
	{
		log_print_error( GetString( MSG_PLAYERS_PLAYERSGETPLAYERNAMECOULDNOTACCESSPLAYER ) );
		return NULL;
	}

	return (char *)_player_data[_player_id].name;
}

BOOL players_can_play_module(
	 IN char *file_name)
{
	int   file_name_size       = 0;
	char *file_name_temp       = NULL;
	int   i                    = 0;
	int   extension_size       = 0;
	char *supported_types_temp = NULL;
	char *current_type         = NULL;
	char *next_type            = NULL;
	BOOL  match	               = FALSE;

	if(_player_id == -1)
	{
		log_print_error( GetString( MSG_PLAYERS_PLAYERSCANPLAYMODULECOULDNOTACCESSPLAYER ) );
		goto _RETURN_ERROR;
	}

	file_name_size    = strlen(file_name);
	file_name_temp    = malloc((file_name_size + 1) * sizeof(char));
	if(file_name_temp == NULL)
	{
		log_print_error( GetString( MSG_PLAYERS_PLAYERSCANPLAYMODULECOULDNOTALLOCATEFILENAME ) );
		goto _RETURN_ERROR;
	}
	for(i = 0; i < file_name_size; i++)
	{
		file_name_temp[i] = tolower(file_name[i]);
	}
	file_name_temp[i] = '\0';

	// Tygre 22/12/17: strtok() sucks!
	// I cannot use strtok() here, because this function
	// is called by players_load_modules(), which itself
	// already uses strtok(). Actually, I should remove
	// all uses of strtok(), too stupid and unsafe.
	
	// First, I make sure that the delimiter exist
	supported_types_temp    = malloc((strlen(_player_data[_player_id].supported_types) + 1 +1) * sizeof(char));
	if(supported_types_temp == NULL)
	{
		log_print_error("players_can_play_module(), couldn't allocate supported types\n");
		goto _RETURN_ERROR;
	}
	supported_types_temp[0] = '\0';
	strcat(supported_types_temp, _player_data[_player_id].supported_types);
	strcat(supported_types_temp, "|");

	// Second, I run my own pseudo-strtok()
	current_type = supported_types_temp;
	next_type    = strchr(current_type, '|');
	while(next_type != NULL)
	{
		*next_type = '\0';
		 next_type++;

		extension_size = strlen(current_type);
		// The file name should include the extension + '.' + at least one char
		if(file_name_size > extension_size + 1)
		{
			match = string_start_with(file_name_temp, current_type)
				    &&
					file_name_temp[extension_size] == '.'
					||
					string_end_with(file_name_temp, current_type)
					&&
					file_name_temp[file_name_size - extension_size - 1] == '.';

			if(match)
			{
				goto _RETURN_OK;
			}
		}
		current_type = next_type;
		next_type    = strchr(current_type, '|');
	}

	// If there was no match, report an unknown file format.
	goto _RETURN_ERROR;

	goto _RETURN_OK;
	_RETURN_OK:
		free(supported_types_temp);
		supported_types_temp = NULL;
		free(file_name_temp);
		file_name_temp = NULL;
		return TRUE;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(supported_types_temp != NULL)
		{
			free(supported_types_temp);
			supported_types_temp = NULL;
		}
		if(file_name_temp != NULL)
		{
			free(file_name_temp);
			file_name_temp = NULL;
		}
		return FALSE;
}

/**
 * Returns:
 * - RETURN_OK    if it could load and play some modules.
 * - RETURN_WARN  of it could load files but could not play them.
 * - RETURN_ERROR if it could not use the source or memory.
 */
int players_load_modules(
	IN char *files_path,
	IN char *list)
{
	char *list_temp        = NULL;
	char *token            = NULL;
	int   length           = 0;
	char *command          = NULL;
	BOOL  anything_to_play = FALSE;

	if(_player_id == -1)
	{
		log_print_error( GetString( MSG_PLAYERS_PLAYERSLOADANDPLAYMODULESCOULDNOTACCESSPLAYER ) );
		goto _RETURN_ERROR;
	}

	// Tygre 2015/06/20: Const...
	// string_duplicate() to convert from "const char *" to "char *".
	list_temp = string_duplicate(list);
	if(list_temp == NULL)
	{
		goto _RETURN_ERROR;
	}

	token = strtok(list_temp, "\n");
	while(token != NULL)
	{
		if(players_can_play_module(token + strlen(files_path) * sizeof(char)))
		{
			if(command != NULL)
			{
				free(command);
				command = NULL;
			}
			length     = strlen(_player_data[_player_id].load_modules_cmd1) + strlen(token) + 1;
			command    = malloc(length * sizeof(char));
			if(command == NULL)
			{
				log_print_error( GetString( MSG_PLAYERS_PLAYERSLOADANDPLAYMODULESCOULDNOTALLOCATEMEMORYFORPLAYMODCO ) );
				goto _RETURN_ERROR;
			}
			string_snprintf(command, length, _player_data[_player_id].load_modules_cmd1, token);
			if(arexx_send_command(_get_player_port, command, NULL, NULL) == RETURN_OK)
			{
				anything_to_play = TRUE;
				// Tygre 22/02/25: Can't catch a break
				// I should not break here so that I
				// add all the playable modules.
				//	break;
			}
		}

		token = strtok(NULL, "\n");
	}

	if(anything_to_play)
	{
		arexx_send_command(_get_player_port, _player_data[_player_id].load_modules_cmd2, NULL, NULL);
	}
	else
	{
		log_print_warning( GetString( MSG_PLAYERS_PLAYERSLOADANDPLAYMODULESCOULDNOTFINDANYMODULETOPLAY ) );
		goto _RETURN_WARN;
	}

	goto _RETURN_OK;
	_RETURN_OK:
		free(list_temp);
		free(command);
		return RETURN_OK;

	goto _RETURN_WARN;
	_RETURN_WARN:
		free(list_temp);
		free(command);
		return RETURN_WARN;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(list_temp != NULL)
		{
			free(list_temp);
		}
		if(command != NULL)
		{
			free(command);
		}
		return RETURN_ERROR;

}

int players_play_modules(
	void)
{
	if(_player_id == -1)
	{
		log_print_error( GetString( MSG_PLAYERS_PLAYERSPLAYMODULESCOULDNOTACCESSPLAYER ) );
		return RETURN_ERROR;
	}

	arexx_send_command(_get_player_port, _player_data[_player_id].play_modules_cmd1, NULL, NULL);
	arexx_send_command(_get_player_port, _player_data[_player_id].play_modules_cmd2, NULL, NULL);

	players_workers_player_listener_listen();

	return RETURN_OK;
}

int players_replay_modules(
	void)
{
	if(_player_id == -1)
	{
		log_print_error( GetString( MSG_PLAYERS_PLAYERSREPLAYMODULESCOULDNOTACCESSPLAYER ) );
		return RETURN_ERROR;
	}

	arexx_send_command(_get_player_port, _player_data[_player_id].replay_modules_cmd1, NULL, NULL);
	arexx_send_command(_get_player_port, _player_data[_player_id].replay_modules_cmd2, NULL, NULL);

	players_workers_player_listener_listen();

	return RETURN_OK;
}

int players_pause_modules(
	void)
{
	if(_player_id == -1)
	{
		log_print_error( GetString( MSG_PLAYERS_PLAYERSPAUSEMODULESCOULDNOTACCESSPLAYER ) );
		return RETURN_ERROR;
	}

	players_workers_player_listener_wait();

	arexx_send_command(_get_player_port, _player_data[_player_id].pause_modules_cmd1, NULL, NULL);
	arexx_send_command(_get_player_port, _player_data[_player_id].pause_modules_cmd2, NULL, NULL);

	return RETURN_OK;
}

int players_remove_modules(
	void)
{
	if(_player_id == -1)
	{
		log_print_error( GetString( MSG_PLAYERS_PLAYERSREMOVEMODULESCOULDNOTACCESSPLAYER ) );
		return RETURN_ERROR;
	}

	players_workers_player_listener_wait();

	arexx_send_command(_get_player_port, _player_data[_player_id].remove_modules_cmd1, NULL, NULL);
	arexx_send_command(_get_player_port, _player_data[_player_id].remove_modules_cmd2, NULL, NULL);

	return RETURN_OK;
}

BOOL players_check_player_playing(
	 void)
{
	BOOL  has_playing_stopped = FALSE;
	char *result              = NULL;

	if(_player_id == -1)
	{
		log_print_error( GetString( MSG_PLAYERS_PLAYERSREMOVEMODULESCOULDNOTACCESSPLAYER ) );
		return RETURN_ERROR;
	}

	has_playing_stopped = arexx_send_command(
								_get_player_port,
								_player_data[_player_id].player_listener_cmd,
								NULL,
								&result) == RETURN_ERROR
						  ||
						  string_equal(result, _player_data[_player_id].player_listener_ret) == TRUE;

	if(result != NULL)
	{
		free(result);
		result = NULL;
	}

	return has_playing_stopped;
}

static int _parse_player_data(
	void)
{
	int   sanity_count     = 0;
	int   keys_number      = 0;
	int   i, j             = 0;
	char *section_name     = NULL;
	char *section_key_name = NULL;
	char *key_name         = NULL;
	char *key_value        = NULL;
	char *tmp_value1       = NULL;
	char *tmp_value2       = NULL;

	_players_number    = iniparser_getnsec(_players_ini_data);
	if(_players_number == 0)
	{
		log_print_error("_parse_player_data(), couldn't find any player data\n");
		goto _RETURN_ERROR;
	}

	_player_data    = malloc(_players_number * sizeof(_player_data_t));
	if(_player_data == NULL)
	{
		log_print_error("_parse_player_data(), couldn't allocate player data\n");
		goto _RETURN_ERROR;
	}

	for(i = 0; i < _players_number; i++)
	{
		sanity_count = 0;
		section_name = (char *)iniparser_getsecname(_players_ini_data, i);
		keys_number  = iniparser_getsecnkeys(_players_ini_data, section_name);
		for(j = 0; j < keys_number; j++)
		{
			section_key_name  = (char *)iniparser_getkeyname(_players_ini_data, section_name, j);
			key_name          = section_key_name + (strlen(section_name) + 1) * sizeof(char);
			key_value         = (char *)iniparser_getstring(_players_ini_data, section_key_name, "");

			if(strlen(key_value) == 0)
			{
				// Eempty values become NULL, the sanity count will handle them.
				key_value = NULL;
			}

			if(string_equal(key_name, DISPLAY) == TRUE)
			{
				_player_data[i].name = key_value;
				sanity_count++;
			}
			else if(string_equal(key_name, COMMAND) == TRUE)
			{
				_player_data[i].command_cli = key_value;
				sanity_count++;
			}
			else if(string_equal(key_name, PORT) == TRUE)
			{
				_player_data[i].arexx_port_name = key_value;
				sanity_count++;
			}
			else if(string_equal(key_name, ICONIFY) == TRUE)
			{
				_player_data[i].command_arexx = key_value;
			}
			else if(string_equal(key_name, LOAD_1) == TRUE)
			{
				_player_data[i].load_modules_cmd1 = key_value;
				sanity_count++;
			}
			else if(string_equal(key_name, LOAD_2) == TRUE)
			{
				_player_data[i].load_modules_cmd2 = key_value;
			}
			else if(string_equal(key_name, PLAY_1) == TRUE)
			{
				_player_data[i].play_modules_cmd1 = key_value;
				sanity_count++;
			}
			else if(string_equal(key_name, PLAY_2) == TRUE)
			{
				_player_data[i].play_modules_cmd2 = key_value;
			}
			else if(string_equal(key_name, REPLAY_1) == TRUE)
			{
				_player_data[i].replay_modules_cmd1 = key_value;
				sanity_count++;
			}
			else if(string_equal(key_name, REPLAY_2) == TRUE)
			{
				_player_data[i].replay_modules_cmd2 = key_value;
			}
			else if(string_equal(key_name, PAUSE_1) == TRUE)
			{
				_player_data[i].pause_modules_cmd1 = key_value;
				sanity_count++;
			}
			else if(string_equal(key_name, PAUSE_2) == TRUE)
			{
				_player_data[i].pause_modules_cmd2 = key_value;
			}
			else if(string_equal(key_name, REMOVE_1) == TRUE)
			{
				_player_data[i].remove_modules_cmd1 = key_value;
				sanity_count++;
			}
			else if(string_equal(key_name, REMOVE_2) == TRUE)
			{
				_player_data[i].remove_modules_cmd2 = key_value;
			}
			else if(string_equal(key_name, STATUS_1) == TRUE)
			{
				_player_data[i].player_listener_cmd = key_value;
				sanity_count++;
			}
			else if(string_equal(key_name, STATUS_2) == TRUE)
			{
				_player_data[i].player_listener_ret = key_value;
				sanity_count++;
			}
			else if(string_equal(key_name, TYPES) == TRUE)
			{
				tmp_value1 = key_value;
				tmp_value2 = key_value;
				while(*tmp_value1 != '\0')
				{
					if(*tmp_value1 != ' ')
					{
						*tmp_value2 = tolower(*tmp_value1);
						 tmp_value2++;
					}
					tmp_value1++;
				}
				*tmp_value2 = '\0';
				_player_data[i].supported_types = key_value;
				sanity_count++;
			}
			else
			{
				log_print_warning("_parse_player_data(), doesn't understand key named \"%s\"\n", key_name);
			}
		}

		if(sanity_count < 11)
		{
			log_print_error("_parse_player_data(), data for player \"%s\" is incomplete!\n", section_name);
			goto _RETURN_ERROR;
		}
	}

	goto _RETURN_OK;
	_RETURN_OK:
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(_player_data != NULL)
		{
			free(_player_data);
			_player_data = NULL;
		}
		return RETURN_ERROR;
}

static struct MsgPort *_get_player_port(
	void)
{
	struct MsgPort *arexx_port = NULL;

	// Tygre 22/12/12: Efficiency
	// I check first if the current player still exists.
	// Only if it doesn't anymore do I look for one.
	if(-1 < _player_id && _player_id < _players_number)
	{
		Forbid();
		arexx_port = FindPort(_player_data[_player_id].arexx_port_name);
		Permit();
		if(arexx_port != NULL)
		{
			return arexx_port;
		}
	}

	// Either no previous play was found,
	// or the previously found disappeared.
	for(_player_id = _players_number - 1; _player_id > -1; _player_id--)
	{
		Forbid();
		arexx_port = FindPort(_player_data[_player_id].arexx_port_name);
		Permit();
		if(arexx_port != NULL)
		{
			return arexx_port;
		}
	}

	return NULL;
}

static int _start_player(
	void)
{
	for(_player_id = _players_number - 1; _player_id > -1; _player_id--)
	{
		log_print_information("Trying to start %s\n", _player_data[_player_id].name);
		if(_start_player_simply(TEMPORARY_PLAYER_OUTPUT, _player_data[_player_id].command_cli) == RETURN_OK)
		{
			log_print_information("Started %s\n", _player_data[_player_id].name);
			arexx_send_command(_get_player_port, _player_data[_player_id].command_arexx, NULL, NULL);
			return RETURN_OK;
		}
	}

	return RETURN_ERROR;
}

static int _start_player_simply(
	IN char *temporary_file,
	IN char *command)
{
	BPTR output1               = BPTR_ZERO;
	BPTR output2               = BPTR_ZERO;
	char buffer[OUTPUT_BUFFER] = { 0 };

	// Tygre 2015/12/07: Free!
	// Even if the command fails, the call to SystemTags succeeds
	// and, thus, all resources are freed, in particular the copy
	// of the path list and the file opened and given for output.

	output1 = Open(temporary_file, MODE_NEWFILE);
	if(output1 == 0)
	{
		return RETURN_ERROR;
	}
	if(SystemTags(command,
		SYS_Input,  NULL,
		SYS_Output, output1,
		SYS_Asynch, TRUE,
		NP_Path,    io_path_list_copy(_path_list),
		TAG_END) == -1)
	{
		// The call to SystemTags itself failed
		Close(output1);
		DeleteFile(temporary_file);
		return RETURN_ERROR;
	}

	Delay(25);

	output2 = Open(temporary_file, MODE_OLDFILE);
	if(output1 == 0)
	{
		return RETURN_ERROR;
	}
	FGets(output2, buffer, OUTPUT_BUFFER);
	Close(output2);

	if(strlen(buffer) > 0)
	{
		// The command executed by SystemTags failed
		DeleteFile(temporary_file);
		return RETURN_ERROR;
	}

	return RETURN_OK;
}

