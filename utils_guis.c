/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include "utils_guis.h"

#include "globals.h"
#include "prefs.h"
#include "utils.h"
#include "z_fortify.h"

#include <proto/icon.h> // For GetDiskObject()
#include <stdio.h>      // For FILE



/* Constants and declarations */

int                utils_guis_get_image_path(IN char *, IN char *, IN char *, OUT char *);
int                utils_guis_get_background_image_path(IN char *, IN char *, IN char *, OUT char *);
struct DiskObject *utils_guis_get_disk_object(void);



/* Definitions */

int utils_guis_get_image_path(
	IN  char *current_directory,
	IN  char *image_mui_prefix,
	IN  char *image_name,
	OUT char *image_path)
{
	FILE *file = NULL;

	// If image does not exist, use default
	string_snprintf(image_path, GLOBALS_MAX_LINE_LENGTH, "%s/images/%s%s", current_directory, prefs_get_images_directory(), image_name);
	if((file = fopen(image_path, "r")) != NULL) /* Flawfinder: ignore */
	{
		string_snprintf(image_path, GLOBALS_MAX_LINE_LENGTH, "%s%s/images/%s%s", image_mui_prefix, current_directory, prefs_get_images_directory(), image_name);
		fclose(file);
	}
	else
	{
		string_snprintf(image_path, GLOBALS_MAX_LINE_LENGTH, "%s%s/images/%s%s", image_mui_prefix, current_directory, "Default/", image_name);
	}
	return RETURN_OK;
}

int utils_guis_get_background_image_path(
	IN  char *current_directory,
	IN  char *image_mui_prefix,
	IN  char *image_name,
	OUT char *image_path)
{
	FILE *file = NULL;

	// If image does not exist, do not use default, use color
	string_snprintf(image_path, GLOBALS_MAX_LINE_LENGTH, "%s/images/%s%s", current_directory, prefs_get_images_directory(), image_name);
	if((file = fopen(image_path, "r")) != NULL) /* Flawfinder: ignore */
	{
		string_snprintf(image_path, GLOBALS_MAX_LINE_LENGTH, "%s%s/images/%s%s", image_mui_prefix, current_directory, prefs_get_images_directory(), image_name);
		fclose(file);
		return RETURN_OK;
	}
	else
	{
		strncpy(image_path, "", 1 + 1);
		return RETURN_WARN;
	}
}

struct DiskObject *utils_guis_get_disk_object(
	void)
{
	struct DiskObject *disk_object = NULL;

	disk_object = GetDiskObject("PROGDIR:AmiModRadio");
	if(disk_object == NULL)
	{
		disk_object = GetDiskObject("PROGDIR:AmiModRadio_ACT");
		if(disk_object == NULL)
		{
			disk_object = GetDiskObject("PROGDIR:AmiModRadio_CLI");
			if(disk_object == NULL)
			{
				disk_object = GetDiskObject("PROGDIR:AmiModRadio_MUI");
				if(disk_object == NULL)
				{
					disk_object = GetDiskObject("PROGDIR:AmiModRadio_REA");
					if(disk_object == NULL)
					{
						// Will use the default icon
					}
				}
			}
		}
	}

	return disk_object;
}
