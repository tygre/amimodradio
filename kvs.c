/*
 * http://codereview.stackexchange.com/questions/63493/simple-key-value-store-in-c-take-2
 * CC BY-SA
 *
 * Parts copyright (c) 2015-2024 Tygre <tygre@chingu.asia>
 * These parts are under the same license CC BY-SA as the original source code.
 */



/* Includes */

#include "kvs.h"

#include "locale.h"
#include "log.h"
#include "utils.h"  // For IN, OUT...
#include "z_fortify.h"

#include <stdlib.h> // For realloc()...
#include <string.h> // For memmove()...



/* Constants and declarations */

struct KVSpair {
	const KVSkey   *key;
		  KVSvalue *value;
};

struct KVSstore {
	KVSpair       *pairs;
	IN KVScompare *compare;
	size_t         length;
	size_t         space;
};

	   KVSstore *kvs_create(IN KVScompare *);
	   void      kvs_destroy(IN KVSstore *);
	   void      kvs_put(IN KVSstore *, IN KVSkey *, IN KVSvalue *);
	   KVSkey   *kvs_get_key(IN KVSstore *, IN size_t);
	   KVSvalue *kvs_get_value(IN KVSstore *, IN KVSkey *);
	   void      kvs_remove(IN KVSstore *, IN KVSkey *);
	   size_t    kvs_length(IN KVSstore *);
	   int       kvs_compare_pointers(IN KVSkey *, IN KVSkey *);
	   int       kvs_compare_unsigned_longs(IN KVSkey *, IN KVSkey *);
	   int       kvs_compare_strings(IN KVSkey *, IN KVSkey *);
static KVSpair  *kvs_search(IN KVSstore *, IN KVSkey *, IN int);
static KVSpair  *kvs_get_pair(IN KVSstore *, IN KVSkey *);
static void      kvs_resize_pairs(IN KVSstore *, IN size_t);
static void      _kvs_resize_pairs(IN KVSstore *, IN size_t);
static size_t    kvs_get_pair_index(IN KVSstore *, IN KVSpair *);
static size_t    kvs_get_bytes_from_pair(IN KVSstore *, IN KVSpair *);
static void      kvs_create_pair(IN KVSstore *, IN KVSkey *, IN KVSvalue *);
static void      kvs_remove_pair(IN KVSstore *, IN KVSpair *);

static const size_t _kvs_pair_size  = sizeof(KVSpair);
static const size_t _kvs_store_size = sizeof(KVSstore);



/* Definitions */

KVSstore *kvs_create(
	      IN KVScompare *compare)
{
	KVSstore *store = NULL;
	
	store    = malloc(_kvs_store_size);
	if(store == NULL)
	{
		log_print_error( GetString( MSG_KVS_KVSCREATECOULDNOTALLOCATEMEMORY ) );
		return NULL;
	}
	store->pairs  = NULL;
	store->length = 0;
	store->space  = 0;
	if(compare != NULL)
	{
		store->compare = compare;
	}
	else
	{
		store->compare = kvs_compare_pointers;
	}
	kvs_resize_pairs(store, 0);

	return store;
}

void kvs_destroy(
	 IN KVSstore *store)
{
	KVSkey   *key    = NULL;
	KVSvalue *value  = NULL;
	int       length = 0;
	int       i      = 0;
	
	if(store == NULL)
	{
		return;
	}
	if(store->pairs != NULL)
	{
		length = kvs_length(store);
		while(length > 0)
		{
			length--;

			key   = kvs_get_key  (store, length);
			value = kvs_get_value(store, key);

			free((void *)key);
			free((void *)value);
		}
		free((void *)store->pairs);
	}
	free((void *)store);
}

void kvs_put(
	 IN KVSstore *store,
	 IN KVSkey   *key,
	 IN void     *value)
{
	KVSpair *pair = kvs_get_pair(store, key);
	if(pair != NULL)
	{
		if(value != NULL)
		{
			free((void *)pair->key);
			free((void *)pair->value);
			
			pair->key   = (void *)key;
			pair->value = (void *)value;
		}
		else
		{
			kvs_remove_pair(store, pair);
		}
	}
	else if(value != NULL)
	{
		kvs_create_pair(store, key, value);
	}
}

KVSkey *kvs_get_key(
	IN KVSstore *store,
	IN size_t    index)
{
	if(store == NULL ||
	   index >= store->length)
	{
		return NULL;
	}
	return (store->pairs + index)->key;
}

KVSvalue *kvs_get_value(
	IN KVSstore *store,
	IN KVSkey   *key)
{
	KVSpair *pair = kvs_get_pair(store, key);
	return pair ? pair->value : NULL;
}

void kvs_remove(
	IN KVSstore *store,
	IN KVSkey   *key)
{
	kvs_put(store, key, NULL);
}

size_t kvs_length(
	IN KVSstore *store)
{
	if(store == NULL)
	{
		return 0;
	}
	return store->length;
}

int kvs_compare_pointers(
	IN KVSkey *a,
	IN KVSkey *b)
{
	return (char *)a - (char *)b;
}

int kvs_compare_unsigned_longs(
	IN KVSkey *a,
	IN KVSkey *b)
{
	ULONG x = *((ULONG *)a);
	ULONG y = *((ULONG *)b);

	return x - y;
}

int kvs_compare_strings(
	IN KVSkey *a,
	IN KVSkey *b)
{
	char *x = (char *)a;
	char *y = (char *)b;

	return strcmp(x, y);
}

static KVSpair *kvs_search(
	IN KVSstore *store,
	IN KVSkey   *key,
	IN int       exact)
{
	size_t      lbound  = 0;
	size_t      rbound  = store->length;
	size_t      index   = 0;
	KVSpair    *element = NULL;
	KVSpair    *pairs   = store->pairs;
	int         result  = 0;
	KVScompare *compare = (KVScompare *)store->compare;
	// Cannot compile with VBCC v0.8f or v0.9g because of "internal error 0 in line 5307 of file machines/m68k/machine.c":
	// 	IN KVScompare *compare = store->compare;

	while(lbound < rbound)
	{
		index   = lbound + ((rbound - lbound) >> 1);
		element = pairs + index;
		result  = compare(key, element->key);
		if(result < 0)
		{
			rbound = index;
		}
		else if(result > 0)
		{
			lbound = index + 1;
		}
		else
		{
			return element;
		}
	}
	return exact ? NULL : pairs + lbound;
}

static KVSpair *kvs_get_pair(
	IN KVSstore *store,
	IN KVSkey   *key)
{
	if(store        == NULL ||
	   store->pairs == NULL)
	{
		return NULL;
	}
	return kvs_search(store, key, 1);
}

static void kvs_resize_pairs(
	IN KVSstore *store,
	IN size_t    amount)
{
	_kvs_resize_pairs((KVSstore *)store, amount);
}

static void _kvs_resize_pairs(
	IN KVSstore *store,
	IN size_t    amount)
{
	if(store == NULL)
	{
		return;
	}
	store->length += amount;
	if(store->space > store->length * _kvs_pair_size)
	{
		return;
	}
	store->space += _kvs_pair_size;
	store->pairs  = realloc(store->pairs, store->space);

	if(store->pairs == NULL)
	{
		log_print_error( GetString( MSG_KVS_KVSRESIZEPAIRSCOULDNOTALLOCATEMEMORY ) );
	}
}

static size_t kvs_get_pair_index(
	IN KVSstore *store,
	IN KVSpair  *pair)
{
	if(store == NULL ||
	   pair  == NULL)
	{
		return -1;
	}
	return pair - store->pairs;
}

static size_t kvs_get_bytes_from_pair(
	IN KVSstore *store,
	IN KVSpair  *pair)
{
	size_t pair_index;

	if(store == NULL ||
	   pair  == NULL)
	{
		return 0;
	}
	pair_index = kvs_get_pair_index(store, pair);
	return (store->length - pair_index) * _kvs_pair_size;
}

static void kvs_create_pair(
	IN KVSstore *store,
	IN KVSkey   *key,
	IN KVSvalue *value)
{
	KVSpair *pair;

	if(store == NULL)
	{
		return;
	}
	pair = kvs_search(store, key, 0);
	if(pair < store->pairs + store->length)
	{
		size_t bytes = kvs_get_bytes_from_pair(store, pair);
		memmove(pair + 1, pair, bytes);
	}
	pair->key   = (void *)key;
	pair->value = (void *)value;
	kvs_resize_pairs(store, +1);
}

static void kvs_remove_pair(
	IN KVSstore *store,
	IN KVSpair  *pair)
{
	if(store == NULL ||
	   pair  == NULL)
	{
		return;
	}
	free((void *)pair->key);
	free((void *)pair->value);
	memmove((void *)pair, (void *)(pair + 1), kvs_get_bytes_from_pair(store, pair + 1));
	kvs_resize_pairs(store, -1);
}

