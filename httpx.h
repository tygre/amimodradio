/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



#ifndef HTTPX_H
#define HTTPX_H 1

#include "utils.h"

typedef enum
{
	HTTP,
	HTTPS,
	BOTH
} scheme_type_t;

typedef struct parsed_url
{
	const char *uri;      // mandatory
		  char *scheme;   // mandatory
		  char *host;     // mandatory
	const char *ip;       // mandatory
		  char *port;     // optional
		  char *path;     // optional
		  char *query;    // optional
		  char *fragment; // optional
		  char *username; // optional
		  char *password; // optional
} parsed_url;

typedef struct httpx_response
{
	const parsed_url    *request_uri;
	const unsigned char *body;
	const int            body_size_in_bytes;
	const char          *status_code;
	const int            status_code_value;
	const char          *status_text;
	const char          *request_headers;
		  char          *response_headers;
} httpx_response;

int   httpx_setup(
	  IN BOOL (*continue_long_operation_fp)(),
	  IN void (*httpx_size_callback_fp)(IN char *origin, IN int number_of_bytes),
	  IN void (*httpx_read_callback_fp)(IN char *origin, IN int number_of_bytes));

void  httpx_cleanup(
	  void);

int   httpx_init_connection(
	  IN scheme_type_t required_scheme);

void  httpx_close_connection(
	  IN scheme_type_t required_scheme);

int   httpx_head(
	  IN  char            *url,
	  IN  char            *custom_headers,
	  OUT httpx_response **response);

int   httpx_get(
	  IN  char            *url,
	  IN  char            *custom_headers,
	  IN  int              bytes_limit,
	  OUT httpx_response **response);

int   httpx_post(
	  IN  char            *url,
	  IN  char            *custom_headers,
	  IN  char            *post_data,
	  IN  int              bytes_limit,
	  OUT httpx_response **response);

void  httpx_free_httpx_response(
	  IN  httpx_response  *response);

char *get_until(
	  IN char *haystack,
	  IN char *until);

char *strrpl(
	  IN char *needle,
	  IN char *new_needle,
	  IN char *haystack);

#endif
