/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include "xad.h"

#include "globals.h"
#include "locale.h"
#include "log.h"
#include "utils.h"
#include "z_fortify.h"

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <proto/xadmaster.h>
#include <proto/exec.h>
#include <proto/dos.h>



/* Constants and declarations */

// Necessary! (Below only for VBCC.)
#define REG(reg,arg) __reg(#reg) arg

int   xad_setup(IN BOOL (*)(),IN void (*)(IN char *, IN int), IN void (*)(IN char *, IN int));
void  xad_cleanup(void);
int   xad_extract_archive(IN char *, IN char *, OUT char **);
#ifndef S_SPLINT_S
ULONG progress_function(REG(a0, struct Hook *), REG(a1, struct xadProgressInfo *));
#endif



/* Definitions */

	   struct       xadMasterBase *xadMasterBase    = NULL;
static BOOL        _is_xad_setup                    = FALSE;
static BOOL        (*_continue_long_operation_fp)() = NULL; // Tygre 20/08/02: To stop long-running HTTP operations
static void        (*_data_size_callback_fp)(
					IN char *origin,
					IN int number_of_bytes)         = NULL; // Tygre 22/12/19: To update a progress bar when necessary
static void        (*_data_read_callback_fp)(
					IN char *origin,
					IN int number_of_bytes)         = NULL; // Tygre 22/12/19: To update a progress bar when necessary
static struct Hook _progress_hook                                    = {{0, 0}, (ULONG (*)()) progress_function, 0, 0};

int  xad_setup(
	 IN  BOOL (*continue_long_operation_fp)(),
	 IN  void (*data_size_callback_fp)(IN char *origin, IN int number_of_bytes),
	 IN  void (*data_read_callback_fp)(IN char *origin, IN int number_of_bytes))
{
	log_print_debug("xad_setup()\n");

	if(xadMasterBase == NULL)
	{
		if((xadMasterBase = (struct xadMasterBase *)OpenLibrary("xadmaster.library", 11)) == NULL)
		{
			log_print_error( GetString( MSG_XAD_EXTRACTARCHIVECOULDNOTOPENXADMASTERLIBRARYV11 ) );
			_is_xad_setup = FALSE;
			return RETURN_ERROR;
		}
	}

	_continue_long_operation_fp = continue_long_operation_fp;
	_data_size_callback_fp      = data_size_callback_fp;
	_data_read_callback_fp      = data_read_callback_fp;

	_is_xad_setup = TRUE;
	return RETURN_OK;
}

void xad_cleanup(
	 void)
{
	log_print_debug("xad_cleanup()\n");

	if(xadMasterBase != NULL)
	{
		CloseLibrary((struct Library *)xadMasterBase);
		xadMasterBase = NULL;
	}
	_is_xad_setup = FALSE;
}

int xad_extract_archive(
	IN  char  *archive_path,
	IN  char  *destination_path,
	OUT char **list)
{
	struct xadArchiveInfo *ai                      = NULL;
	struct xadFileInfo    *fi                      = NULL;
	LONG                   e                       = 0;
	char                  *list_temp               = NULL;
	int                    number_of_chars         = 0;
	int                    maximum_number_of_chars = 0;
	UBYTE                 *destination_name        = NULL;
	struct TagItem         tag_item[4]             = { 0 };

	if(_is_xad_setup == FALSE)
	{
		log_print_warning("xad_extract_archive(), XAD wasn't initialised\n");
		goto _RETURN_ERROR;
	}

	maximum_number_of_chars = GLOBALS_MAX_LINE_LENGTH;
	*list    = malloc(maximum_number_of_chars * sizeof(char));
	if(*list == NULL)
	{
		log_print_error( GetString( MSG_XAD_EXTRACTARCHIVECOULDNOTALLOCATEMEMORY ) );
		goto _RETURN_ERROR;
	}
	*list[0] = '\0';

	if(!(ai = (struct xadArchiveInfo *)xadAllocObjectA(XADOBJ_ARCHIVEINFO, 0)))
	{
		log_print_error( GetString( MSG_XAD_EXTRACTARCHIVECOULDNOTALLOCATESTRUCTXADARCHIVEINFO ) );
		goto _RETURN_ERROR;
	}

	// Tygre 2015/12/25: Vargs
	// When compiling with C89 option of VBCC, I cannot
	// use xadGetInfo(...) and xadFileUnArc(...) so I
	// must their "A" versions with sets of tag items.
	// (See eab.abime.net/showthread.php?goto=newpost&t=80781)
	tag_item[0].ti_Tag  = XAD_INFILENAME;
	tag_item[0].ti_Data = (ULONG)archive_path;
	tag_item[1].ti_Tag  = TAG_END;
	e = xadGetInfoA(ai, tag_item);
	if(e)
	{
		log_print_error( GetString( MSG_XAD_EXTRACTARCHIVECOULDNOTCOMPLETETHECALLTOXADGETINFO ) );
		goto _RETURN_ERROR;
	}

	destination_name    = malloc(GLOBALS_MAX_LINE_LENGTH * sizeof(char));
	if(destination_name == NULL)
	{
		log_print_error( GetString( MSG_XAD_EXTRACTARCHIVECOULDALLOCATEMEMORY ) );
		goto _RETURN_ERROR;
	}

	if(_data_size_callback_fp)
	{
		_data_size_callback_fp("xad_extract_archive() (size)", ai->xai_FileInfo->xfi_Size);
	}

	// Everything is set, let's extract the content of the archive :-)
	// This loop ignores links and directories. It only extracts files in the archive root.
	fi = ai->xai_FileInfo;
	while(fi)
	{
		if(!(fi->xfi_Flags & XADFIF_LINK || fi->xfi_Flags & XADFIF_DIRECTORY))
		{
			CopyMem((void *)destination_path, destination_name, strlen(destination_path) + 1);
			AddPart(destination_name, FilePart(fi->xfi_FileName), GLOBALS_MAX_LINE_LENGTH);
			tag_item[0].ti_Tag  = XAD_OUTFILENAME;
			tag_item[0].ti_Data = (ULONG)destination_name;
			tag_item[1].ti_Tag  = XAD_ENTRYNUMBER;
			tag_item[1].ti_Data = fi->xfi_EntryNumber;
			tag_item[2].ti_Tag  = XAD_PROGRESSHOOK;
			tag_item[2].ti_Data = (ULONG)&_progress_hook;
			tag_item[3].ti_Tag  = TAG_END;
			e = xadFileUnArcA(ai, tag_item);
			if(e)
			{
				log_print_error( GetString( MSG_XAD_EXTRACTARCHIVECOULDNOTCOMPLETETHECALLTOXADFILEUNARC ) );
			}
			else
			{
				number_of_chars += strlen(destination_name) + 2;
				if(number_of_chars >= maximum_number_of_chars)
				{
					maximum_number_of_chars += GLOBALS_MAX_LINE_LENGTH;
					list_temp    = realloc(*list, maximum_number_of_chars);
					if(list_temp == NULL)
					{
						log_print_error( GetString( MSG_XAD_EXTRACTARCHIVECOULDREALLOCATEMEMORY ) );
						goto _RETURN_ERROR;
					}
					*list = list_temp;
				}
				strncat(*list, destination_name, maximum_number_of_chars - strlen(*list) - 1);
				strncat(*list, "\n",             maximum_number_of_chars - strlen(*list) - 1);
			}
		}
		else
		{
			// Skip link or directory
		}
		fi = fi->xfi_Next;
	}

	goto _RETURN_OK;
	_RETURN_OK:
		xadFreeInfo(ai);
		xadFreeObjectA(ai, 0);
		free(destination_name);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(ai != NULL)
		{
			xadFreeInfo(ai);
			xadFreeObjectA(ai, 0);
		}
		if(destination_name != NULL)
		{
			free(destination_name);
			destination_name = NULL;
		}
		if(*list != NULL)
		{
			free(*list);
			*list = NULL;
		}
		return RETURN_ERROR;
}

#ifndef S_SPLINT_S
ULONG progress_function(
	REG(a0, struct Hook *hook),
	REG(a1, struct xadProgressInfo *info))
{
	static int previous_value = 0;

	switch(info->xpi_Mode)
	{
		// case XADPMODE_ASK:
		// case XADPMODE_ERROR:
		// case XADPMODE_NEWENTRY:
		// case XADPMODE_GETINFOEND:
		case XADPMODE_END:
			if(_data_read_callback_fp)
			{
				_data_read_callback_fp("xad_extract_archive()", info->xpi_CurrentSize - previous_value);
				previous_value = 0;
			}
			break;
		case XADPMODE_PROGRESS:
			if(_data_read_callback_fp)
			{
				_data_read_callback_fp("xad_extract_archive()", info->xpi_CurrentSize - previous_value);
				previous_value = info->xpi_CurrentSize;
			}
			break;
	}
	
	if(_continue_long_operation_fp())
	{
		return XADPIF_OK;
	}
	else
	{
		return 0;
	}
}
#endif