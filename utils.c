/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include "utils.h"

#include "globals.h"
#include "locale.h"
#include "log.h"
#include "z_fortify.h"

#include <ctype.h>            // For tolower()
#include <dos/dos.h>          // For BPTR, RETURN_OK, RETURN_ERROR...
#include <dos/dosextens.h>    // For struct Process
// #include <exec/memory.h>   // Only needed with NDK v3.1: For MEMF_PUBLIC and MEMF_CLEAR
#include <exec/types.h>       // For ULONG
#include <proto/dos.h>        // For BADDR, DupLock...
#include <proto/exec.h>       // For AllocVec(), Permit()...
#include <proto/rexxsyslib.h>
#include <stdarg.h>           // For va_start() and va_end()
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>



/* Constants and declarations */

typedef struct path_node
{
	BPTR next;
	BPTR dir;
} path_node_t;

	   void    chrcat(INOUT char *, IN char);
	   void    intcat(INOUT char *, IN int);
	   int     integer_max(IN int, IN int);
	   int     integer_min(IN int, IN int);
	   int     integer_count_digits(IN int);
	   void    io_read_line(IN char(*)(void), OUT char **);
	   int     io_file_copy(IN char *, IN char *);
	   int     io_file_exist(IN char  *);
	   int     io_file_comment(IN char *, OUT char **);
	   int     io_file_date(IN char *, OUT time_t *);
	   BPTR    io_path_list_copy(IN BPTR);
	   void    io_path_list_free(IN BPTR);
	   BOOL    io_is_file_an_archive(IN char *);
	   char   *string_btoa(IN BOOL);
	   char   *string_itoa(IN int, IN int, OUT char *);
	   int     string_atoi(IN char *);
	   int     string_count_char(IN char *, IN char);
	   BOOL    string_end_with(IN char *, IN char *);
	   int     string_remove_first_char(OUT char **, IN char *, IN char);
	   int     string_remove_all_chars(OUT char **, IN char *, IN char);
	   BOOL    string_start_with(IN char *, IN char *);
	   char   *string_duplicate(IN char *);
	   char   *string_duplicate_n(IN char *, IN int);
	   char   *string_replace_all(IN char *, IN char *, IN char *);
	   void    string_encipher(IN int, IN char *, OUT char *);
	   void    string_decipher(IN int, IN char *, OUT char *);
	   int     string_snprintf(INOUT char *, IN int, IN char *, ...);
	   int     process_allocate_signal(OUT ULONG *, OUT ULONG *, IN char *);
	   void    process_send_signal_to_child(IN struct Process *, IN ULONG, IN ULONG);
	   void    process_send_signal_to_AMR(IN struct Process *, IN ULONG);
	   void    process_free_signal(IN ULONG);
static size_t  _strnlen(IN char *, IN size_t);
static int     _strnicmp(char const *, char const *, size_t len);



/* Definitions */



void  chrcat(
	  INOUT char *string,
	  IN    char  c)
{
	while(*string++)
	{
		// Nothing to do.
	}

	*string = '\0';
	string--;
	*string = c;
}

void  intcat(
	  INOUT char *string,
	  IN int i)
{
	int   count  = 0;
	char *buffer = NULL;

	count     = integer_count_digits(i);
	buffer    = malloc((count + 1) * sizeof(char));
	if(buffer == NULL)
	{
		return;
	}
	buffer[0] = '\0';
	string_snprintf(buffer, count + 1, "%d", i);
	strcat(string, buffer);
	free(buffer);
}

int   integer_max(
	  IN int a,
	  IN int b)
{
	return a < b ? b : a;
}

int   integer_min(
	  IN int a,
	  IN int b)
{
	return a < b ? a : b;
}

int   integer_count_digits(
	  IN int i)
{
	long long n     = 0;
	int       count = 0;

	n = i;
	do {
		n = n / 10;
		count++;
	}
	while(n != 0);

	return count;
}

void  io_read_line(
	  IN  char(*a_get_char_function)(void),
	  OUT char **line)
{
	int	  length = 0;
	char *string = NULL;
	char  c      = '\0';

	*line = calloc(sizeof(char), 1);
	if(*line == NULL)
	{
		log_print_error( GetString( MSG_UTILS_IOREADLINECOULDNOTALLOCATEMEMORY ) );
		return;
	}

	do
	{
		length    = length + 1;
		string    = realloc(*line, length * sizeof(char));
		if(string == NULL)
		{
			log_print_error( GetString( MSG_UTILS_IOREADLINECOULDNOTREALLOCATEMEMORY ) );
			return;
		}
		*line = string;
		c = a_get_char_function();
		(*line)[length - 1] = c;
	}
	while((*line)[length - 1] != '\r' && (*line)[length - 1] != '\n');
	(*line)[length - 1] = '\0';
}

int   io_file_copy(
	  IN char *source_file_path,
	  IN char *destination_name)
{
	FILE          *source_fp        = NULL;
	FILE          *target_fp        = NULL;
	int            length           = 0;
	char          *target_file_path = NULL;
	char          *file_name        = NULL;
	size_t         n                = 0;
	size_t         m                = 0;
	unsigned char  buffer[8192]     = { 0 };
	char          *comment          = NULL;

	source_fp = fopen(source_file_path, "rb"); /* Flawfinder: ignore */
	if(source_fp == NULL)
	{
		log_print_error( GetString( MSG_UTILS_IOCOPYFILECOULDNOTOPENSOURCEFILE ) );
		goto _RETURN_ERROR;
	}

	file_name = strrchr(source_file_path, GLOBALS_SEPARATOR_VOLUMES_CHAR);
	if(strrchr(source_file_path, GLOBALS_SEPARATOR_PATHS_CHAR) != NULL)
	{
		file_name = strrchr(source_file_path, GLOBALS_SEPARATOR_PATHS_CHAR);
	}
	if(file_name == NULL)
	{
		log_print_error( GetString( MSG_UTILS_IOCOPYFILERECEIVEDMALFORMEDSOURCEFILEPATH ) , source_file_path);
		goto _RETURN_ERROR;
	}
	file_name = file_name + sizeof(char);

	length              = strlen(destination_name) + strlen(file_name) + 1;
	target_file_path    = malloc(length * sizeof(char));
	if(target_file_path == NULL)
	{
		log_print_error( GetString( MSG_UTILS_IOCOPYFILECOULDNOTALLOCATETARGETFILEPATH ) );
		goto _RETURN_ERROR;
	}
	strcpy(target_file_path, destination_name);
	strcat(target_file_path, file_name);

	target_fp = fopen(target_file_path, "wb"); /* Flawfinder: ignore */
	if(target_fp == NULL)
	{
		log_print_error( GetString( MSG_UTILS_IOCOPYFILECOULDNOTOPENDESTINATIONFILE ) );
		goto _RETURN_ERROR;
	}

	// From http://stackoverflow.com/a/5263102
	do
	{
		n = fread(buffer, sizeof(char), sizeof buffer, source_fp);
		if(n > 0)
		{
			m = fwrite(buffer, sizeof(char), n, target_fp);
		}
		else
		{
			m = 0;
		}
	}
	while((n > 0) && (n == m));
	if(m > 0)
	{
		log_print_error( GetString( MSG_UTILS_IOCOPYFILECOULDNOTCOPYFILE ) );
		goto _RETURN_ERROR;
	}

	// Tygre 2016/06/06: Comments!
	// I should not forget to deal appropriately with comments
	if(io_file_comment(source_file_path, &comment) == RETURN_OK)
	{
		SetComment(target_file_path, comment);
		free(comment);
	}

	goto _RETURN_OK;
	_RETURN_OK:
		free(target_file_path);
		fclose(target_fp);
		fclose(source_fp);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(target_file_path != NULL)
		{
			free(target_file_path);
		}
		if(target_fp != NULL)
		{
			fclose(target_fp);
		}
		if(source_fp != NULL)
		{
			fclose(source_fp);
		}
		return RETURN_ERROR;
}

int   io_file_exist(
	  IN char *file_name)
{
	FILE *handle;
	if(handle = fopen(file_name, "r")) /* Flawfinder: ignore */
	{
		fclose(handle);
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

int io_file_comment(
	IN  char  *file_name,
	OUT char **comment)
{
	BPTR                  lock    = BPTR_ZERO;
	struct FileInfoBlock *fib     = NULL;
	BOOL                  result  = FALSE;
	int                   length  = 0;

	lock = Lock(file_name, ACCESS_READ);
	if(lock == BPTR_ZERO)
	{
		goto _RETURN_ERROR;
	}
	fib = AllocDosObject(DOS_FIB, NULL);
	if(!fib)
	{
		goto _RETURN_ERROR;
	}
	result = Examine(lock, fib);
	if(!result)
	{
		goto _RETURN_ERROR;
	}
	length      = strlen(fib->fib_Comment);
	*comment    = malloc((length + 1) * sizeof(char));
	if(*comment == NULL)
	{
	}
	strcpy(*comment, fib->fib_Comment);

	goto _RETURN_OK;
	_RETURN_OK:
		FreeDosObject(DOS_FIB, fib);
		UnLock(lock);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(fib != NULL)
		{
			FreeDosObject(DOS_FIB, fib);
		}
		if(lock != BPTR_ZERO)
		{
			UnLock(lock);
		}
		return RETURN_ERROR;
}

int io_file_date(
	IN  char   *file_name,
	OUT time_t *time)
{
	BPTR                  lock    = BPTR_ZERO;
	struct FileInfoBlock *fib     = NULL;
	BOOL                  result  = FALSE;
	struct tm             tm_time;

	lock = Lock(file_name, ACCESS_READ);
	if(lock == BPTR_ZERO)
	{
		goto _RETURN_ERROR;
	}
	fib = AllocDosObject(DOS_FIB, NULL);
	if(!fib)
	{
		goto _RETURN_ERROR;
	}
	result = Examine(lock, fib);
	if(!result)
	{
		goto _RETURN_ERROR;
	}
	
	// Amiga counts since 1978/01/01
	// but UNIX wants since 1970/01/01.
	*time = (fib->fib_Date.ds_Days * 86400) +
			(fib->fib_Date.ds_Minute * 60) +
			(fib->fib_Date.ds_Tick / TICKS_PER_SECOND) +
			(8 * 365 * 86400) +
			(2 * 86400); // Two leap years between 1970 and 1978

	tm_time = *gmtime(time);
	tm_time.tm_isdst = -1;
	*time = mktime(&tm_time);

	goto _RETURN_OK;
	_RETURN_OK:
		FreeDosObject(DOS_FIB, fib);
		UnLock(lock);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(fib != NULL)
		{
			FreeDosObject(DOS_FIB, fib);
		}
		if(lock != BPTR_ZERO)
		{
			UnLock(lock);
		}
		return RETURN_ERROR;
}

// See http://forum.hyperion-entertainment.biz/viewtopic.php?f=14&t=2155
// See https://github.com/vidarh/FrexxEd/blob/master/src/WBPath.c
BPTR  io_path_list_copy(
	  IN BPTR path_list)
{
	BPTR             path_list_copy = BPTR_ZERO;
	BPTR            *p              = &path_list_copy;
	BPTR             dir            = path_list;
	BPTR             dir2           = BPTR_ZERO;
	struct FileLock *lock           = NULL;
	path_node_t     *node           = NULL;

	Forbid();
	while(dir != BPTR_ZERO)
	{
		lock = BADDR(dir);
		dir  = lock->fl_Link;
		dir2 = DupLock(lock->fl_Key);
		if(dir2 == BPTR_ZERO)
		{
			break;
		}
		node = AllocVec(sizeof(path_node_t), MEMF_PUBLIC);
		if(node == NULL)
		{
			UnLock(dir2);
			break;
		}
		node->next = BPTR_ZERO;
		node->dir  = dir2;
		*p         = MKBADDR(node);
		p = &node->next;
	}
	Permit();

	return path_list_copy;
}

void  io_path_list_free(
	  IN BPTR path_list_copy)
{
	BPTR         path = path_list_copy;
	path_node_t *node = NULL;

	while(path != BPTR_ZERO)
	{
		node = BADDR(path);
		path = node->next;
		UnLock(node->dir);
		FreeVec(node);
	}
}

BOOL  io_is_file_an_archive(
	  IN char *file)
{
	BOOL result = FALSE;

	result = string_start_with(file, "lha.") ||
			 string_end_with  (file, ".lha") ||
			 string_start_with(file, "lzh.") ||
			 string_end_with  (file, ".lzh") ||
			 string_start_with(file, "lzx.") ||
			 string_end_with  (file, ".lzx") ||
			 string_start_with(file, "zip.") ||
			 string_end_with  (file, ".zip") ||
			 string_start_with(file, "gz." ) ||
			 string_end_with  (file, ".gz" );

	return result;
}

char *string_btoa(
	  IN BOOL value)
{
	char *char_value = NULL;

	char_value = malloc((1 + 1) * sizeof(char));
	if(char_value == NULL)
	{
		return NULL;
	}

	if(value)
	{
		strncpy(char_value, "1", 1 + 1);
	}
	else
	{
		strncpy(char_value, "0", 1 + 1);
	}

	return char_value;
}

/**
 * C++ version 0.4 char* style "itoa":
 * Written by Luk?Chmela
 * Released under GPLv3.
 */
char *string_itoa(
	  IN  int   value,
	  IN  int   base,
	  OUT char *result)
{
	char *ptr        = NULL;
	char *ptr1       = NULL;
	char  tmp_char   = '\0';
	int   tmp_value  = 0;
	int   tmp_value2 = 0;

	// check that the base if valid
	if(base <  2 || base > 36)
	{
		*result = '\0';
		return result;
	}
	ptr = result;
	ptr1 = result;
	tmp_value = value;
	do
	{
		tmp_value2 = tmp_value;
		tmp_value /= base;
		*ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz"[35 + (tmp_value2 - tmp_value * base)];
	}
	while(tmp_value);

	// Apply negative sign
	if(tmp_value2 <  0)
	{
		*ptr++ = '-';
	}
	*ptr-- = '\0';
	while(ptr1 <  ptr)
	{
		tmp_char = *ptr;
		*ptr--   = *ptr1;
		*ptr1++  = tmp_char;
	}
	return result;
}

int   string_atoi(
	  IN char *value)
{
	if(value == NULL)
	{
		return 0;
	}
	else
	{
		return atoi(value); /* Flawfinder: ignore */
	}
}

int   string_count_char(
	  IN char *haystack,
	  IN char  needle)
{
	const char *p     = haystack;
		  int   count = 0;

	do
	{
		if(*p == needle)
		{
			count++;
		}
	}
	while(*(p++));

	return count;
}

BOOL  string_end_with(
	  IN char *haystack,
	  IN char *needle)
{
	size_t length_haystack = strlen(haystack);
	size_t length_needle   = strlen(needle);

	return length_haystack < length_needle ? 0 :
		   _strnicmp(haystack + length_haystack - length_needle, needle, length_needle) == 0;
}

int   string_remove_first_char(
	  OUT char **bale,
	  IN  char  *haystack,
	  IN  char   needle)
{
	int   length = 0;
	char *pos    = NULL;

	if(haystack == NULL)
	{
		return RETURN_ERROR;
	}

	length = strlen(haystack) + 1;
	*bale  = malloc(length * sizeof(char));
	if(*bale == NULL)
	{
		return RETURN_ERROR;
	}
	strncpy(*bale, haystack, length);
	
	if((pos = strchr(*bale, needle)) != NULL)
	{
		*pos = '\0';
	}

	return RETURN_OK;
}

int   string_remove_all_chars(
	  OUT char **bale,
	  IN  char  *haystack,
	  IN  char   needle)
{
	int  size  = 0;
	int  count = 0;
	int     i  = 0;
	int     j  = 0;

	size  = strlen(haystack);
	count = string_count_char(haystack, needle);
	*bale = malloc((size - count + 1) * sizeof(char));
	if(*bale == NULL)
	{
		return RETURN_ERROR;
	}

	for(i = 0; i < size; i++)
	{
		if(haystack[i] != needle)
		{
			(*bale)[j] = haystack[i];
			j++;
		}
	}
	(*bale)[j] = '\0';

	return RETURN_OK;
}

BOOL  string_start_with(
	  IN char *haystack,
	  IN char *needle)
{
	size_t length_haystack = strlen(haystack);
	size_t length_needle   = strlen(needle);

	return length_haystack < length_needle ? 0 :
		   _strnicmp(haystack, needle, length_needle) == 0;
}

BOOL  string_equal(
	  IN char *s1,
	  IN char *s2)
{
	if(s1 == NULL || s2 == NULL)
	{
		return FALSE;
	}

	return strcmp(s1, s2) == 0;
}

char *string_duplicate(
	  IN char *s)
{
	int   length = 0;
	char *d      = NULL;

	length = strlen(s);
	d      = malloc((length + 1) * sizeof(char));
	if(d == NULL)
	{
		return NULL;
	}
	strcpy(d, s);
	
	return d;
}

// Replacement for the string.h strndup, fixes a bug
char *string_duplicate_n(
	  IN char *str,
	  IN int   max)
{
	size_t  len = _strnlen(str, max);
	char   *res = (char *)malloc((len + 1) * sizeof(char));
	if(res != NULL)
	{
		memcpy(res, str, len);
		res[len] = '\0';
	}
	return res;
}

/*
Description: Replaces in the string str all the occurrences of the source string old with the destination string new. The lengths of the strings old and new may differ. The string new may be of any length, but the string old must be of non-zero length - the penalty for providing an empty string for the old parameter is an infinite loop. In addition, none of the three parameters may be NULL.
Returns: The post-replacement string, or NULL if memory for the new string could not be allocated. Does not modify the original string. The memory for the returned post-replacement string may be deallocated with the standard library function free when it is no longer required.
Dependencies: For this function to compile, you will need to also #include the following files: <string.h>, <stdlib.h> and <stddef.h>.
Portability: Portable to C compilers implementing the ISO C Standard, any version i.e. from C89/C90 onwards.
Author: Laird Shaw.
Licence: Public domain.
Attribution: Optional. If you choose to indicate attribution when using this function, feel free to link to http://creativeandcritical.net/str-replace-c#repl_str.
(Former name: repl_str.)
*/

char *string_replace_all(const char *str, const char *old, const char *new)
{
	/* Adjust each of the below values to suit your needs. */

	/* Increment positions cache size initially by this number. */
	size_t cache_sz_inc = 16;
	/* Thereafter, each time capacity needs to be increased,
	 * multiply the increment by this factor. */
	const size_t cache_sz_inc_factor = 3;
	/* But never increment capacity by more than this number. */
	const size_t cache_sz_inc_max = 1048576;

	char *pret, *ret = NULL;
	const char *pstr2, *pstr = str;
	size_t i, count = 0;
	ptrdiff_t *pos_cache = NULL;
	ptrdiff_t *pos_cache2 = NULL;
	size_t cache_sz = 0;
	size_t cpylen, orglen, retlen, newlen, oldlen = strlen(old);

	/* Find all matches and cache their positions. */
	while ((pstr2 = strstr(pstr, old)) != NULL) {
		count++;

		/* Increase the cache size when necessary. */
		if (cache_sz < count) {
			cache_sz += cache_sz_inc;
			pos_cache2 = realloc(pos_cache, sizeof(*pos_cache) * cache_sz);
			if (pos_cache2 == NULL) {
				goto end_repl_str;
			}
			pos_cache = pos_cache2;
			cache_sz_inc *= cache_sz_inc_factor;
			if (cache_sz_inc > cache_sz_inc_max) {
				cache_sz_inc = cache_sz_inc_max;
			}
		}

		pos_cache[count-1] = pstr2 - str;
		pstr = pstr2 + oldlen;
	}

	orglen = pstr - str + strlen(pstr);

	/* Allocate memory for the post-replacement string. */
	if (count > 0) {
		newlen = strlen(new);
		retlen = orglen + (newlen - oldlen) * count;
	} else	retlen = orglen;
	ret = malloc(retlen + 1);
	if (ret == NULL) {
		goto end_repl_str;
	}

	if (count == 0) {
		/* If no matches, then just duplicate the string. */
		strncpy(ret, str, retlen + 1);
	} else {
		/* Otherwise, duplicate the string whilst performing
		 * the replacements using the position cache. */
		pret = ret;
		memcpy(pret, str, pos_cache[0]);
		pret += pos_cache[0];
		for (i = 0; i < count; i++) {
			memcpy(pret, new, newlen);
			pret += newlen;
			pstr = str + pos_cache[i] + oldlen;
			cpylen = (i == count-1 ? orglen : pos_cache[i+1]) - pos_cache[i] - oldlen;
			memcpy(pret, pstr, cpylen);
			pret += cpylen;
		}
		ret[retlen] = '\0';
	}

end_repl_str:
	/* Free the cache and return the post-replacement string,
	 * which will be NULL in the event of an error. */
	free(pos_cache);
	return ret;
}

/*
 * Railfence implementation by James Lyons,
 * see https://gist.github.com/jameslyons/8899843
 *
 * Changes by Tygre 2016/06/02
 */

/********************************************************************
void railfence_encipher(int key, char *plain_text, char *cipher_text)
- Uses railfence transposition cipher to encipher some text.
- takes a key, string of plain_text, result returned in cipher_text.
- cipher_text should be an array the same size as plain_text.
- The key is the number of rails to use
********************************************************************/
void  string_encipher(
	  IN  int    key,
	  IN  char  *plain_text,
	  OUT char  *cipher_text)
{
	int line, i, skip, length = strlen(plain_text), j=0,k=0;
	for(line = 0; line < key-1; line++){
		skip = 2*(key - line - 1);
		k=0;
		for(i = line; i < length;){
			cipher_text[j] = plain_text[i];
			if((line==0) || (k%2 == 0)) i+=skip;
			else i+=2*(key-1) - skip;
			j++;   k++;
		}
	}
	for(i=line; i<length; i+=2*(key-1)) cipher_text[j++] = plain_text[i];
	cipher_text[j] = '\0'; /* Null terminate */
}

/********************************************************************
void railfence_decipher(int key, char *cipher_text, char *plain_text)
- Uses railfence transposition cipher to decipher some text.
- takes a key, string of cipher_text, result returned in plain_text.
- plain_text should be an array the same size as plain_text.
- The key is the number of rails to use
********************************************************************/
void  string_decipher(
	  IN  int    key,
	  IN  char  *cipher_text,
	  OUT char  *plain_text)
{
	int i, length = strlen(cipher_text), skip, line, j, k=0;
	for(line=0; line<key-1; line++){
		skip=2*(key-line-1);
		j=0;
		for(i=line; i<length;){
			plain_text[i] = cipher_text[k++];
			if((line==0) || (j%2 == 0)) i+=skip;
			else i+=2*(key-1) - skip;
			j++;
		}
	}
	for(i=line; i<length; i+=2*(key-1)) plain_text[i] = cipher_text[k++];
	plain_text[length] = '\0'; /* Null terminate */
}

int string_snprintf(
	INOUT char *d,
	IN    int   n,
	IN    char *format,
	...)
{
	int     count = 0;
	va_list list;

	// Tygre 2017/10/22: snprintf
	// It seems that snprintf(...) behaves like _snprinf(...)
	//  http://stackoverflow.com/questions/7706936/is-snprintf-always-null-terminating/13067917#13067917)
	// but is also off by one: if len = count - 1, then no null character is appended!
	// So, I create my own that ensures the string is null-terminated, no matter what.

	va_start(list, format);
	count = vsnprintf(d, n, format, list); /* Flawfinder: ignore */
	va_end(list);

	d[n - 1] = '\0';

	return count;
}

/* Copyright (C) 2004 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any
   later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */

/*
#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#if !defined (HAVE_STRNLEN)

#include <sys/types.h>

#if defined (HAVE_UNISTD_H)
#  include <unistd.h>
#endif

#include <stdc.h>
*/

/* Find the length of S, but scan at most MAXLEN characters.  If no '\0'
   terminator is found within the first MAXLEN characters, return MAXLEN. */
static size_t _strnlen(
	IN char   *s,
	IN size_t  maxlen)
{
	IN char *e;
	size_t   n;

	for(e = s, n = 0; *e != '\0' && n < maxlen; e++, n++)
		;
	return n;
}

/*
** Copyright 2001, Travis Geiselbrecht. All rights reserved.
** Distributed under the terms of the NewOS License.
*/
/*
* Copyright (c) 2008 Travis Geiselbrecht
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files
* (the "Software"), to deal in the Software without restriction,
* including without limitation the rights to use, copy, modify, merge,
* publish, distribute, sublicense, and/or sell copies of the Software,
* and to permit persons to whom the Software is furnished to do so,
* subject to the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

static int _strnicmp(
	char const *s1,
	char const *s2,
	size_t len)
{
	unsigned char c1 = '\0';
	unsigned char c2 = '\0';

	if(len > 0)
	{
		do
		{
			c1 = *s1;
			c2 = *s2;
			s1++;
			s2++;
			if(c1 == '\0')
				break;
			if(c2 == '\0')
				break;
			if(c1 == c2)
				continue;
			c1 = tolower(c1);
			c2 = tolower(c2);
			if(c1 != c2)
				break;
		}
		while (--len);
	}
	return (int)c1 - (int)c2;
}

