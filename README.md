IF YOU LIKE IT, BUY ME A COFFEE AT https://www.buymeacoffee.com/amimodradio

MORE INFORMATION AT http://www.chingu.asia/wiki/index.php?title=AmiModRadio


AmiModRadio
===========

Did you know that, as of today (2015/06/02), there are more than 20,000 packages (http://aminet.net/tree) in the mods directory on Aminet? Yes! That's more than 20,000 modules to play and enjoy! So, why not have a program that could automatically download one module at-a-time, extract it, and play it? This is the purpose of AmiModRadio...
 
 
Essentials
==========

AmiModRadio is composed essentially of six parts:

- A small FTP client (http://en.wikipedia.org/wiki/File_Transfer_Protocol) to access Aminet and Modland and download archives containing modules;
- A small HTTP client (https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol) to access Haxor.fi and download modules;
- A small HTTPS client (http://aminet.net/search?query=amissl&ord=DESC&sort=date) to access ModArchive and Modules.pl;
- A XAD client (http://aminet.net/package/util/arc/xadmaster020) to unarchive the downloaded archives;
- A ARexx (http://en.wikipedia.org/wiki/ARexx) command-and-control system to gently ask players to play the module;
- A MUI (http://en.wikipedia.org/wiki/Magic_User_Interface) interface to control the various parts of AmiModRadio, e.g., to pause/play and skip modules.
