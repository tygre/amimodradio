/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/

/*
 * Per-task SocketBase and data using tc_UserData
 * Inspired, generalised from z_tasksocketbase.h
 */

#ifndef UTILS_SOCKETBASE_H
#define UTILS_SOCKETBASE_H

#include <proto/exec.h>

#define __NOLIBBASE__
// I just want the prototypes, without
// using the library base SocketBase.
#include <proto/socket.h>
#undef  __NOLIBBASE__

#define PROTO_AMISSL_H
// I just want the type SSL_CTX, nothing
// more (especially not the prototypes!)
#include <openssl/types.h>
#undef PROTO_AMISSL_H

#include "utils.h" // For IN and OUT



#define SocketBase ((tc_UserData_t *)FindTask(NULL)->tc_UserData)->socket_base

// Tygre 24/08/08: Task User Data
typedef struct {
	struct Library *socket_base;
	BOOL            httpx_http_inited;
	BOOL            httpx_https_inited;
	SSL_CTX        *ssl_context;
} tc_UserData_t;

int      utils_socketbase_init(
		 void);

void     utils_socketbase_close(
		 void);

BOOL     utils_socketbase_httpx_http_inited_get(
		 void);

void     utils_socketbase_httpx_http_inited_set(
		 IN BOOL value);

BOOL     utils_socketbase_httpx_https_inited_get(
		 void);

void     utils_socketbase_httpx_https_inited_set(
		 IN BOOL value);

SSL_CTX *utils_socketbase_ssl_context_get(
		 void);

void     utils_socketbase_ssl_context_set(
		 IN SSL_CTX *ssl_ctx);

#endif
