/*
 * https://en.wikibooks.org/wiki/Aros/Developer/NICDrivers
 * CC BY-SA 3.0
 *
 * Copyright (c) 2010-2020 DannyS712, PokestarFan, Jeff1138, and others
 */

#ifndef TASKSOCKBASE_H
#define TASKSOCKBASE_H

/*
 * Per-task socketbase using tc_UserData
 */

#include <proto/exec.h>

#define SocketBase FindTask(NULL)->tc_UserData
#define __NOLIBBASE__
#include <proto/socket.h>

#endif
