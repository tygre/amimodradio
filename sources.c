/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include "sources.h"

#include "ftp.h"
#include "globals.h"
#include "httpx.h"
#include "kvs.h"
#include "locale.h"
#include "log.h"
#include "prefs.h"
#include "utils.h"
#include "version.h"
#include "z_fortify.h"
#include "z_ftpparse.h"

#include <ctype.h>      // For isdigit(...)
#include <stdio.h>
#include <stdlib.h>
#include <dos/dos.h>    // For RETURN_OK, RETURN_ERROR
#include <proto/dos.h>  // For (Un)Lock(), Examine(), ExNext()
#include <proto/exec.h> // For OpenLibrary() and CloseLibrary()



/* Constants and declarations */

#define NUMBER_OF_SOURCES                8

#define AMINET                           "Aminet"
#define AMINET_HOST_NAME                 "ftp.aminet.net"
#define AMINET_ROOT_DIRECTORY            "/mods"

#define HAXOR_FI                         "Haxor.fi"
#define HAXOR_FI_HOST_NAME               "http://mod.haxor.fi"
#define HAXOR_FI_URL                     "http://mod.haxor.fi"
#define HAXOR_FI_DIRS_START_TAG          "<tr><td valign=\"top\"><img src=\"/icons/folder.gif\" alt=\"[DIR]\"></td><td><a href=\""
#define HAXOR_FI_DIRS_END_TAG            "/\">"
#define HAXOR_FI_FILES_START_TAG         "<tr><td valign=\"top\"><img src=\"/icons/unknown.gif\" alt=\"[   ]\"></td><td><a href=\""
#define HAXOR_FI_FILES_END_TAG           "\">"
#define HAXOR_FI_ROOT_DIRECTORY          "/mods"
#define HAXOR_FI_ROOT_DIRECTORY_FOR_LIST "mods"

#define MODARCHIVE                       "ModArchive"
#define MODARCHIVE_HOST_NAME             "https://www.modarchive.org/"
#define MODARCHIVE_MAXIMUM_NUMBER        26
#define MODARCHIVE_SK                    "4x8mohd7foeg-feaercm7-vbjrew-qdkbm5"
#define MODARCHIVE_LIST_LETTER_URL       "https://api.modarchive.org/xml-tools.php?request=view_by_list&query=%c&format=MOD&key="
#define MODARCHIVE_LIST_NUMBER_URL       "https://api.modarchive.org/xml-tools.php?request=view_by_list&query=%c&format=MOD&page=%s&key="
#define MODARCHIVE_GET_FILE_URL          "https://api.modarchive.org/xml-tools.php?request=search&type=filename&query=%s&key="
#define MODARCHIVE_TOTALPAGES_START_TAG	 "<totalpages>"
#define MODARCHIVE_TOTALPAGES_END_TAG	 "</totalpages>"
#define MODARCHIVE_NAME_START_TAG        "<filename>"
#define MODARCHIVE_NAME_END_TAG          "</filename>"
#define MODARCHIVE_SIZE_START_TAG        "<bytes>"
#define MODARCHIVE_SIZE_END_TAG          "</bytes>"
#define MODARCHIVE_ROOT_DIRECTORY        "/"

#define MODLAND_ALL                      "Modland (mods)"
#define MODLAND_ALL_HOST_NAME            "ftp.modland.com"
#define MODLAND_ALL_ROOT_DIRECTORY       "/pub/modules"
#define MODLAND_FAV                      "Modland (favs)"
#define MODLAND_FAV_HOST_NAME            "ftp.modland.com"
#define MODLAND_FAV_ROOT_DIRECTORY       "/pub/favourites"

#define MODULESPL                        "Modules.pl"
#define MODULESPL_HOST_NAME              "https://www.modules.pl/"
#define MODULESPL_DOWNLOAD_URL           "https://www.modules.pl/dl.php?mid="
#define MODULESPL_MAXIMUM_NUMBER         10000
#define MODULESPL_MAXIMUM_NUMBER_DIGITS  5
#define MODULESPL_MYSTERY_MODULE_NAME    "Mystery Module.zip"
#define MODULESPL_ROOT_DIRECTORY         "/"

#define AMPDASCENE						 "AMP"
#define AMPDASCENE_HOST_NAME             "https://amp.dascene.net/"
#define AMPDASCENE_DOWNLOAD_URL  		 "https://amp.dascene.net/downmod.php?index="
#define AMPDASCENE_MAXIMUM_NUMBER        100000
#define AMPDASCENE_MAXIMUM_NUMBER_DIGITS 6
#define AMPDASCENE_MYSTERY_MODULE_NAME   "Mystery Module.gz"
#define AMPDASCENE_ROOT_DIRECTORY        "/"

#define LOCAL_DRAWER                     "Local"
#define LOCAL_DRAWER_ROOT_DIRECTORY      "/"
#define LOCAL_DRAWER_DOS_LIST            LDF_VOLUMES|LDF_DEVICES|LDF_ASSIGNS|LDF_READ

#define CACHE_FILES_MAXIMUM_LENGTH       20
#define CACHE_FILES_SEPARATOR            '+'
#define CACHE_FILES_DURATION             (1 * 31 * 24 * 60 * 60)

	   void  sources_get_names(OUT char ***);
	   int	 sources_get_number(void);
	   char *sources_get_root_directory(IN char *);
	   int   sources_setup(IN BOOL (*)(), IN void (*)(IN char *, IN int), IN void (*)(IN char *, IN int));
	   void  sources_cleanup(void);
	   int   sources_init_connection(IN char *);
	   int   sources_close_connection(void);
	   int   sources_get_list(IN char *, OUT char **, OUT int *);
	   int   sources_pick_file(IN char *, OUT char **, OUT file_type_t *, OUT int *);
	   int   sources_get_file(IN char *, IN char *, IN int, OUT char **, OUT int *);
	   int   mkdir(const char *, int); // From sys/stat.h but including this header causes compilation errors
static int   _sources_pick_file_MODULESPL(OUT char **, OUT file_type_t *, OUT int *);
static int   _sources_pick_file_AMPDASCENE(OUT char **, OUT file_type_t *, OUT int *);
static int   _sources_pick_file_OTHERS(IN char *, OUT char **, OUT file_type_t *, OUT int *);
static int   _httpx_get(IN char *, IN char *, IN char *, IN int, OUT httpx_response **);
static int   _httpx_length(IN char *, IN char *, IN char *, OUT int *);
static int   _clean_ftp_list(INOUT char *, OUT char **);
static int   _create_list_AMINET(IN char *, OUT char **);
static int   _create_list_HAXOR_FI(IN char *, OUT char **);
static int   _create_list_MODARCHIVE(IN char *, OUT char **);
static int   _create_list_MODLAND(IN char *, OUT char **);
static int   _create_list_MODULESPL(OUT char **);
static int   _create_list_AMPDASCENE(OUT char **);
static int   _create_list_LOCAL_DRAWER(IN char *, OUT char **);
static char *_retrieve_list_from_disk(IN char *, IN char *);
static int   _cache_list_on_disk(IN char *, IN char *, IN char *);
static char *_retrieve_list_from_memory(IN char *, IN char *);
static int   _cache_list_in_memory(IN char *, IN char *, IN char *);
static int   _invalidate_cached_list(IN char *,	IN char *);
static BOOL  _list_is_recently_on_disk(IN char *);
static int   _encode_directory_name(IN char *, OUT char **);
static int   _get_line_delimiters(IN  char *, OUT char **, OUT char **);



/* Definitions */

static int              _socket_handle                   = -1;
static /*@only@*/ char *_current_source_name             = NULL;
static KVSstore        *_cached_directories_list         = NULL;
static BOOL             (*_continue_long_operation_fp)() = NULL; // Tygre 20/08/02: To stop long-running HTTP operations
static void             (*_data_size_callback_fp)(
							IN char *origin,
							IN int number_of_bytes)      = NULL; // Tygre 22/12/19: To update a progress bar when necessary
static void             (*_data_read_callback_fp)(
							IN char *origin,
							IN int number_of_bytes)      = NULL; // Tygre 22/12/19: To update a progress bar when necessary



void sources_get_names(
	 OUT char ***sources_names)
{
	int length        = 0;
	int source_number = 0;
	
	// From http://stackoverflow.com/a/7631177
	*sources_names    = malloc(NUMBER_OF_SOURCES * sizeof (char *));
	if(*sources_names == NULL)
	{
		log_print_error( GetString( MSG_SOURCES_SOURCESGETNAMESCANNOTALLOCATEARRAYOFSOURCESNAMES ) );
		return;
	}

	length                             = strlen(AMINET) + 1;
	(*sources_names)[source_number]    = malloc(length * sizeof(char));
	if((*sources_names)[source_number] == NULL)
	{
		free(*sources_names);
		*sources_names = NULL;
		log_print_error( GetString( MSG_SOURCES_SOURCESGETNAMESCANNOTALLOCATESOURCENAME ) );
		return;
	}
	strncpy((*sources_names)[source_number], AMINET, length);
	source_number++;

	length                             = strlen(HAXOR_FI) + 1;
	(*sources_names)[source_number]    = malloc(length * sizeof(char));
	if((*sources_names)[source_number] == NULL)
	{
		free(*sources_names);
		*sources_names = NULL;
		log_print_error( GetString( MSG_SOURCES_SOURCESGETNAMESCANNOTALLOCATESOURCENAME ) );
		return;
	}
	strncpy((*sources_names)[source_number], HAXOR_FI, length);
	source_number++;
	
	length                             = strlen(MODARCHIVE) + 1;
	(*sources_names)[source_number]    = malloc(length * sizeof(char));
	if((*sources_names)[source_number] == NULL)
	{
		free(*sources_names);
		*sources_names = NULL;
		log_print_error( GetString( MSG_SOURCES_SOURCESGETNAMESCANNOTALLOCATESOURCENAME ) );
		return;
	}
	strncpy((*sources_names)[source_number], MODARCHIVE, length);
	source_number++;

	length                             = strlen(MODLAND_ALL) + 1;
	(*sources_names)[source_number]    = malloc(length * sizeof(char));
	if((*sources_names)[source_number] == NULL)
	{
		free(*sources_names);
		*sources_names = NULL;
		log_print_error( GetString( MSG_SOURCES_SOURCESGETNAMESCANNOTALLOCATESOURCENAME ) );
		return;
	}
	strncpy((*sources_names)[source_number], MODLAND_ALL, length);
	source_number++;

	length                             = strlen(MODLAND_FAV) + 1;
	(*sources_names)[source_number]    = malloc(length * sizeof(char));
	if((*sources_names)[source_number] == NULL)
	{
		free(*sources_names);
		*sources_names = NULL;
		log_print_error( GetString( MSG_SOURCES_SOURCESGETNAMESCANNOTALLOCATESOURCENAME ) );
		return;
	}
	strncpy((*sources_names)[source_number], MODLAND_FAV, length);
	source_number++;

	length                             = strlen(MODULESPL) + 1;
	(*sources_names)[source_number]    = malloc(length * sizeof(char));
	if((*sources_names)[source_number] == NULL)
	{
		free(*sources_names);
		*sources_names = NULL;
		log_print_error( GetString( MSG_SOURCES_SOURCESGETNAMESCANNOTALLOCATESOURCENAME ) );
		return;
	}
	strncpy((*sources_names)[source_number], MODULESPL, length);
	source_number++;

	length                             = strlen(AMPDASCENE) + 1;
	(*sources_names)[source_number]    = malloc(length * sizeof(char));
	if((*sources_names)[source_number] == NULL)
	{
		free(*sources_names);
		*sources_names = NULL;
		log_print_error( GetString( MSG_SOURCES_SOURCESGETNAMESCANNOTALLOCATESOURCENAME ) );
		return;
	}
	strncpy((*sources_names)[source_number], AMPDASCENE, length);
	source_number++;

	length                             = strlen(LOCAL_DRAWER) + 1;
	(*sources_names)[source_number]    = malloc(length * sizeof(char));
	if((*sources_names)[source_number] == NULL)
	{
		free(*sources_names);
		*sources_names = NULL;
		log_print_error( GetString( MSG_SOURCES_SOURCESGETNAMESCANNOTALLOCATESOURCENAME ) );
		return;
	}
	strncpy((*sources_names)[source_number], LOCAL_DRAWER, length);
	source_number++;
}

int sources_get_number(
	void)
{
	return NUMBER_OF_SOURCES;
}

char *sources_get_root_directory(
	 IN char *source_name)
{
	if(string_equal(source_name, AMINET))
	{
		return AMINET_ROOT_DIRECTORY;
	}
	else if(string_equal(source_name, HAXOR_FI))
	{
		return HAXOR_FI_ROOT_DIRECTORY;
	}
	else if(string_equal(source_name, MODARCHIVE))
	{
		return MODARCHIVE_ROOT_DIRECTORY;
	}
	else if(string_equal(source_name, MODLAND_ALL))
	{
		return MODLAND_ALL_ROOT_DIRECTORY;
	}
	else if(string_equal(source_name, MODLAND_FAV))
	{
		return MODLAND_FAV_ROOT_DIRECTORY;
	}
	else if(string_equal(source_name, MODULESPL))
	{
		return MODULESPL_ROOT_DIRECTORY;
	}
	else if(string_equal(source_name, AMPDASCENE))
	{
		return AMPDASCENE_ROOT_DIRECTORY;
	}
	else if(string_equal(source_name, LOCAL_DRAWER))
	{
		return LOCAL_DRAWER_ROOT_DIRECTORY;
	}
	else
	{
		log_print_error( GetString( MSG_SOURCES_SOURCESGETROOTDIRECTORYUNKNOWNSOURCENAME ) );
		return NULL;
	}
}

int sources_setup(
	IN BOOL  (*continue_long_operation_fp)(),
	IN void  (*data_size_callback_fp)(IN char *origin, IN int number_of_bytes),
	IN void  (*data_read_callback_fp)(IN char *origin, IN int number_of_bytes))
{
	log_print_debug("sources_setup()\n");

	_continue_long_operation_fp = continue_long_operation_fp;
	_data_size_callback_fp      = data_size_callback_fp;
	_data_read_callback_fp      = data_read_callback_fp;

	if(ftp_setup(
		_continue_long_operation_fp,
		_data_size_callback_fp,
		_data_read_callback_fp) == RETURN_ERROR)
	{
		return RETURN_ERROR;
	}

	if(httpx_setup(
		_continue_long_operation_fp,
		_data_size_callback_fp,
		_data_read_callback_fp) == RETURN_ERROR)
	{
		return RETURN_ERROR;
	}

	_cached_directories_list = kvs_create(kvs_compare_strings);
	if(_cached_directories_list == NULL)
	{
		return RETURN_ERROR;
	}

	return RETURN_OK;
}

void sources_cleanup(
	 void)
{
	log_print_debug("sources_cleanup()\n");

	kvs_destroy(_cached_directories_list);

	httpx_cleanup();

	ftp_cleanup();
}

/**
 * Returns:
 * - RETURN_OK    if it could connect to the source.
 * - RETURN_WARN  if it could not connect to a not-yet-implementd source.
 * - RETURN_ERROR if it could not connect to the source or use the memory.
 */
int sources_init_connection(
	IN char *source_name)
{
	int length = 0;
	int result = RETURN_ERROR;

	log_print_debug("sources_init_connection()\n");

	// Current source name
	if(_current_source_name != NULL)
	{
		free(_current_source_name);
	}
	length                  = strlen(source_name) + 1;
	_current_source_name    = malloc(length * sizeof(char));
	if(_current_source_name == NULL)
	{
		log_print_error( GetString( MSG_SOURCES_SOURCESINITCONNECTIONCANNOTALLOCATECURRENTSOURCENAM ) );
		return RETURN_ERROR;
	}
	strncpy(_current_source_name, source_name, length);

	// Initialise connection
	if(string_equal(_current_source_name, AMINET))
	{
		result = ftp_init_connection(AMINET_HOST_NAME, &_socket_handle);
	}
	else if(string_equal(_current_source_name, HAXOR_FI))
	{
		result = httpx_init_connection(HTTP);
	}
	else if(string_equal(_current_source_name, MODARCHIVE))
	{
		result = httpx_init_connection(HTTPS);
	}
	else if(string_equal(_current_source_name, MODLAND_FAV))
	{
		result = ftp_init_connection(MODLAND_FAV_HOST_NAME, &_socket_handle);
	}
	else if(string_equal(_current_source_name, MODLAND_ALL))
	{
		result = ftp_init_connection(MODLAND_ALL_HOST_NAME, &_socket_handle);
	}
	else if(string_equal(_current_source_name, MODULESPL))
	{
		result = httpx_init_connection(HTTPS);
	}
	else if(string_equal(_current_source_name, AMPDASCENE))
	{
		// Tygre 24/07/24: Inconsistency
		// The server uses HTTPs but redirects
		// first to a URL that uses the HTTP
		// before redirecting to the same URL
		// in HTTPs, so I need both...
		result = httpx_init_connection(BOTH);
	}
	else if(string_equal(_current_source_name, LOCAL_DRAWER))
	{
		// Nothing to do
		result = RETURN_OK;
	}
	else
	{
		log_print_error( GetString( MSG_SOURCES_SOURCESINITCONNECTIONUNKNOWNSOURCENAME ) );
		result = RETURN_ERROR;
	}

	return result;
}

int sources_close_connection(
	void)
{
	log_print_debug("sources_close_connection()\n");

	if(_current_source_name == NULL)
	{
		log_print_error( GetString( MSG_SOURCES_SOURCESCLOSECONNECTIONNOSOURCEINITIALISEDYET ) );
		goto _RETURN_ERROR;
	}

	if(string_equal(_current_source_name, AMINET))
	{
		if(_socket_handle == -1)
		{
			log_print_error( GetString( MSG_SOURCES_SOURCESCLOSECONNECTIONNOFTPCONNECTIONINITIALISEDYET ) );
			goto _RETURN_ERROR;
		}
		ftp_close_connection(_socket_handle);
		_socket_handle = -1;
	}
	else if(string_equal(_current_source_name, MODARCHIVE))
	{
		httpx_close_connection(HTTPS);
	}
	else if(string_equal(_current_source_name, HAXOR_FI))
	{
		httpx_close_connection(HTTP);
	}
	else if(string_equal(_current_source_name, MODLAND_ALL) ||
			string_equal(_current_source_name, MODLAND_FAV))
	{
		if(_socket_handle == -1)
		{
			log_print_error( GetString( MSG_SOURCES_SOURCESCLOSECONNECTIONNOFTPCONNECTIONINITIALISEDYET ) );
			goto _RETURN_ERROR;
		}
		ftp_close_connection(_socket_handle);
		_socket_handle = -1;
	}
	else if(string_equal(_current_source_name, MODULESPL))
	{
		httpx_close_connection(HTTPS);
	}
	else if(string_equal(_current_source_name, AMPDASCENE))
	{
		httpx_close_connection(BOTH);
	}
	else if(string_equal(_current_source_name, LOCAL_DRAWER))
	{
		// Nothing to do
	}
	else
	{
		log_print_error( GetString( MSG_SOURCES_SOURCESCLOSECONNECTIONUNKNOWNSOURCENAME ) , _current_source_name);
		goto _RETURN_ERROR;
	}

	goto _RETURN_OK;
	_RETURN_OK:
		free(_current_source_name);
		_current_source_name = NULL;
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		free(_current_source_name);
		_current_source_name = NULL;
		return RETURN_ERROR;
}

int sources_get_list(
	IN  char  *directory_name,
	OUT char **list,
	OUT int   *list_size)
{
	char *list_temp = NULL;
	char *line      = NULL;
	char *pos1      = NULL;
	char *pos2      = NULL;

	if(_current_source_name == NULL)
	{
		log_print_error( GetString( MSG_SOURCES_SOURCESGETLISTNOSOURCEINITIALISEDYET ) );
		goto _RETURN_ERROR;
	}

	// Cache handling
	if(prefs_should_cache_list_during_run() == TRUE &&
	   (list_temp = _retrieve_list_from_memory(_current_source_name, directory_name)) != NULL)
	{
		*list = string_duplicate(list_temp);
		goto _RETURN_OK;
	}
	
	if(prefs_should_cache_list_between_runs() == TRUE &&
	   (list_temp = _retrieve_list_from_disk(_current_source_name, directory_name)) != NULL)
	{
		*list = list_temp;

		// In-memory cache handling
		if(prefs_should_cache_list_during_run() == TRUE)
		{
			_cache_list_in_memory(_current_source_name, directory_name, *list);
		}

		goto _RETURN_OK;
	}

	// Normal behaviour
	if(string_equal(_current_source_name, AMINET))
	{
		if(_create_list_AMINET(directory_name, list) != RETURN_OK)
		{
			goto _RETURN_ERROR;
		}
	}
	else if(string_equal(_current_source_name, HAXOR_FI))
	{
		if(_create_list_HAXOR_FI(directory_name, list) != RETURN_OK)
		{
			goto _RETURN_ERROR;
		}
	}
	else if(string_equal(_current_source_name, MODARCHIVE))
	{
		if(_create_list_MODARCHIVE(directory_name, list) != RETURN_OK)
		{
			goto _RETURN_ERROR;
		}
	}
	else if(string_equal(_current_source_name, MODLAND_ALL) ||
			string_equal(_current_source_name, MODLAND_FAV))
	{
		if(_create_list_MODLAND(directory_name, list) != RETURN_OK)
		{
			goto _RETURN_ERROR;
		}
	}
	else if(string_equal(_current_source_name, MODULESPL))
	{
		if(_create_list_MODULESPL(list) != RETURN_OK)
		{
			goto _RETURN_ERROR;
		}
	}
	else if(string_equal(_current_source_name, AMPDASCENE))
	{
		if(_create_list_AMPDASCENE(list) != RETURN_OK)
		{
			goto _RETURN_ERROR;
		}
	}
	else if(string_equal(_current_source_name, LOCAL_DRAWER))
	{
		if(_create_list_LOCAL_DRAWER(directory_name, list) != RETURN_OK)
		{
			goto _RETURN_ERROR;
		}
	}
	else
	{
		log_print_error( GetString( MSG_SOURCES_SOURCESGETLISTUNKNOWNSOURCENAME ) );
		goto _RETURN_ERROR;
	}

	// On-disk cache handling
	if(prefs_should_cache_list_between_runs())
	{
		_cache_list_on_disk(_current_source_name, directory_name, *list);
	}

	// In-memory cache handling
	if(prefs_should_cache_list_during_run())
	{
		_cache_list_in_memory(_current_source_name, directory_name, *list);
	}

	goto _RETURN_OK;
	_RETURN_OK:
		*list_size = string_count_char(*list, '\n');
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(list_temp != NULL)
		{
			free(list_temp);
		}
		if(*list != NULL)
		{
			free(*list);
			*list = NULL;
		}
		*list_size = 0;
		return RETURN_ERROR;
}

int sources_pick_file(
	IN  char         *list,
	OUT char        **file,
	OUT file_type_t  *file_type,
	OUT int          *file_size)
{
	int result = RETURN_ERROR;

	*file      = NULL;
	*file_type = OTHER;
	*file_size = 0;

	if(list == NULL)
	{
		result = RETURN_ERROR;
	}
	else if(string_equal(_current_source_name, MODULESPL))
	{
		result = _sources_pick_file_MODULESPL(file, file_type, file_size);
	}
	else if(string_equal(_current_source_name, AMPDASCENE))
	{
		result = _sources_pick_file_AMPDASCENE(file, file_type, file_size);
	}
	else
	{
		result = _sources_pick_file_OTHERS(list, file, file_type, file_size);
	}

	return result;
}

int sources_get_file(
	IN  char  *directory,
	IN  char  *file,
	IN  int    file_size,
	OUT char **file_bytes,
	OUT int   *file_size_real)
{
	int              bytes_limit = 0;
	int			     path_length = 0;
	char            *path        = NULL;
	BPTR             path_ptr    = BPTR_ZERO;
	char             result      = RETURN_ERROR;
	int              url_length  = 0;
	char            *url_prefix  = NULL;
	char            *url_secret  = NULL;
	httpx_response  *hresp       = NULL;
	char            *end_tag     = NULL;

	// Tygre 22/12/19: Size matters!
	// Before downloading anything,
	// I use its size with the callback.
	// It can be zero and different to
	// the actual real file size.

	// Progress callback
	if(_data_size_callback_fp)
	{
		_data_size_callback_fp("sources_get_file() (size)", file_size);
	}

	if(prefs_should_skip_if_above_maximum_file_size() == TRUE)
	{
		bytes_limit = atoi(prefs_get_list_of_maximum_file_sizes()[prefs_get_maximum_file_size_index()]) * BYTES_IN_ONE_KILO_BYTE; /* Flawfinder: ignore */
	}
	else
	{
		bytes_limit = 0;
	}

	if(_current_source_name == NULL)
	{
		log_print_error( GetString( MSG_SOURCES_SOURCESGETFILENOSOURCEINITIALISEDYET ) );
		return RETURN_ERROR;
	}

	if(string_equal(_current_source_name, AMINET))
	{
		path_length = strlen(directory) + 1 + strlen(file) + 1;
		path        = malloc(path_length * sizeof(char));
		if(path     == NULL)
		{
			log_print_error( GetString( MSG_SOURCES_SOURCESGETFILECOULDNOTALLOCATEMEMORYFORPATH ) );
			return RETURN_ERROR;
		}
		strncpy(path, directory, path_length);
		if(!string_end_with(directory, GLOBALS_SEPARATOR_PATHS) &&
		   !string_end_with(directory, GLOBALS_SEPARATOR_VOLUMES))
		{
			strncat(path, GLOBALS_SEPARATOR_PATHS, path_length - strlen(path) - 1);
		}
		strncat(path, file, path_length - strlen(path) - 1);
		result = ftp_get_file_from_data(AMINET_HOST_NAME, _socket_handle, path, file_bytes, file_size_real);
		free(path);

		return result;
	}
	else if(string_equal(_current_source_name, HAXOR_FI))
	{
		path_length = strlen(directory) + 1 + strlen(file) + 1;
		path        = malloc(path_length * sizeof(char));
		if(path     == NULL)
		{
			log_print_error( GetString( MSG_SOURCES_SOURCESGETFILECOULDNOTALLOCATEMEMORYFORPATH ) );
			return RETURN_ERROR;
		}
		strncpy(path, directory, path_length);
		if(!string_end_with(directory, GLOBALS_SEPARATOR_PATHS) &&
		   !string_end_with(directory, GLOBALS_SEPARATOR_VOLUMES))
		{
			strncat(path, GLOBALS_SEPARATOR_PATHS, path_length - strlen(path) - 1);
		}
		strncat(path, file, path_length - strlen(path) - 1);

		url_length    = strlen(HAXOR_FI_URL) + strlen(path);
		url_prefix    = malloc((url_length + 1) * sizeof(char));
		if(url_prefix == NULL)
		{
			free(path);
			log_print_error("sources_get_file(), could not allocate URL\n");
			return RETURN_ERROR;
		}
		url_prefix[0] = '\0';
		strcat(url_prefix, HAXOR_FI_URL);
		strcat(url_prefix, path);

		result = _httpx_get(url_prefix, "", version_get_user_agent(), bytes_limit, &hresp);

		free(url_prefix);
		url_prefix = NULL;
		free(path);
		path = NULL;
		if(result != RETURN_OK)
		{
			return RETURN_ERROR;
		}

		*file_bytes    = malloc(hresp->body_size_in_bytes);
		if(*file_bytes == NULL)
		{
			log_print_error("sources_get_file(), could not allocate memory for the answer from %s\n", HAXOR_FI);
			return RETURN_ERROR;
		}
		memcpy(*file_bytes, hresp->body, hresp->body_size_in_bytes);
		*file_size_real = hresp->body_size_in_bytes;

		httpx_free_httpx_response(hresp);
		hresp = NULL;
		return RETURN_OK;
	}
	else if(string_equal(_current_source_name, MODARCHIVE))
	{
		url_length    = strlen(MODARCHIVE_GET_FILE_URL) + strlen(file);
		url_prefix    = malloc((url_length + 1) * sizeof(char));
		if(url_prefix == NULL)
		{
			log_print_error("sources_get_file(), could not allocate URL for %s\n", MODARCHIVE);
			return RETURN_ERROR;
		}
		string_snprintf(url_prefix, url_length, MODARCHIVE_GET_FILE_URL, file);
		
		url_length    = strlen(MODARCHIVE_SK);
		url_secret    = malloc((url_length + 1) * sizeof(char));
		if(url_secret == NULL)
		{
			free(url_prefix);
			log_print_error("sources_get_file(), could not allocate secret key for %s\n", MODARCHIVE);
			return RETURN_ERROR;
		}
		string_decipher(7, MODARCHIVE_SK, url_secret);

		result = _httpx_get(url_prefix, url_secret, NULL, bytes_limit, &hresp);

		free(url_prefix);
		url_prefix = NULL;
		free(url_secret);
		url_secret = NULL;
		if(result != RETURN_OK)
		{
			return RETURN_ERROR;
		}

		*file_bytes    = malloc(hresp->body_size_in_bytes);
		if(*file_bytes == NULL)
		{
			log_print_error("sources_get_file(), could not allocate memory for the first answer from %s\n", MODARCHIVE);
			return RETURN_ERROR;
		}
		memcpy(*file_bytes, hresp->body, hresp->body_size_in_bytes);
		*file_size_real = hresp->body_size_in_bytes;

		httpx_free_httpx_response(hresp);
		hresp = NULL;

		// The URL in the XML document returned by The Module Archive is of form:
		//  <![CDATA[https://api.modarchive.org/downloads.php?moduleid=119560#tb_menu.mod]]>
		url_prefix             = strstr(*file_bytes, "<url>");
		if(url_prefix          == NULL)
		{
			log_print_error("sources_get_file(), could not find <url> delimiter in the first answer\n");
			return RETURN_ERROR;
		}
		url_prefix             = url_prefix	+ (5 + 9) * sizeof(char);

		end_tag                = strstr(*file_bytes, "</url>");
		if(end_tag             == NULL)
		{
			log_print_error("sources_get_file(), could not find </url> delimiter in the first answer\n");
			return RETURN_ERROR;
		}
		end_tag                = end_tag - 3 * sizeof(char);
		
		url_length             = (end_tag - url_prefix) / sizeof(char);
		url_prefix[url_length] = '\0';

		result = _httpx_get(url_prefix, "", NULL, bytes_limit, &hresp);

		free(*file_bytes);
		*file_bytes = NULL;
		if(result != RETURN_OK)
		{
			return RETURN_ERROR;
		}

		*file_bytes    = malloc(hresp->body_size_in_bytes);
		if(*file_bytes == NULL)
		{
			log_print_error("sources_get_file(), could not allocate memory for the second answer from %s\n", MODARCHIVE);
			return RETURN_ERROR;
		}
		memcpy(*file_bytes, hresp->body, hresp->body_size_in_bytes);
		*file_size_real = hresp->body_size_in_bytes;

		httpx_free_httpx_response(hresp);
		hresp = NULL;
		return RETURN_OK;
	}
	else if(string_equal(_current_source_name, MODLAND_ALL) ||
			string_equal(_current_source_name, MODLAND_FAV))
	{
		path_length = strlen(directory) + 1 + strlen(file);
		path        = malloc((path_length + 1) * sizeof(char));
		if(path     == NULL)
		{
			log_print_error( GetString( MSG_SOURCES_SOURCESGETFILECOULDNOTALLOCATEMEMORYFORPATH ) );
			return RETURN_ERROR;
		}
		path[0] = '\0';
		strcat(path, directory);
		if(!string_end_with(directory, GLOBALS_SEPARATOR_PATHS) &&
		   !string_end_with(directory, GLOBALS_SEPARATOR_VOLUMES))
		{
			strcat(path, GLOBALS_SEPARATOR_PATHS);
		}
		strcat(path, file);

		result = ftp_get_file_from_data(MODLAND_ALL_HOST_NAME, _socket_handle, path, file_bytes, file_size_real);
		
		free(path);

		return result;
	}
	else if(string_equal(_current_source_name, MODULESPL))
	{
		// Tygre 2016/05/30: Form
		// The file is of the form NNN.zip
		// but the API require only NNN, so I remove
		// the last four characters building the url.
		url_length    = strlen(MODULESPL_DOWNLOAD_URL) + strlen(file) - 4;
		url_prefix    = malloc((url_length + 1) * sizeof(char));
		if(url_prefix == NULL)
		{
			log_print_error("sources_get_file(), could not allocate URL\n");
			return RETURN_ERROR;
		}
		string_snprintf(url_prefix, url_length + 1, "%s%s", MODULESPL_DOWNLOAD_URL, file);

		result = _httpx_get(url_prefix, "", NULL, bytes_limit, &hresp);
		
		free(url_prefix);
		url_prefix = NULL;
		if(result != RETURN_OK)
		{
			return RETURN_ERROR;
		}

		*file_bytes    = malloc(hresp->body_size_in_bytes);
		if(*file_bytes == NULL)
		{
			log_print_error("sources_get_file(), could not allocate memory for the answer from %s\n", MODULESPL);
			return RETURN_ERROR;
		}
		memcpy(*file_bytes, hresp->body, hresp->body_size_in_bytes);
		*file_size_real = hresp->body_size_in_bytes;

		httpx_free_httpx_response(hresp);
		hresp = NULL;
		return RETURN_OK;
	}
	else if(string_equal(_current_source_name, AMPDASCENE))
	{
		// Tygre 2016/05/30: Form
		// The file is of the form NNN.gz
		// but the API require only NNN, so I remove
		// the last three characters building the url.
		url_length    = strlen(AMPDASCENE_DOWNLOAD_URL) + strlen(file) - 3;
		url_prefix    = malloc((url_length + 1) * sizeof(char));
		if(url_prefix == NULL)
		{
			log_print_error("sources_get_file(), could not allocate URL\n");
			return RETURN_ERROR;
		}
		string_snprintf(url_prefix, url_length + 1, "%s%s", AMPDASCENE_DOWNLOAD_URL, file);

		result = _httpx_get(url_prefix, "", NULL, bytes_limit, &hresp);

		free(url_prefix);
		url_prefix = NULL;
		if(result != RETURN_OK)
		{
			return RETURN_ERROR;
		}

		*file_bytes    = malloc(hresp->body_size_in_bytes);
		if(*file_bytes == NULL)
		{
			log_print_error("sources_get_file(), could not allocate memory for the answer from %s\n", AMPDASCENE);
			return RETURN_ERROR;
		}
		memcpy(*file_bytes, hresp->body, hresp->body_size_in_bytes);
		*file_size_real = hresp->body_size_in_bytes;

		httpx_free_httpx_response(hresp);
		hresp = NULL;
		return RETURN_OK;
	}
	else if(string_equal(_current_source_name, LOCAL_DRAWER))
	{
		path_length = strlen(directory) + 1 + strlen(file);
		path        = malloc((path_length + 1) * sizeof(char));
		if(path     == NULL)
		{
			return RETURN_ERROR;
		}
		path[0] = '\0';
		strcat(path, (directory + 1 * sizeof(char)));
		chrcat(path, GLOBALS_SEPARATOR_PATHS_CHAR);
		strcat(path, file);

		path_ptr = Open(path, MODE_OLDFILE);
		free(path);
		if(path_ptr == BPTR_ZERO)
		{
			log_print_error("sources_get_file(), could not open the file\n");
			return RETURN_ERROR;
		}

		*file_bytes    = malloc(file_size);
		if(*file_bytes == NULL)
		{
			log_print_error("sources_get_file(), could not allocate the file\n");
			Close(path_ptr);
			return RETURN_ERROR;
		}
			
		*file_size_real    = Read(path_ptr, *file_bytes, file_size);
		if(*file_size_real != file_size)
		{
			log_print_error("sources_get_file(), could not read the file\n");
			Close(path_ptr);
			return RETURN_ERROR;
		}

		Close(path_ptr);
		return RETURN_OK;
	}
	else
	{
		log_print_error( GetString( MSG_SOURCES_SOURCESGETFILEUNKNOWNSOURCENAME ) );
		return RETURN_ERROR;
	}
}

static int _sources_pick_file_MODULESPL(
	OUT char        **file,
	OUT file_type_t  *file_type,
	OUT int          *file_size)
{
	int  length     = 0;
	char *file_name = NULL;

	// File name
	length   = MODULESPL_MAXIMUM_NUMBER_DIGITS + 4;
	*file    = malloc((length + 1) * sizeof(char));
	if(*file == NULL)
	{
		goto _RETURN_ERROR;
	}
	string_snprintf(*file, length + 1, "%d.zip", rand()	% MODULESPL_MAXIMUM_NUMBER);

	// File type
	*file_type = FILE_NAME;

	// File size
	length       = strlen(*file) - 4;
	file_name    = malloc((length + 1) * sizeof(char));
	if(file_name == NULL)
	{
		goto _RETURN_ERROR;
	}
	file_name[0] = '\0';
	strncat(file_name, *file, length);

	if(_httpx_length(MODULESPL_DOWNLOAD_URL, file_name, NULL, file_size) == RETURN_ERROR)
	{
		goto _RETURN_ERROR;
	}

	goto _RETURN_OK;
	_RETURN_OK:
		free(file_name);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(file_name != NULL)
		{
			free(file_name);
		}
		return RETURN_ERROR;
}

static int _sources_pick_file_AMPDASCENE(
	OUT char        **file,
	OUT file_type_t  *file_type,
	OUT int          *file_size)
{
	int  length     = 0;
	char *file_name = NULL;

	// File name
	length   = AMPDASCENE_MAXIMUM_NUMBER_DIGITS + 3;
	*file    = malloc((length + 1) * sizeof(char));
	if(*file == NULL)
	{
		goto _RETURN_ERROR;
	}
	string_snprintf(*file, length + 1, "%d.gz", rand() % AMPDASCENE_MAXIMUM_NUMBER);

	// File type
	*file_type = FILE_NAME;

	// File size
	length       = strlen(*file) - 3;
	file_name    = malloc((length + 1) * sizeof(char));
	if(file_name == NULL)
	{
		goto _RETURN_ERROR;
	}
	file_name[0] = '\0';
	strncat(file_name, *file, length);

	if(_httpx_length(AMPDASCENE_DOWNLOAD_URL, file_name, NULL, file_size) == RETURN_ERROR)
	{
		goto _RETURN_ERROR;
	}

	goto _RETURN_OK;
	_RETURN_OK:
		free(file_name);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(file_name != NULL)
		{
			free(file_name);
		}
		return RETURN_ERROR;
}

static int _sources_pick_file_OTHERS(
	IN  char         *list,
	OUT char        **file,
	OUT file_type_t  *file_type,
	OUT int          *file_size)
{
	char *list_temp       = NULL;
	int   number_of_lines = 0;
	int   desired_line    = 0;
	char *line            = NULL;
	int   length          = 0;
	char *pos1            = NULL;
	char *pos2            = NULL;
	int   i               = 0;

	number_of_lines = string_count_char(list, '\n');
	if(number_of_lines < 1)
	{
		log_print_error( GetString( MSG_SOURCES_SOURCESPICKFILEZEROORNEGATIVENUMBEROFLINES ) );
		goto _RETURN_ERROR;
	}

	desired_line = (rand() % number_of_lines);

	// Parse the lines
	list_temp    = string_duplicate(list);
	if(list_temp == NULL)
	{
		log_print_error( GetString( MSG_SOURCES_SOURCESPICKFILECOULDNOTALLOCATEFILE ) );
		goto _RETURN_ERROR;
	}
	line = strtok(list_temp, "\n");
	while(line != NULL && i != desired_line)
	{
		i++;
		line = strtok(NULL, "\n");
	}

	// Positions of delimiters
	if(_get_line_delimiters(line, &pos1, &pos2) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_SOURCES_SOURCESPICKFILEUNRECOGNISEDFORMAT ) );
		goto _RETURN_ERROR;
	}

	// File name
	length   = strlen(pos1) - strlen(pos2) - 1;
	*file    = malloc((length + 1) * sizeof(char));
	if(*file == NULL)
	{
		log_print_error( GetString( MSG_SOURCES_SOURCESPICKFILECOULDNOTALLOCATEFILE ) );
		goto _RETURN_ERROR;
	}
	*file[0] = '\0';
	strncat(*file, pos1, length);

	// File type
	if(line[0] == 'd')
	{
		*file_type = DIRECTORY_NAME;
	}
	else if(line[0] == 'f')
	{
		*file_type = FILE_NAME;
	}
	else
	{
		*file_type = OTHER;
	}

	// File size
	*file_size = atoi(pos2); /* Flawfinder: ignore */

	goto _RETURN_OK;
	_RETURN_OK:
		free(list_temp);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(list_temp != NULL)
		{
			free(list_temp);
		}
		return RETURN_ERROR;
}

static int _httpx_get(
	IN  char            *url_prefix,
	IN  char            *file_name,
	IN  char            *custom_headers,
	IN  int              bytes_limit,
	OUT httpx_response **hresp)
{
	int  length          = 0;
	char *url            = NULL;
	int   size           = 0;
	int   result         = RETURN_ERROR;

	length = strlen(url_prefix) + strlen(file_name);
	url    = malloc((length + 1) * sizeof(char));
	if(url == NULL)
	{
		log_print_error( GetString( MSG_SOURCES_SOURCESGETNAMESCANNOTALLOCATESOURCENAME ) );
		return RETURN_ERROR;
	}
	url[0] = '\0';
	strcat(url, url_prefix);
	strcat(url, file_name);
	
	result = httpx_get(url, custom_headers, bytes_limit, hresp);
	
	free(url);
	return result;
}

static int _httpx_length(
	IN  char *url_prefix,
	IN  char *file_name,
	IN  char *custom_headers,
	OUT int  *size)
{
	int             length = 0;
	char           *url    = NULL;
	httpx_response *hresp  = NULL;
	int             result = RETURN_ERROR;
	char           *pos    = NULL;

	length = strlen(url_prefix) + strlen(file_name);
	url    = malloc((length + 1) * sizeof(char));
	if(url == NULL)
	{
		log_print_error( GetString( MSG_SOURCES_SOURCESGETNAMESCANNOTALLOCATESOURCENAME ) );
		goto _RETURN_ERROR;
	}
	url[0] = '\0';
	strcat(url, url_prefix);
	strcat(url, file_name);
	
	result = httpx_head(url, custom_headers, &hresp);
	if(result == RETURN_OK)
	{
		pos = strstr(hresp->response_headers, "Content-Length: ");
		if(pos != NULL)
		{
			pos = pos + 16 * sizeof(char);
			*size = atoi(pos); /* Flawfinder: ignore */
		}
		else
		{
			*size = 0;
		}
	}
	else
	{
		*size = 0;
	}
	
	goto _RETURN_OK;
	_RETURN_OK:
		httpx_free_httpx_response(hresp);
		free(url);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		httpx_free_httpx_response(hresp);
		if(url != NULL)
		{
			free(url);
		}
		return RETURN_ERROR;
}

static int _clean_ftp_list(
	INOUT char  *ftp_list,
	OUT   char **clean_list)
{
	char            *ftp_list_temp                                      = NULL;
	int              ftp_list_temp_size                                 = 0;
	char            *token                                              = NULL;
	struct ftpparse  ftpparsed_line                                     = { 0 };
	char            *file_type                                          = NULL;
	int              file_size                                          = 0;
	char             file_size_char[GLOBALS_MAX_NUMBER_DIGITS_FOR_SIZE] = { 0 };

	// Progress callback
	if(_data_size_callback_fp)
	{
		_data_size_callback_fp("_clean_ftp_list() (size)", string_count_char(ftp_list, '\n'));
	}

	*clean_list	   = malloc((strlen(ftp_list) + 1) * sizeof(char));
	if(*clean_list == NULL)
	{
		log_print_error( GetString( MSG_SOURCES_CLEANFTPLISTCOULDNOTALLOCATENEWLIST ) );
		goto _RETURN_ERROR;
	}
	ftp_list_temp = *clean_list;

	token = strtok(ftp_list, "\n");
	while(token != NULL && (_continue_long_operation_fp == NULL || _continue_long_operation_fp()))
	{
		// Parse the line
		if(ftpparse(&ftpparsed_line, token, strlen(token)))
		{
			// Tygre 2015/12/30: Spaces in front
			// I consider a space to account for
			// discrepencies among FTP servers.
			if(token[0] == 'd' ||
			   token[1] == 'd')
			{
				file_type = "d ";
				file_size = 0;
			}
			else
			{
				file_type = "f ";
				file_size = ftpparsed_line.size;
			}

			memcpy(ftp_list_temp + ftp_list_temp_size * sizeof(char), file_type,           2 * sizeof(char));
			ftp_list_temp_size +=                                                          2;

			memcpy(ftp_list_temp + ftp_list_temp_size * sizeof(char), ftpparsed_line.name, ftpparsed_line.namelen * sizeof(char));
			ftp_list_temp_size +=                                                          ftpparsed_line.namelen;

			// Tygre 2020/05/20: Size matters!
			// I now add size to be used to exclude files
			// that are too big wrt. the user's preference.
			file_size = string_snprintf(file_size_char, GLOBALS_MAX_NUMBER_DIGITS_FOR_SIZE, "%c%d", GLOBALS_SEPARATOR_SIZES_CHAR, file_size);
			memcpy(ftp_list_temp + ftp_list_temp_size * sizeof(char), file_size_char,      file_size * sizeof(char));
			ftp_list_temp_size +=                                                          file_size;

			memcpy(ftp_list_temp + ftp_list_temp_size * sizeof(char), "\n",                1 * sizeof(char));
			ftp_list_temp_size +=                                                          1;
		}

		token = strtok(NULL, "\n");

		// Progress callback
		if(_data_read_callback_fp)
		{
			_data_read_callback_fp("_clean_ftp_list()", 1);
		}
	}

	// Terminate the temporary list with the NULL char
	ftp_list_temp[ftp_list_temp_size] = '\0';

	goto _RETURN_OK;
	_RETURN_OK:
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(*clean_list != NULL)
		{
			free(*clean_list);
			*clean_list = NULL;
		}
		return RETURN_ERROR;
}

static int _create_list_AMINET(
	IN  char  *directory_name,
	OUT char **list)
{
	char *list_temp = NULL;
	int   result    = RETURN_ERROR;

	// Progress callback
	if(_data_size_callback_fp)
	{
		// I cannot predict the size
		_data_size_callback_fp("_create_list_AMINET() (size)", GLOBALS_MAX_LINE_LENGTH);
	}

	if(ftp_get_list_from_data(AMINET_HOST_NAME, _socket_handle, directory_name, &list_temp) == RETURN_OK)
	{
		result = _clean_ftp_list(list_temp, list);
		free(list_temp);
	}

	return result;
}

static int _create_list_HAXOR_FI(
	IN  char  *directory_name,
	OUT char **list)
{
	int             length                                     = 0;
	char           *url_prefix                                 = NULL;
	char           *list_temp                                  = NULL;
	int             list_temp_size                             = 0;
	httpx_response *hresp                                      = NULL;
	char           *start_tag                                  = NULL;
	char           *end_tag                                    = NULL;
	char           *start_tag_size                             = NULL;
	char           *end_tag_size                               = NULL;
	char           *file_name                                  = NULL;
	int             file_size                                  = 0;
	char		    file_size_char[GLOBALS_MAX_NUMBER_DIGITS_FOR_SIZE] = { 0 };
	int             total_pages                                = 0;

	if(string_equal(directory_name, GLOBALS_SEPARATOR_PATHS))
	{
		list_temp_size = 13 + 2 + strlen(HAXOR_FI_ROOT_DIRECTORY_FOR_LIST) + 3;
		*list    = malloc((list_temp_size + 1) * sizeof(char));
		if(*list == NULL)
		{
			log_print_error( GetString( MSG_SOURCES_SOURCESGETLISTCOULDNOTALLOCATELIST ) );
			return RETURN_ERROR;
		}
		*list[0] = '\0';

		// Special directories
		strcat(*list, "d .");
		chrcat(*list, GLOBALS_SEPARATOR_SIZES_CHAR);
		strcat(*list, "0\n");
		strcat(*list, "d ..");
		chrcat(*list, GLOBALS_SEPARATOR_SIZES_CHAR);
		strcat(*list, "0\n");

		// Root directory
		strcat(*list, "d ");
		strcat(*list, HAXOR_FI_ROOT_DIRECTORY_FOR_LIST);
		chrcat(*list, GLOBALS_SEPARATOR_SIZES_CHAR);
		strcat(*list, "0\n");

		return RETURN_OK;
	}
	else
	{
		length     = strlen(HAXOR_FI_URL) + strlen(directory_name) + 1;
		url_prefix = malloc((length + 1) * sizeof(char));
		if(url_prefix == NULL)
		{
			log_print_error("_create_list_HAXOR_FI(), could not allocate URL\n");
			goto _RETURN_ERROR;
		}
		url_prefix[0] = '\0';
		strcat(url_prefix, HAXOR_FI_URL);
		strcat(url_prefix, directory_name);
		if(!string_end_with(directory_name, GLOBALS_SEPARATOR_PATHS))
		{
			strcat(url_prefix, GLOBALS_SEPARATOR_PATHS);
		}

		if(_httpx_get(url_prefix, "", version_get_user_agent(), 0, &hresp) == RETURN_ERROR)
		{
			goto _RETURN_ERROR;
		}

		list_temp    = malloc(hresp->body_size_in_bytes);
		if(list_temp == NULL)
		{
			log_print_error("_create_list_HAXOR_FI(), could not allocate temporary list\n");
			goto _RETURN_ERROR;
		}
		memcpy(list_temp, hresp->body, hresp->body_size_in_bytes);
		list_temp_size = hresp->body_size_in_bytes;
		httpx_free_httpx_response(hresp);

		// I allocate too much memory here because I cannot
		// know the sum of the sizes of all the filenames...
		*list    = malloc((2 * list_temp_size + 1) * sizeof(char));
		if(*list == NULL)
		{
			log_print_error( GetString( MSG_SOURCES_SOURCESGETLISTCOULDNOTALLOCATELIST ) );
			goto _RETURN_ERROR;
		}
		*list[0] = '\0';

		// Special directories
		strcat(*list, "d .");
		chrcat(*list, GLOBALS_SEPARATOR_SIZES_CHAR);
		strcat(*list, "0\n");
		strcat(*list, "d ..");
		chrcat(*list, GLOBALS_SEPARATOR_SIZES_CHAR);
		strcat(*list, "0\n");

		// Progress callback
		if(_data_size_callback_fp)
		{
			_data_size_callback_fp("_create_list_HAXOR_FI() (size)", list_temp_size);
		}

		// Normal directories
		start_tag = strstr(list_temp, HAXOR_FI_DIRS_START_TAG);
		while(start_tag != NULL)
		{
			start_tag  = start_tag + strlen(HAXOR_FI_DIRS_START_TAG) * sizeof(char);
			end_tag    = strstr(start_tag, HAXOR_FI_DIRS_END_TAG);
			if(end_tag == NULL)
			{
				log_print_error("_create_list_HAXOR_FI(), could not find directory delimiter\n");
				goto _RETURN_ERROR;
			}
			list_temp_size = (end_tag - start_tag) / sizeof(char);

			strcat (*list, "d ");
			strncat(*list, start_tag, list_temp_size);
			chrcat (*list, GLOBALS_SEPARATOR_SIZES_CHAR);
			strcat (*list, "0\n");

			start_tag = strstr(end_tag, HAXOR_FI_DIRS_START_TAG);

			// Progress callback
			if(_data_read_callback_fp)
			{
				_data_read_callback_fp("_create_list_HAXOR_FI()", 5 + list_temp_size);
			}
		}

		// Normal files
		start_tag = strstr(list_temp, HAXOR_FI_FILES_START_TAG);
		while(start_tag != NULL)
		{
			start_tag  = start_tag + strlen(HAXOR_FI_FILES_START_TAG) * sizeof(char);
			end_tag    = strstr(start_tag, HAXOR_FI_FILES_END_TAG);
			if(end_tag == NULL)
			{
				log_print_error("_create_list_HAXOR_FI(), could not find file delimiter\n");
				goto _RETURN_ERROR;
			}
			list_temp_size = (end_tag - start_tag) / sizeof(char);

			file_name = start_tag;
			file_name[list_temp_size] = '\0';
			_httpx_length(url_prefix, file_name, version_get_user_agent(), &file_size);

			strcat (*list, "f ");
			strncat(*list, start_tag, list_temp_size);
			chrcat (*list, GLOBALS_SEPARATOR_SIZES_CHAR);
			intcat (*list, file_size);
			chrcat (*list, '\n');

			start_tag = strstr(end_tag, HAXOR_FI_FILES_START_TAG);
		}

		goto _RETURN_OK;
		_RETURN_OK:
			free(url_prefix);
			free(list_temp);
			return RETURN_OK;

		goto _RETURN_ERROR;
		_RETURN_ERROR:
			if(url_prefix != NULL)
			{
				free(url_prefix);
			}
			if(list_temp != NULL)
			{
				free(list_temp);
				list_temp = NULL;
			}
			return RETURN_ERROR;
	}
}

static int _create_list_MODARCHIVE(
	IN  char  *directory_name,
	OUT char **list)
{
	char           *list_temp      = NULL;
	int             list_temp_size = 0;
	int             i              = 0;
	int             url_length     = 0;
	char           *url_prefix     = NULL;
	char           *url_secret     = NULL;
	int             result         = 0;
	httpx_response *hresp          = NULL;
	char           *name_start_tag = NULL;
	char           *name_end_tag   = NULL;
	char           *size_start_tag = NULL;
	char           *size_end_tag   = NULL;
	int             total_pages    = 0;

	// Tygre 2015/12/27: List of modules
	// I hard-code for the moment the list of
	// modules as the 26 upper-case letters.
	// Then, I ask the source for the number
	// of pages per letter. Then, I create a
	// fake list of these page numbers and
	// choose modules in one of these pages.

	if(string_equal(directory_name, sources_get_root_directory(_current_source_name)))
	{
		// In the root directory

		list_temp_size = MODARCHIVE_MAXIMUM_NUMBER * 6;
		*list    = malloc((list_temp_size + 1) * sizeof(char));
		if(*list == NULL)
		{
			log_print_error( GetString( MSG_SOURCES_SOURCESGETLISTCOULDNOTALLOCATELIST ) );
			goto _RETURN_ERROR;
		}
		*list[0] = '\0';

		// Special directories
		// Nothing to do here

		// Progress callback
		if(_data_size_callback_fp)
		{
			_data_size_callback_fp("_create_list_MODARCHIVE() (size)", list_temp_size);
		}

		// Normal directories
		for(i = 0; i < MODARCHIVE_MAXIMUM_NUMBER; i++)
		{
			strcat(*list, "d ");
			chrcat(*list, (char)(65 + i));
			chrcat(*list, GLOBALS_SEPARATOR_SIZES_CHAR);
			strcat(*list, "0\n");

			// Progress callback
			if(_data_read_callback_fp)
			{
				_data_read_callback_fp("_create_list_MODARCHIVE()", 6);
			}
		}
	}
	else if(strlen(directory_name) == 2)
	{
		// In a directory like "/P"

		url_length    = strlen(MODARCHIVE_LIST_LETTER_URL);
		url_prefix    = malloc((url_length + 1) * sizeof(char));
		if(url_prefix == NULL)
		{
			log_print_error("_create_list_MODARCHIVE(), could not allocate URL\n");
			goto _RETURN_ERROR;
		}
		string_snprintf(url_prefix, url_length + 1, MODARCHIVE_LIST_LETTER_URL, directory_name[1]); // Constant includes specifiers
		
		url_length    = strlen(MODARCHIVE_SK);
		url_secret    = malloc((url_length + 1) * sizeof(char));
		if(url_secret == NULL)
		{
			log_print_error("_create_list_MODARCHIVE(), could not allocate secret key\n");
			goto _RETURN_ERROR;
		}
		string_decipher(7, MODARCHIVE_SK, url_secret);

		result = _httpx_get(url_prefix, url_secret, NULL, 0, &hresp);

		free(url_secret);
		url_secret = NULL;

		if(result == RETURN_ERROR)
		{
			log_print_error("_create_list_MODARCHIVE(), could not get directory content\n");
			goto _RETURN_ERROR;
		}

		list_temp    = malloc(hresp->body_size_in_bytes);
		if(list_temp == NULL)
		{
			log_print_error( GetString( MSG_SOURCES_SOURCESGETLISTCOULDNOTALLOCATELIST ) );
			goto _RETURN_ERROR;
		}
		memcpy(list_temp, hresp->body, hresp->body_size_in_bytes);
		list_temp_size = hresp->body_size_in_bytes;
		httpx_free_httpx_response(hresp);

		name_start_tag    = strstr(list_temp, MODARCHIVE_TOTALPAGES_START_TAG);
		name_end_tag      = strstr(list_temp, MODARCHIVE_TOTALPAGES_END_TAG);
		if(name_start_tag == NULL || name_end_tag == NULL)
		{
			log_print_error("_create_list_MODARCHIVE(), could not get delimiters\n");
			goto _RETURN_ERROR;
		}
		name_start_tag    = name_start_tag + strlen(MODARCHIVE_TOTALPAGES_START_TAG);
		i                 = (name_end_tag - name_start_tag) / sizeof(char);
		name_start_tag[i] = '\0';
		total_pages	      = atoi(name_start_tag); /* Flawfinder: ignore */
		list_temp_size    = strlen(name_start_tag);
		free(list_temp);
		
		list_temp    = malloc((list_temp_size + 1) * sizeof(char));
		if(list_temp == NULL)
		{
			log_print_error( GetString( MSG_SOURCES_SOURCESGETLISTCOULDNOTALLOCATELIST ) );
			goto _RETURN_ERROR;
		}

		// On top:
		//	"d ./0\n"
		//	"d ../0\n"
		list_temp_size = 13 + total_pages * (2 + list_temp_size + 3);
		*list    = malloc((list_temp_size + 1) * sizeof(char));
		if(*list == NULL)
		{
			log_print_error( GetString( MSG_SOURCES_SOURCESGETLISTCOULDNOTALLOCATELIST ) );
			goto _RETURN_ERROR;
		}
		*list[0] = '\0';
		
		// Special directories
		strcat(*list, "d .");
		chrcat(*list, GLOBALS_SEPARATOR_SIZES_CHAR);
		strcat(*list, "0\n");
		strcat(*list, "d ..");
		chrcat(*list, GLOBALS_SEPARATOR_SIZES_CHAR);
		strcat(*list, "0\n");

		// Progress callback
		if(_data_size_callback_fp)
		{
			_data_size_callback_fp("_create_list_MODARCHIVE() (size)", list_temp_size);
		}

		// Normal directories
		for(i = 0; i < total_pages; i++)
		{
			strcat(*list, "d ");
			intcat(*list, i);
			chrcat(*list, GLOBALS_SEPARATOR_SIZES_CHAR);
			strcat(*list, "0\n");

			// Progress callback
			if(_data_read_callback_fp)
			{
				_data_read_callback_fp("_create_list_MODARCHIVE()", 5 + integer_count_digits(i));
			}
		}
	}
	else if(strlen(directory_name) > 3)
	{
		// In a directory like "/P/36", with 36 the page number
		// The URL assumes that there cannot be more than 999 pages

		url_secret    = (char *)directory_name + 4 * sizeof(char);
		url_length    = strlen(MODARCHIVE_LIST_NUMBER_URL) + 1 + strlen(url_secret);
		url_prefix    = malloc((url_length + 1) * sizeof(char));
		if(url_prefix == NULL)
		{
			log_print_error("_create_list_MODARCHIVE(), could not allocate URL\n");
			goto _RETURN_ERROR;
		}
		string_snprintf(url_prefix, url_length + 1, MODARCHIVE_LIST_NUMBER_URL, directory_name[1], url_secret); // Constant includes specifiers

		url_length    = strlen(MODARCHIVE_SK);
		url_secret    = malloc((url_length + 1) * sizeof(char));
		if(url_secret == NULL)
		{
			log_print_error("_create_list_MODARCHIVE(), could not allocate secret key\n");
			goto _RETURN_ERROR;
		}
		string_decipher(7, MODARCHIVE_SK, url_secret);

		result = _httpx_get(url_prefix, url_secret, NULL, 0, &hresp);

		free(url_secret);
		url_secret = NULL;
		
		if(result == RETURN_ERROR)
		{
			goto _RETURN_ERROR;
		}

		list_temp    = malloc(hresp->body_size_in_bytes);
		if(list_temp == NULL)
		{
			log_print_error( GetString( MSG_SOURCES_SOURCESGETLISTCOULDNOTALLOCATELIST ) );
			goto _RETURN_ERROR;
		}
		memcpy(list_temp, hresp->body, hresp->body_size_in_bytes);
		list_temp_size = hresp->body_size_in_bytes;
		httpx_free_httpx_response(hresp);

		// I allocate too much memory here but cannot foresee
		// the exact sum of the sizes of all the filenames...
		// TODO: Compute the proper, exact size before allocation

		// On top:
		//	"d ./0\n"
		//	"d ../0\n"
		list_temp_size = 13 + list_temp_size;
		*list    = malloc((list_temp_size + 1) * sizeof(char));
		if(*list == NULL)
		{
			log_print_error( GetString( MSG_SOURCES_SOURCESGETLISTCOULDNOTALLOCATELIST ) );
			goto _RETURN_ERROR;
		}
		*list[0] = '\0';
		
		// Special directories
		strcat(*list, "d .");
		chrcat(*list, GLOBALS_SEPARATOR_SIZES_CHAR);
		strcat(*list, "0\n");
		strcat(*list, "d ..");
		chrcat(*list, GLOBALS_SEPARATOR_SIZES_CHAR);
		strcat(*list, "0\n");

		// Progress callback
		if(_data_size_callback_fp)
		{
			_data_size_callback_fp("_create_list_MODARCHIVE() (size)", list_temp_size);
		}

		// Normal files
		name_start_tag    = strstr(list_temp, MODARCHIVE_NAME_START_TAG);
		if(name_start_tag == NULL)
		{
			log_print_error("_create_list_MODARCHIVE(), could not get directory content\n");
			goto _RETURN_ERROR;
		}
		while(name_start_tag != NULL)
		{
			name_start_tag  = name_start_tag + strlen(MODARCHIVE_NAME_START_TAG) * sizeof(char);
			name_end_tag    = strstr(name_start_tag, MODARCHIVE_NAME_END_TAG);
			if(name_end_tag == NULL)
			{
				log_print_error("_create_list_MODARCHIVE(), could not get delimiter\n");
				goto _RETURN_ERROR;
			}
			size_start_tag    = strstr(name_end_tag, MODARCHIVE_SIZE_START_TAG);
			if(size_start_tag == NULL)
			{
				log_print_error("_create_list_MODARCHIVE(), could not get delimiter\n");
				goto _RETURN_ERROR;
			}
			size_start_tag  = size_start_tag + strlen(MODARCHIVE_SIZE_START_TAG) * sizeof(char);
			size_end_tag    = strstr(size_start_tag, MODARCHIVE_SIZE_END_TAG);
			if(size_end_tag == NULL)
			{
				log_print_error("_create_list_MODARCHIVE(), could not get delimiter\n");
				goto _RETURN_ERROR;
			}

			strcat (*list, "f ");
			strncat(*list, name_start_tag, (name_end_tag - name_start_tag) / sizeof(char));
			chrcat (*list, GLOBALS_SEPARATOR_SIZES_CHAR);
			strncat(*list, size_start_tag, (size_end_tag - size_start_tag) / sizeof(char));
			chrcat (*list, '\n');

			name_start_tag = strstr(name_end_tag, MODARCHIVE_NAME_START_TAG);
			
			// Progress callback
			if(_data_read_callback_fp)
			{
				_data_read_callback_fp("_create_list_MODARCHIVE()", 2 + (size_end_tag - name_start_tag) / sizeof(char) + 1);
			}
		}
	}
	else
	{
		goto _RETURN_ERROR;
	}

	goto _RETURN_OK;
	_RETURN_OK:
		free(url_prefix);
		free(list_temp);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(url_prefix != NULL)
		{
			free(url_prefix);
		}
		if(list_temp != NULL)
		{
			free(list_temp);
			list_temp = NULL;
		}
		if(*list != NULL)
		{
			free(*list);
			*list = NULL;
		}
		return RETURN_ERROR;
}

static int _create_list_MODLAND(
	IN  char  *directory_name,
	OUT char **list)
{
	char *list_temp = NULL;
	int   result    = RETURN_ERROR;

	// Progress callback
	if(_data_size_callback_fp)
	{
		// I cannot predict the size
		_data_size_callback_fp("_create_list_MODLAND()", GLOBALS_MAX_LINE_LENGTH);
	}

	if(ftp_get_list_from_data(MODLAND_ALL_HOST_NAME, _socket_handle, directory_name, &list_temp) == RETURN_OK)
	{
		result = _clean_ftp_list(list_temp, list);
		free(list_temp);
	}

	return result;
}

static int _create_list_MODULESPL(
	OUT char **list)
{
	int list_size = 0;

	// Tygre 2015/12/27: List of modules
	// I hard-code for the moment the number of modules,
	// which is a little bit more than MODULESPL_MAXIMUM_NUMBER.
	list_size = 2 + strlen(MODULESPL_MYSTERY_MODULE_NAME) + 4;
	*list     = malloc((list_size + 1) * sizeof(char));
	if(*list  == NULL)
	{
		log_print_error( GetString( MSG_SOURCES_SOURCESGETLISTCOULDNOTALLOCATELIST ) );
		goto _RETURN_ERROR;
	}
	string_snprintf(*list, list_size + 1, "f %s%c42\n", MODULESPL_MYSTERY_MODULE_NAME, GLOBALS_SEPARATOR_SIZES_CHAR);

	goto _RETURN_OK;
	_RETURN_OK:
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		return RETURN_ERROR;
}

static int _create_list_AMPDASCENE(
	OUT char **list)
{
	int list_size = 0;

	// Tygre 2015/12/27: List of modules
	// I hard-code for the moment the number of modules,
	// which is a little bit more than AMPDASCENE_MAXIMUM_NUMBER.
	list_size = 2 + strlen(AMPDASCENE_MYSTERY_MODULE_NAME) + 4;
	*list     = malloc((list_size + 1) * sizeof(char));
	if(*list  == NULL)
	{
		log_print_error( GetString( MSG_SOURCES_SOURCESGETLISTCOULDNOTALLOCATELIST ) );
		goto _RETURN_ERROR;
	}
	string_snprintf(*list, list_size + 1, "f %s%c42\n", AMPDASCENE_MYSTERY_MODULE_NAME, GLOBALS_SEPARATOR_SIZES_CHAR);

	goto _RETURN_OK;
	_RETURN_OK:
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		return RETURN_ERROR;
}

static int _create_list_LOCAL_DRAWER(
	IN  char  *directory_name,
	OUT char **list)
{
	KVSstore       *items_store      = NULL;
	struct DosList *dos_list         = NULL;
	char           *dos_name         = NULL;
	io_dir         *directory        = NULL;
	char           *item_name        = NULL;
	int             item_size        = 0;
	int             item_size_digits = 0;
	char		   *item_size_string = NULL;
	int             list_size        = 0;
	int             i                = 0;

	items_store    = kvs_create(kvs_compare_strings);
	if(items_store == NULL)
	{
		log_print_error("_create_list_LOCAL_DRAWER(), could not create store for items\n");
		goto _RETURN_ERROR;
	}

	if(string_equal(directory_name, GLOBALS_SEPARATOR_PATHS))
	{
		dos_list = LockDosList(LOCAL_DRAWER_DOS_LIST);
		while(dos_list = NextDosEntry(dos_list, LOCAL_DRAWER_DOS_LIST))
		{
			switch(dos_list->dol_Type)
			{
				case DLT_DIRECTORY:
				case DLT_VOLUME:
					// BSTR, data is 32-bit BPTR to byte count followed by a byte string
					item_name           = ((char *)BADDR(dos_list->dol_Name)) + 1 * sizeof(char);
					item_size           = 0;
					item_size_digits    = 1;
					item_size_string    = malloc((item_size_digits + 1) * sizeof(char));
					if(item_size_string == NULL)
					{
						log_print_error("_create_list_LOCAL_DRAWER(), could not allocate item size\n");
						goto _RETURN_ERROR;
					}
					string_itoa(item_size, 10, item_size_string);
					kvs_put(items_store, string_duplicate(item_name), item_size_string);

					// "d " + item_name + ":" + GLOBALS_SEPARATOR_SIZES + "0\n"
					list_size = list_size + 2 + strlen(item_name) + 4;
					break;
				case DLT_DEVICE:
					break;
			}
		}
		UnLockDosList(LOCAL_DRAWER_DOS_LIST);

		*list    = malloc((list_size + 1) * sizeof(char *));
		if(*list == NULL)
		{
			log_print_error("_create_list_LOCAL_DRAWER(), could not allocate volume list\n");
			goto _RETURN_ERROR;
		}
		*list[0] = '\0';

		// Special directories
		// Nothing to do here

		// Progress callback
		if(_data_size_callback_fp)
		{
			_data_size_callback_fp("_create_list_LOCAL_DRAWER() (size)", list_size);
		}

		// Assignes and volumes
		for(i = 0; i < kvs_length(items_store); i++)
		{
			item_name = kvs_get_key(items_store, i);
			strcat(*list, "d ");
			strcat(*list, item_name);
			chrcat(*list, ':');
			chrcat(*list, GLOBALS_SEPARATOR_SIZES_CHAR);
			strcat(*list, "0\n");

			// Progress callback
			if(_data_read_callback_fp)
			{
				_data_read_callback_fp("_create_list_LOCAL_DRAWER()", 6 + strlen(item_name));
			}
		}
	}
	else
	{
		dos_name = (char *)directory_name + 1 * sizeof(char);

		directory    = calloc(1, sizeof(io_dir));
		if(directory == NULL)
		{
			log_print_error("_create_list_LOCAL_DRAWER(), could not allocate directory %s\n", dos_name);
			goto _RETURN_ERROR;
		}

		directory->lock    = Lock(dos_name, ACCESS_READ);
		if(directory->lock == BPTR_ZERO)
		{
			log_print_error("_create_list_LOCAL_DRAWER(), could not lock directory %s\n", dos_name);
			goto _RETURN_ERROR;
		}

		if(Examine(directory->lock, &directory->fib) == 0)
		{
			log_print_error("_create_list_LOCAL_DRAWER(), could not examine directory %s\n", dos_name);
			goto _RETURN_ERROR;
		}

		// First, I count the number of possible images sets
		while(ExNext(directory->lock, &directory->fib) != 0)
		{
			item_name           = directory->fib.fib_FileName;
			item_size           = directory->fib.fib_DirEntryType > 0 ?
									0 :
									directory->fib.fib_Size; // fib_Size may not be reset to 0 for a directory
			item_size_digits    = integer_count_digits(item_size);
			item_size_string    = malloc((item_size_digits + 1) * sizeof(char));
			if(item_size_string == NULL)
			{
				log_print_error("_create_list_LOCAL_DRAWER(), could not allocate item size\n");
				goto _RETURN_ERROR;
			}
			string_itoa(item_size, 10, item_size_string);
			kvs_put(items_store, string_duplicate(item_name), item_size_string);
			
			// The list is of format
			// 	"d " + item_name + GLOBALS_SEPARATOR_SIZES + "0\n"
			// OR
			// 	"f " + item_name + GLOBALS_SEPARATOR_SIZES + item_size + "\n"
			list_size = list_size + 2 + strlen(item_name) + 1 + item_size_digits + 1;
		}
		list_size += 5; // "d ."  + GLOBALS_SEPARATOR_SIZES + "0\n"
		list_size += 7; // "d .." + GLOBALS_SEPARATOR_SIZES + "0\n"

		UnLock(directory->lock);
		free(directory);

		*list    = malloc((list_size + 1) * sizeof(char *));
		if(*list == NULL)
		{
			log_print_error("_create_list_LOCAL_DRAWER(), could not allocate item list\n");
			goto _RETURN_ERROR;
		}
		*list[0] = '\0';

		// Special directories
		strcat(*list, "d .");
		chrcat(*list, GLOBALS_SEPARATOR_SIZES_CHAR);
		strcat(*list, "0\n");
		strcat(*list, "d ..");
		chrcat(*list, GLOBALS_SEPARATOR_SIZES_CHAR);
		strcat(*list, "0\n");

		// Progress callback
		if(_data_size_callback_fp)
		{
			_data_size_callback_fp("_create_list_LOCAL_DRAWER() (size)", list_size);
		}

		// Normal directories and files
		for(i = 0; i < kvs_length(items_store); i++)
		{
			item_name        = kvs_get_key(items_store, i);
			item_size_string = kvs_get_value(items_store, item_name);

			if(string_equal(item_size_string, "0"))
			{
				strcat(*list, "d ");
				strcat(*list, item_name);
				strcat(*list, GLOBALS_SEPARATOR_SIZES);
				strcat(*list, "0\n");
			}
			else
			{
				strcat(*list, "f ");
				strcat(*list, item_name);
				strcat(*list, GLOBALS_SEPARATOR_SIZES);
				strcat(*list, item_size_string);
				strcat(*list, "\n");
			}

			// Progress callback
			if(_data_read_callback_fp)
			{
				_data_read_callback_fp("_create_list_LOCAL_DRAWER()", 4 + strlen(item_name) + strlen(item_size_string));
			}
		}
	}

	goto _RETURN_OK;
	_RETURN_OK:
		kvs_destroy(items_store);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(dos_list != NULL)
		{
			UnLockDosList(LOCAL_DRAWER_DOS_LIST);
		}
		if(directory->lock != BPTR_ZERO)
		{
			UnLock(directory->lock);
		}
		if(directory != NULL)
		{
			free(directory);
		}
		if(items_store != NULL)
		{
			kvs_destroy(items_store);
		}
		return RETURN_ERROR;
}

static char *_retrieve_list_from_disk(
	IN char *source_name,
	IN char *directory_name)
{
	char *encoded_directory_name = NULL;
	int   length                 = 0;
	char *cache_file_path        = NULL;
	FILE *cache_file             = NULL;
	int   list_size              = 0;
	char *list                   = NULL;
	char *line                   = NULL;
	char *pos1                   = NULL;
	char *pos2                   = NULL;

	if(_encode_directory_name(directory_name, &encoded_directory_name) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_SOURCES_RETRIEVELISTFROMDISKCOULDNOTENCODEDIRECTORYNAME ) );
		goto _RETURN_ERROR;
	}

	length             = strlen(prefs_get_cache_directory()) + strlen(source_name) + 1 + strlen(encoded_directory_name);
	cache_file_path    = malloc((length + 1) * sizeof(char));
	if(cache_file_path == NULL)
	{
		log_print_error( GetString( MSG_SOURCES_RETRIEVELISTFROMDISKCOULDNOTALLOCATECACHEPATH ) );
		goto _RETURN_ERROR;
	}
	cache_file_path[0] = '\0';
	strcat(cache_file_path, prefs_get_cache_directory());
	strcat(cache_file_path, source_name);
	chrcat(cache_file_path, CACHE_FILES_SEPARATOR);
	strcat(cache_file_path, encoded_directory_name);

	if(_list_is_recently_on_disk(cache_file_path) == FALSE)
	{
		goto _RETURN_ERROR;
	}

	cache_file = fopen(cache_file_path, "r"); /* Flawfinder: ignore */
	if(cache_file == NULL)
	{
		log_print_error( GetString( MSG_SOURCES_RETRIEVELISTFROMDISKCOULDNOTOPENCACHEFILE ) , cache_file_path);
		goto _RETURN_ERROR;
	}

	fseek(cache_file, 0, SEEK_END);
	list_size = ftell(cache_file);
	fseek(cache_file, 0, SEEK_SET);
			
	list    = malloc((list_size + 1) * sizeof(char));
	if(list == NULL)
	{
		log_print_error( GetString( MSG_SOURCES_RETRIEVELISTFROMDISKCOULDNOTALLOCATELIST ) );
		goto _RETURN_ERROR;
	}
			
	if(fread(list, sizeof(char), list_size, cache_file) != list_size)
	{
		log_print_error( GetString( MSG_SOURCES_RETRIEVELISTFROMDISKCOULDNOTREADCACHEFILE ) , cache_file_path);
		goto _RETURN_ERROR;
	}
	list[list_size] = '\0';

	fclose(cache_file);
	cache_file = NULL;

	// Tygre 23/02/22: Sanity!
	// I check the format of the first
	// line, if any, before returning
	// the cached list, else nothing.

	// Parse the first line
	line    = strchr(list, '\n');
	if(line == NULL)
	{
		_invalidate_cached_list(source_name, directory_name);
		goto _RETURN_ERROR;
	}

	line    = string_duplicate_n(list, (line - list) / sizeof(char));
	if(line == NULL)
	{
		_invalidate_cached_list(source_name, directory_name);
		goto _RETURN_ERROR;
	}
	
	// Positions of delimiters
	if(_get_line_delimiters(line, &pos1, &pos2) == RETURN_ERROR)
	{
		_invalidate_cached_list(source_name, directory_name);
		goto _RETURN_ERROR;
	}

	goto _RETURN_OK;
	_RETURN_OK:
		free(line);
		free(cache_file_path);
		free(encoded_directory_name);
		return list;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(line != NULL)
		{
			free(line);
		}
		if(cache_file != NULL)
		{
			fclose(cache_file);
		}
		if(cache_file_path != NULL)
		{
			free(cache_file_path);
		}
		if(encoded_directory_name != NULL)
		{
			free(encoded_directory_name);
		}
		if(list != NULL)
		{
			free(list);
		}
		return NULL;
}

static int _cache_list_on_disk(
	IN char *source_name,
	IN char *directory_name,
	IN char *list)
{
	int   length                 = 0;
	char *encoded_directory_name = NULL;
	char *cache_file_path        = NULL;
	FILE *cache_file             = NULL;

	if(_encode_directory_name(directory_name, &encoded_directory_name) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_SOURCES_CACHELISTONDISKCOULDNOTENCODEDIRECTORYNAME ) );
		goto _RETURN_ERROR;
	}

	length          = strlen(prefs_get_cache_directory()) + strlen(source_name) + 1 + strlen(encoded_directory_name);
	cache_file_path = malloc((length + 1) * sizeof(char));
	if(cache_file_path == NULL)
	{
		log_print_error( GetString( MSG_SOURCES_CACHELISTONDISKCOULDNOTALLOCATECACHEPATH ) );
		goto _RETURN_ERROR;	   
	}
	cache_file_path[0] = '\0';
	strcat(cache_file_path, prefs_get_cache_directory());
	strcat(cache_file_path, source_name);
	chrcat(cache_file_path, CACHE_FILES_SEPARATOR);
	strcat(cache_file_path, encoded_directory_name);

	if(_list_is_recently_on_disk(cache_file_path) == FALSE)
	{
		cache_file = fopen(cache_file_path, "w"); /* Flawfinder: ignore */
		if(cache_file == NULL)
		{
			log_print_error( GetString( MSG_SOURCES_CACHELISTONDISKCOULDNOTCREATECACHEFILE ) , cache_file_path);
			goto _RETURN_ERROR;
		}
		else
		{
			if(fputs(list, cache_file) == EOF)
			{
				log_print_error( GetString( MSG_SOURCES_CACHELISTONDISKCOULDNOTWRITECACHEFILE ) , cache_file_path);
				goto _RETURN_ERROR;
			}
		}
	}

	goto _RETURN_OK;
	_RETURN_OK:
		fclose(cache_file);
		free(cache_file_path);
		free(encoded_directory_name);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(cache_file != NULL)
		{
			fclose(cache_file);
		}
		if(cache_file_path != NULL)
		{
			free(cache_file_path);
		}
		if(encoded_directory_name != NULL)
		{
			free(encoded_directory_name);
		}
		return RETURN_ERROR;
}

static char *_retrieve_list_from_memory(
	IN char *source_name,
	IN char *directory_name)
{
	int   length                 = 0;
	char *encoded_directory_name = NULL;
	char *cache_file_path        = NULL;
	char *list                   = NULL;

	if(_encode_directory_name(directory_name, &encoded_directory_name) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_SOURCES_RETRIEVELISTFROMMEMORYCOULDNOTENCODEDIRECTORYNAME ) );
		goto _RETURN_ERROR;
	}

	length             = strlen(source_name) + 1 + strlen(encoded_directory_name);
	cache_file_path    = malloc((length + 1) * sizeof(char));
	if(cache_file_path == NULL)
	{
		log_print_error( GetString( MSG_SOURCES_RETRIEVELISTFROMMEMORYCOULDNOTALLOCATECACHEPATH ) );
		goto _RETURN_ERROR;
	}
	cache_file_path[0] = '\0';
	strcat(cache_file_path, source_name);
	chrcat(cache_file_path, CACHE_FILES_SEPARATOR);
	strcat(cache_file_path, encoded_directory_name);

	list = kvs_get_value(_cached_directories_list, cache_file_path);

	goto _RETURN_OK;
	_RETURN_OK:
		free(cache_file_path);
		free(encoded_directory_name);
		return list;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(list != NULL)
		{
			free(list);
		}
		if(cache_file_path != NULL)
		{
			free(cache_file_path);
		}
		if(encoded_directory_name != NULL)
		{
			free(encoded_directory_name);
		}
		return NULL;
}

static int _cache_list_in_memory(
	IN char *source_name,
	IN char *directory_name,
	IN char *list)
{
	int   length                 = 0;
	char *encoded_directory_name = NULL;
	char *cache_file_path        = NULL;

	if(_encode_directory_name(directory_name, &encoded_directory_name) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_SOURCES_CACHELISTINMEMORYCOULDNOTENCODEDIRECTORYNAME ) );
		goto _RETURN_ERROR;
	}

	length          = strlen(source_name) + 1 + strlen(encoded_directory_name);
	cache_file_path = malloc((length + 1) * sizeof(char));
	if(cache_file_path == NULL)
	{
		log_print_error( GetString( MSG_SOURCES_CACHELISTINMEMORYCOULDNOTALLOCATECACHEPATH ) );
		goto _RETURN_ERROR;
	}
	cache_file_path[0] = '\0';
	strcat(cache_file_path, source_name);
	chrcat(cache_file_path, CACHE_FILES_SEPARATOR);
	strcat(cache_file_path, encoded_directory_name);

	kvs_put(_cached_directories_list, string_duplicate(cache_file_path), string_duplicate(list));

	goto _RETURN_OK;
	_RETURN_OK:
		free(cache_file_path);
		free(encoded_directory_name);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(cache_file_path != NULL)
		{
			free(cache_file_path);
		}
		if(encoded_directory_name != NULL)
		{
			free(encoded_directory_name);
		}
		return RETURN_ERROR;
}

static int _invalidate_cached_list(
	IN char *source_name,
	IN char *directory_name)
{
	int   length                 = 0;
	char *encoded_directory_name = NULL;
	char *cache_file_path        = NULL;

	if(_encode_directory_name(directory_name, &encoded_directory_name) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_SOURCES_CACHELISTONDISKCOULDNOTENCODEDIRECTORYNAME ) );
		goto _RETURN_ERROR;
	}

	length          = strlen(prefs_get_cache_directory()) + strlen(source_name) + 1 + strlen(encoded_directory_name);
	cache_file_path = malloc((length + 1) * sizeof(char));
	if(cache_file_path == NULL)
	{
		log_print_error( GetString( MSG_SOURCES_CACHELISTONDISKCOULDNOTALLOCATECACHEPATH ) );
		goto _RETURN_ERROR;
	}
	cache_file_path[0] = '\0';
	strcat(cache_file_path, prefs_get_cache_directory());
	strcat(cache_file_path, source_name);
	chrcat(cache_file_path, CACHE_FILES_SEPARATOR);
	strcat(cache_file_path, encoded_directory_name);

	DeleteFile(cache_file_path);

	goto _RETURN_OK;
	_RETURN_OK:
		free(cache_file_path);
		free(encoded_directory_name);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(cache_file_path != NULL)
		{
			free(cache_file_path);
		}
		if(encoded_directory_name != NULL)
		{
			free(encoded_directory_name);
		}
		return RETURN_ERROR;
}

static BOOL  _list_is_recently_on_disk(
	IN char *cache_file_path)
{
	char   *cache_path           = NULL;
	BPTR    cache_path_lock      = BPTR_ZERO;
	BPTR    cache_file_path_lock = BPTR_ZERO;
	time_t  time_file            = { 0 };
	time_t  time_now             = { 0 };

	cache_path = prefs_get_cache_directory();

	// Tygre 2017/12/30: Trailing slash...
	// The mkdir() function doesn't like the trailing slash,
	// so I must remove it if it exists from the file name.
	if(string_end_with(cache_path, GLOBALS_SEPARATOR_PATHS) == FALSE)
	{
		cache_path = string_duplicate(cache_path);
		if(cache_path == NULL)
		{
			log_print_error( GetString( MSG_SOURCES_LISTEXISTSONDISKCOULDNOTALLOCATECACHEPATH1 ) );
			goto _RETURN_ERROR;
		}
	}
	else
	{
		cache_path = string_duplicate_n(cache_path, strlen(cache_path) - 1);
		if(cache_path == NULL)
		{
			log_print_error( GetString( MSG_SOURCES_LISTEXISTSONDISKCOULDNOTALLOCATECACHEPATH2 ) );
			goto _RETURN_ERROR;
		}
	}

	// Now, do the real work...
	cache_path_lock    = Lock(cache_path, ACCESS_READ);
	if(cache_path_lock == BPTR_ZERO)
	{
		// If the cache directory does not exist,
		// then create it and the cache file
		// obviously cannot exist.
		cache_path_lock    = CreateDir(cache_path);
		if(cache_path_lock == BPTR_ZERO)
		{
			log_print_error( GetString( MSG_SOURCES_LISTEXISTSONDISKCOULDNEITHERFINDNORCREATECACHEDIREC ) , cache_path);
		}
		goto _RETURN_ERROR;
	}

	cache_file_path_lock    = Lock(cache_file_path, ACCESS_READ);
	if(cache_file_path_lock == BPTR_ZERO)
	{
		// The cache file may just not exist!
		// No need to log an error...
		//	log_print_error("_list_is_recently_on_disk(), could not lock cache file\n");
		goto _RETURN_ERROR;
	}

	// If the cache file exists and it's
	// recent, then return that it exists.
	time_now = time(NULL);
	if(io_file_date(cache_file_path, &time_file) == RETURN_ERROR)
	{
		goto _RETURN_ERROR;
	}
	if(time_now - time_file < CACHE_FILES_DURATION)
	{
		goto _RETURN_OK;
	}
	else
	{
		goto _RETURN_ERROR;
	}

	goto _RETURN_OK;
	_RETURN_OK:
		UnLock(cache_file_path_lock);
		UnLock(cache_path_lock);
		free(cache_path);
		return TRUE;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(cache_file_path_lock != BPTR_ZERO)
		{
			UnLock(cache_file_path_lock);
		}
		if(cache_path_lock != BPTR_ZERO)
		{
			UnLock(cache_path_lock);
		}
		if(cache_path != NULL)
		{
			free(cache_path);
		}
		return FALSE;
}

static int _encode_directory_name(
	IN  char  *directory_name,
	OUT char **encoded_directory_name)
{
	int  size = 0;
	int  i    = 0;
	char c    = '\0';

	*encoded_directory_name    = malloc((CACHE_FILES_MAXIMUM_LENGTH + 1) * sizeof(char));
	if(*encoded_directory_name == NULL)
	{
		log_print_error( GetString( MSG_SOURCES_ENCODEDIRECTORYNAMECOULDNOTALLOCATEENCODEDDIRECTORY ) );
		return RETURN_ERROR;
	}

	size = strlen(directory_name) - 1;
	while(i < size && i < CACHE_FILES_MAXIMUM_LENGTH)
	{
		c = directory_name[size - i];
		if(c == GLOBALS_SEPARATOR_PATHS_CHAR ||
		   c == GLOBALS_SEPARATOR_VOLUMES_CHAR)
		{
			(*encoded_directory_name)[i] = CACHE_FILES_SEPARATOR;
		}
		else
		{
			(*encoded_directory_name)[i] = c;
		}
		i++;
	}
	(*encoded_directory_name)[i] = '\0';

	return RETURN_OK;
}

static int _get_line_delimiters(
	IN  char  *line,
	OUT char **pos1,
	OUT char **pos2)
{
	if(line == NULL || strlen(line) < 2)
	{
		return RETURN_ERROR;
	}
	
	// The file name itself and the size of the file
	*pos1 = strchr(line, ' ');
	if(*pos1 == NULL)
	{
		// No "space" in the line
		return RETURN_ERROR;
	}
	(*pos1)++;

	*pos2 = strrchr(line, GLOBALS_SEPARATOR_SIZES_CHAR);
	if(*pos2 == NULL)
	{
		// No GLOBALS_SEPARATOR_SIZES_CHAR in the line
		return RETURN_ERROR;
	}

	if(*pos1 >= *pos2)
	{
		// Empty name                (pos1 = pos2)
		// Wrong order of delimiters (pos1 > pos2)
		return RETURN_ERROR;
	}
	
	(*pos2)++;

	return RETURN_OK;
}

