/*
	AmiModRadio
	All of Aminet modules at your fingertips


	
	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.

	
	
	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#ifdef UI_ACT
	#include "controls_act.h"
#endif
#ifdef UI_CLI
	#include "controls_cli.h"
#endif
#ifdef UI_MUI
	#include "controls_mui.h"
#endif
#ifdef UI_REA
	#include "controls_rea.h"
#endif
#include "globals.h"
#include "locale.h"
#include "log.h"
#include "ringhio.h"
#include "utils.h"
#include "version.h"
#include "z_fortify.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <proto/alib.h>
#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/icon.h>
#include <proto/intuition.h>
#include <exec/execbase.h>
#include <intuition/intuition.h>
#include <workbench/startup.h>



/* Constants and declarations */

#define TT_ACT           "ACT"
#define TT_CLI           "CLI"
#define TT_MUI           "MUI"
#define TT_REA           "REA"
#define TT_VERSION       "VERSION"
#define ARG_TEMPLATE     TT_ACT "/S," TT_CLI "/S," TT_MUI "/S," TT_REA "/S," TT_VERSION "/S"
#define ERR_READINGARGS  "Error when reading arguments: %ld\n"
#define ERR_TOOMANYARGS  "Error when analysing arguments: too many arguments"

	   int main(int, char **);



/* Definitions */

struct Library       *AslBase         = NULL;
struct Library       *IconBase        = NULL;
struct IntuitionBase *IntuitionBase   = NULL;

struct ClassLibrary  *BitMapBase      = NULL;
struct ClassLibrary  *ButtonBase      = NULL;
struct ClassLibrary  *CheckBoxBase    = NULL;
struct ClassLibrary  *ChooserBase     = NULL;
struct ClassLibrary  *ClickTabBase    = NULL;
struct ClassLibrary  *FuelGaugeBase   = NULL;
struct ClassLibrary  *LabelBase       = NULL;
struct ClassLibrary  *ListBrowserBase = NULL;
struct ClassLibrary  *LayoutBase      = NULL;
struct ClassLibrary  *PageBase        = NULL;
struct ClassLibrary  *StringBase      = NULL;
struct ClassLibrary  *WindowBase      = NULL;

// Tygre 2017/04/17: Stack size
// I ask the compilers to set a stack size.
// I must use "__entry" for VBCC to keep it.
size_t            __stack            = GLOBALS_TASK_STACK_SIZE;
#ifndef S_SPLINT_S
static const char __entry *MIN_STACK = "$STACK:" GLOBALS_TASK_STACK_SIZE_STR;
#endif

static struct EasyStruct requester_struct = {
	sizeof(struct EasyStruct),
	0,
	"AmiModRadio",
	"%s",
	"Okay"
};

int main(
	int    argc,
	char **argv)
{
	struct WBStartup            *wbstartup                     = NULL;
	int                          i                             = 0;
	struct WBArg                *wbarg                         = NULL;
	BPTR                         old_dir                       = BPTR_ZERO;
	struct DiskObject           *disk_object                   = NULL;
	STRPTR                      *tool_type                     = NULL;
	UBYTE                       *s                             = NULL;
	struct {
		long act;
		long cli;
		long mui;
		long rea;
		long version; }          arguments_values              = { 0 };
	struct RDArgs               *arguments                     = NULL;
	struct Process              *process                       = NULL;
	struct CommandLineInterface *cli                           = NULL;
	char                        *command_path                  = NULL;
	char                        *char1                         = NULL;
	char                        *char2                         = NULL;
	int                          pos1                          = 0;
	int                          pos2                          = 0;
	char                         path[GLOBALS_MAX_LINE_LENGTH] = { 0 };
	BPTR                         path_list                     = BPTR_ZERO;
	int                          result                        = RETURN_ERROR;

	Fortify_EnterScope();

	IconBase = OpenLibrary("icon.library", 37);
	if(NULL == IconBase)
	{
		log_print_error("Could not open icon.library v37+\n");
		result = RETURN_ERROR;
		goto _RETURN;
	}

	IntuitionBase = (struct IntuitionBase *)OpenLibrary("intuition.library", 36);
	if(NULL == IntuitionBase)
	{
		log_print_error("Could not open intuition.library v36+\n");
		result = RETURN_ERROR;
		goto _RETURN;
	}

	Locale_Open("AmiModRadio.catalog", 1, 0);

	log_setup(NULL, NULL, NULL, NULL);
	
	srand(time(NULL)); /* Flawfinder: ignore */



	// +-----------+
	// | From icon |
	// +-----------+
	
	if(argc == 0)
	{
		// Basic init
		wbstartup = (struct WBStartup *)argv;

		// Process
		process = wbstartup->sm_Message.mn_ReplyPort->mp_SigTask;

		// Arguments
		for(i = 0, wbarg = wbstartup->sm_ArgList;
			i < wbstartup->sm_NumArgs;
			i++, wbarg++)
		{
			if(wbarg->wa_Lock && *wbarg->wa_Name)
			{
				old_dir = CurrentDir(wbarg->wa_Lock);
			}

			if(*wbarg->wa_Name && (disk_object = GetDiskObject(wbarg->wa_Name)))
			{
				tool_type = disk_object->do_ToolTypes;

				s  = FindToolType(tool_type, TT_ACT);
				if(s != NULL)
				{
					arguments_values.act = 1;
				}
				s  = FindToolType(tool_type, TT_CLI);
				if(s != NULL)
				{
					arguments_values.cli = 1;
				}
				s  = FindToolType(tool_type, TT_MUI);
				if(s != NULL)
				{
					arguments_values.mui = 1;
				}
				s  = FindToolType(tool_type, TT_REA);
				if(s != NULL)
				{
					arguments_values.rea = 1;
				}
				s  = FindToolType(tool_type, TT_VERSION);
				if(s != NULL)
				{
					arguments_values.version = 1;
				}

				FreeDiskObject(disk_object);
			}

			if(old_dir >= BPTR_ZERO)
			{
				CurrentDir(old_dir);
			}
		}
	}

	// +----------+
	// | From CLI |
	// +----------+

	else
	{
		// Basic init
		command_path = (char *)argv[0];
		
		char1     = strrchr(command_path, GLOBALS_SEPARATOR_PATHS_CHAR);
		char2     = strrchr(command_path, GLOBALS_SEPARATOR_VOLUMES_CHAR);
		if(char1 != NULL || char2 != NULL)
		{
			// Tygre 2015/01/06: #33
			// I make sure that the path includes whatever the user entered.
			pos1  = char1 - command_path;
			pos2  = char2 - command_path;
			pos1  = integer_max(pos1, pos2);
			strncpy(path, command_path, pos1);
			old_dir = Lock(path, ACCESS_READ);
			if(old_dir >= BPTR_ZERO)
			{
				SetCurrentDirName(path);
				CurrentDir(old_dir);
			}
		}
		
		// Process
		process = (struct Process *)FindTask(NULL);
		
		// Arguments
		arguments = ReadArgs(ARG_TEMPLATE, (APTR)&arguments_values, NULL);
		if(arguments == NULL)
		{
			printf(ERR_READINGARGS, IoErr()); /* Flawfinder: ignore */
			result = RETURN_ERROR;
			goto _RETURN;
		}

		// Basic cleanup
		FreeArgs(arguments);
	}

	// +--------------+
	// | Sanity check |
	// +--------------+
	
	if(abs(arguments_values.act + arguments_values.mui + arguments_values.rea + arguments_values.version) > 1)
	{
		if(argc == 0)
		{
			EasyRequest(NULL, &requester_struct, NULL, ERR_TOOMANYARGS "\nPlease remove some of the tool types");
		}
		else
		{
			printf(ERR_TOOMANYARGS "\nPlease remove some of the arguments\n"); /* Flawfinder: ignore */
		}
		result = RETURN_ERROR;
		goto _RETURN;
	}

	// +-----------+
	// | Path List |
	// +-----------+
	
	if(process != NULL)
	{
		// Tygre 2015/12/07: WBRun is NULL!
		// The variable "process" is NULL when running "WBRun AmiModRadio" from a CLI!
		cli       = BADDR(process->pr_CLI);
		path_list = cli->cli_CommandDir;
	}

	// +-----+
	// | GUI |
	// +-----+
	
	if(arguments_values.version)
	{
		if(argc == 0)
		{
			EasyRequest(NULL, &requester_struct, NULL, version_get_version_description());
		}
		else
		{
			printf("%s\n", version_get_version_description());
			result = RETURN_OK;
			goto _RETURN;
		}
	}
	else if(arguments_values.act)
	{
		#ifdef UI_ACT
		result = controls_act(path_list);
		#endif
		#ifndef UI_ACT
		log_print_warning("This executable wasn't compiled with the ClassAct GUI.\nPick another GUI or another executable.\n");
		#endif
		goto _RETURN;
	}
	else if(arguments_values.cli)
	{
		#ifdef UI_CLI
		result = controls_cli(path_list);
		#endif
		#ifndef UI_CLI
		log_print_warning("This executable wasn't compiled with the CLI.\nPick another GUI or another executable.\n");
		#endif
		goto _RETURN;
	}
	else if(arguments_values.mui)
	{
		#ifdef UI_MUI
		result = controls_mui(path_list);
		#endif
		#ifndef UI_MUI
		log_print_warning("This executable wasn't compiled with the MUI GUI.\nPick another GUI or another executable.\n");
		#endif
		goto _RETURN;
	}
	else if(arguments_values.rea)
	{
		#ifdef UI_REA
		result = controls_rea(path_list);
		#endif
		#ifndef UI_REA
		log_print_warning("This executable wasn't compiled with the ReAction GUI.\nPick another GUI or another executable.\n");
		#endif
		goto _RETURN;
	}
	else
	{
		// Default UI
		#ifdef UI_CLI
		result = controls_cli(path_list);
		goto _RETURN;
		#endif

		// Other GUIs
		#ifdef UI_ACT
		result = controls_act(path_list);
		goto _RETURN;
		#endif
		#ifdef UI_MUI
		result = controls_mui(path_list);
		goto _RETURN;
		#endif
		#ifdef UI_REA
		result = controls_rea(path_list);
		goto _RETURN;
		#endif
	}

	goto _RETURN;
	_RETURN:
		log_cleanup();
		Locale_Close();
		if(IntuitionBase != NULL)
		{
			CloseLibrary((struct Library *)IntuitionBase);
			IntuitionBase = NULL;
		}
		if(IconBase != NULL)
		{
			CloseLibrary(IconBase);
			IconBase = NULL;
		}
		
		Fortify_LeaveScope();
		Fortify_OutputStatistics();

		return result;
}

