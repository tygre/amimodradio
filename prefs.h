/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



#ifndef PREFS_H
#define PREFS_H 1

#include "utils.h"      // For IN, OUT...

#include <exec/types.h> // For BOOL...

int    prefs_setup(
	   void);

void   prefs_cleanup(void);

char  *prefs_get_saved_modules_directory(
	   void);

void   prefs_set_saved_modules_directory(
	   IN char *new_current_program_directory);

BOOL   prefs_should_play_on_startup(
	   void);

void   prefs_set_play_on_startup(
	   IN BOOL should_play_on_startup);

BOOL   prefs_should_show_log_page_on_fetch(
	   void);

void   prefs_set_show_log_page_on_fetch(
	   IN BOOL should_show_log_page_on_fetch);

int    prefs_get_maximum_log_size_index(
	   void);

void   prefs_set_maximum_log_size_index(
	   IN int new_maximum_log_size_index);

char **prefs_get_list_of_maximum_log_sizes(
	   void);

int    prefs_get_number_of_maximum_log_sizes(
	   void);

BOOL   prefs_should_remember_source_between_runs(
	   void);

void   prefs_set_remember_source_between_runs(
	   IN BOOL should_remember_source_between_runs);

int    prefs_get_remembered_source_index(
	   void);

void   prefs_set_remembered_source_index(
	   IN int new_remembered_source_index);

char  *prefs_get_remembered_root_directory(
	   void);

void   prefs_set_remembered_root_directory(
	   IN char *new_remembered_root_directory);

BOOL   prefs_should_cache_list_during_run(
	   void);

void   prefs_set_cache_list_during_run(
	   IN BOOL should_cache_list_during_run);

BOOL   prefs_should_cache_list_between_runs(
	   void);

void   prefs_set_cache_list_between_runs(
	   IN BOOL should_cache_list_between_runs);

char  *prefs_get_cache_directory(
	   void);

void   prefs_set_cache_directory(
	   IN char *new_cache_directory);

BOOL   prefs_should_save_prefs_on_exit(
	   void);

void   prefs_set_save_prefs_on_exit(
	   IN BOOL should_save_prefs_on_exit);

char  *prefs_get_images_directory(
	   void);

void   prefs_set_images_directory(
	   IN char *new_images_directory);

char **prefs_get_list_of_images_sets(
	   void);

int    prefs_get_number_of_images_sets(
	   void);

int    prefs_get_current_images_set_index(
	   void);

void   prefs_set_current_images_set_index(
	   IN int new_current_image_set_index);

BOOL   prefs_should_skip_if_banned(
	   void);

void   prefs_set_skip_if_banned(
	   IN BOOL should_skip_if_banned);

BOOL   prefs_should_skip_if_user_rating_low(
	   void);

void   prefs_set_skip_if_user_rating_low(
	   IN BOOL should_skip_if_user_rating_low);

BOOL   prefs_should_skip_if_all_rating_exists(
	   void);

void   prefs_set_skip_if_all_rating_exists(
	   IN BOOL should_skip_if_all_rating_exists);

BOOL   prefs_should_skip_if_all_rating_low(
	   void);

void   prefs_set_skip_if_all_rating_low(
	   IN BOOL should_skip_if_all_rating_low);

BOOL   prefs_should_skip_if_above_maximum_file_size(
	   void);

void   prefs_set_skip_if_above_maximum_file_size(
	   IN BOOL should_skip_if_above_maximum_file_size);

int    prefs_get_maximum_file_size_index(
	   void);

void   prefs_set_maximum_file_size_index(
	   IN int new_maximum_file_size_index);

char **prefs_get_list_of_maximum_file_sizes(
	   void);

int    prefs_get_number_of_maximum_file_sizes(
	   void);

int    prefs_get_main_window_X(
	   void);

void   prefs_set_main_window_X(
	   IN int x_pos);

int    prefs_get_main_window_Y(
	   void);

void   prefs_set_main_window_Y(
	   IN int y_pos);

void   prefs_save_preferences(
	   void);

void   prefs_reset_preferences(
	   void);

void   prefs_default_preferences(
	   void);

#endif

