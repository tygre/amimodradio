/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include "ringhio.h"

#include "arexx.h"
#include "locale.h"
#include "log.h"
#include "z_fortify.h"

#include <proto/exec.h>



/* Constants and declarations */

#define AREXX_PORT_NAME1 "RANCHERO"
#define AREXX_PORT_NAME2 "RINGHIO"

	   int             ringhio_setup(void);
	   void            ringhio_cleanup(void);
	   int             ringhio_inform_user(IN char *);
static struct MsgPort *_get_ringhio_port(void);



/* Definitions */

static BOOL _is_ringhio_setup = FALSE;

int ringhio_setup(
	void)
{
	log_print_debug("ringhio_setup()\n");

	if(arexx_setup() == RETURN_ERROR)
	{
		_is_ringhio_setup = FALSE;
		return RETURN_ERROR;
	}

	// Registering AmiModRadio with Ringhio
	// Assuming that a user won't stop and
	// start Ringhio while AmiModRadio runs.
	if(arexx_send_command(
		_get_ringhio_port,
		"REGISTERAPP APP=AmiModRadio",
		NULL,
		NULL) == RETURN_OK)
	{
		_is_ringhio_setup = TRUE;
		return RETURN_OK;
	}
	else
	{
		_is_ringhio_setup = FALSE;
		return RETURN_ERROR;
	}
}

void ringhio_cleanup(
	 void)
{
	log_print_debug("ringhio_cleanup()\n");

	arexx_cleanup();
	_is_ringhio_setup = FALSE;
}

int ringhio_inform_user(
	IN char *message)
{
	if(_is_ringhio_setup == FALSE)
	{
		log_print_warning("ringhio_inform_user(), Ringhio wasn't initialised\n");
		return RETURN_ERROR;
	}

	// Sending the message to Ringhio/Ranchero
	return arexx_send_command(
		_get_ringhio_port,
		"RANCHERO APP=AmiModRadio CLOSEONDC SCREEN=\"FRONT\" TITLE=\"AmiModRadio\"",
		message,
		NULL);
}

static struct MsgPort *_get_ringhio_port(void)
{
	struct MsgPort *arexx_port = NULL;

	// First possible port of two
	if((arexx_port = FindPort(AREXX_PORT_NAME1)) != NULL)
	{
		return arexx_port;
	}

	// Second
	if((arexx_port = FindPort(AREXX_PORT_NAME2)) != NULL)
	{
		return arexx_port;
	}

	return NULL;
}

