/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



#ifndef HTTPX_HTTP_H
#define HTTPX_HTTP_H 1

#include "httpx.h"
#include "utils.h"

int             httpx_http_setup(
	            IN BOOL (*continue_long_operation_fp)(),
				IN void (*httpx_size_callback_fp)(IN char *origin, IN int number_of_bytes),
				IN void (*httpx_read_callback_fp)(IN char *origin, IN int number_of_bytes));

void            httpx_http_cleanup(
	            void);

int             httpx_http_init_connection(
	            void);

void            httpx_http_close_connection(
	            void);

httpx_response *httpx_http_request(
	            IN char       *http_headers,
	            IN parsed_url *purl,
				IN  int        bytes_limit);


#endif

