/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/

/*
 * Per-task SocketBase and data using tc_UserData
 * Inspired, generalised from z_tasksocketbase.h
 */



/* Includes */

#include "log.h"
#include "utils.h"
#include "utils_socketbase.h"

#include <dos/dos.h>



/* Constants and declarations */

int      utils_socketbase_init(void);
void     utils_socketbase_close(void);
BOOL     utils_socketbase_httpx_http_inited_get(void);
void     utils_socketbase_httpx_http_inited_set(IN BOOL);
BOOL     utils_socketbase_httpx_https_inited_get(void);
void     utils_socketbase_httpx_https_inited_set(IN BOOL);
SSL_CTX *utils_socketbase_ssl_context_get(void);
void     utils_socketbase_ssl_context_set(IN SSL_CTX *);



/* Definitions */

int	     utils_socketbase_init(
		 void)
{
	tc_UserData_t *task_data = NULL;

	task_data = calloc(1, 1 * sizeof(tc_UserData_t));
	if(task_data == NULL)
	{
		log_print_error("utils_socketbase_init(), could not instantiate the user data!\n");
		return RETURN_ERROR;
	}
	FindTask(NULL)->tc_UserData = task_data;

	return RETURN_OK;
}

void     utils_socketbase_close(
	     void)
{
	if(FindTask(NULL)->tc_UserData != NULL)
	{
		free(FindTask(NULL)->tc_UserData);
		FindTask(NULL)->tc_UserData = NULL;
	}
}

BOOL     utils_socketbase_httpx_http_inited_get(
		 void)
{
	return ((tc_UserData_t *)FindTask(NULL)->tc_UserData)->httpx_http_inited;
}

void     utils_socketbase_httpx_http_inited_set(
		 IN BOOL value)
{
	((tc_UserData_t *)FindTask(NULL)->tc_UserData)->httpx_http_inited = value;
}

BOOL     utils_socketbase_httpx_https_inited_get(
		 void)
{
	return ((tc_UserData_t *)FindTask(NULL)->tc_UserData)->httpx_https_inited;
}

void     utils_socketbase_httpx_https_inited_set(
		 IN BOOL value)
{
	((tc_UserData_t *)FindTask(NULL)->tc_UserData)->httpx_https_inited = value;
}

SSL_CTX *utils_socketbase_ssl_context_get(
		 void)
{
	SSL_CTX *ssl_context = NULL;

	ssl_context = ((tc_UserData_t *)FindTask(NULL)->tc_UserData)->ssl_context;
	if(ssl_context == NULL)
	{
		// printf("%s: ssl_context is NULL!\n", FindTask(NULL)->tc_Node.ln_Name);
		log_print_error("utils_socketbase_init(), ssl_context is NULL!\n");
	}
	return ssl_context;
}

void     utils_socketbase_ssl_context_set(
		 IN SSL_CTX *ssl_context)
{
	((tc_UserData_t *)FindTask(NULL)->tc_UserData)->ssl_context = (SSL_CTX *)ssl_context;
}

