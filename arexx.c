/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include "arexx.h"

#include "locale.h"
#include "log.h"
#include "z_fortify.h"

#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/rexxsyslib.h>
#include <stdio.h>



/* Constants and declarations */

#define MAXIMUM_WAITING_SECONDS 5

int arexx_send_command(IN port_getter, IN char *, IN char *, /*@null@*/ OUT char **);



/* Definitions */

/*@null@*/
struct RxsLib *RexxSysBase  = NULL;



int arexx_setup(
	void)
{
	log_print_debug("arexx_setup()\n");

	if(RexxSysBase == NULL)
	{
		if((RexxSysBase = (struct RxsLib *)OpenLibrary("rexxsyslib.library", 0)) == NULL)
		{
			log_print_error( GetString( MSG_PLAYERS_PLAYERSINITCONNECTIONCOULDNOTOPENREXXSYSLIBLIBRARYV0 ) );
			return RETURN_ERROR;
		}
	}

	return RETURN_OK;
}

void arexx_cleanup(
	 void)
{
	log_print_debug("arexx_cleanup()\n");

	if(RexxSysBase != NULL)
	{
		CloseLibrary((struct Library *)RexxSysBase);
		RexxSysBase = NULL;
	}
}

int arexx_send_command(
	IN  port_getter   port_getter_fp,
	IN  char         *command,
	IN  char         *message,
	/*@null@*/
	OUT char        **result)
{
	char           *command_temp   = NULL;
	char           *args_string    = NULL;
	struct MsgPort *reply_port     = NULL;
	struct RexxMsg *rexx_msg       = NULL;
	struct MsgPort *arexx_port     = NULL;
	BOOL            msg_received   = FALSE;
	int             wait_count     = 0;
	long		    returned_code  = 0;
	char		   *returned_value = NULL;

	if(port_getter_fp == NULL || command == NULL || strlen(command) == 0)
	{
		goto _RETURN_ERROR;
	}

	if(message == NULL)
	{
		command_temp    = malloc((strlen(command) + 1) * sizeof(char));
		if(command_temp == NULL)
		{
			goto _RETURN_ERROR;
		}
		strcpy(command_temp, command);
	}
	else
	{
		command_temp    = malloc((strlen(command) + 1 + strlen(message) + 1) * sizeof(char));
		if(command_temp == NULL)
		{
			goto _RETURN_ERROR;
		}
		strcpy(command_temp, command);
		strcat(command_temp, " ");
		strcat(command_temp, message);
	}

	args_string = CreateArgstring(command_temp, strlen(command_temp));
	if(args_string == NULL)
	{
		goto _RETURN_ERROR;
	}

	if((reply_port = CreateMsgPort()) == NULL)
	{
		goto _RETURN_ERROR;
	}

	if((rexx_msg = CreateRexxMsg(reply_port, NULL, NULL)) == NULL)
	{
		goto _RETURN_ERROR;
	}
	rexx_msg->rm_Action  = RXCOMM | RXFF_RESULT;
	rexx_msg->rm_Args[0] = args_string;
	// Tygre 2019/02/05: Bug!
	// ln_Name must be set to REXX
	// See http://www.amigans.net/modules/xforum/viewtopic.php?post_id=94610#forumpost94610
	rexx_msg->rm_Node.mn_Node.ln_Name = "REXX";

	// Tygre 2023/01/15: Waiting for Godot...
	// By pure chance, I found out that, at a
	// very particular time, quitting EaglePlayer
	// while playing would "lock" AmiModRadio:
	// EaglePlayer would not reply to the RexxMsg
	// and WaitPort() would never return. Thanks
	// to great help on EAB, there's a solution!
	// 		http://eab.abime.net/showthread.php?p=1589440
	// The solution to protect from any possible case:
	Forbid();
	if((arexx_port = port_getter_fp()) == NULL)
	{
		Permit();
		goto _RETURN_ERROR;
	}
	PutMsg(arexx_port, (struct Message *)rexx_msg);
	Permit();
	// Can't use WaitPort() if a program misbehaves:
	// 	WaitPort(reply_port);
	// 	GetMsg(reply_port);
	// Instead, I actively listen and wait until
	// the MAXIMUM_WAITING_TIME or a message:
	while(wait_count < (MAXIMUM_WAITING_SECONDS * 10) && msg_received == FALSE)
	{
		if(GetMsg(reply_port) == NULL)
		{
			Delay(50 / 10);
			wait_count++;
		}
		else
		{
			msg_received = TRUE;
		}
	}
	// If I didn't get a message in reply
	// and there's still no message for me
	// then I set the reply port to NULL
	// so that none can reply to it.
	Forbid();
	if(msg_received == FALSE && GetMsg(reply_port) == NULL)
	{
		rexx_msg->rm_Node.mn_ReplyPort = NULL;
	}
	Permit();

	returned_code    = rexx_msg->rm_Result1;
	if(returned_code == 0 && rexx_msg->rm_Result2 > 0)
	{
		returned_value = (char *)rexx_msg->rm_Result2;
		if(result != NULL)
		{
			*result = malloc((strlen(returned_value) + 1) * sizeof(char));
			if(*result == NULL)
			{
				goto _RETURN_ERROR;
			}
			strcpy(*result, returned_value);
		}
		DeleteArgstring(returned_value);
	}

	goto _RETURN_OK;
	_RETURN_OK:
		free(command_temp);
		DeleteArgstring(args_string);
		DeleteRexxMsg(rexx_msg);
		DeleteMsgPort(reply_port);
		if(returned_code == 0)
		{
			return RETURN_OK;
		}
		else
		{
			return RETURN_WARN;
		}

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(command_temp != NULL)
		{
			free(command_temp);
		}
		if(args_string != NULL)
		{
			DeleteArgstring(args_string);
		}
		if(rexx_msg != NULL)
		{
		   DeleteRexxMsg(rexx_msg);
		}
		if(reply_port != NULL)
		{
			DeleteMsgPort(reply_port);
		}
		return RETURN_ERROR;
}
