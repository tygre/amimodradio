/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



#ifndef UTILS_TASKS_H
#define UTILS_TASKS_H 1

#include "utils.h"

#include <exec/types.h>    // For ULONG
#include <dos/dosextens.h> // For struct Task

int          utils_tasks_setup(
	         void);

void         utils_tasks_cleanup(
	         void);

int          utils_tasks_allocate_signal(
	         OUT ULONG *signal,
	         OUT ULONG *signalset,
	         IN  char  *signal_name);

void         utils_tasks_free_signal(
	         IN  ULONG  signal);

void         utils_tasks_send_signal_without_ack(
			 IN  struct Task *receiver,
	         IN  ULONG        signalset);

void         utils_tasks_send_signal_and_wait_ack(
			 IN  struct Task *receiver,
	         IN  ULONG        signalset,
	         IN  ULONG        signalset_ack);

void         utils_tasks_send_signal_and_wait_ack_from_last_sender(
	         IN  ULONG  signalset,
	         IN  ULONG  signalset_ack);

void         utils_tasks_send_back_ack(
	         void);

struct Task *utils_tasks_get_main_task(
			 void);

char        *utils_tasks_get_task_name(
			 IN  struct Task *task);

char        *utils_tasks_get_current_task_name(
	         void);

char        *utils_tasks_get_signalset_name(
	         IN ULONG signalset);

ULONG		 utils_tasks_signal_task_to_do(
			 void);

ULONG		 utils_tasks_signal_task_to_wait(
			 void);

ULONG		 utils_tasks_signal_task_to_stop(
			 void);

ULONG		 utils_tasks_signal_control_task_stopped(
			 void);

#endif

