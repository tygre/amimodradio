/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



#ifndef UTILS_GUIS_H
#define UTILS_GUIS_H 1

#include "utils.h"               // For IN and OUT
#include <workbench/workbench.h> // For struct DiskObject

int utils_guis_get_image_path(
				   IN  char *current_directory,
				   IN  char *image_mui_prefix,
				   IN  char *image_name,
				   OUT char *image_path);

int utils_guis_get_background_image_path(
				   IN  char *current_directory,
				   IN  char *image_mui_prefix,
				   IN  char *image_name,
				   OUT char *image_path);

struct DiskObject *utils_guis_get_disk_object(
				   void);

#endif
