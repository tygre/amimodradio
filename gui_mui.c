 /*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include "gui_mui.h"

#include "controls_mui.h"
#include "globals.h"
#include "locale.h"
#include "log.h"
#include "prefs.h"
#include "utils.h"
#include "utils_guis.h"
#include "version.h"
#include "z_fortify.h"

#include <stdio.h>
// #include <exec/memory.h>      // Only needed with NDK v3.1: For MEMF_PUBLIC and MEMF_CLEAR
#include <libraries/asl.h>       // Only for ASL_FileRequest
#include <libraries/mui.h>
// #include <utility/hooks.h>    // Only needed with NDK v3.1: For the declaration of HookEntry below
#include <proto/alib.h>          // For DoMethod()
#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/icon.h>
#include <proto/intuition.h>
#include <proto/muimaster.h>
#include <workbench/workbench.h>



/* Declarations and constants */

#define MAKE_ID(a, b, c, d) ((ULONG) (a)<<24 | (ULONG) (b)<<16 | (ULONG) (c)<<8 | (ULONG) (d))
#define MUI_OBSOLETE

// extern ULONG HookEntry(struct Hook *hookPtr, Object *obj, APTR message); // Only needed with NDK v3.1

	   int   gui_mui_init_ui(OUT struct MUI_UI **);
	   void  gui_mui_close_ui(IN struct MUI_UI **);
	   void  gui_mui_child_window_list_open(IN struct MUI_UI *, IN char *, IN char *, IN char *, OUT APTR *, OUT APTR *, IN gui_mui_child_window_clicked_callback_fp, IN gui_mui_child_window_closed_callback_fp);
	   void  gui_mui_child_window_list_update(IN APTR, IN char *);
static void	 _child_window_clicked(struct Hook *, Object *, APTR *);
static void  _child_window_closed(struct Hook *, Object *, APTR *);



/* Definitions */

static char /*@only@*/    *_IM_label_logo_spec                  = NULL;
static char /*@only@*/    *_IM_label_play_spec                  = NULL;
static char /*@only@*/    *_IM_label_replay_spec                = NULL;
static char /*@only@*/    *_IM_label_pause_spec                 = NULL;
static char /*@only@*/    *_IM_label_stop_spec                  = NULL;
static char /*@only@*/    *_IM_label_next_spec                  = NULL;
static char /*@only@*/    *_IM_label_root_directory_prev_spec   = NULL;
static char /*@only@*/    *_IM_label_root_directory_next_spec   = NULL;
static char /*@only@*/    *_IM_label_root_directory_reset_spec  = NULL;
static char /*@only@*/    *_IM_label_directory_list_spec        = NULL;
static char /*@only@*/    *_IM_label_directory_ban_spec         = NULL;
static char /*@only@*/    *_IM_label_directories_ban_list_spec  = NULL;
static char /*@only@*/    *_IM_label_directory_set_as_root_spec = NULL;
static char /*@only@*/    *_IM_label_file_list_spec             = NULL;
static char /*@only@*/    *_IM_label_file_ban_spec        	    = NULL;
static char /*@only@*/    *_IM_label_files_ban_list_spec        = NULL;
static char /*@only@*/    *_IM_label_modules_save_spec          = NULL;
static char /*@only@*/    *_IM_label_modules_email_spec         = NULL;
static char /*@only@*/    *_IM_controls_background_spec		    = NULL;
static char /*@only@*/    *_current_directory_name              = NULL;
static char /*@only@*/    *_pages_names[4]                      = { NULL };
static char /*@only@*/   **_CY_label_maximum_log_sizes          = NULL;
static char /*@only@*/   **_CY_label_maximum_file_sizes         = NULL;
static int                 _window_id                           = 0;

int gui_mui_init_ui(OUT struct MUI_UI **ui_object)
{
	struct MUI_UI            *object                            		     = NULL;
	APTR					  GR_grp_controls            		   		     = NULL;
	APTR					  GR_grp_controls_buttons           		     = NULL;
	APTR					  GR_control_buttons                		     = NULL;
	APTR					  GR_grp_root_directory             		     = NULL;
	APTR					  GR_grp_directory                  		     = NULL;
	APTR					  GR_grp_file                       		     = NULL;
	APTR					  GR_grp_modules                    		     = NULL;
	APTR					  GR_grp_modules_ratings            		     = NULL;
	APTR					  GR_grp_preferences                		     = NULL;
	APTR                      GR_grp_log_controls                            = NULL;
	APTR                      BT_prefs_save                                  = NULL;
	APTR                      BT_prefs_reset                                 = NULL;
	APTR                      BT_prefs_default                               = NULL;
	APTR                      BT_log_save                                    = NULL;
	APTR                      BT_log_clear                                   = NULL;
	char					  image_path[GLOBALS_MAX_LINE_LENGTH]            = { 0 };
	static const struct Hook  controls_mui_doublestart_hook      		     = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)controls_mui_doublestart,                   NULL };
	static const struct Hook  controls_mui_about_hook            		     = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)controls_mui_about,                         NULL };
	static const struct Hook  controls_mui_play_hook             		     = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)controls_mui_play,                          NULL };
	static const struct Hook  controls_mui_replay_hook           		     = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)controls_mui_replay,                        NULL };
	static const struct Hook  controls_mui_pause_hook            		     = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)controls_mui_pause,                         NULL };
	static const struct Hook  controls_mui_stop_hook             		     = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)controls_mui_stop,                          NULL };
	static const struct Hook  controls_mui_next_hook             		     = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)controls_mui_next,                          NULL };
	static const struct Hook  controls_mui_root_directory_prev_hook   	     = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)controls_mui_root_directory_prev,           NULL };
	static const struct Hook  controls_mui_root_directory_next_hook    	     = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)controls_mui_root_directory_next,           NULL };
	static const struct Hook  controls_mui_root_directory_reset_hook	     = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)controls_mui_root_directory_reset,          NULL };
	static const struct Hook  controls_mui_directory_list_hook   		     = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)controls_mui_directory_list,                NULL };
	static const struct Hook  controls_mui_directory_ban_hook    		     = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)controls_mui_directory_ban,                 NULL };
	static const struct Hook  controls_mui_directories_ban_list_open_hook    = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)controls_mui_open_directories_ban_list,     NULL };
	static const struct Hook  controls_mui_directory_set_as_root_hook	     = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)controls_mui_directory_set_as_root,         NULL };
	static const struct Hook  controls_mui_file_list_hook        		     = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)controls_mui_file_list,                     NULL };
	static const struct Hook  controls_mui_file_ban_hook         		     = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)controls_mui_file_ban,                      NULL };
	static const struct Hook  controls_mui_files_ban_list_open_hook	         = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)controls_mui_open_files_ban_list,           NULL };
	static const struct Hook  controls_mui_modules_save_hook     		     = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)controls_mui_modules_save,                  NULL };
	static const struct Hook  controls_mui_change_directory_hook 		     = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)controls_mui_change_directory,              NULL };
	static const struct Hook  controls_mui_modules_email_hook    		     = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)controls_mui_modules_email,                 NULL };
	static const struct Hook  controls_mui_set_user_rating_hook			     = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)controls_mui_set_user_rating,               NULL };
	static const struct Hook  controls_mui_keyboard_space_hook		         = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)controls_mui_keyboard_space,                NULL };
	static const struct Hook  mui_prefs_play_on_startup_hook                 = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)mui_prefs_play_on_startup,                  NULL };
	static const struct Hook  mui_prefs_show_log_page_on_fetch_hook          = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)mui_prefs_show_log_page_on_fetch,           NULL };
	static const struct Hook  mui_prefs_maximum_log_size_changed_hook        = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)mui_prefs_maximum_log_size_changed,         NULL };
	static const struct Hook  mui_prefs_remember_source_between_runs_hook    = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)mui_prefs_remember_source_between_runs,     NULL };
	static const struct Hook  mui_prefs_saved_modules_directory_changed_hook = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)mui_prefs_saved_modules_directory_changed,  NULL };
	static const struct Hook  mui_prefs_cache_list_during_run_hook           = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)mui_prefs_cache_list_during_run,            NULL };
	static const struct Hook  mui_prefs_cache_list_between_runs_hook         = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)mui_prefs_cache_list_between_runs,          NULL };
	static const struct Hook  mui_prefs_cache_directory_changed_hook         = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)mui_prefs_cache_directory_changed,          NULL };
	static const struct Hook  mui_prefs_save_prefs_on_exit_hook              = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)mui_prefs_save_prefs_on_exit,               NULL };
	static const struct Hook  mui_prefs_remember_images_set_hook             = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)mui_prefs_remember_images_set,              NULL };
	static const struct Hook  mui_prefs_skip_if_banned_hook                  = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)mui_prefs_skip_if_banned,                   NULL };
	static const struct Hook  mui_prefs_skip_if_user_rating_low_hook         = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)mui_prefs_skip_if_user_rating_low,          NULL };
	static const struct Hook  mui_prefs_skip_if_all_rating_exists_hook       = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)mui_prefs_skip_if_all_rating_exists,        NULL };
	static const struct Hook  mui_prefs_skip_if_all_rating_low_hook          = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)mui_prefs_skip_if_all_rating_low,           NULL };
	static const struct Hook  mui_prefs_skip_if_above_maximum_file_size_hook = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)mui_prefs_skip_if_above_maximum_file_size,  NULL };
	static const struct Hook  mui_prefs_maximum_file_size_changed_hook       = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)mui_prefs_maximum_file_size_changed,        NULL };
	static const struct Hook  mui_prefs_save_hook                            = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)mui_prefs_save,                             NULL };
	static const struct Hook  mui_prefs_reset_hook                           = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)mui_prefs_reset,                            NULL };
	static const struct Hook  mui_prefs_default_hook                         = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)mui_prefs_default,                          NULL };
	static const struct Hook  mui_log_save_hook                              = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)mui_log_save,                               NULL };
	static const struct Hook  mui_log_clear_hook                             = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)mui_log_clear,                              NULL };
	int                       i                                              = 0;
	char                     *description_temp                               = NULL;
	int                       description_temp_length                        = 0;
	char                      description[40]                                = { 0 };

	log_print_debug("gui_mui_init_ui()\n");

	if((object = AllocVec(sizeof(struct MUI_UI), MEMF_PUBLIC|MEMF_CLEAR)) == NULL)
	{
		log_print_error("gui_mui_init_ui(), could not allocate MUI UI\n");
		*ui_object = NULL;
		return RETURN_ERROR;
	}
	
	/*
	 * Various initialisations.
	 */
	
	object->LV_label_log_content = calloc(
		atoi(prefs_get_list_of_maximum_log_sizes()[prefs_get_maximum_log_size_index()]) * BYTES_IN_ONE_KILO_BYTE, /* Flawfinder: ignore */
		sizeof(char));
	if(object->LV_label_log_content == NULL)
	{
		log_print_error( GetString( MSG_MUI_MUIINITUICOULDNOTALLOCATEDEFAULTLOGMESSAGE ) );
		return RETURN_ERROR;
	}

	_current_directory_name = malloc(GLOBALS_MAX_LINE_LENGTH * sizeof(char));
	if(_current_directory_name == NULL)
	{
		log_print_error("gui_mui_init_ui(), could not allocate current directory name\n");
		return RETURN_ERROR;
	}
	GetCurrentDirName(_current_directory_name, GLOBALS_MAX_LINE_LENGTH);
	
	_pages_names[0] = GetString(MSG_MUI_CONTROLS);
	_pages_names[1] = GetString(MSG_MUI_PREFS);
	_pages_names[2] = GetString(MSG_MUI_LOG);

	// Tygre 22/12/12: Size matters!
	// CxObject description should be not longer than CBD_DESCRLEN.
	description_temp        =  GetString( MSG_MUI_ALLOFAMINETMODULESATYOURFINGERTIPS ) ;
	description_temp_length = strlen(description_temp);
	for(i = 0; i < description_temp_length && i < CBD_DESCRLEN; i++)
	{
		description[i] = description_temp[i];
	}
	description[i - 1] = '\0';

	/*
	 * The definition of the commands page.
	 */

	// Tygre 2015/06/20: Duplication...
	// I must duplicate the image_path to have different images.
	utils_guis_get_image_path(_current_directory_name, "5:", "Radio107x107.iff", image_path);
	_IM_label_logo_spec = string_duplicate(image_path);
	object->IM_label_logo = ImageObject,
		MUIA_CycleChain,      TRUE,
		MUIA_Frame,           MUIV_Frame_ImageButton,
		MUIA_Image_Spec,      _IM_label_logo_spec,
		MUIA_Image_FreeVert,  TRUE,
		MUIA_Image_FreeHoriz, TRUE,
		MUIA_FixHeight,       107,
		MUIA_FixWidth,        107,
		MUIA_InputMode,       MUIV_InputMode_RelVerify,
		MUIA_HelpNode,        "Controls-About",
		MUIA_ShortHelp,       GetString( MSG_MUI_DISPLAYPROGRAMINFORMATION ) ,
	End;

	utils_guis_get_image_path(_current_directory_name, "5:", "Play40x40.iff", image_path);
	_IM_label_play_spec = string_duplicate(image_path);
	object->IM_label_play = ImageObject,
		MUIA_CycleChain,      TRUE,
		MUIA_Frame,           MUIV_Frame_ImageButton,
		MUIA_Image_Spec,      _IM_label_play_spec,
		MUIA_Image_FreeVert,  TRUE,
		MUIA_Image_FreeHoriz, TRUE,
		MUIA_FixHeight,       40,
		MUIA_FixWidth,        40,
		MUIA_InputMode,       MUIV_InputMode_RelVerify,
		MUIA_HelpNode,        "Controls-Play",
		MUIA_ShortHelp,       GetString( MSG_MUI_PLAYCURRENTMODULEORFETCHANDPLAYNEWMODULE ) ,
	End;

	utils_guis_get_image_path(_current_directory_name, "5:", "Replay40x40.iff", image_path);
	_IM_label_replay_spec = string_duplicate(image_path);
	object->IM_label_replay = ImageObject,
		MUIA_CycleChain,      TRUE,
		MUIA_Frame,           MUIV_Frame_ImageButton,
		MUIA_Image_Spec,      _IM_label_replay_spec,
		MUIA_Image_FreeVert,  TRUE,
		MUIA_Image_FreeHoriz, TRUE,
		MUIA_FixHeight,       40,
		MUIA_FixWidth,        40,
		MUIA_InputMode,       MUIV_InputMode_RelVerify,
		MUIA_HelpNode,        "Controls-Replay",
		MUIA_ShortHelp,       GetString( MSG_MUI_REPLAYCURRENTMODULEFROMITSBEGINNING ) ,
	End;

	utils_guis_get_image_path(_current_directory_name, "5:", "Pause40x40.iff", image_path);
	_IM_label_pause_spec = string_duplicate(image_path);
	object->IM_label_pause = ImageObject,
		MUIA_CycleChain,      TRUE,
		MUIA_Frame,           MUIV_Frame_ImageButton,
		MUIA_Image_Spec,      _IM_label_pause_spec,
		MUIA_Image_FreeVert,  TRUE,
		MUIA_Image_FreeHoriz, TRUE,
		MUIA_FixHeight,       40,
		MUIA_FixWidth,        40,
		MUIA_InputMode,       MUIV_InputMode_RelVerify,
		MUIA_HelpNode,        "Controls-Pause",
		MUIA_ShortHelp,       GetString( MSG_MUI_PAUSETHEPLAYINGOFTHECURRENTMODULE ) ,
	End;

	utils_guis_get_image_path(_current_directory_name, "5:", "Stop40x40.iff", image_path);
	_IM_label_stop_spec = string_duplicate(image_path);
	object->IM_label_stop = ImageObject,
		MUIA_CycleChain,      TRUE,
		MUIA_Frame,           MUIV_Frame_ImageButton,
		MUIA_Image_Spec,      _IM_label_stop_spec,
		MUIA_Image_FreeVert,  TRUE,
		MUIA_Image_FreeHoriz, TRUE,
		MUIA_FixHeight,       40,
		MUIA_FixWidth,        40,
		MUIA_InputMode,       MUIV_InputMode_RelVerify,
		MUIA_HelpNode,        "Controls-Stop",
		MUIA_ShortHelp,       GetString( MSG_MUI_STOPTHEPLAYINGOFTHECURRENTMODULE ) ,
	End;

	utils_guis_get_image_path(_current_directory_name, "5:", "Next40x40.iff", image_path);
	_IM_label_next_spec = string_duplicate(image_path);
	object->IM_label_next = ImageObject,
		MUIA_CycleChain,      TRUE,
		MUIA_Frame,           MUIV_Frame_ImageButton,
		MUIA_Image_Spec,       _IM_label_next_spec,
		MUIA_Image_FreeVert,  TRUE,
		MUIA_Image_FreeHoriz, TRUE,
		MUIA_FixHeight,       40,
		MUIA_FixWidth,        40,
		MUIA_InputMode,       MUIV_InputMode_RelVerify,
		MUIA_HelpNode,        "Controls-Next",
		MUIA_ShortHelp,       GetString( MSG_MUI_FETCHANDPLAYNEWMODULE ) ,
	End;

	GR_control_buttons = GroupObject,
		MUIA_Group_Horiz, TRUE,
		Child, object->IM_label_play,
		Child, object->IM_label_replay,
		Child, object->IM_label_pause,
		Child, object->IM_label_stop,
		Child, object->IM_label_next,
	End;

	object->TX_label_root_directory = TextObject,
		MUIA_CycleChain,  TRUE,
		MUIA_Frame,       MUIV_Frame_Text,
		MUIA_Text_SetMin, TRUE,
		MUIA_HelpNode,    "Controls-Root-Directory",
		MUIA_ShortHelp,   GetString( MSG_MUI_DISPLAYSOURCEANDROOTDIRECTORYINWHICHDIRECTORIESAREFETCH ) ,
	End;

	utils_guis_get_image_path(_current_directory_name, "5:", "Reset10x10.iff", image_path);
	_IM_label_root_directory_reset_spec = string_duplicate(image_path);
	object->IM_label_root_directory_reset = ImageObject,
		MUIA_CycleChain,      TRUE,
		MUIA_Frame,           MUIV_Frame_ImageButton,
		MUIA_Image_Spec,      _IM_label_root_directory_reset_spec,
		MUIA_Image_FreeVert,  TRUE,
		MUIA_Image_FreeHoriz, TRUE,
		MUIA_FixHeight,       10,
		MUIA_FixWidth,        10,
		MUIA_InputMode,       MUIV_InputMode_RelVerify,
		MUIA_HelpNode,        "Controls-Reset-Root-Directory",
		MUIA_ShortHelp,       GetString( MSG_MUI_RESETROOTDIRECTORYTOSOURCESDEFAULT ) ,
	End;

	utils_guis_get_image_path(_current_directory_name, "5:", "Prev10x10.iff", image_path);
	_IM_label_root_directory_prev_spec = string_duplicate(image_path);
	object->IM_label_root_directory_prev = ImageObject,
		MUIA_CycleChain, TRUE,
		MUIA_Frame,           MUIV_Frame_ImageButton,
		MUIA_Image_Spec,      _IM_label_root_directory_prev_spec,
		MUIA_Image_FreeVert,  TRUE,
		MUIA_Image_FreeHoriz, TRUE,
		MUIA_FixHeight,       10,
		MUIA_FixWidth,        10,
		MUIA_InputMode,       MUIV_InputMode_RelVerify,
		MUIA_HelpNode,        "Controls-Prev-Root-Directory",
		MUIA_ShortHelp,       GetString( MSG_MUI_CHANGESOURCETOUSEPREVIOUSSOURCE ) ,
	End;

	utils_guis_get_image_path(_current_directory_name, "5:", "Next10x10.iff", image_path);
	_IM_label_root_directory_next_spec = string_duplicate(image_path);
	object->IM_label_root_directory_next = ImageObject,
		MUIA_CycleChain,      TRUE,
		MUIA_Frame,           MUIV_Frame_ImageButton,
		MUIA_Image_Spec,      _IM_label_root_directory_next_spec,
		MUIA_Image_FreeVert,  TRUE,
		MUIA_Image_FreeHoriz, TRUE,
		MUIA_FixHeight,       10,
		MUIA_FixWidth,        10,
		MUIA_InputMode,       MUIV_InputMode_RelVerify,
		MUIA_HelpNode,        "Controls-Next-Root-Directory",
		MUIA_ShortHelp,       GetString( MSG_MUI_CHANGESOURCETOUSENEXTSOURCE ) ,
	End;

	GR_grp_root_directory = GroupObject,
		MUIA_Group_Horiz, TRUE,
		Child, object->TX_label_root_directory,
		Child, object->IM_label_root_directory_reset,
		Child, object->IM_label_root_directory_prev,
		Child, object->IM_label_root_directory_next,
	End;

	object->STR_label_directory = StringObject,
		MUIA_CycleChain, TRUE,
		MUIA_Frame,      MUIV_Frame_String,
		MUIA_HelpNode,   "Controls-Current-Directory",
		MUIA_ShortHelp,  GetString( MSG_MUI_DISPLAYTHEDIRECTORYINWHICHMODULESAREFETCHED ) ,
	End;

	utils_guis_get_image_path(_current_directory_name, "5:", "Set10x10.iff", image_path);
	_IM_label_directory_set_as_root_spec = string_duplicate(image_path);
	object->IM_label_directory_set_as_root = ImageObject,
		MUIA_CycleChain,      TRUE,
		MUIA_Frame,           MUIV_Frame_ImageButton,
		MUIA_Image_Spec,      _IM_label_directory_set_as_root_spec,
		MUIA_Image_FreeVert,  TRUE,
		MUIA_Image_FreeHoriz, TRUE,
		MUIA_FixHeight,       10,
		MUIA_FixWidth,        10,
		MUIA_InputMode,       MUIV_InputMode_RelVerify,
		MUIA_HelpNode,        "Controls-Set-Current-Directory-As-Root",
		MUIA_ShortHelp,       GetString( MSG_MUI_SETTHISDIRECTORYASROOTDIRECTORY ) ,
	End;

	utils_guis_get_image_path(_current_directory_name, "5:", "List10x10.iff", image_path);
	_IM_label_directory_list_spec = string_duplicate(image_path);
	object->IM_label_directory_list = ImageObject,
		MUIA_CycleChain,      TRUE,
		MUIA_Frame,           MUIV_Frame_ImageButton,
		MUIA_Image_Spec,      _IM_label_directory_list_spec,
		MUIA_Image_FreeVert,  TRUE,
		MUIA_Image_FreeHoriz, TRUE,
		MUIA_FixHeight,       10,
		MUIA_FixWidth,        10,
		MUIA_InputMode,       MUIV_InputMode_RelVerify,
		MUIA_HelpNode,        "Controls-List-Current-Directory",
		MUIA_ShortHelp,       GetString( MSG_MUI_LISTTHECONTENTOFTHISDIRECTORY ) ,
	End;

	utils_guis_get_image_path(_current_directory_name, "5:", "Ban10x10.iff", image_path);
	_IM_label_directory_ban_spec = string_duplicate(image_path);
	object->IM_label_directory_ban = ImageObject,
		MUIA_CycleChain,      TRUE,
		MUIA_Frame,           MUIV_Frame_ImageButton,
		MUIA_Image_Spec,      _IM_label_directory_ban_spec,
		MUIA_Image_FreeVert,  TRUE,
		MUIA_Image_FreeHoriz, TRUE,
		MUIA_FixHeight,       10,
		MUIA_FixWidth,        10,
		MUIA_InputMode,       MUIV_InputMode_RelVerify,
		MUIA_HelpNode,        "Controls-Ban-Current-Directory",
		MUIA_ShortHelp,       GetString( MSG_MUI_BANTHISDIRECTORYTONEVERFETCHMODULESINIT ) ,
	End;

	utils_guis_get_image_path(_current_directory_name, "5:", "List10x10.iff", image_path);
	_IM_label_directories_ban_list_spec = string_duplicate(image_path);
	object->IM_label_directories_ban_list = ImageObject,
		MUIA_CycleChain,      TRUE,
		MUIA_Frame,           MUIV_Frame_ImageButton,
		MUIA_Image_Spec,      _IM_label_directories_ban_list_spec,
		MUIA_Image_FreeVert,  TRUE,
		MUIA_Image_FreeHoriz, TRUE,
		MUIA_FixHeight,       10,
		MUIA_FixWidth,        10,
		MUIA_InputMode,       MUIV_InputMode_RelVerify,
		MUIA_HelpNode,        "Controls-List-Banned-Directories",
		MUIA_ShortHelp,       GetString( MSG_MUI_LISTANYBANNEDDIRECTORIES ) ,
	End;

	GR_grp_directory = GroupObject,
		MUIA_Group_Horiz, TRUE,
		Child, object->STR_label_directory,
		Child, object->IM_label_directory_set_as_root,
		Child, object->IM_label_directory_list,
		Child, object->IM_label_directory_ban,
		Child, object->IM_label_directories_ban_list,
	End;

	object->STR_label_file = TextObject,
		MUIA_Frame,     MUIV_Frame_Text,
		MUIA_HelpNode,  "Controls-Current-File",
		MUIA_ShortHelp, GetString( MSG_MUI_DISPLAYTHEARCHIVEFILEFROMWHICHMODULESAREPLAYED ) ,
	End;

	utils_guis_get_image_path(_current_directory_name, "5:", "List10x10.iff", image_path);
	_IM_label_file_list_spec = string_duplicate(image_path);
	object->IM_label_file_list = ImageObject,
		MUIA_CycleChain,      TRUE,
		MUIA_Frame,           MUIV_Frame_ImageButton,
		MUIA_Image_Spec,      _IM_label_file_list_spec,
		MUIA_Image_FreeVert,  TRUE,
		MUIA_Image_FreeHoriz, TRUE,
		MUIA_FixHeight,       10,
		MUIA_FixWidth,        10,
		MUIA_InputMode,       MUIV_InputMode_RelVerify,
		MUIA_HelpNode,        "Controls-List-Current-File",
		MUIA_ShortHelp,       GetString( MSG_MUI_LISTTHECONTENTOFTHISARCHIVEFILE ) ,
	End;

	utils_guis_get_image_path(_current_directory_name, "5:", "Ban10x10.iff", image_path);
	_IM_label_file_ban_spec = string_duplicate(image_path);
	object->IM_label_file_ban = ImageObject,
		MUIA_CycleChain,      TRUE,
		MUIA_Frame,           MUIV_Frame_ImageButton,
		MUIA_Image_Spec,      _IM_label_file_ban_spec,
		MUIA_Image_FreeVert,  TRUE,
		MUIA_Image_FreeHoriz, TRUE,
		MUIA_FixHeight,       10,
		MUIA_FixWidth,        10,
		MUIA_InputMode,       MUIV_InputMode_RelVerify,
		MUIA_HelpNode,        "Controls-Ban-Current-File",
		MUIA_ShortHelp,       GetString( MSG_MUI_BANTHISARCHIVEFILETONEVERPLAYMODULESFROMIT ) ,
	End;

	utils_guis_get_image_path(_current_directory_name, "5:", "List10x10.iff", image_path);
	_IM_label_files_ban_list_spec = string_duplicate(image_path);
	object->IM_label_files_ban_list = ImageObject,
		MUIA_CycleChain,      TRUE,
		MUIA_Frame,           MUIV_Frame_ImageButton,
		MUIA_Image_Spec,      _IM_label_files_ban_list_spec,
		MUIA_Image_FreeVert,  TRUE,
		MUIA_Image_FreeHoriz, TRUE,
		MUIA_FixHeight,       10,
		MUIA_FixWidth,        10,
		MUIA_InputMode,       MUIV_InputMode_RelVerify,
		MUIA_HelpNode,        "Controls-List-Banned-Files",
		MUIA_ShortHelp,       GetString( MSG_MUI_LISTANYBANNEDFILES ) ,
	End;

	GR_grp_file = GroupObject,
		MUIA_Group_Horiz, TRUE,
		Child, object->STR_label_file,
		Child, object->IM_label_file_list,
		Child, object->IM_label_file_ban,
		Child, object->IM_label_files_ban_list,
	End;

	object->TX_label_modules = TextObject,
		MUIA_Frame,       MUIV_Frame_Text,
		MUIA_Text_SetMin, TRUE,
		MUIA_HelpNode,    "Controls-Modules",
		MUIA_ShortHelp,   GetString( MSG_MUI_DISPLAYTHEMODULESBEINGPLAYED ) ,
	End;

	utils_guis_get_image_path(_current_directory_name, "5:", "Save10x10.iff", image_path);
	_IM_label_modules_save_spec = string_duplicate(image_path);
	object->IM_label_modules_save = ImageObject,
		MUIA_CycleChain,      TRUE,
		MUIA_Frame,           MUIV_Frame_ImageButton,
		MUIA_Image_Spec,      _IM_label_modules_save_spec,
		MUIA_Image_FreeVert,  TRUE,
		MUIA_Image_FreeHoriz, TRUE,
		MUIA_FixHeight,       10,
		MUIA_FixWidth,        10,
		MUIA_InputMode,       MUIV_InputMode_RelVerify,
		MUIA_HelpNode,        "Controls-Save-Modules",
		MUIA_ShortHelp,       GetString( MSG_MUI_SAVETODISKTHEMODULESBEINGPLAYED ) ,
	End;

	utils_guis_get_image_path(_current_directory_name, "5:", "Mail10x10.iff", image_path);
	_IM_label_modules_email_spec = string_duplicate(image_path);
	object->IM_label_modules_email = ImageObject,
		MUIA_CycleChain,      TRUE,
		MUIA_Frame,           MUIV_Frame_ImageButton,
		MUIA_Image_Spec,      _IM_label_modules_email_spec,
		MUIA_Image_FreeVert,  TRUE,
		MUIA_Image_FreeHoriz, TRUE,
		MUIA_FixHeight,       10,
		MUIA_FixWidth,        10,
		MUIA_InputMode,       MUIV_InputMode_RelVerify,
		MUIA_HelpNode,        "Controls-Email-Modules",
		MUIA_ShortHelp,       GetString( MSG_MUI_SENDANEMAILWITHTHEMODULESBEINGPLAYED ) ,
	End;

	GR_grp_modules = GroupObject,
		MUIA_Group_Horiz, TRUE,
		Child, object->TX_label_modules,
		Child, object->IM_label_modules_save,
		Child, object->IM_label_modules_email,
	End;

	// Special for the ratings
	for(i = 1; i < 6; i++)
	{
		string_itoa(i, 10, description);

		object->IM_label_user_rating[i - 1] = TextObject,
			MUIA_CycleChain,    TRUE,
			MUIA_Frame,         MUIV_Frame_ImageButton,
			MUIA_Text_Contents, description,
			MUIA_Text_HiChar,   i + '0',
			MUIA_ControlChar,   i + '0',
			MUIA_Text_SetMin,   TRUE,
			MUIA_InputMode,     MUIV_InputMode_RelVerify,
			MUIA_HelpNode,      "Controls-User-Rating",
			MUIA_ShortHelp,     GetString( MSG_MUI_YOURRATING ) ,
		End;
	}
	for(i = 1; i < 6; i++)
	{
		string_itoa(i, 10, description);
		
		object->TX_label_all_rating[i - 1] = TextObject,
			MUIA_Frame,         MUIV_Frame_Text,
			MUIA_Text_Contents, description,
			MUIA_Text_SetMin,   TRUE,
			MUIA_HelpNode,      "Controls-All-Rating",
			MUIA_ShortHelp,     GetString( MSG_MUI_RATINGFROMTHECOMMUNITY ) ,
		End;
	}
	object->TX_label_all_tally = TextObject,
		MUIA_Frame,         MUIV_Frame_Text,
		MUIA_Text_Contents, "0  ",
		MUIA_Text_SetMin,   TRUE,
		MUIA_HelpNode,      "Controls-All-Tally",
		MUIA_ShortHelp,     GetString( MSG_MUI_NUMBEROFVOTESFROMTHECOMMUNITY ) ,
	End;

	GR_grp_modules_ratings = GroupObject,
		MUIA_Group_Horiz, TRUE,
		Child, object->IM_label_user_rating[0],
		Child, object->IM_label_user_rating[1],
		Child, object->IM_label_user_rating[2],
		Child, object->IM_label_user_rating[3],
		Child, object->IM_label_user_rating[4],
		Child, HSpace(0),
		Child, object->TX_label_all_rating[0],
		Child, object->TX_label_all_rating[1],
		Child, object->TX_label_all_rating[2],
		Child, object->TX_label_all_rating[3],
		Child, object->TX_label_all_rating[4],
		Child, object->TX_label_all_tally,
	End;

	GR_grp_controls_buttons = GroupObject,
		MUIA_Group_Rows, 6,
		Child, GR_control_buttons,
		Child, GR_grp_root_directory,
		Child, GR_grp_directory,
		Child, GR_grp_file,
		Child, GR_grp_modules,
		Child, GR_grp_modules_ratings,
	End;

	if(utils_guis_get_background_image_path(
		_current_directory_name,
		"5:",
		"Controls480x280.iff",
		image_path) == RETURN_OK)
	{
		_IM_controls_background_spec = string_duplicate(image_path);
		GR_grp_controls = GroupObject,
			MUIA_Background,  _IM_controls_background_spec,
			MUIA_Group_Horiz, TRUE,
			Child, object->IM_label_logo,
			Child, GR_grp_controls_buttons,
		End;
	}
	else
	{
		GR_grp_controls = GroupObject,
			// No background
			MUIA_Group_Horiz, TRUE,
			Child, object->IM_label_logo,
			Child, GR_grp_controls_buttons,
		End;
	}

	/*
	 * The definition of the prefs page.
	 */

	object->GR_grp_save_prefs_on_exit = GroupObject,
		MUIA_CycleChain,  TRUE,
		MUIA_Group_Horiz, TRUE,
		MUIA_InputMode,   MUIV_InputMode_Toggle,
		MUIA_HelpNode,    "Prefs-Save-Prefs-On-Exit",
		MUIA_ShortHelp,   GetString( MSG_MUI_WHETHERORNOTTOSAVETHEPREFERENCESWHENEXITING ) ,
		Child, ImageObject,
			TextFrame,
			MUIA_Image_Spec,     MUII_CheckMark,
			MUIA_Image_FreeVert, TRUE,
			MUIA_ShowSelState,   FALSE,
		End,
		Child, Label2( GetString( MSG_MUI_SAVEPREFERENCESONEXIT ) ),
		Child, HSpace(0),
	End;

	SetAttrs(
		object->GR_grp_save_prefs_on_exit,
		MUIA_Selected, prefs_should_save_prefs_on_exit(),
		TAG_DONE);

	object->GR_grp_play_on_startup = GroupObject,
		MUIA_CycleChain,  TRUE,
		MUIA_Group_Horiz, TRUE,
		MUIA_InputMode,   MUIV_InputMode_Toggle,
		MUIA_HelpNode,    "Prefs-Play-On-Startup",
		MUIA_ShortHelp,   GetString( MSG_MUI_WHETHERORNOTTOPLAYMODULESRIGHTAFTERSTARTING ) ,
		Child, ImageObject,
			MUIA_Frame,          MUIV_Frame_Text,
			MUIA_Image_Spec,     MUII_CheckMark,
			MUIA_Image_FreeVert, TRUE,
			MUIA_ShowSelState,   FALSE,
		End,
		Child, Label2( GetString( MSG_MUI_PLAYONSTARTUP ) ),
		Child, HSpace(0),
	End;

	SetAttrs(
		object->GR_grp_play_on_startup,
		MUIA_Selected, prefs_should_play_on_startup(),
		TAG_DONE);

	object->GR_grp_show_log_page_on_fetch = GroupObject,
		MUIA_CycleChain,  TRUE,
		MUIA_Group_Horiz, TRUE,
		MUIA_InputMode,   MUIV_InputMode_Toggle,
		MUIA_HelpNode,    "Prefs-Show-Log-Page-On-Fetch",
		MUIA_ShortHelp,   GetString( MSG_MUI_WHETHERORNOTTOSHOWTHELOGPAGEWHILEFETCHINGNEWMODULES ) ,
		Child, ImageObject,
			MUIA_Frame,          MUIV_Frame_Text,
			MUIA_Image_Spec,     MUII_CheckMark,
			MUIA_Image_FreeVert, TRUE,
			MUIA_ShowSelState,   FALSE,
		End,
		Child, Label2( GetString( MSG_MUI_SHOWLOGPAGEONFETCH ) ),
		Child, HSpace(0),
	End;

	SetAttrs(
		object->GR_grp_show_log_page_on_fetch,
		MUIA_Selected, prefs_should_show_log_page_on_fetch(),
		TAG_DONE);

	_CY_label_maximum_log_sizes = malloc((prefs_get_number_of_maximum_log_sizes() + 1) * sizeof(char *));
	for(i = 0; i < prefs_get_number_of_maximum_log_sizes(); i++)
	{
		_CY_label_maximum_log_sizes[i] = prefs_get_list_of_maximum_log_sizes()[i];
	}
	_CY_label_maximum_log_sizes[prefs_get_number_of_maximum_log_sizes()] = NULL;

	object->CY_label_maximum_log_size = CycleObject,
		MUIA_CycleChain,    TRUE,
		MUIA_Frame,         MUIV_Frame_Button,
		MUIA_Cycle_Active,  prefs_get_maximum_log_size_index(),
		MUIA_Cycle_Entries, _CY_label_maximum_log_sizes,
		MUIA_HelpNode,      "Prefs-Maximum-Log-Size",
	End;

	object->GR_grp_remember_source_between_runs = GroupObject,
		MUIA_CycleChain,  TRUE,
		MUIA_Group_Horiz, TRUE,
		MUIA_InputMode,   MUIV_InputMode_Toggle,
		MUIA_HelpNode,    "Prefs-Remember-Source-Between-Runs",
		MUIA_ShortHelp,   GetString( MSG_MUI_WHETHERORNOTTOREMEMBERTHESOURCEANDROOTDIRECTORYBETWEENR ) ,
		Child, ImageObject,
			MUIA_Frame,          MUIV_Frame_Text,
			MUIA_Image_Spec,     MUII_CheckMark,
			MUIA_Image_FreeVert, TRUE,
			MUIA_ShowSelState,   FALSE,
		End,
		Child, Label2( GetString( MSG_MUI_REMEMBERSOURCEBETWEENRUNS ) ),
		Child, HSpace(0),
	End;

	SetAttrs(
		object->GR_grp_remember_source_between_runs,
		MUIA_Selected, prefs_should_remember_source_between_runs(),
		TAG_DONE);

	object->GR_grp_remember_images_set = HGroup,
		MUIA_Cycle_Active, prefs_get_current_images_set_index(),
		MUIA_HelpNode,     "Prefs-Images-Set",
		MUIA_ShortHelp,    GetString( MSG_MUI_CHOICEOFSKINSINCLUDINGBUTTONSBACKGROUND ) ,
		Child, Label2( GetString( MSG_MUI_IMAGESSETS ) ),
		Child, CycleObject,
			MUIA_CycleChain,    TRUE,
			MUIA_Font,          MUIV_Font_Button,
			MUIA_Cycle_Entries, prefs_get_list_of_images_sets(),
		End,
		Child, HSpace(0),
	End;

	object->STR_label_saved_modules_directory = String(prefs_get_saved_modules_directory(), 80);
	object->PA_label_saved_modules_directory = PopaslObject,
		MUIA_CycleChain,  	   TRUE,
		MUIA_Popasl_Type,      ASL_FileRequest,
		ASLFR_DrawersOnly,     TRUE,
		MUIA_Popstring_String, object->STR_label_saved_modules_directory,
		MUIA_Popstring_Button, PopButton(MUII_PopDrawer),
		// MUIA_HelpNode,      "Prefs-Saved-Modules-Directory-String",
		MUIA_HelpNode,         "Prefs-Saved-Modules-Directory-Requester",
		MUIA_ShortHelp,        GetString( MSG_MUI_CHOICEOFTHEDIRECTORYWHERETOSAVEMODULESBYDEFAULT ) ,
	End;

	object->GR_grp_cache_list_during_run = GroupObject,
		MUIA_CycleChain,  TRUE,
		MUIA_Group_Horiz, TRUE,
		MUIA_InputMode,   MUIV_InputMode_Toggle,
		MUIA_HelpNode,    "Prefs-Cache-List-During-Run",
		MUIA_ShortHelp,   GetString( MSG_MUI_WHETHERORNOTTOCACHETHEDIRECTORIESANDFILESLISTSINMEMORYM ) ,
		Child, ImageObject,
			TextFrame,
			MUIA_Image_Spec,     MUII_CheckMark,
			MUIA_Image_FreeVert, TRUE,
			MUIA_ShowSelState,   FALSE,
		End,
		Child, Label2( GetString( MSG_MUI_CACHEDIRECTORIESMODULESLISTSDURINGRUN ) ),
		Child, HSpace(0),
	End;

	SetAttrs(
		object->GR_grp_cache_list_during_run,
		MUIA_Selected, prefs_should_cache_list_during_run(),
		TAG_DONE);

	object->GR_grp_cache_list_between_runs = GroupObject,
		MUIA_CycleChain,  TRUE,
		MUIA_Group_Horiz, TRUE,
		MUIA_InputMode,   MUIV_InputMode_Toggle,
		MUIA_HelpNode,    "Prefs-Cache-List-Between-Runs",
		MUIA_ShortHelp,   GetString( MSG_MUI_WHETHERORNOTTOCACHETHEDIRECTORIESANDFILESLISTSONDISKMOR ) ,
		Child, ImageObject,
			TextFrame,
			MUIA_Image_Spec,     MUII_CheckMark,
			MUIA_Image_FreeVert, TRUE,
			MUIA_ShowSelState,   FALSE,
		End,
		Child, Label2( GetString( MSG_MUI_CACHEDIRECTORIESMODULESLISTSBETWEENRUNS ) ),
		Child, HSpace(0),
	End;

	SetAttrs(
		object->GR_grp_cache_list_between_runs,
		MUIA_Selected, prefs_should_cache_list_between_runs(),
		TAG_DONE);

	object->STR_label_cache_directory = String(prefs_get_cache_directory(), 80);
	object->PA_label_cache_directory = PopaslObject,
		MUIA_CycleChain,       TRUE,
		MUIA_Popasl_Type,      ASL_FileRequest,
		ASLFR_DrawersOnly,     TRUE,
		MUIA_Popstring_String, object->STR_label_cache_directory,
		MUIA_Popstring_Button, PopButton(MUII_PopDrawer),
		// MUIA_HelpNode,      "Prefs-Cache-Directory-String",
		MUIA_HelpNode,         "Prefs-Cache-Directory-Requester",
		MUIA_ShortHelp,        GetString( MSG_MUI_CHOICEOFTHEDIRECTORYWHERETOCACHEMODULESBYDEFAULT ) ,
	End;

	object->GR_grp_skip_if_banned = GroupObject,
		MUIA_CycleChain,  TRUE,
		MUIA_Group_Horiz, TRUE,
		MUIA_InputMode,   MUIV_InputMode_Toggle,
		MUIA_HelpNode,    "Prefs-Skip-If-Banned",
		MUIA_ShortHelp,   GetString( MSG_MUI_WHETHERORNOTTOSKIPTHEMODULESIFYOUBANTHEM ) ,
		Child, ImageObject,
			TextFrame,
			MUIA_Image_Spec,     MUII_CheckMark,
			MUIA_Image_FreeVert, TRUE,
			MUIA_ShowSelState,   FALSE,
		End,
		Child, Label2( GetString( MSG_MUI_SKIPMODULESWHENBANNED ) ),
		Child, HSpace(0),
	End;

	SetAttrs(
		object->GR_grp_skip_if_banned,
		MUIA_Selected, prefs_should_skip_if_banned(),
		TAG_DONE);

	object->GR_grp_skip_if_user_rating_low = GroupObject,
		MUIA_CycleChain,  TRUE,
		MUIA_Group_Horiz, TRUE,
		MUIA_InputMode,   MUIV_InputMode_Toggle,
		MUIA_HelpNode,    "Prefs-Skip-If-User-Rating-Low",
		MUIA_ShortHelp,   GetString( MSG_MUI_WHETHERORNOTTOSKIPTHEMODULESIFYOURATETHEMLOW ) ,
		Child, ImageObject,
			TextFrame,
			MUIA_Image_Spec,     MUII_CheckMark,
			MUIA_Image_FreeVert, TRUE,
			MUIA_ShowSelState,   FALSE,
		End,
		Child, Label2( GetString( MSG_MUI_SKIPMODULESWHENRATINGLOW ) ),
		Child, HSpace(0),
	End;

	SetAttrs(
		object->GR_grp_skip_if_user_rating_low,
		MUIA_Selected,    prefs_should_skip_if_user_rating_low(),
		TAG_DONE);

	object->GR_grp_skip_if_all_rating_exists = GroupObject,
		MUIA_CycleChain,  TRUE,
		MUIA_Group_Horiz, TRUE,
		MUIA_InputMode,   MUIV_InputMode_Toggle,
		MUIA_HelpNode,    "Prefs-Skip-If-All-Rating-Exists",
		MUIA_ShortHelp,   GetString( MSG_MUI_WHETHERORNOTTOSKIPTHEMODULESIFTHECOMMUNITYALREADYLISTEN ) ,
		Child, ImageObject,
			TextFrame,
			MUIA_Image_Spec,     MUII_CheckMark,
			MUIA_Image_FreeVert, TRUE,
			MUIA_ShowSelState,   FALSE,
		End,
		Child, Label2( GetString( MSG_MUI_SKIPMODULESWHENALREADYRATED ) ),
		Child, HSpace(0),
	End;

	SetAttrs(
		object->GR_grp_skip_if_all_rating_exists,
		MUIA_Selected, prefs_should_skip_if_all_rating_exists(),
		TAG_DONE);

	object->GR_grp_skip_if_all_rating_low = GroupObject,
		MUIA_CycleChain,  TRUE,
		MUIA_Group_Horiz, TRUE,
		MUIA_InputMode,   MUIV_InputMode_Toggle,
		MUIA_HelpNode,    "Prefs-Skip-If-All-Rating-Low",
		MUIA_ShortHelp,   GetString( MSG_MUI_WHETHERORNOTTOSKIPTHEMODULESIFTHECOMMUNITYALREADYRATEDLOW ) ,
		Child, ImageObject,
			TextFrame,
			MUIA_Image_Spec,     MUII_CheckMark,
			MUIA_Image_FreeVert, TRUE,
			MUIA_ShowSelState,   FALSE,
		End,
		Child, Label2( GetString( MSG_MUI_SKIPMODULESWHENALREADYRATEDTOOLOW ) ),
		Child, HSpace(0),
	End;

	SetAttrs(
		object->GR_grp_skip_if_all_rating_low,
		MUIA_Selected,    prefs_should_skip_if_all_rating_low(),
		TAG_DONE);

	object->GR_grp_skip_if_above_maximum_file_size = GroupObject,
		MUIA_CycleChain,  TRUE,
		MUIA_Group_Horiz, TRUE,
		MUIA_InputMode,   MUIV_InputMode_Toggle,
		MUIA_HelpNode,    "Prefs-Skip-If-Above-Maximum-Size",
		MUIA_ShortHelp,   GetString( MSG_MUI_WHETHERORNOTTOSKIPTHEMODULESIFTHEIRSIZEISABOVETHECHOSEN ) ,
		Child, ImageObject,
			TextFrame,
			MUIA_Image_Spec,     MUII_CheckMark,
			MUIA_Image_FreeVert, TRUE,
			MUIA_ShowSelState,   FALSE,
		End,
		Child, Label2( GetString( MSG_MUI_SKIPMODULESWHICHSIZESAREABOVEMAXIMUMSIZEOF ) ),
		Child, HSpace(0),
	End;

	SetAttrs(
		object->GR_grp_skip_if_above_maximum_file_size,
		MUIA_Selected, prefs_should_skip_if_above_maximum_file_size(),
		TAG_DONE);

	_CY_label_maximum_file_sizes = malloc((prefs_get_number_of_maximum_file_sizes() + 1) * sizeof(char *));
	for(i = 0; i < prefs_get_number_of_maximum_file_sizes(); i++)
	{
		_CY_label_maximum_file_sizes[i] = prefs_get_list_of_maximum_file_sizes()[i];
	}
	_CY_label_maximum_file_sizes[prefs_get_number_of_maximum_file_sizes()] = NULL;

	object->CY_label_maximum_file_size = CycleObject,
		MUIA_CycleChain,    TRUE,
		MUIA_Frame,         MUIV_Frame_Button,
		MUIA_Cycle_Active,  prefs_get_maximum_file_size_index(),
		MUIA_Cycle_Entries, _CY_label_maximum_file_sizes,
		MUIA_HelpNode,      "Prefs-Maximum-File-Size",
	End;

	GR_grp_preferences = GroupObject,
		MUIA_Group_Horiz, TRUE,
		Child, ScrollgroupObject,
			MUIA_Scrollgroup_Contents, VirtgroupObject,
				MUIA_Frame, MUIV_Frame_Button,
				Child, VGroup,
					MUIA_Frame, MUIV_Frame_Group,
					Child, object->GR_grp_save_prefs_on_exit,
					Child, object->GR_grp_play_on_startup,
					Child, HGroup,
						Child, object->GR_grp_show_log_page_on_fetch,
						Child, object->CY_label_maximum_log_size,
						Child, Label2( GetString( MSG_MUI_KILOBYTES ) ),
					End,
					Child, object->GR_grp_remember_source_between_runs,
					Child, object->GR_grp_remember_images_set,
					Child, HGroup,
						Child, Label2( GetString( MSG_MUI_MODULESDIRECTORY ) ),
						Child, object->PA_label_saved_modules_directory,
					End,
				End,
				Child, VGroup,
					MUIA_Frame, MUIV_Frame_Group,
					Child, object->GR_grp_cache_list_during_run,
					Child, object->GR_grp_cache_list_between_runs,
					Child, HGroup,
						Child, Label2( GetString( MSG_MUI_CACHEDIRECTORY ) ),
						Child, object->PA_label_cache_directory,
					End,
				End,
				Child, VGroup,
					MUIA_Frame, MUIV_Frame_Group,
					Child, object->GR_grp_skip_if_banned,
					Child, object->GR_grp_skip_if_user_rating_low,
					Child, object->GR_grp_skip_if_all_rating_exists,
					Child, object->GR_grp_skip_if_all_rating_low,
					Child, HGroup,
						Child, object->GR_grp_skip_if_above_maximum_file_size,
						Child, object->CY_label_maximum_file_size,
						Child, Label2( GetString( MSG_MUI_KILOBYTES ) ),
					End,
				End,
				Child, VSpace(0),
			End,
		End,
		Child, GroupObject,
			MUIA_FixWidthTxt, (STRPTR) GetString( MSG_MUI_DEFAULT ) ,
			Child, BT_prefs_save    = TextObject,
				MUIA_CycleChain,    TRUE,
				MUIA_Frame,         MUIV_Frame_ImageButton,
				MUIA_Text_Contents, GetString( MSG_MUI_SAVE ) ,
				MUIA_Text_HiChar,   's',
				MUIA_ControlChar,   's',
				MUIA_Text_SetMin,   TRUE,
				MUIA_InputMode,     MUIV_InputMode_RelVerify,
				MUIA_HelpNode,      "Prefs-Save",
				MUIA_ShortHelp,     GetString( MSG_MUI_SAVE ) ,
			End,
			Child, BT_prefs_reset   = TextObject,
				MUIA_CycleChain,    TRUE,
				MUIA_Frame,         MUIV_Frame_ImageButton,
				MUIA_Text_Contents, GetString( MSG_MUI_RESET ) ,
				MUIA_Text_HiChar,   'r',
				MUIA_ControlChar,   'r',
				MUIA_Text_SetMin,   TRUE,
				MUIA_InputMode,     MUIV_InputMode_RelVerify,
				MUIA_HelpNode,      "Prefs-Reset",
				MUIA_ShortHelp,     GetString( MSG_MUI_RESET ) ,
			End,
			Child, BT_prefs_default = TextObject,
				MUIA_CycleChain,    TRUE,
				MUIA_Frame,         MUIV_Frame_ImageButton,
				MUIA_Text_Contents, GetString( MSG_MUI_DEFAULT ) ,
				MUIA_Text_HiChar,   'd',
				MUIA_ControlChar,   'd',
				MUIA_Text_SetMin,   TRUE,
				MUIA_InputMode,     MUIV_InputMode_RelVerify,
				MUIA_HelpNode,      "Prefs-Default",
				MUIA_ShortHelp,     GetString( MSG_MUI_DEFAULT ) ,
			End,
			Child, VSpace(0),
		End,
	End;

	/*
	 * The definition of the log page.
	 */

	object->LV_label_log = FloattextObject,
		MUIA_Floattext_Text, object->LV_label_log_content,
		MUIA_Frame,          MUIV_Frame_InputList,
		MUIA_HelpNode,       "Log-Log-Content",
		MUIA_ShortHelp,      GetString( MSG_MUI_SHOWTHEACTIVITIESOFAMIMODRADIOANDANYERROR ) ,
	End;
	
	GR_grp_log_controls = GroupObject,
		MUIA_Group_Horiz, TRUE,
		Child, ListviewObject,
			MUIA_CycleChain,           TRUE,
			MUIA_Listview_Input,       TRUE,
			MUIA_Listview_MultiSelect, MUIV_Listview_MultiSelect_Default,
			MUIA_Listview_List,        object->LV_label_log,
		End,
		Child, GroupObject,
			MUIA_FixWidthTxt, (STRPTR) GetString( MSG_MUI_DEFAULT ) ,
			MUIA_HelpNode, "Log-Log-Controls",
			Child, BT_log_save  = TextObject,
				MUIA_CycleChain,    TRUE,
				MUIA_Frame,         MUIV_Frame_ImageButton,
				MUIA_Text_Contents, GetString( MSG_MUI_SAVE ) ,
				MUIA_Text_HiChar,   's',
				MUIA_ControlChar,   's',
				MUIA_Text_SetMin,   TRUE,
				MUIA_InputMode,     MUIV_InputMode_RelVerify,
				MUIA_HelpNode,      "Log-Save",
				MUIA_ShortHelp,     GetString( MSG_MUI_SAVE ) ,
			End,
			Child, BT_log_clear = TextObject,
				MUIA_CycleChain,    TRUE,
				MUIA_Frame,         MUIV_Frame_ImageButton,
				MUIA_Text_Contents, GetString( MSG_MUI_CLEAR ) ,
				MUIA_Text_HiChar,   'c',
				MUIA_ControlChar,   'c',
				MUIA_Text_SetMin,   TRUE,
				MUIA_InputMode,     MUIV_InputMode_RelVerify,
				MUIA_HelpNode,      "Log-Clear",
				MUIA_ShortHelp,     GetString( MSG_MUI_CLEAR ) ,
			End,
			Child, VSpace(0),
		End,
	End;

	/*
	 * The definition of the window with its pages.
	 */

	object->GR_grp_root = RegisterObject,
		MUIA_CycleChain,      TRUE,
		MUIA_Register_Titles, _pages_names,
		Child,                GR_grp_controls,
		Child,                GR_grp_preferences,
		Child,                GR_grp_log_controls,
	End;

	object->WI_label_title = WindowObject,
		MUIA_Window_DragBar, TRUE,
		MUIA_Window_ID, MAKE_ID('0', 'A', 'M', 'R'),
		MUIA_Window_ScreenTitle, version_get_version_description(),
		MUIA_Window_SizeGadget, TRUE,
		MUIA_Window_Title, "AmiModRadio",
		WindowContents, GroupObject,
			MUIA_Group_Rows, 3,
			Child, object->GR_grp_root,
			Child, object->TX_label_last_message = TextObject,
				MUIA_HelpNode, "Last-Message",
				MUIA_Frame, MUIV_Frame_Text,
				MUIA_Text_SetMin, TRUE,
				MUIA_ShortHelp,  GetString( MSG_MUI_DISPLAYINFORMATIONMESSAGES ) ,
			End,
			Child, object->GA_progression_bar = GaugeObject,
				GaugeFrame,
				MUIA_Gauge_Horiz, TRUE,
				MUIA_FixHeight,   3,
			End,
		End,
	End;

	object->App = ApplicationObject,
		MUIA_Application_Author, "Tygre <tygre@chingu.asia>",
		MUIA_Application_Base, "AmiModRadio",
		MUIA_Application_Copyright, "Tygre <tygre@chingu.asia>",
		MUIA_Application_Description, description,
		MUIA_Application_DiskObject, utils_guis_get_disk_object(),
		MUIA_Application_HelpFile, "PROGDIR:doc/English/AmiModRadio.guide",
		MUIA_Application_SingleTask, TRUE,
		MUIA_Application_Title, "AmiModRadio",
		MUIA_Application_Version, version_get_version_description(),
		SubWindow, object->WI_label_title,
	End;

	if(!object->App)
	{
		log_print_error("gui_mui_init_ui(), could not create MUI application\n");

		free(object->LV_label_log_content);
		object->LV_label_log_content = NULL;

		// Something already has free'd this disk object!?
		/*
		if(_disk_object != NULL)
		{
			FreeDiskObject(_disk_object);
		}
		*/

		FreeVec(object);
		*ui_object = NULL;

		return RETURN_ERROR;
	}

	DoMethod(
		object->App,
		MUIM_Notify,
		MUIA_Application_DoubleStart,
		TRUE,
		object->App,
		2,
		MUIM_CallHook,
		&controls_mui_doublestart_hook);

	DoMethod(
		object->WI_label_title,
		MUIM_Notify,
		MUIA_Window_CloseRequest,
		TRUE,
		object->App,
		2,
		MUIM_Application_ReturnID,
		MUIV_Application_ReturnID_Quit);

	DoMethod(
		object->WI_label_title,
		MUIM_Notify,
		MUIA_Window_InputEvent,
		"p",
		object->WI_label_title,
		2,
		MUIM_CallHook,
		&controls_mui_keyboard_space_hook);

	DoMethod(
		object->IM_label_logo,
		MUIM_Notify,
		MUIA_Pressed,
		FALSE,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&controls_mui_about_hook);

	DoMethod(
		object->IM_label_play,
		MUIM_Notify,
		MUIA_Pressed,
		FALSE,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&controls_mui_play_hook);

	DoMethod(
		object->IM_label_replay,
		MUIM_Notify,
		MUIA_Pressed,
		FALSE,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&controls_mui_replay_hook);

	DoMethod(
		object->IM_label_pause,
		MUIM_Notify,
		MUIA_Pressed,
		FALSE,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&controls_mui_pause_hook);

	DoMethod(
		object->IM_label_stop,
		MUIM_Notify,
		MUIA_Pressed,
		FALSE,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&controls_mui_stop_hook);

	DoMethod(
		object->IM_label_next,
		MUIM_Notify,
		MUIA_Pressed,
		FALSE,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&controls_mui_next_hook);

	DoMethod(
		object->IM_label_root_directory_prev,
		MUIM_Notify,
		MUIA_Pressed,
		TRUE,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&controls_mui_root_directory_prev_hook);

	DoMethod(
		object->IM_label_root_directory_next,
		MUIM_Notify,
		MUIA_Pressed,
		FALSE,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&controls_mui_root_directory_next_hook);

	DoMethod(
		object->IM_label_root_directory_reset,
		MUIM_Notify,
		MUIA_Pressed,
		FALSE,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&controls_mui_root_directory_reset_hook);

	DoMethod(
		object->STR_label_directory,
		MUIM_Notify,
		MUIA_String_Acknowledge,
		MUIV_EveryTime,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&controls_mui_change_directory_hook);

	DoMethod(
		object->IM_label_directory_list,
		MUIM_Notify,
		MUIA_Pressed,
		TRUE,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&controls_mui_directory_list_hook);

	DoMethod(
		object->IM_label_directory_ban,
		MUIM_Notify,
		MUIA_Pressed,
		FALSE,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&controls_mui_directory_ban_hook);

	DoMethod(
		object->IM_label_directories_ban_list,
		MUIM_Notify,
		MUIA_Pressed,
		FALSE,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&controls_mui_directories_ban_list_open_hook);

	DoMethod(
		object->IM_label_directory_set_as_root,
		MUIM_Notify,
		MUIA_Pressed,
		FALSE,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&controls_mui_directory_set_as_root_hook);

	DoMethod(
		object->IM_label_file_list,
		MUIM_Notify,
		MUIA_Pressed,
		FALSE,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&controls_mui_file_list_hook);

	DoMethod(
		object->IM_label_file_ban,
		MUIM_Notify,
		MUIA_Pressed,
		FALSE,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&controls_mui_file_ban_hook);

	DoMethod(
		object->IM_label_files_ban_list,
		MUIM_Notify,
		MUIA_Pressed,
		FALSE,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&controls_mui_files_ban_list_open_hook);

	DoMethod(
		object->IM_label_modules_save,
		MUIM_Notify,
		MUIA_Pressed,
		FALSE,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&controls_mui_modules_save_hook);

	DoMethod(
		object->IM_label_modules_email,
		MUIM_Notify,
		MUIA_Pressed,
		TRUE,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&controls_mui_modules_email_hook);

	for(i = 0; i < 5; i++)
	{
		DoMethod(
			object->IM_label_user_rating[i],
			MUIM_Notify,
			MUIA_Pressed,
			TRUE,
			MUIV_Notify_Self,
			3,
			MUIM_CallHook,
			&controls_mui_set_user_rating_hook,
			i);
	}

	DoMethod(
		object->GR_grp_play_on_startup,
		MUIM_Notify,
		MUIA_Selected,
		MUIV_EveryTime,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&mui_prefs_play_on_startup_hook);

	// Log
	DoMethod(
		object->GR_grp_show_log_page_on_fetch,
		MUIM_Notify,
		MUIA_Selected,
		MUIV_EveryTime,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&mui_prefs_show_log_page_on_fetch_hook);
	DoMethod(
		object->CY_label_maximum_log_size,
		MUIM_Notify,
		MUIA_Cycle_Active,
		MUIV_EveryTime,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&mui_prefs_maximum_log_size_changed_hook);

	DoMethod(
		object->GR_grp_remember_source_between_runs,
		MUIM_Notify,
		MUIA_Selected,
		MUIV_EveryTime,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&mui_prefs_remember_source_between_runs_hook);

	DoMethod(
		object->STR_label_saved_modules_directory,
		MUIM_Notify,
		MUIA_String_Acknowledge,
		MUIV_EveryTime,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&mui_prefs_saved_modules_directory_changed_hook);

	DoMethod(
		object->GR_grp_cache_list_during_run,
		MUIM_Notify,
		MUIA_Selected,
		MUIV_EveryTime,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&mui_prefs_cache_list_during_run_hook);

	DoMethod(
		object->GR_grp_cache_list_between_runs,
		MUIM_Notify,
		MUIA_Selected,
		MUIV_EveryTime,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&mui_prefs_cache_list_between_runs_hook);

	DoMethod(
		object->STR_label_cache_directory,
		MUIM_Notify,
		MUIA_String_Acknowledge,
		MUIV_EveryTime,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&mui_prefs_cache_directory_changed_hook);

	DoMethod(
		object->GR_grp_save_prefs_on_exit,
		MUIM_Notify,
		MUIA_Selected,
		MUIV_EveryTime,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&mui_prefs_save_prefs_on_exit_hook);

	DoMethod(
		object->GR_grp_remember_images_set,
		MUIM_Notify,
		MUIA_Cycle_Active,
		MUIV_EveryTime,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&
		mui_prefs_remember_images_set_hook);

	DoMethod(
		object->GR_grp_skip_if_banned,
		MUIM_Notify,
		MUIA_Selected,
		MUIV_EveryTime,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&mui_prefs_skip_if_banned_hook);

	DoMethod(
		object->GR_grp_skip_if_user_rating_low,
		MUIM_Notify,
		MUIA_Selected,
		MUIV_EveryTime,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&mui_prefs_skip_if_user_rating_low_hook);

	DoMethod(
		object->GR_grp_skip_if_all_rating_exists,
		MUIM_Notify,
		MUIA_Selected,
		MUIV_EveryTime,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&mui_prefs_skip_if_all_rating_exists_hook);

	DoMethod(
		object->GR_grp_skip_if_all_rating_low,
		MUIM_Notify,
		MUIA_Selected,
		MUIV_EveryTime,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&mui_prefs_skip_if_all_rating_low_hook);

	// File size
	DoMethod(
		object->GR_grp_skip_if_above_maximum_file_size,
		MUIM_Notify,
		MUIA_Selected,
		MUIV_EveryTime,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&mui_prefs_skip_if_above_maximum_file_size_hook);
	DoMethod(
		object->CY_label_maximum_file_size,
		MUIM_Notify,
		MUIA_Cycle_Active,
		MUIV_EveryTime,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&mui_prefs_maximum_file_size_changed_hook);

	DoMethod(
		BT_prefs_save,
		MUIM_Notify,
		MUIA_Pressed,
		TRUE,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&mui_prefs_save_hook);

	DoMethod(
		BT_prefs_reset,
		MUIM_Notify,
		MUIA_Pressed,
		TRUE,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&mui_prefs_reset_hook);

	DoMethod(
		BT_prefs_default,
		MUIM_Notify,
		MUIA_Pressed,
		TRUE,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&mui_prefs_default_hook);

	DoMethod(
		BT_log_save,
		MUIM_Notify,
		MUIA_Pressed,
		TRUE,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&mui_log_save_hook);

	DoMethod(
		BT_log_clear,
		MUIM_Notify,
		MUIA_Pressed,
		TRUE,
		MUIV_Notify_Self,
		2,
		MUIM_CallHook,
		&mui_log_clear_hook);

	set(object->WI_label_title,
		MUIA_Window_Open,
		TRUE);

	*ui_object = object;

	return RETURN_OK;
}

void gui_mui_close_ui(
	IN struct MUI_UI **ui_object)
{
	log_print_debug("gui_mui_close_ui()\n");

	if(*ui_object != NULL)
	{
		free((*ui_object)->LV_label_log_content);
		(*ui_object)->LV_label_log_content = NULL;

		// Tygre 2020/05/21: Guru...
		// Somehow, this call invites a
		// Guru (0100 000F), first recoverable
		// but then red and angry and reboot.
		//	FreeDiskObject(_disk_object);

		MUI_DisposeObject((*ui_object)->App);

		FreeVec(*ui_object);
		*ui_object = NULL;
	}

	if(_IM_label_logo_spec)
	{
		free(_IM_label_logo_spec);
		_IM_label_logo_spec = NULL;
	}
	if(_IM_label_play_spec)
	{
		free(_IM_label_play_spec);
		_IM_label_play_spec = NULL;
	}
	if(_IM_label_replay_spec)
	{
		free(_IM_label_replay_spec);
		_IM_label_replay_spec = NULL;
	}
	if(_IM_label_pause_spec)
	{
		free(_IM_label_pause_spec);
		_IM_label_pause_spec = NULL;
	}
	if(_IM_label_stop_spec)
	{
		free(_IM_label_stop_spec);
		_IM_label_stop_spec = NULL;
	}
	if(_IM_label_next_spec)
	{
		free(_IM_label_next_spec);
		_IM_label_next_spec = NULL;
	}
	if(_IM_label_root_directory_prev_spec)
	{
		free(_IM_label_root_directory_prev_spec);
		_IM_label_root_directory_prev_spec = NULL;
	}
	if(_IM_label_root_directory_next_spec)
	{
		free(_IM_label_root_directory_next_spec);
		_IM_label_root_directory_next_spec = NULL;
	}
	if(_IM_label_root_directory_reset_spec)
	{
		free(_IM_label_root_directory_reset_spec);
		_IM_label_root_directory_reset_spec = NULL;
	}
	if(_IM_label_directory_list_spec)
	{
		free(_IM_label_directory_list_spec);
		_IM_label_directory_list_spec = NULL;
	}
	if(_IM_label_directory_ban_spec)
	{
		free(_IM_label_directory_ban_spec);
		_IM_label_directory_ban_spec = NULL;
	}
	if(_IM_label_directories_ban_list_spec)
	{
		free(_IM_label_directories_ban_list_spec);
		_IM_label_directories_ban_list_spec = NULL;
	}
	if(_IM_label_directory_set_as_root_spec)
	{
		free(_IM_label_directory_set_as_root_spec);
		_IM_label_directory_set_as_root_spec = NULL;
	}
	if(_IM_label_file_list_spec)
	{
		free(_IM_label_file_list_spec);
		_IM_label_file_list_spec = NULL;
	}
	if(_IM_label_file_ban_spec)
	{
		free(_IM_label_file_ban_spec);
		_IM_label_file_ban_spec = NULL;
	}
	if(_IM_label_files_ban_list_spec)
	{
		free(_IM_label_files_ban_list_spec);
		_IM_label_files_ban_list_spec = NULL;
	}
	if(_IM_label_modules_save_spec)
	{
		free(_IM_label_modules_save_spec);
		_IM_label_modules_save_spec = NULL;
	}
	if(_IM_label_modules_email_spec)
	{
		free(_IM_label_modules_email_spec);
		_IM_label_modules_email_spec = NULL;
	}
	if(_IM_controls_background_spec)
	{
		free(_IM_controls_background_spec);
		_IM_controls_background_spec = NULL;
	}
	if(_current_directory_name)
	{
		free(_current_directory_name);
		_current_directory_name = NULL;
	}
	if(_CY_label_maximum_log_sizes)
	{
		free(_CY_label_maximum_log_sizes);
		_CY_label_maximum_log_sizes = NULL;
	}
	if(_CY_label_maximum_file_sizes)
	{
		free(_CY_label_maximum_file_sizes);
		_CY_label_maximum_file_sizes = NULL;
	}
}

void gui_mui_child_window_list_open(
	IN  struct MUI_UI                        *ui_object,
	IN  char                                 *list,
	IN  char                                 *help,
	IN  char                                 *title,
	OUT APTR                                 *view,
	OUT APTR	                             *window,
	IN  gui_mui_child_window_clicked_callback_fp  callback_func_clicked,
	IN  gui_mui_child_window_closed_callback_fp   callback_func_closed)
{
	static const struct Hook mui_child_window_clicked_hook = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)_child_window_clicked, NULL };
	static const struct Hook mui_child_window_closed_hook  = { { NULL,NULL }, (HOOKFUNC)HookEntry, (HOOKFUNC)_child_window_closed,  NULL };
	char                     window_id[4]                  = "";

	*view = ListObject,
		MUIA_Frame, MUIV_Frame_String,
		MUIA_List_ConstructHook, MUIV_List_ConstructHook_String,
		MUIA_List_DestructHook, MUIV_List_DestructHook_String,
	End;

	*view = ListviewObject,
		MUIA_HelpNode, help,
		MUIA_Listview_MultiSelect, MUIV_Listview_MultiSelect_None,
		MUIA_Listview_List, *view,
	End;

	string_snprintf(window_id, 4, "%dWIN", ++_window_id);
	*window = WindowObject,
		MUIA_Window_Title, title,
		MUIA_Window_ID, MAKE_ID(
			window_id[0],
			window_id[1],
			window_id[2],
			window_id[3]),
		WindowContents, GroupObject,
			Child, *view,
		End,
	End;

	if(*window == NULL)
	{
		return;
	}

	DoMethod(
		*view,
		MUIM_Notify,
		MUIA_Listview_DoubleClick,
		TRUE,
		MUIV_Notify_Self,
		3,
		MUIM_CallHook,
		&mui_child_window_clicked_hook,
		callback_func_clicked);

	DoMethod(
		*window,
		MUIM_Notify,
		MUIA_Window_CloseRequest,
		TRUE,
		MUIV_Notify_Self,
		4,
		MUIM_CallHook,
		&mui_child_window_closed_hook,
		callback_func_closed,
		ui_object);

	gui_mui_child_window_list_update(
		*view,
		 list);

	DoMethod(
		ui_object->App,
		OM_ADDMEMBER,
		*window);

	SetAttrs(
		*window,
		MUIA_Window_Open,
		TRUE,
		TAG_DONE);
}

void gui_mui_child_window_list_update(
	IN APTR	 list_view,
	IN char *list)
{
	char *token     = NULL;
	char *list_temp = NULL;

	if(list_view == NULL)
	{
		return;
	}

	DoMethod(
		list_view,
		MUIM_List_Clear);

	// Tygre 2016/09/29: About list
	// The list could be empty:
	// could contain only '\n'.
	if(list == NULL || strlen(list) < 2)
	{
		DoMethod(
			list_view,
			MUIM_List_InsertSingle,
			 GetString( MSG_MUI_NOTHINGTODISPLAY ) ,
			MUIV_List_Insert_Bottom);
		return;
	}

	list_temp = string_duplicate(list);
	if(list_temp == NULL)
	{
		log_print_error( GetString( MSG_MUI_MUICHILDWINDOWLISTUPDATECOULDNOTALLOCATETEMPORARYSTRING ) );
		return;
	}
			
	SetAttrs(
		list_view,
		MUIA_List_Quiet,
		TRUE,
		TAG_DONE);

	token = strtok(list_temp, "\n");
	while(token != NULL)
	{
		DoMethod(
			list_view,
			MUIM_List_InsertSingle,
			token,
			MUIV_List_Insert_Bottom);

		token = strtok(NULL, "\n");
	}

	SetAttrs(
		list_view,
		MUIA_List_Quiet,
		FALSE,
		TAG_DONE);

	free(list_temp);
}                        

static void _child_window_clicked(
	struct Hook *h,
	Object      *o,
	APTR        *d)
{
	gui_mui_child_window_clicked_callback_fp  callback_func = (gui_mui_child_window_clicked_callback_fp)d[0];
	APTR                                  view          = (APTR)o;
	char                                 *entry         = NULL;

	DoMethod(
		view,
		MUIM_List_GetEntry,
		MUIV_List_GetEntry_Active,
		&entry);

	if(entry != NULL && !string_start_with(entry, "<") && callback_func != NULL)
	{
		callback_func(entry);
	}
}

static void _child_window_closed(
	struct Hook *h,
	Object      *o,
	APTR        *d)
{
	gui_mui_child_window_closed_callback_fp  callback_func = (gui_mui_child_window_closed_callback_fp)d[0];
	struct MUI_UI                       *object        = (struct MUI_UI *)d[1];
	APTR                                 window        = (APTR)o;

	if(callback_func != NULL)
	{
		callback_func(window);
	}
}

