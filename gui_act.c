 /*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/

/* Includes */

#include "gui_act.h"

#include "globals.h"
#include "log.h"
#include "locale.h"
#include "prefs.h"
#include "utils.h"
#include "utils_guis.h"
#include "version.h"
#include "z_fortify.h"

#include <stdio.h>
// #include <exec/memory.h>     // Only needed with NDK v3.1: For MEMF_PUBLIC and MEMF_CLEAR
#include <exec/ports.h>
#include <libraries/gadtools.h> // For PLACETEXT_RIGHT
#include <proto/alib.h>         // For DoMethod()
#include <proto/intuition.h>
#include <proto/dos.h>
#include <proto/exec.h>

#include <classact.h>
#include <classact_author.h>
#include <proto/chooser.h>
#include <proto/clicktab.h>
#include <gadgets/clicktab.h>



/* Constants and declarations */

	   int  gui_act_init_ui(IN struct MsgPort *, OUT struct ACT_UI **);
	   void gui_act_close_ui(IN struct MsgPort *, IN struct ACT_UI **);
	   void gui_act_child_window_list_open(IN struct ACT_UI  *,	IN char *, IN char *, IN char *, OUT struct Gadget **, OUT Object **);
static BOOL _create_tab_nodes(INOUT struct List *, IN char **);
static void _free_tab_nodes(INOUT struct List *);
static BOOL _create_img_nodes(INOUT struct List *, IN char **, IN int);
static void _free_img_nodes(INOUT struct List *);
static BOOL _create_mls_nodes(INOUT struct List *, IN char **, IN int);
static void _free_mls_nodes(INOUT struct List *);
static BOOL _create_mfs_nodes(INOUT struct List *, IN char **, IN int);
static void _free_mfs_nodes(INOUT struct List *);
static BOOL _create_log_nodes(INOUT struct List *);
static void _free_log_nodes(INOUT struct List *);

// Defined as NULL in main.c
extern struct ClassLibrary *BitMapBase;
extern struct ClassLibrary *ButtonBase;
extern struct ClassLibrary *CheckBoxBase;
extern struct ClassLibrary *ChooserBase;
extern struct ClassLibrary *ClickTabBase;
extern struct ClassLibrary *FuelGaugeBase;
extern struct ClassLibrary *LabelBase;
extern struct ClassLibrary *ListBrowserBase;
extern struct ClassLibrary *LayoutBase;
extern struct ClassLibrary *PageBase;
extern struct ClassLibrary *StringBase;
extern struct ClassLibrary *WindowBase;



/* Definitions */

static struct Screen *_screen      = NULL;
static char          *_tab_data[4] = { NULL };
static struct List    _tab_nodes   = { 0 };
static struct List    _img_nodes   = { 0 };
static struct List    _mls_nodes   = { 0 };
static struct List    _mfs_nodes   = { 0 };
static struct List    _log_nodes   = { 0 };

int gui_act_init_ui(
	IN  struct MsgPort *port,
	OUT struct ACT_UI **ui_object)
{
	char current_directory_name[GLOBALS_MAX_LINE_LENGTH] = { 0 };

	Object        *lay_object = NULL;
	Object        *win_object = NULL;
	struct Window *window     = NULL;
				  *ui_object  = NULL;

	char image_path_for_about[GLOBALS_MAX_LINE_LENGTH]   = { 0 };
	char image_path_for_play[GLOBALS_MAX_LINE_LENGTH]    = { 0 };
	char image_path_for_replay[GLOBALS_MAX_LINE_LENGTH]  = { 0 };
	char image_path_for_pause[GLOBALS_MAX_LINE_LENGTH]   = { 0 };
	char image_path_for_stop[GLOBALS_MAX_LINE_LENGTH]    = { 0 };
	char image_path_for_next[GLOBALS_MAX_LINE_LENGTH]    = { 0 };
	char image_path_for_reset[GLOBALS_MAX_LINE_LENGTH]   = { 0 };
	char image_path_for_prevs[GLOBALS_MAX_LINE_LENGTH]   = { 0 };
	char image_path_for_nexts[GLOBALS_MAX_LINE_LENGTH]   = { 0 };
	char image_path_for_set[GLOBALS_MAX_LINE_LENGTH]     = { 0 };
	char image_path_for_list[GLOBALS_MAX_LINE_LENGTH]    = { 0 };
	char image_path_for_ban[GLOBALS_MAX_LINE_LENGTH]     = { 0 };
	char image_path_for_save[GLOBALS_MAX_LINE_LENGTH]    = { 0 };
	char image_path_for_email[GLOBALS_MAX_LINE_LENGTH]   = { 0 };

	log_print_debug("gui_act_init_ui()\n");

	// Lock public screen
	_screen = LockPubScreen(NULL);
	if(_screen == NULL)
	{
		log_print_error("gui_act_init_ui(), could not lock public screen");
		goto _RETURN_ERROR;
	}

	// Allocate UI object
	if((*ui_object = AllocVec(sizeof(struct ACT_UI), MEMF_PUBLIC|MEMF_CLEAR)) == NULL)
	{
		log_print_error("gui_act_init_ui(), could not allocate memory for UI object");
		goto _RETURN_ERROR;
	}

	// Allocate tab nodes
	_tab_data[0] = GetString(MSG_MUI_CONTROLS);
	_tab_data[1] = GetString(MSG_MUI_PREFS);
	_tab_data[2] = GetString(MSG_MUI_LOG);

	if(_create_tab_nodes(
		&_tab_nodes,
		_tab_data) == FALSE)
	{
		log_print_error("gui_act_init_ui(), could not allocate nodes for pages");
		goto _RETURN_ERROR;
	}

	// Allocate image nodes
	if(_create_img_nodes(
		&_img_nodes,
		prefs_get_list_of_images_sets(),
		prefs_get_number_of_images_sets()) == FALSE)
	{
		log_print_error("gui_act_init_ui(), could not allocate nodes for images");
		goto _RETURN_ERROR;
	}

	// Allocate maximum log sizes
	if(_create_mls_nodes(
		&_mls_nodes,
		prefs_get_list_of_maximum_log_sizes(),
		prefs_get_number_of_maximum_log_sizes()) == FALSE)
	{
		log_print_error("gui_act_init_ui(), could not allocate nodes for log sizes");
		goto _RETURN_ERROR;
	}

	// Allocate maximum file sizes
	if(_create_mfs_nodes(
		&_mfs_nodes,
		prefs_get_list_of_maximum_file_sizes(),
		prefs_get_number_of_maximum_file_sizes()) == FALSE)
	{
		log_print_error("gui_act_init_ui(), could not allocate nodes for file sizes");
		goto _RETURN_ERROR;
	}

	// Allocate log nodes
	if(_create_log_nodes(
		&_log_nodes) == FALSE)
	{
		log_print_error("gui_act_init_ui(), could not allocate nodes for log");
		goto _RETURN_ERROR;
	}

	// Create image paths
	GetCurrentDirName(current_directory_name, GLOBALS_MAX_LINE_LENGTH);
	utils_guis_get_image_path(current_directory_name, "", "Radio107x107.iff", image_path_for_about);
	utils_guis_get_image_path(current_directory_name, "", "Play40x40.iff",    image_path_for_play);
	utils_guis_get_image_path(current_directory_name, "", "Replay40x40.iff",  image_path_for_replay);
	utils_guis_get_image_path(current_directory_name, "", "Pause40x40.iff",   image_path_for_pause);
	utils_guis_get_image_path(current_directory_name, "", "Stop40x40.iff",    image_path_for_stop);
	utils_guis_get_image_path(current_directory_name, "", "Next40x40.iff",    image_path_for_next);
	utils_guis_get_image_path(current_directory_name, "", "Reset10x10.iff",   image_path_for_reset);
	utils_guis_get_image_path(current_directory_name, "", "Prev10x10.iff",    image_path_for_prevs);
	utils_guis_get_image_path(current_directory_name, "", "Next10x10.iff",    image_path_for_nexts);
	utils_guis_get_image_path(current_directory_name, "", "Set10x10.iff",     image_path_for_set);
	utils_guis_get_image_path(current_directory_name, "", "List10x10.iff",    image_path_for_list);
	utils_guis_get_image_path(current_directory_name, "", "Ban10x10.iff",     image_path_for_ban);
	utils_guis_get_image_path(current_directory_name, "", "Save10x10.iff",    image_path_for_save);
	utils_guis_get_image_path(current_directory_name, "", "Mail10x10.iff",    image_path_for_email);

	// Create the GUI
	(*ui_object)->CLICKTAB_pages = NewObject(
		PAGE_GetClass(),    NULL,
		GA_TabCycle,        TRUE,
		LAYOUT_Orientation, LAYOUT_VERTICAL,
		LAYOUT_SpaceOuter,  TRUE,

		/*
		 * The definition of the commands page.
		 */

		PAGE_Add, NewObject(
			LAYOUT_GetClass(),     NULL,
			GA_TabCycle,           TRUE,
			LAYOUT_SpaceOuter,     TRUE,
			LAYOUT_Orientation,    LAYOUT_VERTICAL,
			LAYOUT_HorizAlignment, LALIGN_CENTER,
			LAYOUT_VertAlignment,  LALIGN_CENTER,
			LAYOUT_AddChild,       NewObject(
				LAYOUT_GetClass(), NULL,
				LAYOUT_Orientation, LAYOUT_HORIZONTAL,
				LAYOUT_VertAlignment, LALIGN_CENTER,
				// About
				LAYOUT_AddChild, NewObject(
					BUTTON_GetClass(),  NULL,
					GA_ID,              CTRL_ABOUT,
					GA_RelVerify,       TRUE,
					BUTTON_RenderImage, NewObject(
						BITMAP_GetClass(), NULL,
						BITMAP_Screen,     _screen,
						BITMAP_SourceFile, image_path_for_about,
						TAG_END),
					TAG_END),
				CHILD_WeightedHeight, 0,
				CHILD_WeightedWidth,  0,
				LAYOUT_AddChild, NewObject(
					LAYOUT_GetClass(), NULL,
					LAYOUT_Orientation, LAYOUT_VERTICAL,
					// First line
					LAYOUT_AddChild, NewObject(
						LAYOUT_GetClass(), NULL,
						LAYOUT_Orientation, LAYOUT_HORIZONTAL,
						LAYOUT_AddChild, NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ID,              CTRL_PLAY,
							GA_RelVerify,       TRUE,
							BUTTON_RenderImage, NewObject(
								BITMAP_GetClass(), NULL,
								BITMAP_Screen,     _screen,
								BITMAP_SourceFile, image_path_for_play,
								TAG_END),
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						LAYOUT_AddChild, NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ID,              CTRL_REPLAY,
							GA_RelVerify,       TRUE,
							BUTTON_RenderImage, NewObject(
								BITMAP_GetClass(), NULL,
								BITMAP_Screen,     _screen,
								BITMAP_SourceFile, image_path_for_replay,
								TAG_END),
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						LAYOUT_AddChild, NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ID,              CTRL_PAUSE,
							GA_RelVerify,       TRUE,
							BUTTON_RenderImage, NewObject(
								BITMAP_GetClass(), NULL,
								BITMAP_Screen,     _screen,
								BITMAP_SourceFile, image_path_for_pause,
								TAG_END),
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						LAYOUT_AddChild, NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ID,              CTRL_STOP,
							GA_RelVerify,       TRUE,
							BUTTON_RenderImage, NewObject(
								BITMAP_GetClass(), NULL,
								BITMAP_Screen,     _screen,
								BITMAP_SourceFile, image_path_for_stop,
								TAG_END),
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						LAYOUT_AddChild, NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ID,              CTRL_NEXT,
							GA_RelVerify,       TRUE,
							BUTTON_RenderImage, NewObject(
								BITMAP_GetClass(), NULL,
								BITMAP_Screen,     _screen,
								BITMAP_SourceFile, image_path_for_next,
								TAG_END),
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						TAG_END),
					// Second line
					LAYOUT_AddChild, NewObject(
						LAYOUT_GetClass(), NULL,
						LAYOUT_Orientation, LAYOUT_HORIZONTAL,
						LAYOUT_AddChild, (*ui_object)->STR_controls_root_directory = NewObject(
							STRING_GetClass(),     NULL,
							GA_ReadOnly,           TRUE,
							STRINGA_Justification, BCJ_LEFT,
							TAG_END),
						LAYOUT_AddChild, NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ID,              CTRL_SOURCE_RESET,
							GA_RelVerify,       TRUE,
							BUTTON_RenderImage, NewObject(
								BITMAP_GetClass(), NULL,
								BITMAP_Screen,     _screen,
								BITMAP_SourceFile, image_path_for_reset,
								TAG_END),
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						LAYOUT_AddChild, NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ID,              CTRL_SOURCE_PREV,
							GA_RelVerify,       TRUE,
							BUTTON_RenderImage, NewObject(
								BITMAP_GetClass(), NULL,
								BITMAP_Screen,     _screen,
								BITMAP_SourceFile, image_path_for_prevs,
								TAG_END),
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						LAYOUT_AddChild, NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ID,              CTRL_SOURCE_NEXT,
							GA_RelVerify,       TRUE,
							BUTTON_RenderImage, NewObject(
								BITMAP_GetClass(), NULL,
								BITMAP_Screen,     _screen,
								BITMAP_SourceFile, image_path_for_nexts,
								TAG_END),
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						TAG_END),
					// Third line
					LAYOUT_AddChild, NewObject(
						LAYOUT_GetClass(), NULL,
						LAYOUT_Orientation, LAYOUT_HORIZONTAL,
						LAYOUT_AddChild, (*ui_object)->STR_controls_directory = NewObject(
							STRING_GetClass(),     NULL,
							GA_ID,                 CTRL_DIRECTORY,
							GA_RelVerify,          TRUE,
							STRINGA_Justification, BCJ_LEFT,
							STRINGA_MaxChars,      SG_DEFAULTMAXCHARS,
							STRINGA_MinVisible,    26,
							TAG_END),
						LAYOUT_AddChild, NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ID,              CTRL_DIRECTORY_SET,
							GA_RelVerify,       TRUE,
							BUTTON_RenderImage, NewObject(
								BITMAP_GetClass(), NULL,
								BITMAP_Screen,     _screen,
								BITMAP_SourceFile, image_path_for_set,
								TAG_END),
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						LAYOUT_AddChild, (*ui_object)->BTN_controls_directory_list = NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ID,              CTRL_DIRECTORY_LIST,
							GA_RelVerify,       TRUE,
							GA_ToggleSelect,    TRUE,
							BUTTON_RenderImage, NewObject(
								BITMAP_GetClass(), NULL,
								BITMAP_Screen,     _screen,
								BITMAP_SourceFile, image_path_for_list,
								TAG_END),
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						LAYOUT_AddChild, (*ui_object)->BTN_controls_directory_ban = NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ID,              CTRL_DIRECTORY_BAN,
							GA_RelVerify,       TRUE,
							BUTTON_RenderImage, NewObject(
								BITMAP_GetClass(), NULL,
								BITMAP_Screen,     _screen,
								BITMAP_SourceFile, image_path_for_ban,
								TAG_END),
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						LAYOUT_AddChild, NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ID,              CTRL_DIRECTORY_BAN_LIST,
							GA_RelVerify,       TRUE,
							GA_ToggleSelect,    TRUE,
							BUTTON_RenderImage, NewObject(
								BITMAP_GetClass(), NULL,
								BITMAP_Screen,     _screen,
								BITMAP_SourceFile, image_path_for_list,
								TAG_END),
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						TAG_END),
					// Fourth line
					LAYOUT_AddChild, NewObject(
						LAYOUT_GetClass(), NULL,
						LAYOUT_Orientation, LAYOUT_HORIZONTAL,
						LAYOUT_AddChild, (*ui_object)->STR_controls_file = NewObject(
							STRING_GetClass(),     NULL,
							GA_ReadOnly,           TRUE,
							STRINGA_Justification, BCJ_LEFT,
							TAG_END),
						LAYOUT_AddChild, (*ui_object)->BTN_controls_file_list = NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ID,              CTRL_FILE_LIST,
							GA_RelVerify,       TRUE,
							GA_ToggleSelect,    TRUE,
							BUTTON_RenderImage, NewObject(
								BITMAP_GetClass(), NULL,
								BITMAP_Screen,     _screen,
								BITMAP_SourceFile, image_path_for_list,
								TAG_END),
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						LAYOUT_AddChild, (*ui_object)->BTN_controls_file_ban  =NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ID,              CTRL_FILE_BAN,
							GA_RelVerify,       TRUE,
							BUTTON_RenderImage, NewObject(
								BITMAP_GetClass(), NULL,
								BITMAP_Screen,     _screen,
								BITMAP_SourceFile, image_path_for_ban,
								TAG_END),
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						LAYOUT_AddChild, NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ID,              CTRL_FILE_BAN_LIST,
							GA_RelVerify,       TRUE,
							GA_ToggleSelect,    TRUE,
							BUTTON_RenderImage, NewObject(
								BITMAP_GetClass(), NULL,
								BITMAP_Screen,     _screen,
								BITMAP_SourceFile, image_path_for_list,
								TAG_END),
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						TAG_END),
					// Fifth line
					LAYOUT_AddChild, NewObject(
						LAYOUT_GetClass(), NULL,
						LAYOUT_Orientation, LAYOUT_HORIZONTAL,
						LAYOUT_AddChild, (*ui_object)->STR_controls_modules = NewObject(
							STRING_GetClass(),     NULL,
							GA_ReadOnly,           TRUE,
							STRINGA_Justification, BCJ_LEFT,
							TAG_END),
						LAYOUT_AddChild, (*ui_object)->BTN_controls_modules_save = NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ID,              CTRL_MODULES_SAVE,
							GA_RelVerify,       TRUE,
							BUTTON_RenderImage, NewObject(
								BITMAP_GetClass(), NULL,
								BITMAP_Screen,     _screen,
								BITMAP_SourceFile, image_path_for_save,
								TAG_END),
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						LAYOUT_AddChild, (*ui_object)->BTN_controls_modules_email = NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ID,              CTRL_MODULES_MAIL,
							GA_RelVerify,       TRUE,
							BUTTON_RenderImage, NewObject(
								BITMAP_GetClass(), NULL,
								BITMAP_Screen,     _screen,
								BITMAP_SourceFile, image_path_for_email,
								TAG_END),
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						TAG_END),
					// Sixth line
					LAYOUT_AddChild, NewObject(
						LAYOUT_GetClass(), NULL,
						LAYOUT_Orientation, LAYOUT_HORIZONTAL,
						LAYOUT_AddChild, (*ui_object)->BTN_controls_user_rating[0] = NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ID,              CTRL_USER_RATING_ONE,
							GA_RelVerify,       TRUE,
							GA_Text,            "_1",
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						LAYOUT_AddChild, (*ui_object)->BTN_controls_user_rating[1] = NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ID,              CTRL_USER_RATING_TWO,
							GA_RelVerify,       TRUE,
							GA_Text,            "_2",
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						LAYOUT_AddChild, (*ui_object)->BTN_controls_user_rating[2] = NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ID,              CTRL_USER_RATING_THREE,
							GA_RelVerify,       TRUE,
							GA_Text,            "_3",
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						LAYOUT_AddChild, (*ui_object)->BTN_controls_user_rating[3] = NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ID,              CTRL_USER_RATING_FOUR,
							GA_RelVerify,       TRUE,
							GA_Text,            "_4",
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						LAYOUT_AddChild, (*ui_object)->BTN_controls_user_rating[4] = NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ID,              CTRL_USER_RATING_FIVE,
							GA_RelVerify,       TRUE,
							GA_Text,            "_5",
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						LAYOUT_AddChild, (*ui_object)->BTN_controls_all_rating[0] = NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ReadOnly,        TRUE,
							GA_Text,            "1",
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						LAYOUT_AddChild, (*ui_object)->BTN_controls_all_rating[1] = NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ReadOnly,        TRUE,
							GA_Text,            "2",
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						LAYOUT_AddChild, (*ui_object)->BTN_controls_all_rating[2] = NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ReadOnly,        TRUE,
							GA_Text,            "3",
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						LAYOUT_AddChild, (*ui_object)->BTN_controls_all_rating[3] = NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ReadOnly,        TRUE,
							GA_Text,            "4",
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						LAYOUT_AddChild, (*ui_object)->BTN_controls_all_rating[4] = NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ReadOnly,        TRUE,
							GA_Text,            "5",
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						LAYOUT_AddChild, (*ui_object)->BTN_controls_all_tally = NewObject(
							BUTTON_GetClass(),  NULL,
							GA_ReadOnly,        TRUE,
							GA_Text,            "   0",
							TAG_END),
						CHILD_WeightedHeight, 0,
						CHILD_WeightedWidth,  0,
						TAG_END),
					TAG_END),
				CHILD_WeightedHeight, 0,
				TAG_END),
			CHILD_WeightedHeight, 0,
			CHILD_WeightedWidth,  0,
			TAG_END),

		/*
		 * The definition of the prefs page.
		 */

		PAGE_Add, NewObject(
			LAYOUT_GetClass(),  NULL,
			GA_TabCycle,        TRUE,
			LAYOUT_Orientation, LAYOUT_VERTICAL,
			LAYOUT_SpaceOuter,  TRUE,
			LAYOUT_AddChild, NewObject(
				LAYOUT_GetClass(),  NULL,
				LAYOUT_Orientation, LAYOUT_VERTICAL,
				LAYOUT_AddChild, NewObject(
					LAYOUT_GetClass(),  NULL,
					LAYOUT_Orientation, LAYOUT_VERTICAL,
					LAYOUT_SpaceOuter,  TRUE,
					LAYOUT_BevelStyle,  GroupFrame,
					LAYOUT_AddChild,    (*ui_object)->CKB_prefs_save_prefs_on_exit = NewObject(
						CHECKBOX_GetClass(), NULL,
						GA_ID,               PREFS_SAVE_PREFERENCES_ON_EXIT,
						GA_RelVerify,        TRUE,
						GA_Text,             GetString( MSG_MUI_SAVEPREFERENCESONEXIT ),
						CHECKBOX_TextPlace,  PLACETEXT_RIGHT,
						CHECKBOX_Checked,    prefs_should_save_prefs_on_exit(),
						TAG_END),
					LAYOUT_AddChild,    (*ui_object)->CKB_prefs_play_on_startup = NewObject(
						CHECKBOX_GetClass(), NULL,
						GA_ID,               PREFS_PLAY_ON_STARTUP,
						GA_RelVerify,        TRUE,
						GA_Text,             GetString( MSG_MUI_PLAYONSTARTUP ),
						CHECKBOX_TextPlace,  PLACETEXT_RIGHT,
						CHECKBOX_Checked,    prefs_should_play_on_startup(),
						TAG_END),
					LAYOUT_AddChild, NewObject(
						LAYOUT_GetClass(),  NULL,
						LAYOUT_Orientation, LAYOUT_HORIZONTAL,
						LAYOUT_AddChild,    (*ui_object)->CKB_prefs_show_log_page_on_fetch = NewObject(
							CHECKBOX_GetClass(), NULL,
							GA_ID,               PREFS_SHOW_LOG_PAGE_ON_FETCH,
							GA_RelVerify,        TRUE,
							GA_Text,             GetString( MSG_MUI_SHOWLOGPAGEONFETCH ),
							CHECKBOX_TextPlace,  PLACETEXT_RIGHT,
							CHECKBOX_Checked,    prefs_should_show_log_page_on_fetch(),
							TAG_END),
						LAYOUT_AddChild,    (*ui_object)->CHR_prefs_choose_maximum_log_sizes = NewObject(
							CHOOSER_GetClass(), NULL,
							GA_ID,             PREFS_CHOOSE_MAXIMUM_LOG_SIZES,
							GA_RelVerify,      TRUE,
							CHOOSER_PopUp,     TRUE,
							CHOOSER_MaxLabels, prefs_get_number_of_maximum_log_sizes(), // Must be before CHOOSER_Labels
							CHOOSER_Selected,  prefs_get_maximum_log_size_index(),
							CHOOSER_Labels,    &_mls_nodes,
							TAG_END),
							// TODO For some reason, it doesn't appear...
							LAYOUT_AddChild, LabelObject,
								LABEL_Text,  GetString( MSG_MUI_KILOBYTES ),
								LabelEnd,
						TAG_END),
					LAYOUT_AddChild,    (*ui_object)->CKB_prefs_remember_source_between_runs = NewObject(
						CHECKBOX_GetClass(), NULL,
						GA_ID,               PREFS_REMEMBER_SOURCE_BETWEEN_RUNS,
						GA_RelVerify,        TRUE,
						GA_Text,             GetString( MSG_MUI_REMEMBERSOURCEBETWEENRUNS ),
						CHECKBOX_TextPlace,  PLACETEXT_RIGHT,
						CHECKBOX_Checked,    prefs_should_remember_source_between_runs(),
						TAG_END),
					LAYOUT_AddChild,    (*ui_object)->CHR_prefs_choose_images_set = NewObject(
						CHOOSER_GetClass(), NULL,
						GA_ID,             PREFS_CHOOSE_IMAGES_SET,
						GA_RelVerify,      TRUE,
						CHOOSER_PopUp,     TRUE,
						CHOOSER_MaxLabels, prefs_get_number_of_images_sets(),    // Must be before CHOOSER_Labels
						CHOOSER_Selected,  prefs_get_current_images_set_index(),
						CHOOSER_Labels,    &_img_nodes,
						TAG_END),
					TAG_END),
				LAYOUT_AddChild, NewObject(
					LAYOUT_GetClass(),  NULL,
					LAYOUT_Orientation, LAYOUT_VERTICAL,
					LAYOUT_SpaceOuter,  TRUE,
					LAYOUT_BevelStyle,  GroupFrame,
					LAYOUT_AddChild, NewObject(
						LAYOUT_GetClass(),  NULL,
						LAYOUT_Orientation, LAYOUT_HORIZONTAL,
						LAYOUT_AddChild,    (*ui_object)->STR_prefs_saved_module_directory = NewObject(
							STRING_GetClass(),  NULL,
							GA_ID,              PREFS_SAVED_MODULES_DIRECTORY_STRING,
							GA_RelVerify,       TRUE,
							STRINGA_TextVal,    prefs_get_saved_modules_directory(),
							STRINGA_MinVisible, 10,
							STRINGA_MaxChars,   80,
							TAG_END),
						CHILD_Label, LabelObject,
							LABEL_Text, GetString( MSG_MUI_MODULESDIRECTORY ),
							LabelEnd,
						LAYOUT_AddChild,    (*ui_object)->BTN_prefs_saved_module_directory = NewObject(
							BUTTON_GetClass(), NULL,
							GA_ID,             PREFS_SAVED_MODULES_DIRECTORY_POPDRAWER,
							GA_RelVerify,      TRUE,
							BUTTON_AutoButton, BAG_POPDRAWER,
							TAG_END),
						CHILD_WeightedWidth, 0,
						TAG_END),
					TAG_END),
				LAYOUT_AddChild, NewObject(
					LAYOUT_GetClass(),  NULL,
					LAYOUT_Orientation, LAYOUT_VERTICAL,
					LAYOUT_SpaceOuter,  TRUE,
					LAYOUT_BevelStyle,  GroupFrame,
					LAYOUT_AddChild,    (*ui_object)->CKB_prefs_should_cache_list_during_run = NewObject(
						CHECKBOX_GetClass(), NULL,
						GA_ID,               PREFS_CACHE_DIRECTORIES_MODULES_LISTS_DURING_RUN,
						GA_RelVerify,        TRUE,
						GA_Text,             GetString( MSG_MUI_CACHEDIRECTORIESMODULESLISTSDURINGRUN ),
						CHECKBOX_TextPlace,  PLACETEXT_RIGHT,
						CHECKBOX_Checked,    prefs_should_cache_list_during_run(),
						TAG_END),
					LAYOUT_AddChild,    (*ui_object)->CKB_prefs_should_cache_list_between_runs = NewObject(
						CHECKBOX_GetClass(), NULL,
						GA_ID,               PREFS_CACHE_DIRECTORIES_MODULES_LISTS_BETWEEN_RUNS,
						GA_RelVerify,        TRUE,
						GA_Text,             GetString( MSG_MUI_CACHEDIRECTORIESMODULESLISTSBETWEENRUNS ),
						CHECKBOX_TextPlace,  PLACETEXT_RIGHT,
						CHECKBOX_Checked,    prefs_should_cache_list_between_runs(),
						TAG_END),
					LAYOUT_AddChild, NewObject(
						LAYOUT_GetClass(),  NULL,
						LAYOUT_Orientation, LAYOUT_HORIZONTAL,
						LAYOUT_AddChild,    (*ui_object)->STR_prefs_cache_directory = NewObject(
							STRING_GetClass(),  NULL,
							GA_ID,              PREFS_CACHE_DIRECTORY_STRING,
							GA_RelVerify,       TRUE,
							STRINGA_TextVal,    prefs_get_cache_directory(),
							STRINGA_MinVisible, 10,
							STRINGA_MaxChars,   80,
							TAG_END),
						CHILD_Label, LabelObject,
							LABEL_Text, GetString( MSG_MUI_CACHEDIRECTORY ),
							LabelEnd,
						LAYOUT_AddChild,    (*ui_object)->BTN_prefs_cache_directory = NewObject(
							BUTTON_GetClass(), NULL,
							GA_ID,             PREFS_CACHE_DIRECTORY_POPDRAWER,
							GA_RelVerify,      TRUE,
							BUTTON_AutoButton, BAG_POPDRAWER,
							TAG_END),
						CHILD_WeightedWidth, 0,
						TAG_END),
					TAG_END),
				LAYOUT_AddChild, NewObject(
					LAYOUT_GetClass(),  NULL,
					LAYOUT_Orientation, LAYOUT_VERTICAL,
					LAYOUT_SpaceOuter,  TRUE,
					LAYOUT_BevelStyle,  GroupFrame,
					LAYOUT_AddChild,    (*ui_object)->CKB_prefs_should_skip_if_banned = NewObject(
						CHECKBOX_GetClass(), NULL,
						GA_ID,               PREFS_SKIP_MODULES_WHEN_BANNED,
						GA_RelVerify,        TRUE,
						GA_Text,             GetString( MSG_MUI_SKIPMODULESWHENBANNED ),
						CHECKBOX_TextPlace,  PLACETEXT_RIGHT,
						CHECKBOX_Checked,    prefs_should_skip_if_banned(),
						TAG_END),
					LAYOUT_AddChild,    (*ui_object)->CKB_prefs_should_skip_if_user_rating_low = NewObject(
						CHECKBOX_GetClass(), NULL,
						GA_ID,               PREFS_SKIP_MODULES_WHEN_RATING_LOW,
						GA_RelVerify,        TRUE,
						GA_Text,             GetString( MSG_MUI_SKIPMODULESWHENRATINGLOW ),
						CHECKBOX_TextPlace,  PLACETEXT_RIGHT,
						CHECKBOX_Checked,    prefs_should_skip_if_user_rating_low(),
						TAG_END),
					LAYOUT_AddChild,    (*ui_object)->CKB_prefs_should_skip_if_all_rating_exists = NewObject(
						CHECKBOX_GetClass(), NULL,
						GA_ID,               PREFS_SKIP_MODULES_WHEN_ALREADY_RATED,
						GA_RelVerify,        TRUE,
						GA_Text,             GetString( MSG_MUI_SKIPMODULESWHENALREADYRATED ),
						CHECKBOX_TextPlace,  PLACETEXT_RIGHT,
						CHECKBOX_Checked,    prefs_should_skip_if_all_rating_exists(),
						TAG_END),
					LAYOUT_AddChild,    (*ui_object)->CKB_prefs_should_skip_if_all_rating_low = NewObject(
						CHECKBOX_GetClass(), NULL,
						GA_ID,               PREFS_SKIP_MODULES_WHEN_ALREADY_RATED_TOO_LOW,
						GA_RelVerify,        TRUE,
						GA_Text,             GetString( MSG_MUI_SKIPMODULESWHENALREADYRATEDTOOLOW ),
						CHECKBOX_TextPlace,  PLACETEXT_RIGHT,
						CHECKBOX_Checked,    prefs_should_skip_if_all_rating_low(),
						TAG_END),
					LAYOUT_AddChild, NewObject(
						LAYOUT_GetClass(),  NULL,
						LAYOUT_Orientation, LAYOUT_HORIZONTAL,
						LAYOUT_AddChild,    (*ui_object)->CKB_prefs_should_skip_if_above_maximum_file_size = NewObject(
							CHECKBOX_GetClass(), NULL,
							GA_ID,               PREFS_SKIP_MODULES_IF_ABOVE_MAXIMUM_FILE_SIZES,
							GA_RelVerify,        TRUE,
							GA_Text,             GetString( MSG_MUI_SKIPMODULESWHICHSIZESAREABOVEMAXIMUMSIZEOF ),
							CHECKBOX_TextPlace,  PLACETEXT_RIGHT,
							CHECKBOX_Checked,    prefs_should_skip_if_above_maximum_file_size(),
							TAG_END),
						LAYOUT_AddChild,    (*ui_object)->CHR_prefs_choose_maximum_file_size = NewObject(
							CHOOSER_GetClass(), NULL,
							GA_ID,             PREFS_CHOOSE_MAXIMUM_FILE_SIZES,
							GA_RelVerify,      TRUE,
							CHOOSER_PopUp,     TRUE,
							CHOOSER_MaxLabels, prefs_get_number_of_maximum_file_sizes(), // Must be before CHOOSER_Labels
							CHOOSER_Selected,  prefs_get_maximum_file_size_index(),
							CHOOSER_Labels,    &_mfs_nodes,
							TAG_END),
						// TODO For some reason, it doesn't appear...
						LAYOUT_AddChild, LabelObject,
							LABEL_Text,  GetString( MSG_MUI_KILOBYTES ) ,
							LabelEnd,
						TAG_END),
					TAG_END),
				TAG_END),
			LAYOUT_AddChild, NewObject(
				LAYOUT_GetClass(),  NULL,
				LAYOUT_Orientation, LAYOUT_HORIZONTAL,
				LAYOUT_AddChild,    NewObject(
					BUTTON_GetClass(), NULL,
					GA_ID,             PREFS_SAVE,
					GA_RelVerify,      TRUE,
					GA_Text,           GetString( MSG_MUI_SAVE ),
					TAG_END),
				LAYOUT_AddChild,    NewObject(
					BUTTON_GetClass(), NULL,
					GA_ID,             PREFS_RESET,
					GA_RelVerify,      TRUE,
					GA_Text,           GetString( MSG_MUI_RESET ),
					TAG_END),
				LAYOUT_AddChild,    NewObject(
					BUTTON_GetClass(), NULL,
					GA_ID,             PREFS_DEFAULT,
					GA_RelVerify,      TRUE,
					GA_Text,           GetString( MSG_MUI_DEFAULT ),
					TAG_END),
				TAG_END),
			CHILD_WeightedHeight, 0,
			TAG_END),

		/*
		 * The definition of the log page.
		 */

		PAGE_Add, NewObject(
			LAYOUT_GetClass(),  NULL,
			GA_TabCycle,        TRUE,
			LAYOUT_Orientation, LAYOUT_VERTICAL,
			LAYOUT_SpaceOuter,  TRUE,
			LAYOUT_AddChild,    (*ui_object)->LBR_log_list = NewObject(
				LISTBROWSER_GetClass(),     NULL,
				GA_ReadOnly,                TRUE,
				LISTBROWSER_Labels,         &_log_nodes,
				LISTBROWSER_AutoFit,        TRUE,
				LISTBROWSER_HorizontalProp, TRUE,
				TAG_END),
			LAYOUT_AddChild, NewObject(
				LAYOUT_GetClass(),  NULL,
				LAYOUT_Orientation, LAYOUT_HORIZONTAL,
				LAYOUT_AddChild,    NewObject(
					BUTTON_GetClass(), NULL,
					GA_ID,             LOG_SAVE,
					GA_RelVerify,      TRUE,
					GA_Text,           GetString( MSG_MUI_SAVE ),
					TAG_END),
				LAYOUT_AddChild,    NewObject(
					BUTTON_GetClass(), NULL,
					GA_ID,             LOG_CLEAR,
					GA_RelVerify,      TRUE,
					GA_Text,           GetString( MSG_MUI_CLEAR ),
					TAG_END),
				TAG_END),
			CHILD_WeightedHeight, 0,
			TAG_END),
		 TAG_END);

	(*ui_object)->CLICKTAB_object = NewObject(
		CLICKTAB_GetClass(), NULL,
		GA_TabCycle,        TRUE,
		CLICKTAB_Labels,    &_tab_nodes,
		CLICKTAB_Current,   0L,
		CLICKTAB_PageGroup, (*ui_object)->CLICKTAB_pages,
		TAG_END);

	lay_object = NewObject(
		LAYOUT_GetClass(),   NULL,
		LAYOUT_Orientation,  LAYOUT_VERTICAL,
		LAYOUT_SpaceOuter,   TRUE,
		LAYOUT_AddChild,     (*ui_object)->CLICKTAB_object,
		LAYOUT_AddChild,     (*ui_object)->STR_controls_last_message = NewObject(
			STRING_GetClass(),     NULL,
			GA_ReadOnly,           TRUE,
			STRINGA_Justification, BCJ_LEFT,
			TAG_END),
		LAYOUT_AddChild,     (*ui_object)->FGA_controls_progress_bar = NewObject(
			FUELGAUGE_GetClass(), NULL,
			GA_ReadOnly,       TRUE,
			FUELGAUGE_Level,   0,
			FUELGAUGE_Max,     100,
			FUELGAUGE_Percent, FALSE,
			FUELGAUGE_Ticks,   FALSE,
			TAG_END),
		CHILD_WeightedHeight, 0,
		CHILD_MinHeight,      6,
		TAG_END);

	win_object = NewObject(
		WINDOW_GetClass(),  NULL,
		WA_Activate,          TRUE,
		WA_ScreenTitle,       version_get_version_description(),
		WA_Title,             "AmiModRadio",
		WA_Left,              prefs_get_main_window_X(),
		WA_Top,               prefs_get_main_window_Y(),
		WA_CloseGadget,       TRUE,
		WA_DepthGadget,       TRUE,
		WA_DragBar,           TRUE,
		WA_SmartRefresh,      TRUE,
		WA_IDCMP,             IDCMP_CHANGEWINDOW,
		WINDOW_ParentGroup,   lay_object,
		WINDOW_AppPort,       port,
		WINDOW_IconifyGadget, TRUE,
		WINDOW_IconTitle,     "AmiModRadio",
		WINDOW_Icon,          utils_guis_get_disk_object(),
		TAG_END);

	if(win_object == NULL)
	{
		log_print_error("gui_act_init_ui(), could not create window object");
		goto _RETURN_ERROR;
	}
	(*ui_object)->WINDOW_object = win_object;

	window = (struct Window *)CA_OpenWindow(win_object);
	if(window == NULL)
	{
		log_print_error("gui_act_init_ui(), could not create window");
		goto _RETURN_ERROR;
	}
	(*ui_object)->window = window;

	goto _RETURN_OK;
	_RETURN_OK:
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		_free_tab_nodes(&_tab_nodes);
		_free_img_nodes(&_img_nodes);
		_free_mls_nodes(&_mls_nodes);
		_free_mfs_nodes(&_mfs_nodes);
		_free_log_nodes(&_log_nodes);
		if(*ui_object != NULL && (*ui_object)->WINDOW_object != NULL)
		{
			DisposeObject((*ui_object)->WINDOW_object);
			(*ui_object)->WINDOW_object = NULL;
		}
		if(*ui_object != NULL)
		{
			FreeVec(*ui_object);
			*ui_object = NULL;
		}
		if(_screen != NULL)
		{
			UnlockPubScreen(NULL, _screen);
			_screen = NULL;
		}
		return RETURN_ERROR;
}

void gui_act_close_ui(
	 IN struct MsgPort  *port,
	 IN struct ACT_UI  **ui_object)
{
	log_print_debug("gui_act_close_ui()\n");

	if(*ui_object != NULL && (*ui_object)->WINDOW_object != NULL)
	{
		_free_tab_nodes(&_tab_nodes);
		_free_img_nodes(&_img_nodes);
		_free_mls_nodes(&_mls_nodes);
		_free_mfs_nodes(&_mfs_nodes);
		_free_log_nodes(&_log_nodes);
		DisposeObject((*ui_object)->WINDOW_object);
		(*ui_object)->WINDOW_object = NULL;
	}

	// Something already has free'd this port!?
	/*
	if(port != NULL)
	{
		DeleteMsgPort((struct MsgPort *)port);
		port = NULL;
	}
	*/

	// Something already has free'd this disk object!?
	/*
	if(_disk_object != NULL)
	{
		FreeDiskObject(_disk_object);
		_disk_object = NULL;
	}
	*/

	if(*ui_object != NULL)
	{
		FreeVec(*ui_object);
		*ui_object = NULL;
	}

	if(_screen != NULL)
	{
		UnlockPubScreen(NULL, _screen);
		_screen = NULL;
	}
}

void gui_act_child_window_list_open(
	IN  struct ACT_UI  *ui_object,
	IN  char           *list,
	IN  char           *help,
	IN  char           *title,
	OUT struct Gadget **list_object,
	OUT Object        **win_object)
{
	char         *list_copy    = NULL;
	int           number_lines = 0;
	char        **array_labels = NULL;
	struct List   list_labels;
	int           i            = 0;
	struct Node  *label        = NULL;

	// Create array of labels
	list_copy      = string_duplicate(list);
	if(list_object == NULL)
	{
		return;
	}
	number_lines    = string_count_char(list_copy, '\n') + 1;
	array_labels    = malloc(number_lines * sizeof(char *));
	if(array_labels == NULL)
	{
		return;
	}

	number_lines = 0;
	array_labels[number_lines] = strtok(list_copy, "\n");
	while(array_labels[number_lines] != NULL)
	{
		number_lines++;
		array_labels[number_lines] = strtok(NULL, "\n");
	}

	// Create list of labels
	NewList(&list_labels);

	number_lines = 0;
	while(array_labels[number_lines] != NULL)
	{
		if(label = AllocListBrowserNode(
			1,
			LBNA_Column,    0,
			LBNCA_CopyText, TRUE,
			LBNCA_Text,     array_labels[number_lines],
			LBNCA_Editable, FALSE,
			TAG_DONE))
		{
			AddTail(&list_labels, label);
		}
		else
		{
			break;
		}
		number_lines++;
	}

	// Free temporary data
	free(array_labels);
	free(list_copy);

	// Create list browser
	*list_object = NewObject(
		LISTBROWSER_GetClass(), NULL,
		GA_ID,                      1,
		GA_Top,                     10,
		GA_Left,                    10,
		GA_Width,                   200,
		GA_Height,                  200,
		GA_RelVerify,               TRUE,
		LISTBROWSER_Labels,         (ULONG)&list_labels,
		LISTBROWSER_VerticalProp,   TRUE,
		LISTBROWSER_HorizontalProp, TRUE,
		TAG_END);

	if(*list_object == NULL)
	{
		return;
	}

	// Create window
	*win_object = WindowObject,
		WA_Title,           title,
		WA_Left,            prefs_get_main_window_X(),
		WA_Top,             prefs_get_main_window_Y(),
		WA_Width, 			210,
		WA_Height, 			210,
		WA_DepthGadget,     TRUE,
		WA_DragBar,         TRUE,
		// WA_SizeGadget,      TRUE,
		// WA_CloseGadget,     TRUE,
		WA_Activate,        TRUE,
		WA_SmartRefresh,    TRUE,
		WINDOW_ParentGroup, LayoutObject,
			LAYOUT_Orientation, LAYOUT_VERTICAL,
			LAYOUT_AddChild,    *list_object,
		End,
	End;

	if(*win_object == NULL)
	{
		return;
	}

	DoMethod(
		ui_object->WINDOW_object,
		OM_ADDMEMBER,
		*win_object);
	DoMethod(
		*win_object,
		WM_OPEN);
}

/*
void gui_act_child_window_list_update(
	IN APTR	 list_view,
	IN char *list)
{
	char *token     = NULL;
	char *list_temp = NULL;

	if(list_view == NULL)
	{
		return;
	}

	DoMethod(
		list_view,
		MUIM_List_Clear);

	// Tygre 2016/09/29: About list
	// The list could be empty:
	// could contain only '\n'.
	if(list == NULL || strlen(list) < 2)
	{
		DoMethod(
			list_view,
			MUIM_List_InsertSingle,
			 GetString( MSG_MUI_NOTHINGTODISPLAY ) ,
			MUIV_List_Insert_Bottom);
		return;
	}

	list_temp = string_duplicate(list);
	if(list_temp == NULL)
	{
		log_print_error( GetString( MSG_MUI_MUICHILDWINDOWLISTUPDATECOULDNOTALLOCATETEMPORARYSTRING ) );
		return;
	}

	SetAttrs(
		list_view,
		MUIA_List_Quiet,
		TRUE,
		TAG_DONE);

	token = strtok(list_temp, "\n");
	while(token != NULL)
	{
		DoMethod(
			list_view,
			MUIM_List_InsertSingle,
			token,
			MUIV_List_Insert_Bottom);

		token = strtok(NULL, "\n");
	}

	SetAttrs(
		list_view,
		MUIA_List_Quiet,
		FALSE,
		TAG_DONE);

	free(list_temp);
}
*/

/*
static void _act_child_window_clicked(
	struct Hook *h,
	Object      *o,
	APTR        *d)
{
	act_child_window_clicked_callback_fp  callback_func = (act_child_window_clicked_callback_fp)d[0];
	APTR                                  view          = (APTR)o;
	char                                 *entry         = NULL;

	DoMethod(
		view,
		MUIM_List_GetEntry,
		MUIV_List_GetEntry_Active,
		&entry);

	if(entry != NULL && !string_start_with(entry, "<") && callback_func != NULL)
	{
		callback_func(entry);
	}
}
*/

/*
static void _act_child_window_closed(
	struct Hook *h,
	Object      *o,
	APTR        *d)
{
	act_child_window_closed_callback_fp  callback_func = (act_child_window_closed_callback_fp)d[0];
	struct MUI_UI                       *object        = (struct MUI_UI *)d[1];
	APTR                                 window        = (APTR)o;

	if(callback_func != NULL)
	{
		callback_func(window);
	}
}
*/

static BOOL _create_tab_nodes(
	INOUT struct List  *list,
	IN    char        **labels)
{
	struct Node *node;
	WORD         i = 0;

	NewList(list);

	while(*labels != NULL)
	{
		if(node = (struct Node *)AllocClickTabNode(
			TNA_Text,    *labels,
			TNA_Number,  i,
			TNA_Enabled, TRUE,
			TNA_Spacing, 6,
			TAG_DONE))
		{
			AddTail(list, node);
		}
		else
		{
			return FALSE;
		}
		labels++;
		i++;
	}

	return TRUE;
}

static void _free_tab_nodes(
	INOUT struct List *list)
{
	struct Node *node      = NULL;
	struct Node *next_node = NULL;

	if(list == NULL)
	{
		return;
	}

	node = list->lh_Head;
	while(next_node = node->ln_Succ)
	{
		FreeClickTabNode(node);
		node = next_node;
	}
	NewList(list);
}

static BOOL _create_img_nodes(
	INOUT struct List  *list,
	IN    char        **labels,
	IN    int           number)
{
	int          i    = 0;
	struct Node *node = NULL;

	NewList(list);

	for(i = 0; i < number; i++)
	{
		if(node = AllocChooserNode(
			CNA_Text, labels[i],
			TAG_DONE))
		{
			AddTail(list, node);
		}
		else
		{
			return FALSE;
		}
	}

	return TRUE;
}

static void _free_img_nodes(
	INOUT struct List *list)
{
	struct Node *node      = NULL;
	struct Node *next_node = NULL;

	if(list == NULL)
	{
		return;
	}

	node = list->lh_Head;
	while(next_node = node->ln_Succ)
	{
		FreeChooserNode(node);
		node = next_node;
	}
	NewList(list);
}

static BOOL _create_mls_nodes(
	INOUT struct List  *list,
	IN    char        **labels,
	IN    int           number)
{
	return _create_img_nodes(list, labels, number);
}

static void _free_mls_nodes(
	INOUT struct List *list)
{
	_free_img_nodes(list);
}

static BOOL _create_mfs_nodes(
	INOUT struct List  *list,
	IN    char        **labels,
	IN    int           number)
{
	return _create_img_nodes(list, labels, number);
}

static void _free_mfs_nodes(
	INOUT struct List *list)
{
	_free_img_nodes(list);
}

static BOOL _create_log_nodes(
	INOUT struct List *list)
{
	NewList(list);

	// Tygre 24/07/07: Inconsistency
	// I cannot allocate nodes from
	// static data because I assume
	// in _free_log_nodes(...) that
	// the labels were malloc'ed.

	return TRUE;
}

static void _free_log_nodes(
	INOUT struct List *list)
{
	struct Node *node      = NULL;
	struct Node *next_node = NULL;
	char        *node_text = NULL;

	if(list == NULL)
	{
		return;
	}

	node = list->lh_Head;
	while(next_node = node->ln_Succ)
	{
		// Tygre 24/07/07: Freeing messages
		// I don't forget to get the messages
		// from the log and to free them...
		GetListBrowserNodeAttrs(
			node,
			LBNA_Column, 0,
			LBNCA_Text,  &node_text);
		free(node_text);
		Remove(node);
		FreeListBrowserNode(node);
		node = next_node;
	}
	NewList(list);
}

