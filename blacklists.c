/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include "blacklists.h"

#include "locale.h"
#include "log.h"
#include "utils.h"
#include "z_fortify.h"

#include <dos/dos.h>    // For RETURN_OK, RETURN_ERROR
#include <errno.h>      // For EINVAL
#include <stdio.h>      // For FILE, ferror()



/* Constants and declarations */

#define FILE_NAME_BLACKLISTED_DIRECTORIES "data/Blacklisted Directories"
#define FILE_NAME_BLACKLISTED_FILES       "data/Blacklisted Files"

	   int  blacklists_setup(void);
	   void blacklists_cleanup(void);
	   int  blacklists_init_files(void);
	   void blacklists_close_files(void);
	   BOOL blacklists_is_blacklisted_directory(IN char *);
	   BOOL blacklists_is_blacklisted_file(IN char *);
	   int  blacklists_list_blacklisted_directories(OUT char **);
	   int  blacklists_list_blacklisted_files(OUT char **);
	   int  blacklists_blacklist_directory(IN char *);
	   int  blacklists_blacklist_file(IN char *);
static BOOL _is_blacklisted_element(IN FILE *, IN char *);
static int  _list_blacklisted_element(IN FILE *, OUT char **);
static int  _blacklist_element(IN FILE *, IN char *);
static int  _get_line(OUT char **, OUT size_t *, IN FILE *);
static FILE *_file_duplicate(IN FILE *);



/* Definitions */

static FILE *_blacklisted_directories_file_pointer = NULL;
static FILE *_blacklisted_files_file_pointer       = NULL;

int  blacklists_setup(
	 void)
{
	log_print_debug("blacklists_setup()\n");

	// Nothing to do

	return RETURN_OK;
}

void blacklists_cleanup(
	 void)
{
	log_print_debug("blacklists_cleanup()\n");

	// Nothing to do
}

int  blacklists_init_files(
	 void)
{
	log_print_debug("blacklists_init_files()\n");

	_blacklisted_directories_file_pointer = fopen(FILE_NAME_BLACKLISTED_DIRECTORIES, "r"); /* Flawfinder: ignore */
	if(_blacklisted_directories_file_pointer == NULL)
	{
		log_print_warning( GetString( MSG_BLACKLISTS_INITBLACKLISTSFILESCANNOTOPENLISTOFBLACKLISTEDDIRECTORIES ) );
		goto _RETURN_WARN;
	}

	_blacklisted_files_file_pointer = fopen(FILE_NAME_BLACKLISTED_FILES, "r");			   /* Flawfinder: ignore */
	if(_blacklisted_files_file_pointer == NULL)
	{
		log_print_warning( GetString( MSG_BLACKLISTS_INITBLACKLISTSFILESCANNOTOPENLISTOFBLACKLISTEDFILES ) );
		goto _RETURN_WARN;
	}

	goto _RETURN_OK;
	_RETURN_OK:
		return RETURN_OK;

	goto _RETURN_WARN;
	_RETURN_WARN:
		blacklists_close_files();
		return RETURN_WARN;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		blacklists_close_files();
		return RETURN_ERROR;
}

void blacklists_close_files(
	 void)
{
	log_print_debug("blacklists_close_files()\n");

	if(_blacklisted_directories_file_pointer != NULL)
	{
		fclose(_blacklisted_directories_file_pointer);
		_blacklisted_directories_file_pointer = NULL;
	}

	if(_blacklisted_files_file_pointer != NULL)
	{
		fclose(_blacklisted_files_file_pointer);
		_blacklisted_files_file_pointer = NULL;
	}
}

BOOL blacklists_is_blacklisted_directory(
	 IN  char  *directory_name)
{
	return _is_blacklisted_element(_blacklisted_directories_file_pointer, directory_name);
}

BOOL blacklists_is_blacklisted_file(
	 IN  char  *file_name)
{
	return _is_blacklisted_element(_blacklisted_files_file_pointer, file_name);
}

int  blacklists_list_blacklisted_directories(
	 OUT char **list)
{
	return _list_blacklisted_element(_blacklisted_directories_file_pointer, list);
}

int  blacklists_list_blacklisted_files(
	 OUT char **list)
{
	return _list_blacklisted_element(_blacklisted_files_file_pointer, list);
}

int  blacklists_blacklist_directory(
	 IN  char  *directory_name)
{
	if(_blacklisted_directories_file_pointer != NULL)
	{
		fclose(_blacklisted_directories_file_pointer);
	}
	_blacklisted_directories_file_pointer = fopen(FILE_NAME_BLACKLISTED_DIRECTORIES, "a+"); /* Flawfinder: ignore */
	if(_blacklisted_directories_file_pointer == NULL)
	{
		log_print_error( GetString( MSG_BLACKLISTS_BLACKLISTSBLACKLISTDIRECTORYCOULDNOTOPENLISTOFBLACKLISTEDDI ) );
		return RETURN_ERROR;
	}
	else
	{
		return _blacklist_element(_blacklisted_directories_file_pointer, directory_name);
	}
}

int  blacklists_blacklist_file(
	 IN  char  *file_name)
{
	if(_blacklisted_files_file_pointer != NULL)
	{
		fclose(_blacklisted_files_file_pointer);
	}
	_blacklisted_files_file_pointer = fopen(FILE_NAME_BLACKLISTED_FILES, "a+"); /* Flawfinder: ignore */
	if(_blacklisted_files_file_pointer == NULL)
	{
		log_print_error( GetString( MSG_BLACKLISTS_BLACKLISTSBLACKLISTFILECOULDNOTOPENLISTOFBLACKLISTEDFILES ) );
		return RETURN_ERROR;
	}
	else
	{
		return _blacklist_element(_blacklisted_files_file_pointer, file_name);
	}
}

static BOOL _is_blacklisted_element(
			IN  FILE  *file_pointer,
			IN  char  *name)
{
	FILE   *file_pointer_temp = NULL;
	char   *line              = NULL;
	size_t  length            = 0;
	int     read              = 0;
	
	// Tygre 2015/07/18: Special directories!
	// I also blacklist any special directories like "." and "..".
	// I seek "/." and "/.." because I don't want to match file extensions.
	if(strstr(name, "/.")  != NULL ||
	   strstr(name, "/..") != NULL)
	{
		return TRUE;
	}

	if(file_pointer == NULL)
	{
		log_print_warning( GetString( MSG_BLACKLISTS_ISBLACKLISTEDELEMENTNOBLACKLISTAVAILABLE ) );
		return FALSE;
	}
	
	// Tygre 2015/08/25: Const...
	// _file_duplicate() to convert from "const FILE *" to "FILE *".
	file_pointer_temp = _file_duplicate(file_pointer);
	if(file_pointer_temp == NULL)
	{
		log_print_error( GetString( MSG_BLACKLISTS_ISBLACKLISTEDELEMENTCOULDNOTALLOCATEFILEPOINTER ) );
		return FALSE;
	}
	
	while((read = _get_line(&line, &length, file_pointer_temp)) != -1)
	{
		if(string_equal(name, line))
		{
			free(line);
			rewind(file_pointer_temp);
			// No need to free because it is just a pointer pointing on the same data.
			//	free(file_pointer_temp);
			return TRUE;
		}
	}
	
	free(line);
	rewind(file_pointer_temp);
	// No need to free because it is just a pointer pointing on the same data.
	//	free(file_pointer_temp);
	return FALSE;
}

static int  _list_blacklisted_element(
			IN  FILE  *file_pointer,
			OUT char **list)
{
	FILE   *file_pointer_temp = NULL;
	int     file_size         = 0;
	int     length            = 0;
	char   *line              = NULL;
	size_t  line_length       = 0;
	int     line_read         = 0;
	
	if(file_pointer == NULL)
	{
		log_print_error( GetString( MSG_BLACKLISTS_LISTBLACKLISTEDELEMENTNOBLACKLISTAVAILABLE ) );
		goto _RETURN_ERROR;
	}

	// Tygre 2015/08/25: Const...
	// _file_duplicate() to convert from "const FILE *" to "FILE *".
	file_pointer_temp = _file_duplicate(file_pointer);
	if(file_pointer_temp == NULL)
	{
		log_print_error( GetString( MSG_BLACKLISTS_LISTBLACKLISTEDELEMENTCOULDNOTALLOCATEFILEPOINTER ) );
		return FALSE;
	}

	fseek(file_pointer_temp, 0L, SEEK_END);
	file_size = ftell(file_pointer_temp);
	fseek(file_pointer_temp, 0L, SEEK_SET);

	length = file_size + 1;
	*list  = calloc(sizeof(char), length * sizeof(char));
	if(*list == NULL)
	{
		log_print_error( GetString( MSG_BLACKLISTS_LISTBLACKLISTEDELEMENTCOULDNOTALLOCATELIST ) );
		goto _RETURN_ERROR;
	}

	while((line_read = _get_line(&line, &line_length, file_pointer_temp)) != -1)
	{
		if(strlen(line) > 0)
		{
			strncat(*list, line, length - strlen(*list) - 1);
			strncat(*list, "\n", length - strlen(*list) - 1);
		}
	}
	rewind(file_pointer_temp);

	goto _RETURN_OK;
	_RETURN_OK:
		free(line);
		// No need to free because it is just a pointer pointing on the same data.
		//	free(file_pointer_temp);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(line != NULL)
		{
			free(line);
		}
		if(file_pointer_temp != NULL)
		{
			// No need to free because it is just a pointer pointing on the same data.
			//	free(file_pointer_temp);
		}
		return RETURN_ERROR;
}

static int  _blacklist_element(
			IN  FILE  *file_pointer,
			IN  char  *name)
{
	FILE *file_pointer_temp = NULL;

	if(file_pointer == NULL)
	{
		log_print_error( GetString( MSG_BLACKLISTS_BLACKLISTELEMENTNOBLACKLISTAVAILABLE ) );
		return RETURN_ERROR;
	}

	// Tygre 2015/08/25: Const...
	// _file_duplicate() to convert from "const FILE *" to "FILE *".
	file_pointer_temp = _file_duplicate(file_pointer);
	if(file_pointer_temp == NULL)
	{
		log_print_error( GetString( MSG_BLACKLISTS_BLACKLISTELEMENTCOULDNOTALLOCATEFILEPOINTER ) );
		return RETURN_ERROR;
	}

	fputs(name, file_pointer_temp);
	putc('\n', file_pointer_temp);

	// No need to free because it is just a pointer pointing on the same data.
	//	free(file_pointer_temp);
	return RETURN_OK;
}

/* Copyright (C) 1991 Free Software Foundation, Inc.
This file is part of the GNU C Library.

The GNU C Library is free software; you can redistribute it and/or
modify it under the terms of the GNU Library General Public License as
published by the Free Software Foundation; either version 2 of the
License, or (at your option) any later version.

The GNU C Library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Library General Public License for more details.

You should have received a copy of the GNU Library General Public
License along with the GNU C Library; see the file COPYING.LIB.  If
not, write to the Free Software Foundation, Inc., 675 Mass Ave,
Cambridge, MA 02139, USA.  */

/* Read up to (and including) a newline from STREAM into *LINEPTR
   (and null-terminate it). *LINEPTR is a pointer returned from malloc (or
   NULL), pointing to *N characters of space.  It is realloc'd as
   necessary.  Returns the number of characters read (not including the
   null terminator), or -1 on error or EOF.  */

static int _get_line(OUT char **lineptr, OUT size_t *n, IN FILE *stream)
{
FILE *stream_temp;
static char line[256]; /* Flawfinder: ignore */
char *ptr;
unsigned int len;

   if (lineptr == NULL || n == NULL)
   {
	  errno = EINVAL;
	  return -1;
   }

   /*@access FILE@*/
   if (ferror((FILE *)stream))
	  return -1;

   /*@access FILE@*/
   if (feof((FILE *)stream))
	  return -1;

   // Tygre 2015/08/25: Const...
   // _file_duplicate() to convert from "const FILE *" to "FILE *".
   stream_temp = _file_duplicate(stream);
   if(stream_temp == NULL)
   {
	  return -1;
   }
   fgets(line,256,stream_temp);

   ptr = strchr(line, '\n');
   if (ptr)
	  *ptr = '\0';

   len = strlen(line);

   if ((len+1) < 256)
   {
	  ptr = realloc(*lineptr, 256);
	  if (ptr == NULL)
		 return(-1);
	  *lineptr = ptr;
	  *n = 256;
   }

   strncpy(*lineptr, line, len + 1);
   return(len);
}

static FILE *_file_duplicate(IN FILE *file_pointer)
{
	FILE *file_pointer_temp;
	memcpy(&file_pointer_temp, &file_pointer, sizeof(FILE *));
	return file_pointer_temp;
}
