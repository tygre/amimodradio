/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include "controls_mui.h"

#include "controls.h"
#include "controls_workers.h"
#include "globals.h"
#include "gui_mui.h"
#include "locale.h"
#include "log.h"
#include "prefs.h"
#include "utils.h"
#include "utils_tasks.h"
#include "z_fortify.h"

#include <dos/dos.h>          // For RETURN_OK, RETURN_ERROR
#include <libraries/mui.h>
#include <limits.h>           // For UINT_MAX
#include <proto/alib.h>       // For CreatePort(), DeletePort()
#include <proto/asl.h>
#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/muimaster.h>
#include <stdio.h>            // For FILE, fprintf()...
#include <time.h>             // For time_t, struct tm, localtime()...




/* Constants and declarations */

#define MUIV_Application_ReturnID_RestartUI -2

	   int  controls_mui(IN BPTR);
	   void controls_mui_about(IN struct Hook *, IN Object *, IN APTR *);
	   void controls_mui_play(IN struct Hook *, IN Object *, IN APTR *);
	   void controls_mui_replay(IN struct Hook *, IN Object *, IN APTR *);
	   void controls_mui_pause(IN struct Hook *, IN Object *, IN APTR *);
	   void controls_mui_stop(IN struct Hook *, IN Object *, IN APTR *);
	   void controls_mui_next(IN struct Hook *, IN Object *, IN APTR *);
	   void controls_mui_root_directory_prev(IN struct Hook *, IN Object *, IN APTR *);
	   void controls_mui_root_directory_next(IN struct Hook *, IN Object *, IN APTR *);
	   void controls_mui_root_directory_reset(IN struct Hook *, IN Object *, IN APTR *);
	   void controls_mui_change_directory(IN struct Hook *, IN Object *, IN APTR *);
	   void controls_mui_directory_list(IN struct Hook *, IN Object *, IN APTR *);
	   void controls_mui_directory_ban(IN struct Hook *, IN Object *, IN APTR *);
	   void controls_mui_directory_set_as_root(IN struct Hook *, IN Object *, IN APTR *);
	   void controls_mui_file_list(IN struct Hook *, IN Object *, IN APTR *);
	   void controls_mui_file_ban(IN struct Hook *, IN Object *, IN APTR *);
	   void controls_mui_modules_save(IN struct Hook *, IN Object *, IN APTR *);
	   void controls_mui_modules_email(IN struct Hook *, IN Object *, IN APTR *);
	   void controls_mui_set_user_rating(IN struct Hook *, IN Object *, IN APTR *);
	   void controls_mui_keyboard_space(IN struct Hook *, IN Object *, IN APTR *);
	   void mui_prefs_play_on_startup(IN struct Hook *, IN Object *, IN APTR *);
	   void mui_prefs_show_log_page_on_fetch(IN struct Hook *, IN Object *, IN APTR *);
	   void mui_prefs_maximum_log_size_changed(IN struct Hook *, IN Object *, IN APTR *);
	   void mui_prefs_remember_source_between_runs(IN struct Hook *, IN Object *, IN APTR *);
	   void mui_prefs_saved_modules_directory_changed(IN struct Hook *, IN Object *, IN APTR *);
	   void mui_prefs_cache_list_during_run(IN struct Hook *, IN Object *, IN APTR *);
	   void mui_prefs_cache_list_between_runs(IN struct Hook *, IN Object *, IN APTR *);
	   void mui_prefs_cache_directory_changed(IN struct Hook *, IN Object *, IN APTR *);
	   void mui_prefs_save_prefs_on_exit(IN struct Hook *, IN Object *, IN APTR *);
	   void mui_prefs_remember_images_set(IN struct Hook *, IN Object *, IN APTR *);
	   void mui_prefs_skip_if_banned(IN struct Hook *, IN Object *, IN APTR *);
	   void mui_prefs_skip_if_user_rating_low(IN struct Hook *, IN Object *, IN APTR *);
	   void mui_prefs_skip_if_all_rating_exists(IN struct Hook *, IN Object *, IN APTR *);
	   void mui_prefs_skip_if_all_rating_low(IN struct Hook *, IN Object *, IN APTR *);
	   void mui_prefs_skip_if_above_maximum_file_size(IN struct Hook *, IN Object *, IN APTR *);
	   void mui_prefs_maximum_file_size_changed(IN struct Hook *, IN Object *, IN APTR *);
	   void mui_prefs_save(IN struct Hook *, IN Object *, IN APTR *);
	   void mui_prefs_reset(IN struct Hook *, IN Object *, IN APTR *);
	   void mui_prefs_default(IN struct Hook *, IN Object *, IN APTR *);
	   void mui_log_save(IN struct Hook *, IN Object *, IN APTR *);
	   void mui_log_clear(IN struct Hook *, IN Object *, IN APTR *);
	   void controls_mui_open_directories_ban_list(IN struct Hook *, IN Object *, IN APTR *);
	   void controls_mui_open_files_ban_list(IN struct Hook *, IN Object *, IN APTR *);
static void _commands_update_current_root_directory(void);
static void _commands_update_current_directory(void);
static void _commands_update_current_file(void);
static void _commands_update_current_modules(void);
static void _commands_update_current_ratings(void);
static void _commands_update_last_message(void);
static void _commands_update_lists(void);
static void _commands_update_all(void);
static void _prefs_update_all(void);
static void _print_debug(IN char *);
static void _print_information(IN char *);
static void _print_warning(IN char *);
static void _print_error(IN char *);
static void _push_to_log(IN char *);
static void _enable_gadgets_upon_playing_modules(IN BOOL);
static void _enable_gadgets_for_rating_modules(IN BOOL);
static void _mui_directory_list_clicked(IN char *);
static void _mui_directory_list_closed(IN APTR);
static void _mui_directories_ban_list_closed(IN APTR);
static void _mui_file_list_clicked(IN char *);
static void _mui_file_list_closed(IN APTR);
static void _mui_files_ban_list_closed(IN APTR);
static void _set_progress_max(IN char *, IN int);
static void _set_progress_val(IN char *, IN int);

// Defined as NULL elsewhere
extern struct Library       *AslBase;
extern struct GfxBase       *GfxBase;
extern struct Library       *IconBase;
extern struct IntuitionBase *IntuitionBase;



/* Definitions */

	   struct Library *MUIMasterBase = NULL;
static struct MUI_UI  *_object       = NULL;
static int             _user_rating  = 0;
static int             _progress_max = 0;
static int             _progress_val = 0;

int controls_mui(IN BPTR path_list)
{
	BOOL  is_closing     = FALSE;
	int   return_id      = 0;
	ULONG window_signals = 0;

	log_print_debug("controls_mui()\n");

	/*
	 * Initialising base utils.
	 */

	if(log_setup(_print_debug, _print_information, _print_warning, _print_error) == RETURN_ERROR)
	{
		log_print_error("controls_mui(), could not setup log\n");
		goto _RETURN_ERROR;
	}

	if(prefs_setup() == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_CONTROLSINITCOULDNOTINITIALISEPREFERENCES ) );
		goto _RETURN_ERROR;
	}

	/*
	 * Opening the required library and the GUI.
	 */

	if((GfxBase = (struct GfxBase *)OpenLibrary("graphics.library", 0)) == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSMUI_CONTROLSMUICOULDNOTOPENGRAPHICSLIBRARYV0 ) );
		goto _RETURN_ERROR;
	}

	if((MUIMasterBase = OpenLibrary("muimaster.library", 19)) == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSMUI_CONTROLSMUICOULDNOTOPENMUIMASTERLIBRARYV19 ) );
		goto _RETURN_ERROR;
	}

	if(gui_mui_init_ui(&_object) == RETURN_ERROR)
	{
		goto _RETURN_ERROR;
	}

	/*
	 * Initialising the controls.
	 */

	if(controls_setup(
		path_list,
		_set_progress_max,
		_set_progress_val) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_CONTROLSMUI_CONTROLSMUICOULDNOTINITIALISECONTROLS ) );
		goto _RETURN_ERROR;
	}

	// Make sure that everything
	// is displayed after starting.
	_enable_gadgets_upon_playing_modules(FALSE);
	_enable_gadgets_for_rating_modules(FALSE);
	_commands_update_current_root_directory();
	_commands_update_current_directory();

	/*
	 * The main loop.
	 */

	_MAIN_LOOP:

	if(prefs_should_play_on_startup() == TRUE &&
	   controls_are_modules_playing() == FALSE)
	{
		controls_mui_next(NULL, NULL, NULL);
	}

	while(!is_closing)
	{
		return_id = DoMethod(_object->App, MUIM_Application_NewInput, &window_signals);
			
		if(window_signals == 0)
		{
			is_closing = TRUE;
			break;
		}

		window_signals = Wait(SIGBREAKF_CTRL_C |
							  controls_signal_control_playing_stopped() |
							  controls_workers_signal_control_display_last_message() |
							  controls_workers_signal_control_fetching_modules() |
							  controls_workers_signal_control_modules_rated() |
							  controls_workers_signal_control_modules_downloaded() |
							  window_signals);

		if((window_signals & SIGBREAKF_CTRL_C) == SIGBREAKF_CTRL_C)
		{
			// I make sure that AMR is quitting...
			return_id  = MUIV_Application_ReturnID_Quit;
			is_closing = TRUE;
		}
				
		if((window_signals & controls_signal_control_playing_stopped()) == controls_signal_control_playing_stopped())
		{
			controls_mui_next(NULL, NULL, NULL);
		}
				
		if((window_signals & controls_workers_signal_control_display_last_message()) == controls_workers_signal_control_display_last_message())
		{
			_commands_update_last_message();
		}
				
		if((window_signals & controls_workers_signal_control_fetching_modules()) == controls_workers_signal_control_fetching_modules())
		{
			_enable_gadgets_upon_playing_modules(FALSE);
			_enable_gadgets_for_rating_modules(FALSE);

			if(prefs_should_show_log_page_on_fetch() == TRUE)
			{
				SetAttrs(
					_object->GR_grp_root,
					MUIA_Group_ActivePage,
					2,
					TAG_DONE);
			}

			_commands_update_all();

			utils_tasks_send_back_ack();
		}

		if((window_signals & controls_workers_signal_control_modules_rated()) == controls_workers_signal_control_modules_rated())
		{
			log_print_information( GetString( MSG_CONTROLSMUI_RATINGSAVED ) );
			// Tygre 24/07/23: Order matters
			// I must first enable the gadgets
			// and then update their backgrounds
			_enable_gadgets_for_rating_modules(TRUE);
			_commands_update_current_ratings();

			// Stopping current module if appropriate
			if(prefs_should_skip_if_user_rating_low() == TRUE &&
			   _user_rating < LOWEST_PLAYABLE_RATE)
			{
				controls_ban_current_file();
				controls_mui_stop(NULL, NULL, NULL);
				controls_mui_next(NULL, NULL, NULL);
			}
		}

		if((window_signals & controls_workers_signal_control_modules_downloaded()) == controls_workers_signal_control_modules_downloaded())
		{
			_enable_gadgets_upon_playing_modules(TRUE);
			_enable_gadgets_for_rating_modules(TRUE);

			if(prefs_should_show_log_page_on_fetch() == TRUE)
			{
				SetAttrs(
					_object->GR_grp_root,
					MUIA_Group_ActivePage,
					0,
					TAG_DONE);
			}

			_commands_update_all();

			utils_tasks_send_back_ack();
		}
	}

	// Tygre 2022/03/27: The Others
	// When changing the global MUI preferences
	// DoMethod() will return some other value.
	// I must close cleanly the UI and reopen it
	// for the changes to take effect.
	if(return_id != MUIV_Application_ReturnID_Quit)
	{
		gui_mui_close_ui(&_object);
		_object = NULL;
		if(gui_mui_init_ui(&_object) == RETURN_ERROR)
		{
			goto _RETURN_ERROR;
		}

		if(controls_are_modules_loaded() == TRUE)
		{
			_enable_gadgets_upon_playing_modules(TRUE);
			_enable_gadgets_for_rating_modules(TRUE);
			
			_commands_update_all();
		}
		else
		{
			_enable_gadgets_upon_playing_modules(FALSE);
			_enable_gadgets_for_rating_modules(FALSE);
			
			_commands_update_current_root_directory();
			_commands_update_current_directory();
		}
		
		is_closing = FALSE;
		goto _MAIN_LOOP;
	}

	goto _RETURN_OK;
	_RETURN_OK:
		gui_mui_close_ui(&_object);
		CloseLibrary(MUIMasterBase);
		MUIMasterBase = NULL;
		CloseLibrary((struct Library *)GfxBase);
		GfxBase	= NULL;
		controls_cleanup();
		prefs_cleanup();
		log_cleanup();
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		gui_mui_close_ui(&_object);
		if(MUIMasterBase != NULL)
		{
			CloseLibrary(MUIMasterBase);
			MUIMasterBase = NULL;
		}
		if(GfxBase != NULL)
		{
			CloseLibrary((struct Library *)GfxBase);
			GfxBase	= NULL;
		}
		controls_cleanup();
		prefs_cleanup();
		log_cleanup();
		return RETURN_ERROR;
}

void controls_mui_doublestart(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	SetAttrs(
		_object->App,
		MUIA_Application_Iconified,
		FALSE,
		TAG_DONE);
	DoMethod(
		_object->WI_label_title,
		MUIM_Window_ToFront);
}

void controls_mui_about(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	controls_display_version();
	_commands_update_last_message();
}

void controls_mui_play(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	if(controls_are_modules_loaded() == FALSE)
	{
		controls_mui_next(h, o, d);
	}
	else if(controls_are_modules_playing() == FALSE)
	{
		controls_play_modules();
		_commands_update_all();
	}
}

void controls_mui_replay(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	if(controls_are_modules_loaded() == TRUE)
	{
		controls_replay_modules();
		_commands_update_all();
	}
}

void controls_mui_pause(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	if(controls_are_modules_loaded()  == TRUE &&
	   controls_are_modules_playing() == TRUE)
	{
		controls_pause_modules();
		_commands_update_all();
	}
}

void controls_mui_stop(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	// The fetcher should step aside and wait
	controls_workers_modules_fetcher_wait();

	if(controls_are_modules_loaded() == TRUE)
	{
		controls_remove_modules();
		_commands_update_all();

		// Also, some gadgets should be disabled
		_enable_gadgets_upon_playing_modules(FALSE);
		_enable_gadgets_for_rating_modules(FALSE);
	}
}

void controls_mui_next(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	controls_workers_modules_fetcher_fetch();
}

void controls_mui_root_directory_prev(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	// The fetcher should step aside and wait
	controls_workers_modules_fetcher_wait();

	controls_set_current_source_index(controls_get_prev_source_index());
	controls_set_current_root_directory(NULL);
	controls_set_current_directory(NULL);

	_commands_update_all();
}

void controls_mui_root_directory_next(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	// The fetcher should step aside and wait
	controls_workers_modules_fetcher_wait();

	controls_set_current_source_index(controls_get_next_source_index());
	controls_set_current_root_directory(NULL);
	controls_set_current_directory(NULL);

	_commands_update_all();
}

void controls_mui_root_directory_reset(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	// The fetcher should step aside and wait
	controls_workers_modules_fetcher_wait();

	controls_set_current_root_directory(NULL);
	controls_set_current_directory(NULL);

	_commands_update_all();
}

void controls_mui_change_directory(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	char *new_directory = NULL;

	// The fetcher should step aside and wait
	controls_workers_modules_fetcher_wait();

	GetAttr(
		MUIA_String_Contents,
		_object->STR_label_directory,
		(ULONG *)&new_directory);
	SetAttrs(
		_object->STR_label_directory,
		MUIA_String_Contents, new_directory,
		TAG_DONE);
	controls_set_current_root_directory(new_directory);

	controls_mui_next(h, o, d);
	_commands_update_all();
}

void controls_mui_directory_set_as_root(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	// The fetcher should step aside and wait
	controls_workers_modules_fetcher_wait();

	controls_set_current_root_directory(controls_get_current_directory());

	_commands_update_all();
}

void controls_mui_directory_list(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	ULONG is_opened = 0;
	
	if(_object->WI_directory_list == NULL)
	{
		gui_mui_child_window_list_open(
			_object,
			controls_get_current_directory_list(),
			"LV_directory_list",
			 GetString( MSG_CONTROLSMUI_DIRECTORYCONTENT ) ,
			&_object->LV_directory_list,
			&_object->WI_directory_list,
			_mui_directory_list_clicked,
			_mui_directory_list_closed);
	}
	else
	{
		GetAttr(
			MUIA_Window_Open,
			_object->WI_directory_list,
			&is_opened);

		SetAttrs(
			_object->WI_directory_list,
			MUIA_Window_Open,
			is_opened == 0 ? 1 : 0,
			TAG_DONE);
	}
}

void controls_mui_directory_ban(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	char *list = NULL;

	// The fetcher should step aside and wait
	controls_workers_modules_fetcher_wait();

	controls_ban_current_directory();

	controls_get_blacklisted_directories_list(&list);
	gui_mui_child_window_list_update(
		_object->LV_directories_ban_list,
		list);
	if(list != NULL)
	{
		free(list);
		list = NULL;
	}

	_commands_update_all();

	if(prefs_should_skip_if_banned())
	{
		controls_workers_modules_fetcher_fetch();
	}
}

void controls_mui_open_directories_ban_list(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	char  *list      = NULL;
	ULONG  is_opened = 0;

	if(_object->WI_directories_ban_list == NULL)
	{
		controls_get_blacklisted_directories_list(&list);

		gui_mui_child_window_list_open(
			_object,
			list,
			"LV_directories_ban_list",
			 GetString( MSG_CONTROLSMUI_BANNEDDIRECTORIES ) ,
			&_object->LV_directories_ban_list,
			&_object->WI_directories_ban_list,
			NULL,
			_mui_directories_ban_list_closed);

		if(list != NULL)
		{
			free(list);
			list = NULL;
		}
	}
	else
	{
		GetAttr(
			MUIA_Window_Open,
			_object->WI_directories_ban_list,
			&is_opened);

		SetAttrs(
			_object->WI_directories_ban_list,
			MUIA_Window_Open,
			is_opened == 0 ? 1 : 0,
			TAG_DONE);
	}
}

void controls_mui_file_list(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	ULONG is_opened = 0;

	if(_object->WI_file_list == NULL)
	{
		gui_mui_child_window_list_open(
			_object,
			controls_get_current_file_list(),
			"LV_file_list",
			 GetString( MSG_CONTROLSMUI_FILECONTENT ) ,
			&_object->LV_file_list,
			&_object->WI_file_list,
			_mui_file_list_clicked,
			_mui_file_list_closed);
	}
	else
	{
		GetAttr(
			MUIA_Window_Open,
			_object->WI_file_list,
			&is_opened);

		SetAttrs(
			_object->WI_file_list,
			MUIA_Window_Open,
			is_opened == 0 ? 1 : 0,
			TAG_DONE);
	}
}

void controls_mui_file_ban(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	char *list = NULL;

	// The fetcher should step aside and wait
	controls_workers_modules_fetcher_wait();

	controls_ban_current_file();

	controls_get_blacklisted_files_list(&list);
	gui_mui_child_window_list_update(
		_object->LV_files_ban_list,
		list);
	if(list != NULL)
	{
		free(list);
		list = NULL;
	}

	_commands_update_all();

	if(prefs_should_skip_if_banned())
	{
		controls_workers_modules_fetcher_fetch();
	}
}

void controls_mui_open_files_ban_list(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	char  *list      = NULL;
	ULONG  is_opened = 0;

	if(_object->LV_files_ban_list == NULL)
	{
		controls_get_blacklisted_files_list(&list);

		gui_mui_child_window_list_open(
			_object,
			list,
			"LV_files_ban_list",
			 GetString( MSG_CONTROLSMUI_BANNEDFILES ) ,
			&_object->LV_files_ban_list,
			&_object->WI_files_ban_list,
			NULL,
			_mui_files_ban_list_closed);
	
		if(list != NULL)
		{
			free(list);
			list = NULL;
		}
	}
	else
	{
		GetAttr(
			MUIA_Window_Open,
			_object->WI_files_ban_list,
			&is_opened);

		SetAttrs(
			_object->WI_files_ban_list,
			MUIA_Window_Open,
			is_opened == 0 ? 1 : 0,
			TAG_DONE);
	}
}

void controls_mui_modules_save(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	int                   length               = 0;
	char                 *default_directory    = NULL;
	struct FileRequester *request              = NULL;
	char                 *current_modules_temp = NULL;
	char                 *token                = NULL;

	if((AslBase = OpenLibrary("asl.library", 37)) == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSMUI_MUICONTROLSMODULESSAVECOULDNOTOPENASLLIBRARY ) );
		goto _RETURN_ERROR;
	}

	if((request = (struct FileRequester *)AllocAslRequestTags(
		ASL_FileRequest,
		ASLFR_DrawersOnly,     TRUE,
		ASLFR_InitialLeftEdge, 20,
		ASLFR_InitialTopEdge,  20,
		ASLFR_InitialWidth,    300,
		ASLFR_InitialHeight,   350,
		ASLFR_InitialDrawer,   prefs_get_saved_modules_directory(),
		ASLFR_PositiveText,    GetString( MSG_CONTROLSMUI_SAVE ) ,
		ASL_Hail,              GetString( MSG_CONTROLSMUI_SAVEDIRECTORY ) ,
		(struct TagItem *)TAG_DONE)) == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSMUI_MUICONTROLSMODULESSAVECOULDNOTALLOCATEDIRECTORYREQUESTER ) );
		goto _RETURN_ERROR;
	}

	if(AslRequest(request, NULL))
	{
		length               = strlen(request->rf_Dir) + 1;
		// Include a possible extra separator added later
		default_directory    = malloc((length + 1 + 1) * sizeof(char));
		if(default_directory == NULL)
		{
			log_print_error( GetString( MSG_CONTROLSMUI_MUICONTROLSMODULESSAVECOULDNOTALLOCATEDIRECTORYNAME ) );
			goto _RETURN_ERROR;
		}
		strcpy(default_directory, request->rf_Dir);
		if(!(string_end_with(default_directory, GLOBALS_SEPARATOR_PATHS) ||
			 string_end_with(default_directory, GLOBALS_SEPARATOR_VOLUMES)))
		{
			// Adding extra separator if needed
			strncat(default_directory, GLOBALS_SEPARATOR_PATHS, 1);
		}
		prefs_set_saved_modules_directory(default_directory);

		current_modules_temp = string_duplicate(controls_get_current_file_list());
		token = strtok(current_modules_temp, "\n");
		while(token != NULL)
		{
			if(io_file_copy(token, prefs_get_saved_modules_directory()) == RETURN_ERROR)
			{
				log_print_error( GetString( MSG_CONTROLSMUI_SAVECURRENTMODULESCOULDNOTCOPYFILE ) );
				goto _RETURN_ERROR;
			}
			token = strtok(NULL, "\n");
		}
	}

	goto _RETURN_OK;
	_RETURN_OK:
		// Not freeing token because it is freed with current_modules_temp
		free(default_directory);
		default_directory = NULL;
		free(current_modules_temp);
		current_modules_temp = NULL;
		FreeAslRequest(request);
		CloseLibrary(AslBase);
		log_print_information( GetString( MSG_CONTROLSMUI_MODULESSAVED ) );
		return;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		// Not freeing token because it is freed with current_modules_temp
		if(default_directory != NULL)
		{
			free(default_directory);
			default_directory = NULL;
		}
		if(current_modules_temp != NULL)
		{
			free(current_modules_temp);
			current_modules_temp = NULL;
		}
		if(request != NULL)
		{
			FreeAslRequest(request);
		}
		if(AslBase != NULL)
		{
			CloseLibrary(AslBase);
		}
		log_print_information( GetString( MSG_CONTROLSMUI_MODULESNOTSAVED ) );
		return;
}

void controls_mui_modules_email(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	log_print_information("Sorry, this feature is not yet implemented in this UI\n");
	// TODO
}

void controls_mui_set_user_rating(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	_enable_gadgets_for_rating_modules(FALSE);

	_user_rating = ((int)*d) + 1;
	controls_set_current_user_rating(_user_rating);
}

void controls_mui_keyboard_space(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d)
{
	if(controls_are_modules_loaded()  == TRUE &&
	   controls_are_modules_playing() == FALSE)
	{
		controls_mui_play(h, o, d);
	}
	else if(controls_are_modules_loaded()  == TRUE &&
			controls_are_modules_playing() == TRUE)
	{
		controls_mui_pause(h, o, d);
	}
	else
	{
		controls_mui_next(h, o, d);
	}
}

void mui_prefs_play_on_startup(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	BOOL is_selected = FALSE;
	
	is_selected = !prefs_should_play_on_startup();
	prefs_set_play_on_startup(is_selected);
}

void mui_prefs_show_log_page_on_fetch(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	BOOL is_selected = FALSE;

	is_selected = !prefs_should_show_log_page_on_fetch();
	prefs_set_show_log_page_on_fetch(is_selected);
}

void mui_prefs_maximum_log_size_changed(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d)
{
	int value  = 0;
	int length = 0;

	GetAttr(
		MUIA_Cycle_Active,
		_object->CY_label_maximum_log_size,
		(ULONG *)&value);
	prefs_set_maximum_log_size_index(value);

	// Do not forget to allocate the
	// log content with the new size
	length = atoi(prefs_get_list_of_maximum_log_sizes()[prefs_get_maximum_log_size_index()]) * BYTES_IN_ONE_KILO_BYTE; /* Flawfinder: ignore */
	_object->LV_label_log_content = realloc(
		_object->LV_label_log_content,
		length * sizeof(char));
	memset(_object->LV_label_log_content, 0, length * sizeof(char));
	SetAttrs(
		_object->LV_label_log,
		MUIA_Floattext_Text, _object->LV_label_log_content,
		TAG_DONE);
}

void mui_prefs_remember_source_between_runs(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	BOOL is_selected = FALSE;

	is_selected = !prefs_should_remember_source_between_runs();
	prefs_set_remember_source_between_runs(is_selected);
}

void mui_prefs_saved_modules_directory_changed(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	char *new_directory = NULL;

	GetAttr(
		MUIA_String_Contents,
		_object->STR_label_saved_modules_directory,
		(ULONG *)&new_directory);
	prefs_set_saved_modules_directory(new_directory);
}

void mui_prefs_cache_list_during_run(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	BOOL is_selected = FALSE;

	is_selected = !prefs_should_cache_list_during_run();
	prefs_set_cache_list_during_run(is_selected);
}

void mui_prefs_cache_list_between_runs(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	BOOL is_selected = FALSE;

	is_selected = !prefs_should_cache_list_between_runs();
	prefs_set_cache_list_between_runs(is_selected);
}

void mui_prefs_cache_directory_changed(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	char *new_directory = NULL;

	GetAttr(
		MUIA_String_Contents,
		_object->STR_label_cache_directory,
		(ULONG *)&new_directory);
	prefs_set_cache_directory(new_directory);
}

void mui_prefs_save_prefs_on_exit(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	BOOL is_selected = FALSE;

	is_selected = !prefs_should_save_prefs_on_exit();
	prefs_set_save_prefs_on_exit(is_selected);
}

void mui_prefs_remember_images_set(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	int value = 0;

	GetAttr(
		MUIA_Cycle_Active,
		_object->GR_grp_remember_images_set,
		(ULONG *)&value);
	prefs_set_current_images_set_index(value);
	prefs_set_images_directory(prefs_get_list_of_images_sets()[prefs_get_current_images_set_index()]);
}

void mui_prefs_skip_if_banned(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d)
{
	BOOL is_selected = FALSE;

	is_selected = !prefs_should_skip_if_banned();
	prefs_set_skip_if_banned(is_selected);
}

void mui_prefs_skip_if_user_rating_low(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d)
{
	BOOL is_selected = FALSE;

	is_selected = !prefs_should_skip_if_user_rating_low();
	prefs_set_skip_if_user_rating_low(is_selected);
}

void mui_prefs_skip_if_all_rating_exists(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d)
{
	BOOL is_selected = FALSE;

	is_selected = !prefs_should_skip_if_all_rating_exists();
	prefs_set_skip_if_all_rating_exists(is_selected);
}

void mui_prefs_skip_if_all_rating_low(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d)
{
	BOOL is_selected = FALSE;

	is_selected = !prefs_should_skip_if_all_rating_low();
	prefs_set_skip_if_all_rating_low(is_selected);
}

void mui_prefs_skip_if_above_maximum_file_size(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d)
{
	BOOL is_selected = FALSE;

	is_selected = !prefs_should_skip_if_above_maximum_file_size();
	prefs_set_skip_if_above_maximum_file_size(is_selected);
}

void mui_prefs_maximum_file_size_changed(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d)
{
	int value = 0;

	GetAttr(
		MUIA_Cycle_Active,
		_object->CY_label_maximum_file_size,
		(ULONG *)&value);
	prefs_set_maximum_file_size_index(value);
}

void mui_prefs_save(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d)
{
	prefs_save_preferences();

	// Tell the MUI to close and reopen.
	DoMethod(
		_object->App,
		MUIM_Application_ReturnID,
		MUIV_Application_ReturnID_RestartUI);
}

void mui_prefs_reset(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	prefs_reset_preferences();

	_prefs_update_all();
}

void mui_prefs_default(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	prefs_default_preferences();
	
	_prefs_update_all();
}

void mui_log_save(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	int                   length    = 0;
	char                 *file_name = NULL;
	struct FileRequester *request   = NULL;
	FILE                 *file      = NULL;
	time_t                raw_time  = 0;
	struct tm            *time_info = NULL;

	if((AslBase = OpenLibrary("asl.library", 37)) == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSMUI_MUILOGSAVECOULDNOTOPENASLLIBRARY ) );
		goto _RETURN_ERROR;
	}

	if((request = (struct FileRequester *)AllocAslRequestTags(
		ASL_FileRequest,
		ASLFR_DrawersOnly, TRUE,
		ASLFR_InitialLeftEdge, 20,
		ASLFR_InitialTopEdge, 20,
		ASLFR_InitialWidth, 300,
		ASLFR_InitialHeight, 350,
		ASLFR_InitialDrawer, "RAM:",
		ASLFR_PositiveText,  GetString( MSG_CONTROLSMUI_SAVE ) ,
		ASL_Hail,  GetString( MSG_CONTROLSMUI_SAVELOG ) ,
		(struct TagItem *)TAG_DONE)) == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSMUI_MUILOGSAVECOULDNOTALLOCATEDIRECTORYREQUESTER ) );
		goto _RETURN_ERROR;
	}

	if(AslRequest(request, NULL))
	{
		length       = strlen(request->rf_Dir) + 1 + strlen(GLOBALS_LOG_FILE_NAME);
		file_name    = malloc((length + 1) * sizeof(char));
		if(file_name == NULL)
		{
			log_print_error("mui_log_save(), could not allocate file name\n");
			goto _RETURN_ERROR;
		}
		strcpy(file_name, request->rf_Dir);
		if(!string_end_with(file_name, GLOBALS_SEPARATOR_PATHS) &&
		   !string_end_with(file_name, GLOBALS_SEPARATOR_VOLUMES))
		{
			strcat(file_name, GLOBALS_SEPARATOR_PATHS);
		}
		strcat(file_name, GLOBALS_LOG_FILE_NAME);

		file = fopen(file_name, "a");     /* Flawfinder: ignore */
		if(file == NULL)
		{
			file = fopen(file_name, "w"); /* Flawfinder: ignore */
			if(file == NULL)
			{
				log_print_error( GetString( MSG_CONTROLSMUI_MUILOGSAVECOULDNOTOPENLOGFILE ) );
				goto _RETURN_ERROR;
			}
		}
		
		time(&raw_time);
		time_info = localtime(&raw_time);
		fprintf(file, "------------------------\n%s------------------------\n%s\n", asctime(time_info), _object->LV_label_log_content);
	}

	goto _RETURN_OK;
	_RETURN_OK:
		free(file_name);
		fclose(file);
		FreeAslRequest(request);
		CloseLibrary(AslBase);
		_commands_update_all();
		return;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(file_name != NULL)
		{
			free(file_name);
		}
		if(file != NULL)
		{
			fclose(file);
		}
		if(request != NULL)
		{
			FreeAslRequest(request);
		}
		if(AslBase != NULL)
		{
			CloseLibrary(AslBase);
		}
		return;
}

void mui_log_clear(
	IN struct Hook *h,
	IN Object      *o,
	IN APTR        *d)
{
	strncpy(_object->LV_label_log_content, "", 1 + 1);
	SetAttrs(
		_object->LV_label_log,
		MUIA_Floattext_Text, _object->LV_label_log_content,
		TAG_DONE);

	_commands_update_all();
}

static void _commands_update_current_root_directory(void)
{
	int   length = 0;
	char *string = NULL;

	length    = strlen(controls_get_current_source()) + strlen(controls_get_current_root_directory()) + 1;
	string    = malloc(length * sizeof(char));
	if(string == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSMUI_ERRORINDISPLAYCURRENTROOTDIRECTORYCOULDNOTALLOC ) );
		return;
	}
	strcpy(string, controls_get_current_source());
	strcat(string, controls_get_current_root_directory());

	SetAttrs(
		_object->TX_label_root_directory,
		MUIA_Text_Contents, string,
		TAG_DONE);

	free(string);
	string = NULL;
}

static void _commands_update_current_directory(void)
{
	SetAttrs(
		_object->STR_label_directory,
		MUIA_String_Contents, controls_get_current_directory(),
		TAG_DONE);
}

static void _commands_update_current_file(void)
{
	SetAttrs(
		_object->STR_label_file,
		MUIA_Text_Contents, controls_get_current_file(),
		TAG_DONE);
}

static void _commands_update_current_modules(void)
{
	char *current_modules_display = string_replace_all(controls_get_current_file_list(), "\n", ", ");

	if(string_end_with(current_modules_display, ", "))
	{
		current_modules_display[strlen(current_modules_display) - 2] = '\0';
	}

	SetAttrs(
		_object->TX_label_modules,
		MUIA_Text_Contents, current_modules_display,
		TAG_DONE);

	free(current_modules_display);
	current_modules_display = NULL;
}

static void _commands_update_current_ratings(void)
{
	int  i        = 0;
	char tally[4] = "NNN";

	// Setting user's rating
	for(i = 0; i < controls_get_current_user_rating(); i++)
	{
		SetAttrs(
			_object->IM_label_user_rating[i],
			MUIA_Background, MUII_FILLSHINE,
			TAG_DONE);
	}

	// Clearing remaining user's rating buttons
	for(i = controls_get_current_user_rating(); i < 5; i++)
	{
		SetAttrs(
			_object->IM_label_user_rating[i],
			MUIA_Background, MUII_BACKGROUND,
			TAG_DONE);
	}

	// Setting community's rating
	for(i = 0; i < controls_get_current_all_rating(); i++)
	{
		SetAttrs(
			_object->TX_label_all_rating[i],
			MUIA_Background, MUII_FILLSHINE,
			TAG_DONE);
	}

	// Clearing remaining community's rating buttons
	for(i = controls_get_current_all_rating(); i < 5; i++)
	{
		SetAttrs(
			_object->TX_label_all_rating[i],
			MUIA_Background, MUII_BACKGROUND,
			TAG_DONE);
	}

	// Setting community's tally
	string_snprintf(tally, 4, "%3d", controls_get_current_all_tally());
	SetAttrs(
		_object->TX_label_all_tally,
		MUIA_Text_Contents, tally,
		TAG_DONE);
}

static void _commands_update_last_message(void)
{
	char *temp = NULL;

	// Simple optimisation to avoid some flickering.
	GetAttr(
		MUIA_Text_Contents,
		_object->TX_label_last_message,
		(ULONG *)&temp);
	if(temp == NULL || strlen(temp) == 0 ||                      // If there's no previous message
	   string_equal(temp, controls_get_last_message()) == FALSE) // Or the new message is different
	{
		SetAttrs(
			_object->TX_label_last_message,
			MUIA_Text_Contents, controls_get_last_message(),
			TAG_DONE);

		SetAttrs(
			_object->LV_label_log,
			MUIA_Floattext_Text, _object->LV_label_log_content,
			TAG_DONE);
	}

	SetAttrs(
		_object->GA_progression_bar,
		MUIA_Gauge_Max,     100,
		MUIA_Gauge_Divide,  _progress_max,
		MUIA_Gauge_Current, 100 * _progress_val,
		TAG_DONE);
}

static void _commands_update_lists(void)
{
	gui_mui_child_window_list_update(
		_object->LV_directory_list,
		controls_get_current_directory_list());

	gui_mui_child_window_list_update(
		_object->LV_file_list,
		controls_get_current_file_list());
}

static void _commands_update_all(void)
{
	_commands_update_current_root_directory();
	_commands_update_current_directory();
	_commands_update_current_file();
	_commands_update_current_modules();
	_commands_update_current_ratings();
	_commands_update_last_message();

	_commands_update_lists();
}

static void _prefs_update_all(void)
{
	// Misc.
	SetAttrs(
		_object->GR_grp_save_prefs_on_exit,
		MUIA_Selected, prefs_should_save_prefs_on_exit(),
		TAG_DONE);

	SetAttrs(
		_object->GR_grp_play_on_startup,
		MUIA_Selected, prefs_should_play_on_startup(),
		TAG_DONE);

	SetAttrs(
		_object->GR_grp_show_log_page_on_fetch,
		MUIA_Selected, prefs_should_show_log_page_on_fetch(),
		TAG_DONE);

	SetAttrs(
		_object->GR_grp_remember_source_between_runs,
		MUIA_Selected, prefs_should_remember_source_between_runs(),
		TAG_DONE);

	// Modules
	SetAttrs(
		_object->STR_label_saved_modules_directory,
		MUIA_String_Contents, prefs_get_saved_modules_directory(),
		TAG_DONE);

	// Cache
	SetAttrs(
		_object->GR_grp_cache_list_during_run,
		MUIA_Selected, prefs_should_cache_list_during_run(),
		TAG_DONE);

	SetAttrs(
		_object->GR_grp_cache_list_between_runs,
		MUIA_Selected, prefs_should_cache_list_between_runs(),
		TAG_DONE);

	SetAttrs(
		_object->STR_label_cache_directory,
		MUIA_String_Contents, prefs_get_cache_directory(),
		TAG_DONE);

	// Images
	SetAttrs(
		_object->GR_grp_remember_images_set,
		MUIA_Cycle_Active, prefs_get_current_images_set_index(),
		TAG_DONE);

	// Ratings
	SetAttrs(
		_object->GR_grp_skip_if_banned,
		MUIA_Selected, prefs_should_skip_if_banned(),
		TAG_DONE);

	SetAttrs(
		_object->GR_grp_skip_if_user_rating_low,
		MUIA_Selected, prefs_should_skip_if_user_rating_low(),
		TAG_DONE);

	SetAttrs(
		_object->GR_grp_skip_if_all_rating_exists,
		MUIA_Selected, prefs_should_skip_if_all_rating_exists(),
		TAG_DONE);

	SetAttrs(
		_object->GR_grp_skip_if_all_rating_low,
		MUIA_Selected, prefs_should_skip_if_all_rating_low(),
		TAG_DONE);

	// Sizes
	SetAttrs(
		_object->GR_grp_skip_if_above_maximum_file_size,
		MUIA_Selected, prefs_should_skip_if_above_maximum_file_size(),
		TAG_DONE);

	SetAttrs(
		_object->CY_label_maximum_file_size,
		MUIA_Cycle_Active, prefs_get_maximum_file_size_index(),
		TAG_DONE);
}

static void _print_debug(
	IN char *message)
{
	if(_object == NULL)
	{
		printf(message); /* Flawfinder: ignore */
		return;
	}

	_push_to_log(message);
}

static void _print_information(
	IN char *message)
{
	if(_object == NULL)
	{
		printf(message); /* Flawfinder: ignore */
		printf("\n");
		return;
	}

	// This is the (only) type of message that
	// I also want to show as "last message".
	controls_build_last_message(message);

	_push_to_log(message);
}

static void _print_warning(
	IN char *message)
{
	if(_object == NULL)
	{
		printf(message); /* Flawfinder: ignore */
		printf("\n");
		return;
	}

	_push_to_log(message);
}

static void _print_error(
	IN char *message)
{
	if(_object == NULL)
	{
		fprintf(stderr, message); /* Flawfinder: ignore */
		fprintf(stderr, "\n");
		return;
	}

	_push_to_log(message);
}

static void _push_to_log(
	IN char *message)
{
	int maximum_log_size = atoi(prefs_get_list_of_maximum_log_sizes()[prefs_get_maximum_log_size_index()]) * BYTES_IN_ONE_KILO_BYTE; /* Flawfinder: ignore */
	int length           = 0;

	if(_object == NULL)
	{
		return;
	}

	length = strlen(message);
	memmove(_object->LV_label_log_content + length, _object->LV_label_log_content, maximum_log_size - length);
	memcpy (_object->LV_label_log_content, message, length);
	_object->LV_label_log_content[maximum_log_size - 1] = '\0';

	// I want to show that last messages in the controle page, if displayed
	// I do the showing into the main loop to avoid MUI complaining about
	// MUI_Redraw() being called in a different task than the main task.
	utils_tasks_send_signal_without_ack(
		utils_tasks_get_main_task(),
		controls_workers_signal_control_display_last_message());

	goto _RETURN_OK;
	_RETURN_OK:
		return;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		return;
}

static void _enable_gadgets_upon_playing_modules(
	IN BOOL enabled)
{
	// Directories
	/*
	// Even if nothing is playing, these should be enabled.
	SetAttrs(
		_object->STR_label_directory,
		MUIA_Disabled,
		!enabled,
		TAG_DONE);
	SetAttrs(
		_object->IM_label_directory_set_as_root,
		MUIA_Disabled,
		!enabled,
		TAG_DONE);
	if(_is_directory_list_opened)
	{
		SetAttrs(
			_object->IM_label_directory_list,
			MUIA_Disabled,
			TRUE,
			TAG_DONE);
	}
	else
	{
		SetAttrs(
			_object->IM_label_directory_list,
			MUIA_Disabled,
			FALSE,
			TAG_DONE);
	}
	*/
	SetAttrs(
		_object->IM_label_directory_ban,
		MUIA_Disabled,
		!enabled,
		TAG_DONE);
	
	// Files
	/*
	// Even if nothing is playing, these should be enabled.
	if(_is_file_list_opened)
	{
		SetAttrs(
			_object->IM_label_file_list,
			MUIA_Disabled,
			TRUE,
			TAG_DONE);
	}
	else
	{
		SetAttrs(
			_object->IM_label_file_list,
			MUIA_Disabled,
			!enabled,
			TAG_DONE);
	}
	*/
	SetAttrs(
		_object->IM_label_file_ban,
		MUIA_Disabled,
		!enabled,
		TAG_DONE);

	// Modules
	SetAttrs(
		_object->IM_label_modules_save,
		MUIA_Disabled,
		!enabled,
		TAG_DONE);
	SetAttrs(
		_object->IM_label_modules_email,
		MUIA_Disabled,
		!enabled,
		TAG_DONE);
}

static void _enable_gadgets_for_rating_modules(
	IN BOOL enabled)
{
	int i = 0;

	for(i = 0; i < 5; i++)
	{
		// Reset
		SetAttrs(
			_object->IM_label_user_rating[i],
			MUIA_Background, MUII_BACKGROUND,
			MUIA_Disabled,   !enabled,
			TAG_DONE);
		SetAttrs(
			_object->TX_label_all_rating[i],
			MUIA_Background, MUII_BACKGROUND,
			TAG_DONE);
	}

	SetAttrs(
		_object->TX_label_all_tally,
		MUIA_Text_Contents, "  0",
		TAG_DONE);
}

static void _mui_directory_list_clicked(
	IN char *entry)
{
	char *selected  = NULL;
	char *file      = NULL;
	int   file_size = 0;
	char *directory = NULL;
	char *pos1      = NULL;
	char *pos2      = NULL;
	int   length    = 0;

	if(entry[0] == 'f')
	{
		selected    = string_duplicate(entry);
		if(selected == NULL)
		{
			return;
		}
		file  = strchr(selected, ' ');
		file++;
		pos1  = strrchr(file, GLOBALS_SEPARATOR_SIZES_CHAR);
		*pos1 = '\0';
		pos1++;
		file_size = atoi(pos1); /* Flawfinder: ignore */

		log_print_information("Will play selected modules\n");
		if(controls_set_current_root_directory(controls_get_current_directory()) == RETURN_ERROR)
		{
			log_print_error("_mui_directory_list_clicked(), could not set root directory");
			return;
		}

		_enable_gadgets_upon_playing_modules(FALSE);
		_enable_gadgets_for_rating_modules(FALSE);
		controls_workers_modules_downloader_start(
			controls_get_current_directory(),
			file,
			file_size);
		
		free(selected);
		selected = NULL;
	}
	else if(entry[0] == 'd')
	{
		selected    = string_duplicate(entry);
		if(selected == NULL)
		{
			return;
		}
		directory = strchr(selected, ' ');
		directory++;
		pos1    = strrchr(directory, GLOBALS_SEPARATOR_SIZES_CHAR);
		if(pos1 == NULL)
		{
			free(selected);
			selected = NULL;
			return;
		}
		*pos1 = '\0';

		if(string_equal(directory, "..") == TRUE)
		{
			directory = string_duplicate(controls_get_current_directory());
			if((pos1 = strchr(directory, GLOBALS_SEPARATOR_PATHS_CHAR)) < (pos2 = strrchr(directory, GLOBALS_SEPARATOR_PATHS_CHAR)))
			{
				*pos2 = '\0';
			}
			else if(pos1 == pos2)
			{
				*(pos1 + 1) = '\0';
			}
			else if((pos1 = strrchr(directory, GLOBALS_SEPARATOR_VOLUMES_CHAR)) != NULL)
			{
				if(string_end_with(directory, GLOBALS_SEPARATOR_VOLUMES) == FALSE)
				{
					// Remove anything after ':' in
					// "/ABCD:XYZ" to obtain "/ABC:"
					*(pos1 + 1 * sizeof(char)) = '\0';
				}
				else
				{
					// Remove anything after '/' in
					// "/ABCD:" to obtain "/"
					*(directory + 1 * sizeof(char)) = '\0';
				}
			}
			else
			{
				// Nothing to do
			}
		}
		else
		{
			length  = strlen(controls_get_current_directory()) + 1 + strlen(directory) + 1;
			pos1    = malloc(length * sizeof(char));
			if(pos1 == NULL)
			{
				return;
			}
			strcpy(pos1, controls_get_current_directory());
			if(!string_end_with(pos1, GLOBALS_SEPARATOR_PATHS) &&
			   !string_end_with(pos1, GLOBALS_SEPARATOR_VOLUMES))
			{
				strcat(pos1, GLOBALS_SEPARATOR_PATHS);
			}
			strcat(pos1, directory);
			directory = pos1;
		}

		controls_set_current_directory(directory);
		_commands_update_current_directory();
		_commands_update_lists();

		free(directory);
		directory = NULL;

		free(selected);
		selected = NULL;
	}
}

static void _mui_directory_list_closed(
	IN APTR window)
{
	SetAttrs(
		_object->WI_directory_list,
		MUIA_Window_Open,
		FALSE,
		TAG_DONE);
}

static void _mui_directories_ban_list_closed(
	IN APTR window)
{
	SetAttrs(
		_object->WI_directories_ban_list,
		MUIA_Window_Open,
		FALSE,
		TAG_DONE);
}

static void _mui_file_list_clicked(
	IN char *entry)
{
	// Nothing to do
}

static void _mui_file_list_closed(
	IN APTR window)
{
	SetAttrs(
		_object->WI_file_list,
		MUIA_Window_Open,
		FALSE,
		TAG_DONE);
}

static void _mui_files_ban_list_closed(
	IN APTR window)
{
	SetAttrs(
		_object->WI_files_ban_list,
		MUIA_Window_Open,
		FALSE,
		TAG_DONE);
}

static void _set_progress_max(
	IN char *origin,
	IN int   value)
{
	_progress_val = 0;
	if(value > 0)
	{
		_progress_max = value;
	}
	else
	{
		_progress_max = 400;
	}

	// I want to show that last message in the controle page, if displayed
	// I do the showing into the main loop to avoid MUI complaining about
	// MUI_Redraw() being called in a different task than the main task.
	utils_tasks_send_signal_without_ack(
		utils_tasks_get_main_task(),
		controls_workers_signal_control_display_last_message());
}

static void _set_progress_val(
	IN char *origin,
	IN int   value)
{
	_progress_val += value;

	// I want to show that last message in the controle page, if displayed
	// I do the showing into the main loop to avoid MUI complaining about
	// MUI_Redraw() being called in a different task than the main task.
	utils_tasks_send_signal_without_ack(
		utils_tasks_get_main_task(),
		controls_workers_signal_control_display_last_message());

	if(_progress_val > _progress_max)
	{
		_progress_val = 0;
	}
}

