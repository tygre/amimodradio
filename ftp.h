/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



#ifndef FTP_H
#define FTP_H 1

#include "utils.h"

int  ftp_setup(
	 IN  BOOL  (*continue_long_operation_fp)(),
	 IN  void  (*ftp_size_callback_fp)(IN char *origin, IN int number_of_bytes),
	 IN  void  (*ftp_read_callback_fp)(IN char *origin, IN int number_of_bytes));

void ftp_cleanup(
	 void);

int  ftp_init_connection(
	 IN  char  *host_name,
	 OUT int   *socket_handle_for_commands);

void ftp_close_connection(
	 IN  int    socket_handle_for_commands);

int  ftp_get_list(
	 IN  char  *host_name,
	 IN  int    socket_handle_for_commands,
	 IN  char  *directory_name,
	 OUT char **list);

int  ftp_get_list_from_data(
	 IN  char  *host_name,
	 IN  int    socket_handle_for_commands,
	 IN  char  *directory_name,
	 OUT char **list);

int  ftp_check_file_exists(
	 IN  char  *host_name,
	 IN  int    socket_handle_for_commands,
	 IN  char  *file_name,
	 OUT BOOL  *file_exists);

int  ftp_get_file_from_data(
	 IN  char  *host_name,
	 IN  int    socket_handle_for_commands,
	 IN  char  *file_name,
	 OUT char **file_bytes,
	 OUT int   *file_length);

#endif
