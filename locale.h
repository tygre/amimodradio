
#ifndef LOCALE_H
#define LOCALE_H 1

/*
** locale.h
**
** (c) 2006 by Guido Mersmann
**
** Object source created by SimpleCat
*/

/*************************************************************************/

#define  CATCOMP_NUMBERS
#include "locale_strings.h" /* change name to correct locale header if needed */

/*
** Prototypes
*/

BOOL   Locale_Open(STRPTR catname, ULONG version, ULONG revision);
void   Locale_Close(void);
STRPTR GetString(long ID);

/*************************************************************************/

#endif /* LOCALE_H */
