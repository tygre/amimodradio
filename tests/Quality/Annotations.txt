Flawfind
--------

	To tell FlawFind to ignore some line of code and not search/report smells in them: /* Flawfind: ignore */

splint
------

	Annotations are in the form /*@XXX@*/ where XXX can be any of the keyword in https://splint.org/manual/manual.html#annotations, for example /*@notnull@*/
