/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include "controls_cli.h"

#include "controls.h"
#include "globals.h"
#include "log.h"
#include "locale.h"
#include "prefs.h"
#include "utils.h"
#include "version.h"
#include "z_fortify.h"

#include <ctype.h>               // For isdigit(...)
#include <devices/conunit.h>     // For CONU_STANDARD
#include <dos/dos.h>             // For RETURN_OK, RETURN_ERROR
#include <intuition/intuition.h> // For struct Window
#include <proto/alib.h>          // For CreatePort(), DeletePort()
#include <proto/dos.h>           // For GetCurrentDirName(...)
#include <proto/exec.h>          // For OpenDevice(), Wait(), SendIO()...
#include <proto/intuition.h>     // For CloseWindow()
#include <proto/keymap.h>        // For MapRawKey()
#include <stdio.h>               // For FILE



/* Constants and declarations */

	   int  controls_cli(IN BPTR);
static int  _grab_console(void);
static void _release_console(void);
static void _enable_RAW_mode(BPTR, BOOL);
static char _get_char(void);
static int  _convert_CSI_to_InputEvent(IN char *, OUT struct InputEvent **);
static char _convert_InputEvent_to_ANSI(IN struct InputEvent *);
static int  _list_blacklisted_directories(void);
static int  _list_blacklisted_files(void);
static void _print_debug_console(IN char *);
static void _print_information_console(IN char *);
static void _print_error_console(IN char *);
static int  _save_current_modules(void);
static int  _set_current_root_directory_as_current(void);
static int  _set_current_root_directory_manually(void);
static int  _choose_source(void);
static void _set_progress_max(IN char *, IN int);
static void _set_progress_val(IN char *, IN int);

// Defined as NULL in controls.c
extern struct IntuitionBase *IntuitionBase;



/* Definitions */

// https://stackoverflow.com/questions/3978351/how-to-avoid-backslash-escape-when-writing-regular-expression-in-c-c
#define STR(a) #a
#define R(var, re) static char var##_[] = STR(re); const char *var = (var##_[sizeof(var##_) - 2] = '\0', (var##_ + 1));

struct Library           *KeymapBase                           = NULL;
static BPTR               _con_file                            = BPTR_ZERO;
static struct FileHandle *_con_file_handle                     = NULL;
static struct MsgPort    *_con_message_port                    = NULL;
static struct MsgPort    *_con_reply_port                      = NULL;
static char               _con_answer[GLOBALS_MAX_LINE_LENGTH] = { 0 };
static BOOL               _is_console_opened                   = FALSE;
static BOOL               _is_info_ending_with_newline         = FALSE;
static int                _progress_max                        = 0;
static int                _progress_val                        = 0;

int	controls_cli(IN BPTR path_list)
{
	char *current_modules_display = NULL;
	char  user_answer             = '\0';

	R(l1, "                     _ __  __           _ _____           _ _       ");
	R(l2, "     /\             (_)  \/  |         | |  __ \         | (_)      ");
	R(l3, "    /  \   _ __ ___  _| \  / | ___   __| | |__) |__ _  __| |_  ___  ");
	R(l4, "   / /\ \ | '_ ` _ \| | |\/| |/ _ \ / _` |  _  // _` |/ _` | |/ _ \ ");
	R(l5, "  / ____ \| | | | | | | |  | | (_) | (_| | | \ \ (_| | (_| | | (_) |");
	R(l6, " /_/    \_\_| |_| |_|_|_|  |_|\___/ \__,_|_|  \_\__,_|\__,_|_|\___/ ");
	printf("\33c\33[32mStarting %s...\n\n\n\n\n\n\n\n\n%s\n%s\n%s\n%s\n%s\n%s\33[39m\n", version_get_version_description(), l1, l2, l3, l4, l5, l6);

	/*
	 * Initialising base utils.
	 */

printf("1.\n");
	if(log_setup(
		_print_debug_console,
		_print_information_console,
		_print_error_console,
		_print_error_console) == RETURN_ERROR)
	{
		log_print_error("controls_cli(), could not setup log\n");
		goto _RETURN_ERROR;
	}

printf("3.\n");
	if(prefs_setup() == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_CONTROLSINITCOULDNOTINITIALISEPREFERENCES ) );
		goto _RETURN_ERROR;
	}

	/*
	 * Initialising the Controls.
	 */

printf("4.\n");
	if(controls_setup(
		path_list,
		_set_progress_max,
		_set_progress_val) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_CONTROLSCLI_CONTROLSCLICOULDNOTINITIALISECONTROLS ) );
		goto _RETURN_ERROR;
	}

	/*
	 * Opening the required libraries.
	 */

printf("5.\n");
	if((KeymapBase = OpenLibrary("keymap.library", 36)) == NULL)
	{
		log_print_error("controls_cli(), could not open KeyMap.library v36\n");
		goto _RETURN_ERROR;
	}

	/*
	 * Grabbing the Console.
	 */

printf("6.\n");
	if(_grab_console() == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_CONTROLSCLI_CONTROLSCLICOULDNOTGRABCONSOLE ) );
		goto _RETURN_ERROR;
	}

	/*
	 * Main loop.
	 */

	while(TRUE)
	{
		printf("\33c");
		
		current_modules_display = string_replace_all(controls_get_current_file_list(), "\n", ", ");
		if(strlen(current_modules_display) > 2)
		{
			current_modules_display[strlen(current_modules_display) - 2] = '\0';
		}
		printf("\33[32m");
		printf( GetString( MSG_CONTROLSCLI_SOURCE ) ,           controls_get_current_source());			/* Flawfinder: ignore */
		printf( GetString( MSG_CONTROLSCLI_ROOTDIRECTORY ) ,    controls_get_current_root_directory()); /* Flawfinder: ignore */
		printf( GetString( MSG_CONTROLSCLI_CURRENTDIRECTORY ) , controls_get_current_directory());		/* Flawfinder: ignore */
		printf( GetString( MSG_CONTROLSCLI_CURRENTFILE ) ,      controls_get_current_file());			/* Flawfinder: ignore */
		printf( GetString( MSG_CONTROLSCLI_CURRENTMODULES ) ,   current_modules_display);				/* Flawfinder: ignore */
		printf( GetString( MSG_CONTROLSCLI_LASTMESSAGE ) ,      controls_get_last_message());			/* Flawfinder: ignore */
		printf( GetString( MSG_CONTROLSCLI_STATUS ) );													/* Flawfinder: ignore */
		free(current_modules_display);
		if(controls_are_modules_loaded() == FALSE)
		{
			printf( GetString( MSG_CONTROLSCLI_NOMODULELOADED ) ); /* Flawfinder: ignore */
		}
		else
		{
			if(controls_are_modules_playing() == FALSE)
			{
				printf( GetString( MSG_CONTROLSCLI_NOMODULEPLAYING ) );    /* Flawfinder: ignore */
			}
			else
			{
				printf( GetString( MSG_CONTROLSCLI_MODULEPLAYINGENJOY ) ); /* Flawfinder: ignore */
			}
		}
		
		printf("\33[39m\n\n");

		printf( GetString( MSG_CONTROLSCLI_0QUITAMIMODRADIO ) );					/* Flawfinder: ignore */
		printf( GetString( MSG_CONTROLSCLI_1ABOUTAMIMODRADIO ) );					/* Flawfinder: ignore */
		printf( GetString( MSG_CONTROLSCLI_2PLAYTHEMODULES ) );						/* Flawfinder: ignore */
		printf( GetString( MSG_CONTROLSCLI_3REPLAYTHEMODULES ) );					/* Flawfinder: ignore */
		printf( GetString( MSG_CONTROLSCLI_4PAUSETHEMODULES ) );					/* Flawfinder: ignore */
		printf( GetString( MSG_CONTROLSCLI_5REMOVETHEMODULES ) );					/* Flawfinder: ignore */
		printf( GetString( MSG_CONTROLSCLI_6FETCHOTHERMODULES ) );					/* Flawfinder: ignore */
		printf( GetString( MSG_CONTROLSCLI_7LISTBANNEDDIRECTORIES ) );				/* Flawfinder: ignore */
		printf( GetString( MSG_CONTROLSCLI_8BANTHISDIRECTORY ) );					/* Flawfinder: ignore */
		printf( GetString( MSG_CONTROLSCLI_9LISTBANNEDFILES ) );					/* Flawfinder: ignore */
		printf( GetString( MSG_CONTROLSCLI_ABANTHISFILE ) );						/* Flawfinder: ignore */
		printf( GetString( MSG_CONTROLSCLI_BSAVETHESEMODULES ) );					/* Flawfinder: ignore */
		printf( GetString( MSG_CONTROLSCLI_CSETROOTDIRECTORYASCURRENTDIRECTORY ) );	/* Flawfinder: ignore */
		printf( GetString( MSG_CONTROLSCLI_DSETRESETROOTDIRECTORYMANUALLY ) );		/* Flawfinder: ignore */
		printf( GetString( MSG_CONTROLSCLI_ECHOOSESOURCE ) );						/* Flawfinder: ignore */
		printf("\n");
		
		/*
		 * Waiting for interesting signals:
		 * - A character from the Console;
		 * - A break from the user;
		 * - An interruption from the listener :-)
		 */

		printf("\33[1m");
		printf("AmiModRadio> ");
		printf("\33[22m");
		fflush(stdout);
		user_answer = _get_char();
		printf("\n");

		/*
		 * Handling the signals and characters.
		 */

printf("8.\n");
		// Simple use of AmiModRadio
		if(user_answer == ' ')
		{
			if(controls_are_modules_loaded()  == TRUE &&
			   controls_are_modules_playing() == FALSE)
			{
				// Play
				user_answer = '2';
			}
			else if(controls_are_modules_loaded()  ==TRUE &&
					controls_are_modules_playing() == TRUE)
			{
				// Pause
				user_answer = '4';
			}
			else
			{
				// Fetch
				user_answer = '6';
			}
		}
		
		if(user_answer == '0')
		{
			break;
		}
		else if(user_answer == '1')
		{
			controls_display_version();
		}
		else if(user_answer == '2')
		{
			if(controls_are_modules_loaded() == FALSE)
			{
				controls_remove_modules();
				controls_fetch_modules();
			}
			else if(controls_are_modules_playing() == FALSE)
			{
				controls_play_modules();
			}
		}
		else if(user_answer == '3')
		{
			if(controls_are_modules_loaded() == TRUE)
			{
				controls_replay_modules();
			}
		}
		else if(user_answer == '4')
		{
			if(controls_are_modules_loaded()  == TRUE &&
			   controls_are_modules_playing() == TRUE)
			{
				controls_pause_modules();
			}
		}
		else if(user_answer == '5')
		{
			if(controls_are_modules_loaded() == TRUE)
			{
				controls_remove_modules();
			}
		}
		else if(user_answer == '6')
		{
			controls_remove_modules();
			controls_fetch_modules();
		}
		else if(user_answer == '7')
		{
			_list_blacklisted_directories();
		}
		else if(user_answer == '8')
		{
			controls_ban_current_directory();
			if(prefs_should_skip_if_banned() == TRUE)
			{
				controls_fetch_modules();
			}
		}
		else if(user_answer == '9')
		{
			_list_blacklisted_files();
		}
		else if(user_answer == 'a')
		{
			controls_ban_current_file();
			if(prefs_should_skip_if_banned() == TRUE)
			{
				controls_fetch_modules();
			}
		}
		else if(user_answer == 'b')
		{
			_save_current_modules();
		}
		else if(user_answer == 'c')
		{
			_set_current_root_directory_as_current();
		}
		else if(user_answer == 'd')
		{
			_set_current_root_directory_manually();
		}
		else if(user_answer == 'e')
		{
			_choose_source();
		}
	}

	/*
	 * Closing everything.
	 */

	goto _RETURN_OK;
	_RETURN_OK:
		_release_console();
		CloseLibrary(KeymapBase);
		controls_cleanup();
		prefs_cleanup();
		log_cleanup();
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		_release_console();
		if(KeymapBase != NULL)
		{
			CloseLibrary(KeymapBase);
		}
		controls_cleanup();
		prefs_cleanup();
		log_cleanup();
		return RETURN_ERROR;
}

static int _grab_console(void)
{
	if((_con_file = Open("CONSOLE:", MODE_OLDFILE)) == BPTR_ZERO)
	{
		log_print_error("_grab_console(), could not open Console\n");
		goto _RETURN_ERROR;
	}

	if((_con_file_handle = BADDR(_con_file))->fh_Type == NULL)
	{
		log_print_error("_grab_console(), could not get Console file handle\n");
		goto _RETURN_ERROR;
	}
	_enable_RAW_mode(_con_file, TRUE);

	_con_message_port = (struct MsgPort *)_con_file_handle->fh_Type;

	if((_con_reply_port = CreatePort(NULL, 0)) == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSCLI_CONTROLSCLICOULDNOTCREATEMESSAGEPORT ) );
		goto _RETURN_ERROR;
	}

	_is_console_opened = TRUE;

	goto _RETURN_OK;
	_RETURN_OK:
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		return RETURN_ERROR;
}

static void _release_console(void)
{
printf("A.\n");
	if(_con_reply_port != NULL)
	{
		DeletePort(_con_reply_port);
	}
	if(_con_message_port != NULL)
	{
		// Nothing to do, depends on _con_file
	}
	if(_con_file_handle != NULL)
	{
		// Nothing to do, depends on _con_file
	}
	if(_con_file != BPTR_ZERO)
	{
printf("B.\n");
		_enable_RAW_mode(_con_file, FALSE);
printf("C.\n");
		// Close(_con_file);
	}

	_is_console_opened = FALSE;
}

static void _enable_RAW_mode(
	BPTR con_file,
	BOOL enable)
{
	if(enable == TRUE)
	{
		// Enable RAW mode
		SetMode(con_file, 1);

		// Listen to keyboard and close gadget
		printf("\33[1;2;11{");
		VFPrintf(con_file, "\33[1;2;11{", NULL);
	}
	else
	{
		// Stop listening to keyboard and close gadget
		// Logically, it should be:
		//	printf("\33[1;11}");
		// but then the close gadget wouldn't be
		// functional until a carriage return...
		printf("\33[1;2}");

		// Disable RAW mode
		SetMode(con_file, 0);
	}
}

static char _get_char(void)
{
	struct StandardPacket  con_standard_packet = { 0 };
	char				   received_char       = '\0';
	ULONG                  console_signal      = 0;
	ULONG                  received_signals    = 0;
	struct Message        *message             = NULL;
	struct InputEvent     *input_event         = NULL;

	while(TRUE)
	{
printf("9.\n");
		received_char = '\0';
		
		memset(&con_standard_packet, 0, sizeof(struct StandardPacket));
		con_standard_packet.sp_Msg.mn_Node.ln_Name = (char *)&con_standard_packet.sp_Pkt;
		con_standard_packet.sp_Pkt.dp_Link         = &con_standard_packet.sp_Msg;
		con_standard_packet.sp_Pkt.dp_Port         = _con_reply_port;
		con_standard_packet.sp_Pkt.dp_Type         = ACTION_READ;
		con_standard_packet.sp_Pkt.dp_Arg1         = _con_file_handle->fh_Arg1;
		con_standard_packet.sp_Pkt.dp_Arg2         = (LONG)&_con_answer;
		con_standard_packet.sp_Pkt.dp_Arg3         = (LONG)GLOBALS_MAX_LINE_LENGTH;
		PutMsg(_con_message_port, &con_standard_packet.sp_Msg);

		console_signal   = 1L << _con_reply_port->mp_SigBit;
		received_signals = Wait(console_signal   |
								SIGBREAKF_CTRL_C |
								controls_signal_control_playing_stopped());

printf("10.\n");
		if((received_signals & console_signal) == console_signal)
		{
			while((message = GetMsg(_con_reply_port)) != NULL)
			{
				ReplyMsg(message);
			}
printf("11.\n");
			if(_convert_CSI_to_InputEvent((char *)_con_answer, &input_event) == RETURN_OK)
			{
				if(input_event->ie_Class == IECLASS_RAWKEY)
				{
					received_char = _convert_InputEvent_to_ANSI(input_event);
				}
				else if(input_event->ie_Class == IECLASS_CLOSEWINDOW)
				{
					received_char = '0';
				}
				free(input_event);
			}
		}
		else if((received_signals & SIGBREAKF_CTRL_C) == SIGBREAKF_CTRL_C)
		{
			received_char = '0';
		}
		else if((received_signals & controls_signal_control_playing_stopped()) == controls_signal_control_playing_stopped())
		{
			received_char = '6';
		}

		// We stop listening only when we received an interesting char
		if(isprint(received_char) >  0    ||
		   received_char          == '\n' ||
		   received_char          == '\r')
		{
			break;
		}
	}

	printf("%c", received_char);
	fflush(stdout);

	return received_char;
}

static int _convert_CSI_to_InputEvent(
	IN  char               *csi,
	OUT struct InputEvent **input_event)
{
	char *string = NULL;
	char *token  = NULL;

printf("csi = %s\n", csi);
	// Signed 2's complement of 0x9B:
	// -101 = -((UCHAR_MAX - 0x9B) + 1)
	if(csi[0] == -101)
	{
printf("HERE %s\n", &csi[1]);
		string = string_duplicate(&csi[1]);
	}
	else
	{
printf("THERE %s\n", csi);
		string = string_duplicate(csi);
	}

	// ie_NextEvent
	*input_event    = malloc(sizeof(struct InputEvent));
	if(*input_event == NULL)
	{
		goto _RETURN_ERROR;
	}
	(*input_event)->ie_NextEvent           = NULL;

printf("string = %s\n", string);
	// ie_Class
	token = strtok(string, ";");
	if(string_equal(token, "1") == TRUE)
	{
		(*input_event)->ie_Class           = IECLASS_RAWKEY;
	}
	else if(string_equal(token, "11") == TRUE)
	{
		(*input_event)->ie_Class           = IECLASS_CLOSEWINDOW;
		goto _RETURN_OK;
	}
	else
	{
		// Let's pretend it's nothing
		(*input_event)->ie_Class           = IECLASS_NULL;
		goto _RETURN_OK;
	}

	// ie_SubClass
	if(string[0] != ';')
	{
		token = strtok(NULL, ";");
	}
	if(token == NULL)
	{
		goto _RETURN_ERROR;
	}
	(*input_event)->ie_SubClass            = IESUBCLASS_COMPATIBLE;

	// ie_Code
	token = strtok(NULL, ";");
	if(token == NULL)
	{
		goto _RETURN_ERROR;
	}
	(*input_event)->ie_Code                = atoi(token); /* Flawfinder: ignore */

	// ie_Qualifier
	token = strtok(NULL, ";");
	if(token == NULL)
	{
		goto _RETURN_ERROR;
	}
	(*input_event)->ie_Qualifier           = atoi(token); /* Flawfinder: ignore */

	// ie_position
	token = strtok(NULL, ";");
	if(token == NULL)
	{
		goto _RETURN_ERROR;
	}
	(*input_event)->ie_position.ie_xy.ie_x = atoi(token); /* Flawfinder: ignore */
	token = strtok(NULL, ";");
	if(token == NULL)
	{
		goto _RETURN_ERROR;
	}
	(*input_event)->ie_position.ie_xy.ie_y = atoi(token); /* Flawfinder: ignore */

	// ie_TimeStamp
	token = strtok(NULL, ";");
	if(token == NULL)
	{
		goto _RETURN_ERROR;
	}
	(*input_event)->ie_TimeStamp.tv_secs   = atoi(token); /* Flawfinder: ignore */
	token = strtok(NULL, "|");
	if(token == NULL)
	{
		goto _RETURN_ERROR;
	}
	(*input_event)->ie_TimeStamp.tv_micro  = atoi(token); /* Flawfinder: ignore */

	goto _RETURN_OK;
	_RETURN_OK:
		free(string);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(*input_event != NULL)
		{
			free(*input_event);
			*input_event = NULL;
		}
		if(string != NULL)
		{
			free(string);
		}
		return RETURN_ERROR;
}

static char _convert_InputEvent_to_ANSI(
	IN struct InputEvent *input_event)
{
	char buffer[2] = { 0 };
	char c         = '\0';

	if(MapRawKey((struct InputEvent *)input_event, buffer, 2, NULL) > 0)
	{
		c = buffer[0];
	}
	
	return c;
}

static int _set_current_root_directory_as_current(void)
{
	return controls_set_current_root_directory(controls_get_current_directory());
}

/**
 * Returns:
 * - RETURN_OK if it could set the current root directory.
 * - RETURN_WARN if it could not set the current directory because the directory is wrong.
 * - RETURN_ERROR if it could not set the current directory or use the memory.
 */
static int _set_current_root_directory_manually(void)
{
	char directory[GLOBALS_MAX_LINE_LENGTH];
	int  result = RETURN_ERROR;
	
	printf( GetString( MSG_CONTROLSCLI_ENTERNEWROOTDIRECTORYORENTERTORESETTHEROOTDIRECT ) ); /* Flawfinder: ignore */
	fflush(stdout);
	_enable_RAW_mode(_con_file, FALSE);
	fgets(directory, GLOBALS_MAX_LINE_LENGTH, stdin);
	directory[strlen(directory) - 1] = '\0';
	_enable_RAW_mode(_con_file, TRUE);

	result = controls_set_current_root_directory(directory);
	if(result == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_CONTROLSCLI_SETCURRENTROOTDIRECTORYMANUALLYCOULDNOTSETCURREN ) );
		goto _RETURN_ERROR;
	}
	else if(result == RETURN_WARN)
	{
		goto _RETURN_WARN;
	}
	
	goto _RETURN_OK;
	_RETURN_OK:
		log_print_information( GetString( MSG_CONTROLSCLI_CURRENTROOTDIRECTORYCHANGED ) );
		return RETURN_OK;

	goto _RETURN_WARN;
	_RETURN_WARN:
		log_print_information( GetString( MSG_CONTROLSCLI_CURRENTROOTDIRECTORYFORCEDTOMATCHSOURCEROOT ) );
		return RETURN_WARN;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		log_print_information( GetString( MSG_CONTROLSCLI_COULDNOTCHANGEDCURRENTROOTDIRECTORY ) );
		return RETURN_ERROR;
}

static int _list_blacklisted_directories(void)
{
	char *list = NULL;
	
	if(controls_get_blacklisted_directories_list(&list) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_CONTROLSCLI_LISTBLACKLISTEDDIRECTORIESCOULDNOTGETBLACKLISTED ) );
		goto _RETURN_ERROR;
	}
	
	printf("\33c");
	printf("\33[32m");
	printf( GetString( MSG_CONTROLSCLI_BLACKLISTEDDIRECTORIES ) );			 /* Flawfinder: ignore */
	printf( GetString( MSG_CONTROLSCLI_BLACKLISTEDDIRECTORIES_UNDERLINE ) ); /* Flawfinder: ignore */
	printf("\33[39m");
	printf("%s", list);
	printf("\33[1m");
	printf( GetString( MSG_CONTROLSCLI_PRESSENTERTOCONTINUE ) );			 /* Flawfinder: ignore */
	printf("\33[22m");
	fflush(stdout);
	while(_get_char() != '\r');

	goto _RETURN_OK;
	_RETURN_OK:
		free(list);
		log_print_information( GetString( MSG_CONTROLSCLI_SHOWEDBLACKLISTEDDIRECTORIES ) );
		return RETURN_OK;
	
	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(list != NULL)
		{
			free(list);
		}
		log_print_information( GetString( MSG_CONTROLSCLI_COULDNOTSHOWBLACKLISTEDDIRECTORIES ) );
		return RETURN_ERROR;
}

static int _list_blacklisted_files(void)
{
	char *list = NULL;

	if(controls_get_blacklisted_files_list(&list) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_CONTROLSCLI_LISTBLACKLISTEDFILESCOULDNOTGETBLACKLISTEDFILES ) );
		goto _RETURN_ERROR;
	}

	printf("\33c");
	printf("\33[32m");
	printf( GetString( MSG_CONTROLSCLI_BLACKLISTEDFILES ) );		   /* Flawfinder: ignore */
	printf( GetString( MSG_CONTROLSCLI_BLACKLISTEDFILES_UNDERLINE ) ); /* Flawfinder: ignore */
	printf("\33[39m");
	printf("%s", list);
	printf("\33[1m");
	printf( GetString( MSG_CONTROLSCLI_PRESSENTERTOCONTINUE ) );	   /* Flawfinder: ignore */
	printf("\33[22m");
	fflush(stdout);
	while(_get_char() != '\r');

	goto _RETURN_OK;
	_RETURN_OK:
		free(list);
		log_print_information( GetString( MSG_CONTROLSCLI_SHOWEDBLACKLISTEDFILES ) );
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(list != NULL)
		{
			free(list);
		}
		log_print_information( GetString( MSG_CONTROLSCLI_COULDNOTSHOWBLACKLISTEDFILES ) );
		return RETURN_ERROR;
}

static void _print_debug_console(
	IN char *message)
{
	printf("\33[26;1H\33[K%s", message);
	if(message[strlen(message) - 1] != '\n')
	{
		fflush(stdout);
	}
}

static void _print_information_console(
	IN char *message)
{
	// If the previous message didn't end with \n
	// but this new message does, then separate
	// them with a \n.
	if(_is_info_ending_with_newline  == FALSE &&
	   message[strlen(message) - 1] == '\n')
	{
		printf("\n");
	}

	printf("\33[26;1H\33[K%s", message);
	if(message[strlen(message) - 1] == '\n')
	{
		_is_info_ending_with_newline = TRUE;
	}
	else
	{
		_is_info_ending_with_newline = FALSE;
		fflush(stderr);
	}

	// This is the (only) type of message that
	// I also want to show as "last message"
	controls_build_last_message(message);
}

static void _print_error_console(
	IN char *message)
{
	printf("\33[26;1H\33[K%s", message);
	if(message[strlen(message) - 1] != '\n')
	{
		fflush(stdout);
	}
}

static int _save_current_modules(void)
{
	char  default_directory_name[GLOBALS_MAX_LINE_LENGTH];
	char  current_directory_name[GLOBALS_MAX_LINE_LENGTH];
	char  destination[GLOBALS_MAX_LINE_LENGTH];
	char *current_modules_temp = NULL;
	char *file_name            = NULL;

	GetCurrentDirName(current_directory_name, GLOBALS_MAX_LINE_LENGTH);
	strncpy(default_directory_name, current_directory_name, GLOBALS_MAX_LINE_LENGTH);
	strncat(default_directory_name, "/modules/", GLOBALS_MAX_LINE_LENGTH - strlen(default_directory_name) - 1);
	printf( GetString( MSG_CONTROLSCLI_PLEASEENTERDESTINATIONDIRECTORYORENTERFORTHEDEFA ) , default_directory_name); /* Flawfinder: ignore */
	fflush(stdout);
	_enable_RAW_mode(_con_file, FALSE);
	fgets(destination, GLOBALS_MAX_LINE_LENGTH, stdin);
	destination[strlen(destination) - 1] = '\0';
	_enable_RAW_mode(_con_file, TRUE);

	if(strlen(destination) == 0)
	{
		strncat(destination, default_directory_name, strlen(default_directory_name));
	}

	current_modules_temp = string_duplicate(controls_get_current_file_list());
	if(current_modules_temp == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSCLI_SAVECURRENTMODULESCOULDNOTALLOCATEMEMORYFORTEMPO ) );
		goto _RETURN_ERROR;
	}
	file_name = strtok(current_modules_temp, "\n");
	while(file_name != NULL)
	{
		if(io_file_copy(file_name, destination) == RETURN_ERROR)
		{
			log_print_error( GetString( MSG_CONTROLSCLI_SAVECURRENTMODULESCOULDNOTCOPY ) , file_name);
			goto _RETURN_ERROR;
		}
		file_name = strtok(NULL, "\n");
	}
	
	goto _RETURN_OK;
	_RETURN_OK:
		// Not freeing file_name because it is freed with current_modules_temp
		free(current_modules_temp);
		log_print_information( GetString( MSG_CONTROLSCLI_SAVEDCURRENTMODULES ) );
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		// Not freeing file_name because it is freed with current_modules_temp
		if(current_modules_temp != NULL)
		{
			free(current_modules_temp);
		}
		log_print_information( GetString( MSG_CONTROLSCLI_COULDNOTSAVECURRENTMODULES ) );
		return RETURN_ERROR;
}

static int _choose_source(
		   void)
{
	char **sources_names     = NULL;
	int    number_of_sources = 0;
	int    i                 = 0;
	char   user_answer       = '\0';
	int    choice            = 0;
	
	controls_get_sources_names(&sources_names);
	if(sources_names == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSCLI_CHOOSESOURCECOULDNOTFINDSOURCESNAMES ) );
		goto _RETURN_ERROR;
	}

	number_of_sources = controls_get_sources_number();
	if(number_of_sources <= 0)
	{
		log_print_error( GetString( MSG_CONTROLSCLI_CHOOSESOURCECOULDNOTFINDATLEASTONESOURCENAME ) );
		goto _RETURN_ERROR;
	}

	printf("\33c");

	printf("\33[32m");
	printf( GetString( MSG_CONTROLSCLI_SOURCESNAMES ) );		   /* Flawfinder: ignore */
	printf( GetString( MSG_CONTROLSCLI_SOURCESNAMES_UNDERLINE ) ); /* Flawfinder: ignore */
	printf("\33[39m");

	for(i = 0; i < number_of_sources; i++)
	{
		printf("%d. %s\n", i + 1, sources_names[i]);
	}
	
	printf("\33[1m");
	printf( GetString( MSG_CONTROLSCLI_CHOOSEONESOURCEORENTERFORTHEDEFAULTSOURCE ) , sources_names[0]); /* Flawfinder: ignore */
	printf("\33[22m");
	fflush(stdout);
	user_answer = _get_char();

	if(user_answer == '\r')
	{
		choice = 0;
	}
	else if(isdigit(user_answer) == FALSE)
	{
		log_print_error( GetString( MSG_CONTROLSCLI_CHOOSESOURCEUNAVAILABLECHOICECN ) , user_answer);
		goto _RETURN_ERROR;
	}
	else
	{
		choice = user_answer - '0';
		choice--;
		if(choice >= number_of_sources)
		{
			log_print_error( GetString( MSG_CONTROLSCLI_CHOOSESOURCEUNAVAILABLECHOICECN ) , user_answer);
			goto _RETURN_ERROR;
		}
	}

	if(controls_set_current_source_index(choice) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_CONTROLSCLI_CHOOSESOURCECOULDNOTSETCURRENTSOURCE ) );
		goto _RETURN_ERROR;
	}
	controls_set_current_root_directory(NULL);
	controls_set_current_directory(NULL);

	goto _RETURN_OK;
	_RETURN_OK:
		for(i = 0; i < number_of_sources; i++)
		{
			free(sources_names[i]);
		}
		free(sources_names);
		log_print_information( GetString( MSG_CONTROLSCLI_CURRENTSOURCECHANGED ) );
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(sources_names != NULL)
		{
			for(i = 0; i < number_of_sources; i++)
			{
				if(sources_names[i] != NULL)
				{
					free(sources_names[i]);
				}
			}
			free(sources_names);
		}
		log_print_information( GetString( MSG_CONTROLSCLI_COULDNOTCHANGESOURCE ) );
		return RETURN_ERROR;
}

static void _set_progress_max(
	IN char *origin,
	IN int   value)
{
	_progress_val = 0;
	if(value > 0)
	{
		_progress_max = value;
	}
	else
	{
		_progress_max = 100;
	}
}

static void _set_progress_val(
	IN char *origin,
	IN int value)
{
	_progress_val += value;

	if(_progress_val > _progress_max)
	{
		_progress_val = _progress_max;
	}

	printf("\33[27;1H%d/%d\33[K", _progress_val, _progress_max);
}

