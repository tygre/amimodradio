/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



#ifndef CONTROLS_MUI_H
#define CONTROLS_MUI_H 1

#include <libraries/mui.h>

#include "utils.h"

int	 controls_mui(
	 IN BPTR path_list);

void controls_mui_doublestart(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void controls_mui_about(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void controls_mui_play(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void controls_mui_replay(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void controls_mui_pause(
	 IN struct Hook *h,
	IN Object      *o,
	 IN APTR        *d);

void controls_mui_stop(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void controls_mui_next(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void controls_mui_root_directory_prev(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void controls_mui_root_directory_next(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void controls_mui_root_directory_reset(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void controls_mui_directory_list(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void controls_mui_directory_ban(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void controls_mui_directory_set_as_root(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void controls_mui_file_list(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void controls_mui_file_ban(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void controls_mui_modules_save(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void controls_mui_change_directory(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void controls_mui_modules_email(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void controls_mui_set_user_rating(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void controls_mui_keyboard_space(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void mui_prefs_play_on_startup(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void mui_prefs_show_log_page_on_fetch(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void mui_prefs_maximum_log_size_changed(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void mui_prefs_remember_source_between_runs(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void mui_prefs_saved_modules_directory_changed(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void mui_prefs_cache_directory_changed(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void mui_prefs_cache_list_during_run(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void mui_prefs_cache_list_between_runs(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void mui_prefs_save_prefs_on_exit(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void mui_prefs_remember_images_set(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void mui_prefs_skip_if_banned(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void mui_prefs_skip_if_user_rating_low(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void mui_prefs_skip_if_all_rating_exists(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void mui_prefs_skip_if_all_rating_low(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void mui_prefs_skip_if_above_maximum_file_size(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void mui_prefs_maximum_file_size_changed(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void mui_prefs_save(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void mui_prefs_reset(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void mui_prefs_default(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void mui_log_save(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void mui_log_clear(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void controls_mui_open_directories_ban_list(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

void controls_mui_open_files_ban_list(
	 IN struct Hook *h,
	 IN Object      *o,
	 IN APTR        *d);

#endif

