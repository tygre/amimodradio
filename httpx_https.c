/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include "httpx_https.h"

#include "globals.h"
#include "httpx.h"
#include "locale.h"
#include "log.h"
#include "prefs.h"                  // Only to compare the downloading file size against user's preferences
#include "utils.h"
#include "utils_socketbase.h" // Sockets and such from Roadshow SDK, with task-specific SocketBase and data
#include "z_fortify.h"

#include <proto/dos.h>              // For Output()
#include <proto/amissl.h>
#include <proto/amisslmaster.h>
#include <amissl/amissl.h>
#include <libraries/amisslmaster.h>
#include <libraries/amissl.h>



/* Constants and declarations */

#define AMISSL_CURRENT_VERSION_STR "5.16"
#define RANDOM_SEED_BUFFER 24

int             httpx_https_setup(IN BOOL (*)(), IN void (*)(IN char *, IN int), IN void (*)(IN char *, IN int));
void            httpx_https_cleanup(void);
int             httpx_https_init_connection(void);
void            httpx_https_close_connection(void);
httpx_response *httpx_https_request(IN char *, IN parsed_url *, IN int);



/* Definitions */

	   struct Library *UtilityBase                      = NULL;
	   struct Library *AmiSSLMasterBase                 = NULL;
	   struct Library *AmiSSLBase                       = NULL;
static BOOL            (*_continue_long_operation_fp)() = NULL; // Tygre 20/08/02: To stop long-running HTTP operations
static void            (*_httpx_size_callback_fp)(
							IN char *origin,
							IN int number_of_bytes)     = NULL; // Tygre 22/12/19: To update a progress bar when necessary
static void            (*_httpx_read_callback_fp)(
							IN char *origin,
							IN int number_of_bytes)     = NULL; // Tygre 22/12/19: To update a progress bar when necessary

// Tygre 24/08/08: Multi-tasking
// These variables should not be shared between tasks,
// I must use tc_UserData_t from utils_socketbase.h.
//	static SSL_CTX *_ssl_context = NULL;



int httpx_https_setup(
	IN BOOL (*continue_long_operation_fp)(),
	IN void (*httpx_size_callback_fp)(IN char *origin, IN int number_of_bytes),
	IN void (*httpx_read_callback_fp)(IN char *origin, IN int number_of_bytes))
{
	log_print_debug("httpx_https_setup()\n");

	if((UtilityBase = OpenLibrary("utility.library", 0)) == NULL)
	{
		log_print_error("httpx_https_setup(), could not open utility.library!\n");
		return RETURN_ERROR;
	}

	if((AmiSSLMasterBase = OpenLibrary("amisslmaster.library", AMISSLMASTER_MIN_VERSION)) == NULL)
	{
		log_print_error("httpx_https_setup(), could not open amisslmaster.library v%d+!\n", AMISSLMASTER_MIN_VERSION);
		log_print_error("(Do you have AmiSSL v" AMISSL_CURRENT_VERSION_STR " installed?)\n");
		return RETURN_ERROR;
	}

	if(InitAmiSSLMaster(AMISSL_CURRENT_VERSION, TRUE) == FALSE)
	{
		log_print_error("httpx_https_setup(), could not initialise amisslmaster.library!\n");
		log_print_error("(Do you have AmiSSL v" AMISSL_CURRENT_VERSION_STR " installed?)\n");
		return RETURN_ERROR;
	}
	
	if((AmiSSLBase = OpenAmiSSL()) == NULL)
	{
		log_print_error("httpx_https_setup(), could not open AmiSSL!\n");
		return RETURN_ERROR;
	}

	_continue_long_operation_fp = continue_long_operation_fp;
	_httpx_size_callback_fp     = httpx_size_callback_fp;
	_httpx_read_callback_fp     = httpx_read_callback_fp;

	return RETURN_OK;
}

void httpx_https_cleanup(
	 void)
{
	log_print_debug("httpx_https_cleanup()\n");

	if(AmiSSLBase != NULL)
	{
		CloseAmiSSL();
		AmiSSLBase = NULL;
	}

	if(AmiSSLMasterBase != NULL)
	{
		CloseLibrary(AmiSSLMasterBase);
		AmiSSLMasterBase = NULL;
	}

	if(UtilityBase != NULL)
	{
		CloseLibrary(UtilityBase);
		UtilityBase = NULL;
	}
}

int httpx_https_init_connection(
	void)
{
	char     buffer[RANDOM_SEED_BUFFER + 1] = { 0 };
	SSL_CTX *ssl_context                    = NULL;
	
	// printf("%s: \thttpx_https_init_connection()\n", FindTask(NULL)->tc_Node.ln_Name);
	log_print_debug("httpx_https_init_connection()\n");

	if(_httpx_size_callback_fp)
	{
		_httpx_size_callback_fp("httpx_https_init_connection()", GLOBALS_MAX_LINE_LENGTH);
	}
	if(_httpx_read_callback_fp)
	{
		_httpx_read_callback_fp("httpx_https_init_connection()", GLOBALS_MAX_LINE_LENGTH);
	}

	// Initialise AmiSSL and OpenSSL
	if(InitAmiSSL(AmiSSL_ErrNoPtr, &errno, AmiSSL_SocketBase, SocketBase, TAG_DONE) != 0)
	{
		log_print_error("httpx_https_init_connection(), could not initialise AmiSSL base!\n");
		return RETURN_ERROR;
	}

	if(OPENSSL_init_ssl(
		OPENSSL_INIT_SSL_DEFAULT |
		OPENSSL_INIT_ADD_ALL_CIPHERS |
		OPENSSL_INIT_ADD_ALL_DIGESTS,
		NULL) == 0)
	{
		log_print_error("httpx_https_init_connection(), could not initialise SSL!\n");
		return RETURN_ERROR;
	}

	// Set random seed
	string_snprintf(buffer, strlen(buffer), "%lx%lx%lx", (unsigned long)time((time_t *) NULL),
														 (unsigned long)FindTask(NULL),
														 (unsigned long)rand());
	RAND_seed(buffer, RANDOM_SEED_BUFFER);

	// Create SSL context
	if((ssl_context = SSL_CTX_new(TLS_client_method())) == NULL)
	{
		log_print_error("httpx_https_init_connection(), could not create SSL context!\n");
		return RETURN_ERROR;
	}
	utils_socketbase_ssl_context_set(ssl_context);

	return RETURN_OK;
}

void httpx_https_close_connection(
	 void)
{
	// printf("%s: \thttpx_https_close_connection()\n", FindTask(NULL)->tc_Node.ln_Name);
	log_print_debug("httpx_https_close_connection()\n");

	if(utils_socketbase_ssl_context_get() != NULL)
	{
		SSL_CTX_free(utils_socketbase_ssl_context_get());
		utils_socketbase_ssl_context_set(NULL);
	}
		
	CleanupAmiSSLA(NULL);

	if(_httpx_size_callback_fp)
	{
		_httpx_size_callback_fp("httpx_https_init_connection()", GLOBALS_MAX_LINE_LENGTH);
	}
	if(_httpx_read_callback_fp)
	{
		_httpx_read_callback_fp("httpx_https_init_connection()", GLOBALS_MAX_LINE_LENGTH);
	}
}

httpx_response *httpx_https_request(
	IN char       *http_headers,
	IN parsed_url *purl,
	IN  int        bytes_limit)
{
	SSL                 *ssl                   = NULL;
	int                  sock                  = -1;
	struct timeval       tv                    = { 0 };
	struct hostent      *he                    = NULL;
	struct sockaddr_in   remote                = { 0 };
	int                  ssl_result            = 0;
	BOOL                 is_ssl_connected      = FALSE;
	char                *hreq                  = NULL;
	int                  hreq_size             = 0;
	int                  sent                  = 0;
	char                *received_data         = NULL;
	int                  size_of_received_data = 0;
	char                *response              = NULL;
	int                  response_size         = 0;
	char                *response_temp         = NULL;
	int                  number_of_bytes       = 0;
	char                *temp_string           = NULL;
	httpx_response      *hresp                 = NULL;
	char                *status_line           = NULL;

	// Parsed URL
	if(purl == NULL)
	{
		log_print_error("httpx_https_request(), could not parse URL\n");
		goto _RETURN_ERROR;
	}

	// Create new SSL
	if((ssl = SSL_new(utils_socketbase_ssl_context_get())) == NULL)
	{
		// TODO: If the calls fails, then the error is correctly forwarded
		// from here to https_XXX() to _sources_pick_file_XXX() to
		// sources_pick_file() to, finally, _fetch_modules(). However,
		// _fetch_modules() cannot distinguish this (fatal) error from
		// a more benign error, like a module ID that doesn't correspond
		// to a real file in Modules.pl or AMP. I should distinguish
		// between RETURN_OK, RETURN_WARN, and RETURN_ERROR.
		log_print_error("httpx_https_request(), could not create SSL!\n");
		goto _RETURN_ERROR;
	}

	// Create socket
	if((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
	{
		log_print_error("httpx_https_request(), could not create socket\n");
		goto _RETURN_ERROR;
	}

	// Get host name
	he = gethostbyname((char *)purl->host);
	if(he == NULL)
	{
		log_print_error("httpx_https_request(), could not get IP address of %s\n" , purl->host);
		goto _RETURN_ERROR;
	}

	// Zero out the remote address
	memset(&remote, 0, sizeof(remote));

	// Set up remote address for connect
	// Tygre 22/02/05: Guru on 68000
	// The code used to be:
	//	temp_int = inet_pton(AF_INET, purl->ip, (void *)(&(remote->sin_addr.s_addr)));
	// which would Guru 8000 0003 on a 68000
	// because of misalignment of the address.
	// I replaced it by the simpler code:
	memcpy(&remote.sin_addr, he->h_addr, he->h_length);
	remote.sin_family = AF_INET;
	remote.sin_port   = htons(atoi(purl->port)); /* Flawfinder: ignore */

	// Connect to server
	if(connect(sock, (struct sockaddr *)&remote, sizeof(struct sockaddr)) < 0)
	{
		log_print_error("httpx_https_request(), could not connect to server\n");
		goto _RETURN_ERROR;
	}

	// Set a timeout
	tv.tv_secs  = GLOBALS_MAX_WAIT_IN_SECONDS;
	tv.tv_micro = 0;

	if(setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv))) {
		log_print_warning("httpx_https_request(), could not set timeout (send)\n");
		// Not so important?
		//	goto _RETURN_ERROR;
	}
	if(setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv))) {
		log_print_warning("httpx_https_request(), could not set timeout (recv)\n");
		// Not so important?
		//	goto _RETURN_ERROR;
	}

	// Associate SSL to socket
	SSL_set_fd(ssl, sock);

	// Set host name for TLS
	SSL_set_tlsext_host_name(ssl, purl->host);

	// Connect via SSL
	if((ssl_result = SSL_connect(ssl)) >= 0)
	{
		is_ssl_connected = TRUE;
	}
	else
	{
		log_print_error("httpx_https_request(), could not connect via SSL!\n");
		goto _RETURN_ERROR;
	}

	// Send headers to server
	// Tygre 2016/05/15: Format
	// Somehow, the header sent to the Web server was broken
	// because of the missing mandatory trailing "\r\n". The
	// server was timeouting instead of sending a response.
	hreq_size = strlen(http_headers) + 2;
	hreq      = malloc((hreq_size + 1) * sizeof(char));
	if(hreq   == NULL)
	{
		log_print_error("httpx_https_request(), could not allocate HTTP request\n");
		goto _RETURN_ERROR;
	}
	hreq[0] = '\0';
	strcat(hreq, http_headers);
	strcat(hreq, "\r\n");

	if(SSL_write(ssl, hreq, hreq_size) < 1)
	{
		log_print_error("httpx_https_request(), could not send request via SSL!\n");
		goto _RETURN_ERROR;
	}

	// Receive response
	received_data    = malloc((GLOBALS_MAX_BUFFER_LENGTH + 1) * sizeof(char));
	if(received_data == NULL)
	{
		log_print_error("httpx_https_request(), could not allocate buffer\n");
		goto _RETURN_ERROR;
	}
	
	response_size = GLOBALS_MAX_BUFFER_LENGTH;
	response      = malloc((response_size + 1) * sizeof(char));
	if(response   == NULL)
	{
		log_print_error("httpx_https_request(), could not allocate buffer\n");
		goto _RETURN_ERROR;
	}
	
	while((size_of_received_data = SSL_read(ssl, received_data, GLOBALS_MAX_BUFFER_LENGTH)) > 0)
	{
		if(_continue_long_operation_fp != NULL && !_continue_long_operation_fp())
		{
			// Tygre 2020/08/02: Interrupt
			// If the callback function returns false,
			// then the caller code decided to stop
			// this long-running operation.
			log_print_warning("httpx_https_request(), receiving data interrupted by user\n");
			goto _RETURN_ERROR;
		}

		if(_httpx_read_callback_fp)
		{
			_httpx_read_callback_fp("httpx_https_request() (receiving)", size_of_received_data);
		}

		if(response_size < number_of_bytes + size_of_received_data)
		{
			response_size = response_size + 3 * GLOBALS_MAX_BUFFER_LENGTH;
			response_temp = realloc(response, response_size);
			if(response_temp == NULL)
			{
				log_print_error("httpx_https_request(), could not allocate buffer\n");
				goto _RETURN_ERROR;
			}
			response      = response_temp;
		}
		memcpy(response + number_of_bytes, received_data, size_of_received_data);
		number_of_bytes += size_of_received_data;

		if(bytes_limit > 0 && number_of_bytes > bytes_limit)
		{
			log_print_error("Stopped downloading a file too large wrt. user's preferences\n");
			goto _RETURN_ERROR;
		}
	}

	free(received_data);
	received_data = NULL;

	if(size_of_received_data < 0)
	{
		log_print_error("httpx_https_request(), could not receive data from the server\n");
		goto _RETURN_ERROR;
	}

	// Allocate memeory for response
	hresp    = malloc(sizeof(httpx_response));
	if(hresp == NULL)
	{
		log_print_error("httpx_https_request(), could not allocate HTTP response\n");
		goto _RETURN_ERROR;
	}
			 hresp->request_uri        = purl;
	         hresp->body               = NULL;
	*(int *)&hresp->body_size_in_bytes = 0;
	         hresp->status_code        = NULL;
	*(int *)&hresp->status_code_value  = 0;
	         hresp->status_text        = NULL;
			 hresp->request_headers    = http_headers;
	         hresp->response_headers   = NULL;

	// Parse body
	temp_string    = strstr(response, "\r\n\r\n");
	if(temp_string == NULL)
	{
		log_print_error("httpx_https_request(), could not find response delimiter\n");
		goto _RETURN_ERROR;
	}
	number_of_bytes    = number_of_bytes - (temp_string + 4 - response) * sizeof(char);
	if(number_of_bytes > 0)
	{
		// Tygre 23/01/29: With different optimisation
		// malloc(0) either returns non-NULL or NULL.
		// I protect myself by checking the number of
		// bytes to allocate first. I must continue
		// even if the body is NULL because it could
		// be legitimate with, e.g., a 302 status code.
		hresp->body    = malloc(number_of_bytes);
		if(hresp->body == NULL)
		{
			temp_string = NULL;
			log_print_error("httpx_https_request(), could not allocate response body\n");
			goto _RETURN_ERROR;
		}
		memcpy((void *)hresp->body, temp_string + 4, number_of_bytes);
		*(int *)&hresp->body_size_in_bytes = number_of_bytes;
	}

	// Parse status code and text
	temp_string = get_until(response, "\r\n");
	status_line = strrpl("HTTP/1.1 ", "", temp_string);
	free(temp_string);
	temp_string = NULL;

	temp_string        = string_duplicate_n(status_line, 4);
	hresp->status_code = strrpl(" ", "", temp_string);
	free(temp_string);
	temp_string = NULL;

	temp_string        = strrpl(hresp->status_code, "", status_line);
	hresp->status_text = strrpl(" ", "", temp_string);
	free(temp_string);
	temp_string = NULL;

	*(int *)&hresp->status_code_value = atoi(hresp->status_code); /* Flawfinder: ignore */

	// Parse response headers
	hresp->response_headers	= get_until(response, "\r\n\r\n");

	goto _RETURN_OK;
	_RETURN_OK:
		free(status_line);
		free(response);
		free(hreq);
		SSL_shutdown(ssl);
		CloseSocket(sock);
		SSL_free(ssl);
		return hresp;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(status_line != NULL)
		{
			free(status_line);
		}
		if(temp_string != NULL)
		{
			free(temp_string);
		}
		if(received_data != NULL)
		{
			free(received_data);
		}
		if(hresp != NULL)
		{
			// Don't touch the request URL,
			// headers, they come from the
			// caller, which handles them.
			hresp->request_uri     = NULL;
			hresp->request_headers = NULL;
			httpx_free_httpx_response(hresp);
		}
		if(response != NULL)
		{
			free(response);
		}
		if(hreq != NULL)
		{
			free(hreq);
		}

		if(is_ssl_connected == TRUE)
		{
			SSL_shutdown(ssl);
		}
		if(sock > -1)
		{
			CloseSocket(sock);
		}
		if(ssl != NULL)
		{
			SSL_free(ssl);
		}
		return NULL;
}

