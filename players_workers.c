/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include "players_workers.h"

#include "globals.h"
#include "locale.h"
#include "log.h"
#include "players.h"
#include "utils.h"
#include "utils_tasks.h"
#include "z_fortify.h"

#include <ctype.h>
#include <dos/dostags.h>
#include <proto/dos.h>
#include <proto/exec.h>



/* Constants and declarations */

#define LISTENER_NAME  "AmiModRadio Listener"

	   int   players_workers_setup(void);
	   void  players_workers_cleanup(void);
	   int   players_workers_player_listener_start(void);
	   void  players_workers_player_listener_listen(void);
	   void  players_workers_player_listener_wait(void);
	   void  players_workers_player_listener_stop(void);
	   ULONG players_workers_signal_control_playing_stopped(void);
static void  _player_listener_loop(void);
static void  _free_signals(void);



/* Definitions */

static struct Task    *_player_listener_task                 = NULL;
static ULONG           _SIGNAL_TO_CONTROL_PLAYING_STOPPED    = -1;
static ULONG           _SIGNALSET_TO_CONTROL_PLAYING_STOPPED = -1;

int  players_workers_setup(
	void)
{
	log_print_debug("players_workers_setup()\n");

	if(utils_tasks_allocate_signal(
		&_SIGNAL_TO_CONTROL_PLAYING_STOPPED,
		&_SIGNALSET_TO_CONTROL_PLAYING_STOPPED,
		"_SIGNAL_TO_CONTROL_PLAYING_STOPPED") == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_PLAYERS_PLAYERSINITCONNECTIONCOULDNOTALLOCATEONESIGNAL ) );
		_free_signals();
		return RETURN_ERROR;
	}

	return RETURN_OK;
}

void players_workers_cleanup(
	void)
{
	log_print_debug("players_workers_cleanup()\n");

	_free_signals();
}

int players_workers_player_listener_start(
	void)
{
	if(_player_listener_task != NULL)
	{
		log_print_warning("players_workers_player_listener_start(), player listener already started\n");
	}

	_player_listener_task = (struct Task *)CreateNewProcTags(
		NP_Entry,       &_player_listener_loop,
		NP_Priority,    1,
		NP_Name,        LISTENER_NAME,
		NP_Output,      Output(),
		NP_CloseOutput, FALSE,
		NP_StackSize,   GLOBALS_TASK_STACK_SIZE,
		TAG_DONE);

	if(_player_listener_task == NULL)
	{
		log_print_error( GetString( MSG_PLAYERS_PLAYERLISTENERSTARTCOULDNOTCREATEPLAYERLISTENER ) );
		return RETURN_ERROR;
	}

	return RETURN_OK;
}

void players_workers_player_listener_listen(
	void)
{
	if(_player_listener_task == NULL)
	{
		log_print_warning("players_workers_player_listener_listen(), player listener not started\n");
	}

	utils_tasks_send_signal_without_ack(
		_player_listener_task,
		utils_tasks_signal_task_to_do());
}

void players_workers_player_listener_wait(
	void)
{
	if(_player_listener_task == NULL)
	{
		log_print_warning("players_workers_player_listener_wait(), player listener not started\n");
	}

	utils_tasks_send_signal_without_ack(
		_player_listener_task,
		utils_tasks_signal_task_to_wait());
}

void players_workers_player_listener_stop(
	void)
{
	if(_player_listener_task == NULL)
	{
		log_print_warning("players_workers_player_listener_stop(), player listener not started\n");
	}

	utils_tasks_send_signal_and_wait_ack(
		_player_listener_task,
		utils_tasks_signal_task_to_stop(),
		utils_tasks_signal_task_to_stop());

	_player_listener_task = NULL;
}

ULONG players_workers_signal_control_playing_stopped(
	void)
{
	return _SIGNALSET_TO_CONTROL_PLAYING_STOPPED;
}

static void _player_listener_loop(
	void)
{
	ULONG  received_signals    = 0;
	BOOL   has_playing_stopped = FALSE;

	while(TRUE)
	{
		received_signals = Wait(
			SIGBREAKF_CTRL_C |
			utils_tasks_signal_task_to_do() |
			utils_tasks_signal_task_to_wait() |
			utils_tasks_signal_task_to_stop());

		if((received_signals & utils_tasks_signal_task_to_do()) == utils_tasks_signal_task_to_do())
		{
			// Start listening...
			while(TRUE)
			{
				Delay(250);

				received_signals = CheckSignal(
					SIGBREAKF_CTRL_C |
					utils_tasks_signal_task_to_do() |
					utils_tasks_signal_task_to_wait() |
					utils_tasks_signal_task_to_stop());

				if((received_signals & utils_tasks_signal_task_to_do()) == utils_tasks_signal_task_to_do())
				{
					// Nothing to do
				}

				if((received_signals & utils_tasks_signal_task_to_wait()) == utils_tasks_signal_task_to_wait())
				{
					// Go wait
					break;
				}

				if((received_signals & utils_tasks_signal_task_to_stop()) == utils_tasks_signal_task_to_stop() ||
				   (received_signals & SIGBREAKF_CTRL_C)                  == SIGBREAKF_CTRL_C)
				{
					// Stop completely
					break;
				}

				players_init_connection();
				has_playing_stopped = players_check_player_playing();
				players_close_connection();

				if(has_playing_stopped)
				{
					// Tygre 20/11/18: Orders!
					// Tell AMR that playing has stopped.
					// It will tell us what to do next...
					utils_tasks_send_signal_without_ack(
						utils_tasks_get_main_task(),
						players_workers_signal_control_playing_stopped());

					// ... But we can already go wait.
					break;
				}
			}
		}
		
		if((received_signals & utils_tasks_signal_task_to_wait()) == utils_tasks_signal_task_to_wait())
		{
			// Go wait
		}
		
		if((received_signals & utils_tasks_signal_task_to_stop()) == utils_tasks_signal_task_to_stop() ||
		   (received_signals & SIGBREAKF_CTRL_C)                  == SIGBREAKF_CTRL_C)
		{
			// Stop completely
			break;
		}
	}

	utils_tasks_send_back_ack();
}

static void _free_signals(
	void)
{
	utils_tasks_free_signal(_SIGNAL_TO_CONTROL_PLAYING_STOPPED);
}

