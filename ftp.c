/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/




/* Includes */

#include "ftp.h"

#include "globals.h"
#include "locale.h"
#include "log.h"
#include "utils.h"
#include "utils_socketbase.h" // Sockets and such from Roadshow SDK, with task-specific SocketBase and data
#include "z_fortify.h"

#include <stdio.h>
#include <stdlib.h>
#include <dos/dos.h>          // For RETURN_OK, RETURN_ERROR
#include <proto/exec.h>       // For OpenLibrary() and CloseLibrary()



/* Constants and declarations */

	   int   ftp_setup(IN BOOL (*)(), IN void (*)(IN char *, IN int), IN void (*)(IN char *, IN int));
	   void  ftp_cleanup(void);
	   int   ftp_init_connection(IN char *, OUT int *);
	   void  ftp_close_connection(IN int);
	   int   ftp_get_list(IN char *, IN int, IN char *, OUT char **);
	   int   ftp_get_list_from_data(IN char *, IN int, IN char *, OUT char **);
	   int   ftp_check_file_exists(IN char *, IN int, IN char *, OUT BOOL *);
	   int   ftp_get_file_from_data(IN char *, IN int, IN char *, OUT char **, OUT int *);
static int   _connect_to_server(IN char *, IN int, OUT int *);
static int   _connect_to_server_for_commands(IN char *, IN int, OUT int *);
static int   _connect_to_server_for_data(IN char *, IN int, OUT int *);
static int   _get_data_port(IN int, OUT int *);
static int   _execute_command(IN int, IN char *);
static int   _read_lines_from_server_until_code(IN int, IN char *, OUT char **);
static int   _read_lines_from_server(IN int, OUT char **);
static int   _read_line_from_server(IN int, OUT char **);
static int   _read_bytes_from_server(IN int, OUT char **, OUT int *);



/* Definitions */

// Tygre 22/02/20: Fixing #113
// I cannot share a same SocketBase among different tasks.
// The solution was on the Internet, thank you AROS developers!
// Fist, replace the MiamiSDK with the Roadshow one and, then,
// compile without global SocketBase but with a task-based one.
//	struct Library *SocketBase = NULL;

static BOOL  (*_continue_long_operation_fp)() = NULL; // Tygre 2020/08/02: To stop long-running HTTP operations
static void  (*_ftp_size_callback_fp)(
				IN char *origin,
				IN int number_of_bytes)       = NULL; // Tygre 2022/12/19: To update a progress bar when necessary
static void  (*_ftp_read_callback_fp)(
				IN char *origin,
				IN int number_of_bytes)       = NULL; // Tygre 2022/12/19: To update a progress bar when necessary



int ftp_setup(
	IN  BOOL  (*continue_long_operation_fp)(),
	IN  void  (*ftp_size_callback_fp)(IN char *origin, IN int number_of_bytes),
	IN  void  (*ftp_read_callback_fp)(IN char *origin, IN int number_of_bytes))
{
	log_print_debug("ftp_setup()\n");

	_continue_long_operation_fp = continue_long_operation_fp;
	_ftp_size_callback_fp       = ftp_size_callback_fp;
	_ftp_read_callback_fp       = ftp_read_callback_fp;

	return RETURN_OK;
}

void ftp_cleanup(
	void)
{
	log_print_debug("ftp_cleanup()\n");

	// Nothing to do
}

int ftp_init_connection(
	IN  char *host_name,
	OUT int  *socket_handle_for_commands)
{
	// printf("%s: ftp_init_connection()\n", FindTask(NULL)->tc_Node.ln_Name);
	log_print_debug("ftp_init_connection()\n");

	// If I cannot initialise SocketBase and other task data, all hope is lost!
	if(utils_socketbase_init() == RETURN_ERROR)
	{
		log_print_error("ftp_init_connection(), could not instantiate the task data!\n");
		return RETURN_ERROR;
	}

	// If I cannot open this library, there's a major problem.
	if(SocketBase == NULL)
	{
		if((SocketBase = OpenLibrary("bsdsocket.library", 4)) == NULL)
		{
			log_print_error( GetString( MSG_FTP_FTPINITCONNECTIONCOULDNOTOPENBSDSOCKETLIBRARYV4 ) );
			return RETURN_ERROR;
		}
	}
	else
	{
		log_print_error("ftp_init_connection(), another function already initialised SocketBase!\n");
	}

	if(_ftp_size_callback_fp)
	{
		_ftp_size_callback_fp("ftp_init_connection()", GLOBALS_MAX_LINE_LENGTH);
	}
	if(_ftp_read_callback_fp)
	{
		_ftp_read_callback_fp("ftp_init_connection()", 0);
	}

	// Connect to the command port
	if(_connect_to_server_for_commands(host_name, 21, socket_handle_for_commands) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_FTP_FTPINITCONNECTIONCOULDNOTCONNECTTOSERVER ) , host_name);
		goto _RETURN_ERROR;
	}

	// Exit
	goto _RETURN_OK;
	_RETURN_OK:
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(*socket_handle_for_commands >= 0)
		{
			CloseSocket(*socket_handle_for_commands);
	    }
		return RETURN_ERROR;
}

void ftp_close_connection(IN int socket_handle_for_commands)
{
	log_print_debug("ftp_close_connection()\n");

	if(socket_handle_for_commands >= 0)
	{
		CloseSocket(socket_handle_for_commands);
	}

	if(_ftp_size_callback_fp)
	{
		_ftp_size_callback_fp("ftp_close_connection()", GLOBALS_MAX_LINE_LENGTH);
	}
	if(_ftp_read_callback_fp)
	{
		_ftp_read_callback_fp("ftp_close_connection()", GLOBALS_MAX_LINE_LENGTH);
	}

	if(SocketBase != NULL)
	{
		CloseLibrary(SocketBase);
		SocketBase = NULL;
	}
	else
	{
		log_print_error("ftp_init_connection(), another function already closed SocketBase!\n");
	}

	utils_socketbase_close();
}

int ftp_get_list(
	IN  char  *host_name,
	IN  int    socket_handle_for_commands,
	IN  char  *directory_name,
	OUT char **list)
{
	char *command   = NULL;
	char *list_temp = NULL;
	int   retries   = 0;

	// Allocate memory
	command    = malloc(GLOBALS_MAX_LINE_LENGTH * sizeof(char));
	if(command == NULL)
	{
		log_print_error( GetString( MSG_FTP_FTPGETLISTCOULDNOTALLOCATECOMMAND ) );
		goto _RETURN_ERROR;
	}

	// Issue the command to list the directory of interest
	string_snprintf(command, GLOBALS_MAX_LINE_LENGTH, "CWD %s", directory_name);
	if(_execute_command(socket_handle_for_commands, command) < 0)
	{
		log_print_error( GetString( MSG_FTP_FTPGETLISTCOULDNOTEXECUTECOMMAND ) , command);
		goto _RETURN_ERROR;
	}
	if(_read_lines_from_server_until_code(socket_handle_for_commands, "250", &list_temp) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_FTP_FTPGETLISTDIDNORECEIVEEXPECTED250RETURNCODE ) );
		goto _RETURN_ERROR;
	}
	free(list_temp);
	list_temp = NULL;

	string_snprintf(command, GLOBALS_MAX_LINE_LENGTH, "STAT -l");
	if(_execute_command(socket_handle_for_commands, command) < 0)
	{
		log_print_error( GetString( MSG_FTP_FTPGETLISTCOULDNOTEXECUTECOMMAND ) , command);
	    goto _RETURN_ERROR;
	}
	// Tygre 2015/08/01: Implementation variations
	// Aminet returns one single line with the FTP return code 211 after 
	// "STAT -l" and this line contains the content of the directory.
	// Modland returns multiple lines, the first line starting with "213-"
	// and the last line starting with "213 " and every line in between
	// being one directory or file.
	// Modland actually implements the correct protocol.
	// The difference does not bring any problem because I can use for
	// both "ftp_get_list_from_data" but it should be handled somehow.
	if(_read_lines_from_server_until_code(socket_handle_for_commands, "211", list) == RETURN_ERROR &&
	   _read_lines_from_server_until_code(socket_handle_for_commands, "213", list) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_FTP_FTPGETLISTDIDNOTRECEIVEEXPECTED211213RETURNCODE ) );
		goto _RETURN_ERROR;
	}

	// Exit
	goto _RETURN_OK;
	_RETURN_OK:
		free(command);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(command != NULL)
		{
			free(command);
	    }
		return RETURN_ERROR;
}

int ftp_get_list_from_data(
	IN  char  *host_name,
	IN  int    socket_handle_for_commands,
	IN  char  *directory_name,
	OUT char **list)
{
	int   port                   = 0;
	int   socket_handle_for_data = -1;
	char *command                = NULL;
	char *list_temp              = NULL;
	char *list_temp_for_realloc  = NULL;
	int   list_size              = 0;
	int   retries                = 0;

	// Allocate memory
	command    = malloc(GLOBALS_MAX_LINE_LENGTH * sizeof(char));
	if(command == NULL)
	{
		log_print_error( GetString( MSG_FTP_FTPGETLISTFROMDATACOULDNOTALLOCATECOMMAND ) );
		goto _RETURN_ERROR;
	}

	// Get the data port number
	_get_data_port(socket_handle_for_commands, &port);

	// Connect to the data port
	if(_connect_to_server_for_data(host_name, port, &socket_handle_for_data) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_FTP_FTPGETLISTFROMDATACOULDNOTCONNECTTOSERVER ) , host_name);
		goto _RETURN_ERROR;
	}

	// Issue the commands to list the directory of interest
	string_snprintf(command, GLOBALS_MAX_LINE_LENGTH, "CWD %s", directory_name);
	if(_execute_command(socket_handle_for_commands, command) < 0)
	{
		log_print_error( GetString( MSG_FTP_FTPGETLISTFROMDATACOULDNOTEXECUTECOMMAND ) , command);
		goto _RETURN_ERROR;
	}
	if(_read_lines_from_server_until_code(socket_handle_for_commands, "250", &list_temp) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_FTP_FTPGETLISTFROMDATADIDNOTRECEIVEEXPECTED250RETURNCODE ) );
		goto _RETURN_ERROR;
	}
	free(list_temp);
	list_temp = NULL;

	string_snprintf(command, GLOBALS_MAX_LINE_LENGTH, "LIST -al");
	if(_execute_command(socket_handle_for_commands, command) < 0)
	{
		log_print_error( GetString( MSG_FTP_FTPGETLISTFROMDATACOULDNOTEXECUTECOMMAND ) , command);
	    goto _RETURN_ERROR;
	}
	if(_read_lines_from_server_until_code(socket_handle_for_commands, "150", &list_temp) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_FTP_FTPGETLISTFROMDATADIDNOTRECEIVEEXPECTED150RETURNCODE ) );
		goto _RETURN_ERROR;
	}
	free(list_temp);
	list_temp = NULL;

	// Transfer the list
	// Tygre 2015/08/01: Control vs. Data
	// I forgot that, in the data connection, I must read the data
	// until there is nothing more to read, not according to the
	// control rules: that the data would be enclosed by lines of
	// the form "NNN-" and "NNN ", where NNN is some FTP return code.
	// So, I cannot do:
	//	_read_lines_from_server(socket_handle_for_data, &list_temp);
	// but should do:
	if(_read_bytes_from_server(socket_handle_for_data, &list_temp, &list_size) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_FTP_FTPGETLISTFROMDATADIDNOTRECEIVEEXPECTEDDATA ) );
		goto _RETURN_ERROR;
	}

	// Tygre 2016/10/01: NULL-termination
	// I increase the list size by one to
	// terminate it with a NULL character.
	list_size++;
	list_temp_for_realloc    = realloc(list_temp, list_size);
	if(list_temp_for_realloc == NULL)
	{
		log_print_error( GetString( MSG_FTP_FTPGETLISTFROMDATACOULDNOTREALLOCATEMEMORYFORTHELIST ) );
		goto _RETURN_ERROR;
	}
	list_temp = list_temp_for_realloc;
	list_temp[list_size - 1] = '\0';

	string_remove_all_chars(list, list_temp, '\r');

	goto _RETURN_OK;
	_RETURN_OK:
		free(list_temp);
		free(command);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(list_temp != NULL)
		{
			free(list_temp);
		}
		if(command != NULL)
		{
			free(command);
	    }
		return RETURN_ERROR;
}

int ftp_check_file_exists(
	IN  char *host_name,
	IN  int   socket_handle_for_commands,
	IN  char *file_name,
	OUT BOOL *file_exists)
{
	int   port                   = 0;
	int   socket_handle_for_data = -1;
	char *command                = NULL;
	char *file_temp              = NULL;
	int   retries                = 0;

	// Allocate memory
	command    = malloc(GLOBALS_MAX_LINE_LENGTH * sizeof(char));
	if(command == NULL)
	{
		log_print_error( GetString( MSG_FTP_FTPCHECKFILEEXISTSCOULDNOTALLOCATECOMMAND ) );
		goto _RETURN_ERROR;
	}

	// Get the data port number
	_get_data_port(socket_handle_for_commands, &port);

	// Connect to the data port
	if(_connect_to_server_for_data(host_name, port, &socket_handle_for_data) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_FTP_FTPCHECKFILEEXISTSCOULDNOTCONNECTTOSERVER ) , host_name);
		goto _RETURN_ERROR;
	}

	// Issue the commands to get the file last-modified time.
	string_snprintf(command, GLOBALS_MAX_LINE_LENGTH, "MDTM %s", file_name);
	if(_execute_command(socket_handle_for_commands, command) < 0)
	{
		log_print_error( GetString( MSG_FTP_FTPCHECKFILEEXISTSCOULDNOTEXECUTECOMMAND ) , command);
	    goto _RETURN_ERROR;
	}
	if(_read_lines_from_server_until_code(socket_handle_for_commands, "213", &file_temp) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_FTP_FTPCHECKFILEEXISTSDIDNOTRECEIVEEXPECTED213RETURNCODE ) );
		goto _RETURN_ERROR;
	}

	// Exit
	goto _RETURN_OK;
	_RETURN_OK:
		*file_exists = TRUE;
		free(file_temp);
		free(command);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		*file_exists = FALSE;
		if(file_temp != NULL)
		{
			free(file_temp);
		}
		if(command != NULL)
		{
			free(command);
	    }
		return RETURN_ERROR;
}

int ftp_get_file_from_data(
	IN  char  *host_name,
	IN  int    socket_handle_for_commands,
	IN  char  *file_name,
	OUT char **file_bytes,
	OUT int   *file_length)
{
	int	  port                   = 0;
	int   socket_handle_for_data = -1;
	char *command                = NULL;
	char *file_temp              = NULL;
	int   retries                = 0;

	// Allocate memory
	command    = malloc(GLOBALS_MAX_LINE_LENGTH * sizeof(char));
	if(command == NULL)
	{
		log_print_error( GetString( MSG_FTP_FTPGETFILEFROMDATACOULDNOTALLOCATECOMMAND ) );
		goto _RETURN_ERROR;
	}

	// Get the data port number
	_get_data_port(socket_handle_for_commands, &port);

	// Connect to the data port
	if(_connect_to_server_for_data(host_name, port, &socket_handle_for_data) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_FTP_FTPGETFILEFROMDATACOULDNOTCONNECTTOSERVER ) , host_name);
		goto _RETURN_ERROR;
	}

	// Issue the commands to get the file of interest
	string_snprintf(command, GLOBALS_MAX_LINE_LENGTH, "TYPE I");
	if(_execute_command(socket_handle_for_commands, command) < 0)
	{
		log_print_error( GetString( MSG_FTP_FTPGETFILEFROMDATACOULDNOTEXECUTECOMMAND ) , command);
		goto _RETURN_ERROR;
	}
	if(_read_lines_from_server_until_code(socket_handle_for_commands, "200", &file_temp) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_FTP_FTPGETFILEFROMDATADIDNOTRECEIVEEXPECTED200RETURNCODE ) );
		goto _RETURN_ERROR;
	}
	free(file_temp);
	file_temp = NULL;

	string_snprintf(command, GLOBALS_MAX_LINE_LENGTH, "RETR %s", file_name);
	if(_execute_command(socket_handle_for_commands, command) < 0)
	{
		log_print_error( GetString( MSG_FTP_FTPGETFILEFROMDATACOULDNOTEXECUTECOMMAND ) , command);
	    goto _RETURN_ERROR;
	}
	if(_read_lines_from_server_until_code(socket_handle_for_commands, "150", &file_temp) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_FTP_FTPGETFILEFROMDATADIDNOTRECEIVEEXPECTED150RETURNCODE ) );
		goto _RETURN_ERROR;
	}

	// Transfer the file
	if(_read_bytes_from_server(socket_handle_for_data, file_bytes, file_length) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_FTP_FTPGETFILEFROMDATADIDNOTRECEIVEEXPECTEDDATA ) );
		goto _RETURN_ERROR;
	}

	// Exit
	goto _RETURN_OK;
	_RETURN_OK:
		free(file_temp);
		free(command);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(file_temp != NULL)
		{
			free(file_temp);
		}
		if(command != NULL)
		{
			free(command);
	    }
		return RETURN_ERROR;
}

static int _connect_to_server_for_commands(
	IN  char *host_name,
	IN  int   port,
	OUT int  *socket_handle_for_commands)
{
	struct hostent     *hPtr    = NULL;
	char               *command = NULL;
	char               *line    = NULL;
	char               *buffer  = NULL;
	char               *temp    = NULL;

	// Allocate memory
	line    = malloc(GLOBALS_MAX_LINE_LENGTH);
	if(line == NULL)
	{
		log_print_error( GetString( MSG_FTP_CONNECTTOSERVERFORCOMMANDSCOULDNOTALLOCATELINE ) );
		goto _RETURN_ERROR;
	}
	line[0] = '\0';

	command    = malloc(GLOBALS_MAX_LINE_LENGTH * sizeof(char));
	if(command == NULL)
	{
		log_print_error( GetString( MSG_FTP_CONNECTTOSERVERFORCOMMANDSCOULDNOTALLOCATECOMMAND ) );
		goto _RETURN_ERROR;
	}

	// Connect to server
	if(_connect_to_server(host_name, port, socket_handle_for_commands) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_FTP_CONNECTTOSERVERFORCOMMANDSCOULDNOTCONNECTTOSERVER ) , host_name);
		goto _RETURN_ERROR;
	}

	// Wait for service ready
	if(recv(*socket_handle_for_commands, line, GLOBALS_MAX_LINE_LENGTH, 0) < 0)
	{
		log_print_error( GetString( MSG_FTP_CONNECTTOSERVERFORCOMMANDSCOULDNOTRECEIVEANYTHINGFROMSE ) );
		goto _RETURN_ERROR;
	}

	// Some debug information
	string_remove_first_char(&temp, line, '\n');
	free(temp);

	// Send login
	string_snprintf(command, GLOBALS_MAX_LINE_LENGTH, "USER anonymous");
	if(_execute_command(*socket_handle_for_commands, command) < 0 ||
	   _read_lines_from_server(*socket_handle_for_commands, &buffer) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_FTP_CONNECTTOSERVERFORCOMMANDSCOULDNOTEXECUTECOMMAND ) , command);
	    goto _RETURN_ERROR;
	}
	free(buffer);
	buffer = NULL;

	// Send password
	string_snprintf(command, GLOBALS_MAX_LINE_LENGTH, "PASS anonymous@anonymous.net");
	if(_execute_command(*socket_handle_for_commands, command) < 0 ||
	   _read_lines_from_server(*socket_handle_for_commands, &buffer) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_FTP_CONNECTTOSERVERFORCOMMANDSCOULDNOTEXECUTECOMMAND ) , command);
	    goto _RETURN_ERROR;
	}
	free(buffer);
	buffer = NULL;

	// Send commands
	string_snprintf(command, GLOBALS_MAX_LINE_LENGTH, "SYST");
	if(_execute_command(*socket_handle_for_commands, command) < 0 ||
	   _read_lines_from_server(*socket_handle_for_commands, &buffer) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_FTP_CONNECTTOSERVERFORCOMMANDSCOULDNOTEXECUTECOMMAND ) , command);
	    goto _RETURN_ERROR;
	}
	free(buffer);
	buffer = NULL;

	string_snprintf(command, GLOBALS_MAX_LINE_LENGTH, "PWD");
	if(_execute_command(*socket_handle_for_commands, command) < 0)
	{
		log_print_error( GetString( MSG_FTP_CONNECTTOSERVERFORCOMMANDSCOULDNOTEXECUTECOMMAND ) , command);
	    goto _RETURN_ERROR;
	}
	if(_read_lines_from_server_until_code(*socket_handle_for_commands, "257", &buffer) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_FTP_CONNECTTOSERVERFORCOMMANDSDIDNOTRECEIVEEXPECTED257RETUR ) );
		goto _RETURN_ERROR;
	}

	goto _RETURN_OK;
	_RETURN_OK:
		free(buffer);
		free(line);
		free(command);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(buffer != NULL)
		{
			free(buffer);
		}
		if(line != NULL)
		{
			free(line);
	    }
		if(command != NULL)
		{
			free(command);
	    }
		return RETURN_ERROR;
}

static int _connect_to_server_for_data(
	IN  char *host_name,
	IN  int   port,
	OUT int  *socket_handle)
{
	if(_connect_to_server(host_name, port, socket_handle) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_FTP_CONNECTTOSERVERFORDATACOULDNOTCONNECTTOSERVER ) , host_name);
		return RETURN_ERROR;
	}

	return RETURN_OK;
}

static int _connect_to_server(
	IN  char *host,
	IN  int   port,
	OUT int  *socket_handle)
{
	struct sockaddr_in  remote = { 0 };
	struct timeval      tv     = { 0 };
	struct hostent     *he     = NULL;

	// Create socket
	if((*socket_handle = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		log_print_error( GetString( MSG_FTP_CONNECTTOSERVERCOULDNOTCREATESOCKET ) );
	    goto _RETURN_ERROR;
	}

	// Get host name
	he = gethostbyname((char *)host);
	if(he == NULL)
	{
		log_print_error( GetString( MSG_FTP_CONNECTTOSERVERCOULDNOTOBTAINIPADDRESSOF ) , host);
		goto _RETURN_ERROR;
	}

	// Zero out the remote address
	memset(&remote, 0, sizeof(remote));

	// Set up remote address for connect
	// Tygre 22/02/05: Guru on 68000
	// The code used to be:
	// 	remote.sin_addr.s_addr = inet_addr(inet_ntoa(*((struct in_addr *)he->h_addr_list[0])));
	// which is really:
	//	had = (struct in_addr *)he->h_addr;
	//	ina = *(had); // Guru 8000 0003
	//	tmp = inet_ntoa(ina);
	//	remote.sin_addr.s_addr = inet_addr(tmp);
	// but ina = *(had) would Guru 8000 0003 on a 68000.
	// I replaced it by the simpler code:
	memcpy(&remote.sin_addr, he->h_addr, he->h_length);
	remote.sin_family = AF_INET;
	remote.sin_port   = htons(port);
	
	if(connect(*socket_handle, (struct sockaddr *)&remote, sizeof(struct sockaddr)) < 0)
	{
		log_print_error( GetString( MSG_FTP_CONNECTTOSERVERCOULDNOTCONNECTTOSERVER ) , host);
		goto _RETURN_ERROR;
	}

	// Set a timeout
	tv.tv_secs  = GLOBALS_MAX_WAIT_IN_SECONDS;
	tv.tv_micro = 0;

	if(setsockopt(*socket_handle, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv))) {
		log_print_warning("_connect_to_server(), could not set timeout (send)\n");
		// Not so important?
		//	goto _RETURN_ERROR;
	}
	if(setsockopt(*socket_handle, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv))) {
		log_print_warning("_connect_to_server(), could not set timeout (recv)\n");
		// Not so important?
		//	goto _RETURN_ERROR;
	}

	goto _RETURN_OK;
	_RETURN_OK:
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		return RETURN_ERROR;
}

static int _get_data_port(
	IN  int  socket_handle_for_commands,
	OUT int *port)
{
	char *command = NULL;
	char *buffer  = NULL;
	char *token   = NULL;
	int   i       = 0;
	int	  p1      = 0;
	int   p2      = 0;

	// Allocate memory for the commands
	command    = malloc(GLOBALS_MAX_LINE_LENGTH * sizeof(char));
	if(command == NULL)
	{
		log_print_error( GetString( MSG_FTP_GETDATAPORTCOULDNOTALLOCATECOMMAND ) );
		goto _RETURN_ERROR;
	}

	string_snprintf(command, GLOBALS_MAX_LINE_LENGTH, "PASV");
	if(_execute_command(socket_handle_for_commands, command) < 0)
	{
		log_print_error( GetString( MSG_FTP_GETDATAPORTCOULDNOTEXECUTECOMMAND ) , command);
	    goto _RETURN_ERROR;
	}
	if(_read_lines_from_server_until_code(socket_handle_for_commands, "227", &buffer) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_FTP_CONNECTTOSERVERFORCOMMANDSDIDNOTRECEIVEEXPECTED257RETUR ) );
		goto _RETURN_ERROR;
	}

	// Parse the port returned by the server for data
	token = strtok(buffer, "(,).");
	while(token != NULL)
	{
		i++;
		if(i == 6)
		{
			p1 = atoi(token); /* Flawfinder: ignore */
		}
		if(i == 7)
		{
			p2 = atoi(token); /* Flawfinder: ignore */
		}
		token = strtok(NULL, "(,).");
	}

	*port = 256 * p1 + p2;

	goto _RETURN_OK;
	_RETURN_OK:
		free(buffer);
		free(command);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(buffer != NULL)
		{
			free(buffer);
		}
		if(command != NULL)
		{
			free(command);
	    }
		return RETURN_ERROR;
}

static int _execute_command(
	IN int   socket_handle,
	IN char *command)
{
	// Send command
	if((send(socket_handle, command, strlen(command), 0)) < 0)
	{
		log_print_error( GetString( MSG_FTP_EXECUTECOMMANDCOULDNOTSENDCOMMAND ) );
		return RETURN_ERROR;
	}

	// Tygre 20/05/18: No more answer?
	// Aminet FTP server changed behaviour and would
	// not reply to any command terminated by only "\n"
	// until Matt pointed out that the official ending
	// is "\r\n"! Thanks Matt!

	// Send ending
	if((send(socket_handle, "\r\n", 2, 0)) < 0)
	{
		log_print_error(FALSE, "_execute_command(), could not send ending");
		return RETURN_ERROR;
	}

	return RETURN_OK;
}

static int _read_lines_from_server_until_code(
	IN  int    socket_handle,
	IN  char  *expected_code,
	OUT char **lines)
{
	char *file_temp = NULL;
	int   retries   = 0;
	int   length    = 0;

	// Tygre 2015/06/13: Buffering...
	// It is possible that the command response buffer still
	// holds previous messages that were not read, so I just
	// read and discard them until the lines start with the
	// expected code, for example 200: "Command okay".
	do
	{
		if(file_temp != NULL)
		{
			free(file_temp);
			file_temp = NULL;
		}
		_read_lines_from_server(socket_handle, &file_temp);
		retries++;
	}
	while(!string_start_with(file_temp, expected_code)
		  && !string_start_with(file_temp, "4") // Error like 421 (service not available)
		  && !string_start_with(file_temp, "5") // Error like 550 (no such file or directory)
		  && retries < GLOBALS_MAX_NUMBER_OF_RETRIES);

	if(string_start_with(file_temp, "4") ||
	   string_start_with(file_temp, "5") ||
	   retries == GLOBALS_MAX_NUMBER_OF_RETRIES)
	{
		log_print_error( GetString( MSG_FTP_READLINESFROMSERVERUNTILCODESERVERDIDNOTRESPONDWITH ) , expected_code, file_temp);
		goto _RETURN_ERROR;
	}

	length    = strlen(file_temp);
	*lines    = malloc((length + 1) * sizeof(char));
	if(*lines == NULL)
	{
		log_print_error( GetString( MSG_FTP_READLINESFROMSERVERUNTILCODECOULDNOTALLOCATELINES ) );
		goto _RETURN_ERROR;
	}
	strcpy(*lines, file_temp);

	goto _RETURN_OK;
	_RETURN_OK:
		free(file_temp);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(file_temp != NULL)
		{
			free(file_temp);
		}
		return RETURN_ERROR;
}

static int _read_lines_from_server(
	IN  int    socket_handle,
	OUT char **lines)
{
	int   has_next_line           = 0;
	char *line                    = NULL;
	char *lines_for_realloc       = NULL;
	int   number_of_chars         = 0;
	int   maximum_number_of_chars = 0;

	maximum_number_of_chars = GLOBALS_MAX_LINE_LENGTH;
	*lines    = malloc(maximum_number_of_chars);
	if(*lines == NULL)
	{
		log_print_error( GetString( MSG_FTP_READLINESFROMSERVERCOULDNOTALLOCATELINES ) );
		return RETURN_ERROR;
	}
	*lines[0] = '\0';

	do
	{
		if(_continue_long_operation_fp != NULL && !_continue_long_operation_fp())
		{
			// Tygre 2020/08/02: Interrupt
			// If the callback function returns false,
			// then the caller code decided to stop
			// this long-running operation.
			log_print_warning("_read_lines_from_server(), receiving data interrupted by user\n");
			goto _RETURN_ERROR;
		}

		if(line != NULL)
		{
			free(line);
			line = NULL;
		}
		if(_read_line_from_server(socket_handle, &line) == RETURN_ERROR)
		{
			// Tygre 2015/05/23: Not so bad?
			// Maybe it is not so bad if the received data is "wrong",
			// we can just stop reading and exit gracefully :-)
			break;
		}
		
		if(line[3] == '-')
		{
			has_next_line = 1;
		}
		else
		{
			has_next_line = 0;
		}
		number_of_chars += strlen(line) + 1;
		if(number_of_chars >= maximum_number_of_chars)
		{
	        maximum_number_of_chars += GLOBALS_MAX_LINE_LENGTH;
			lines_for_realloc = realloc(*lines, maximum_number_of_chars);
			if(lines_for_realloc == NULL)
			{
				log_print_error( GetString( MSG_FTP_READLINESFROMSERVERCOULDNOTREALLOCATELINES ) );
		        goto _RETURN_ERROR;
			}
			*lines = lines_for_realloc;
		}
		strncat(*lines, line, maximum_number_of_chars - strlen(*lines) - 1);
		strncat(*lines, "\n", maximum_number_of_chars - strlen(*lines) - 1);
	}
	while(has_next_line);

	goto _RETURN_OK;
	_RETURN_OK:
		free(line);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(line != NULL)
		{
			free(line);
		}
		return RETURN_ERROR;
}

static int _read_line_from_server(
	IN  int    socket_handle,
	OUT char **line)
{
	char  c                     = '\0';
	int   size_of_received_data = 0;
	int   number_of_chars       = 0;
	char *line_temp             = NULL;

	// Allocate memory for the commands
	*line    = malloc(GLOBALS_MAX_LINE_LENGTH);
	if(*line == NULL)
	{
		log_print_error( GetString( MSG_FTP_READLINEFROMSERVERCOULDNOTALLOCATETEMPORARYLINE ) );
		goto _RETURN_ERROR;
	}
	line_temp = *line;

	while(TRUE)
	{
		if(_continue_long_operation_fp != NULL && !_continue_long_operation_fp())
		{
			// Tygre 2020/08/02: Interrupt
			// If the callback function returns false,
			// then the caller code decided to stop
			// this long-running operation.
			log_print_warning("_read_line_from_server(), receiving data interrupted by user\n");
			goto _RETURN_ERROR;
		}

		size_of_received_data = recv(socket_handle, &c, sizeof(char), 0);
		if(size_of_received_data != sizeof(char))
		{
			log_print_warning( GetString( MSG_FTP_READLINEFROMSERVERRECEIVEDONLYDATA ) , size_of_received_data);
			// Tygre 2015/05/23: Not so bad?
			// Maybe it is not so bad if the received data is "wrong",
			// we can just stop reading and exit gracefully :-)
			goto _RETURN_WARN;
		}

		if(_ftp_read_callback_fp)
		{
			_ftp_read_callback_fp("_read_line_from_server()", size_of_received_data);
		}

		if(c == '\r')
		{
			// Tygre 2015/05/24: Termination!
			// Some servers terminate a line with '\n'
			// while others do with "\r\n". So, in the
			// latter case, I also consume the '\n'.
			recv(socket_handle, &c, sizeof(char), 0);
			goto _RETURN_OK;
		}
		else if(c == '\n')
		{
			goto _RETURN_OK;
		}
		else
		{
			if(number_of_chars < GLOBALS_MAX_LINE_LENGTH - 1)
			{
				line_temp[number_of_chars] = c;
				number_of_chars++;
			}
			else
			{
				goto _RETURN_OK;
			}
		}
	}

	goto _RETURN_OK;
	_RETURN_OK:
		line_temp[number_of_chars] = '\0';
		return RETURN_OK;

	goto _RETURN_WARN;
	_RETURN_WARN:
		line_temp[number_of_chars] = '\0';
		return RETURN_WARN;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(line_temp != NULL)
		{
			line_temp[number_of_chars] = '\0';
		}
		// Tygre 2015/05/23: Not so bad?
		// Maybe it is not so bad if the received data is "wrong",
		// we can just stop reading and exit gracefully :-)
		//	perror("Error in _read_line_from_server(...)");
		return RETURN_ERROR;
}

static int  _read_bytes_from_server(
	IN  int    socket_handle,
	OUT char **bytes,
	OUT int   *bytes_length)
{
	char *received_data           = NULL;
	int   size_of_received_data   = 0;
	int   maximum_number_of_bytes = 0;
	int   number_of_bytes         = 0;
	char *bytes_temp              = NULL;
	char *bytes_temp_for_realloc  = NULL;

	// Allocate memory for the commands
	maximum_number_of_bytes = GLOBALS_MAX_BUFFER_LENGTH;
	bytes_temp    = malloc(maximum_number_of_bytes);
	if(bytes_temp == NULL)
	{
		log_print_error( GetString( MSG_FTP_READBYTESFROMSERVERCOULDNOTALLOCATETEMPORARYBYTES ) );
		goto _RETURN_ERROR;
	}

	received_data = malloc(GLOBALS_MAX_LINE_LENGTH * sizeof(char));
	if(received_data == NULL)
	{
		log_print_error( GetString( MSG_FTP_READBYTESFROMSERVERCOULDNOTALLOCATEBUFFER ) );
		goto _RETURN_ERROR;
	}

	// Tygre 2015/12/04: Beautification
	//	log_print_information("\n");
	// Not necessary anymore with the callback!

	while(TRUE)
	{
		if(_continue_long_operation_fp != NULL && !_continue_long_operation_fp())
		{
			// Tygre 2020/08/02: Interrupt
			// If the callback function returns false,
			// then the caller code decided to stop
			// this long-running operation.
			log_print_warning("_read_bytes_from_server(), receiving data interrupted by user\n");
			goto _RETURN_ERROR;
		}

		// Could be improved if I could know the size of the
		// file in advance from the listing of the FTP content.
		size_of_received_data = recv(socket_handle, received_data, GLOBALS_MAX_LINE_LENGTH, 0);
		if(size_of_received_data == -1)
		{
			log_print_error( GetString( MSG_FTP_READBYTESFROMSERVERRECEIVEDUNEXPECTEDDATA ) );
			goto _RETURN_ERROR;
		}
		else if(size_of_received_data == 0)
		{
			// Done reading the data
			*bytes    = malloc(number_of_bytes);
			if(*bytes == NULL)
			{
				log_print_error( GetString( MSG_FTP_READBYTESFROMSERVERCOULDNOTALLOCATEBYTES ) );
				goto _RETURN_ERROR;
			}
			memcpy(*bytes, bytes_temp, number_of_bytes);
			*bytes_length = number_of_bytes;
			break;
		}
		else
		{
			if(_ftp_read_callback_fp)
			{
				_ftp_read_callback_fp("_read_bytes_from_server()", size_of_received_data);
			}
			if(number_of_bytes + size_of_received_data * sizeof(char) > maximum_number_of_bytes)
			{
				maximum_number_of_bytes += GLOBALS_MAX_BUFFER_LENGTH * sizeof(char);
				bytes_temp_for_realloc = realloc(bytes_temp, maximum_number_of_bytes);
				if(bytes_temp_for_realloc == NULL)
				{
					log_print_error( GetString( MSG_FTP_READBYTESFROMSERVERCOULDNOTREALLOCATETEMPORARYBYTES ) );
					goto _RETURN_ERROR;
				}
				bytes_temp = bytes_temp_for_realloc;
			}
			memcpy(bytes_temp + (number_of_bytes * sizeof(char)), received_data, size_of_received_data * sizeof(char));
			number_of_bytes += size_of_received_data * sizeof(char);
		}
	}

	goto _RETURN_OK;
	_RETURN_OK:
		free(bytes_temp);
		free(received_data);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(bytes_temp != NULL)
		{
			free(bytes_temp);
		}
		if(received_data != NULL)
		{
			free(received_data);
		}
		return RETURN_ERROR;
}

