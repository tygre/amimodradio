/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



#ifndef GLOBALS_H
#define GLOBALS_H 1

#define GLOBALS_MAX_NUMBER_DIGITS_FOR_SIZE 10
#define GLOBALS_MAX_NUMBER_OF_RETRIES      10
#define GLOBALS_MAX_BUFFER_LENGTH          USHRT_MAX
#define GLOBALS_MAX_LINE_LENGTH            UCHAR_MAX
#define GLOBALS_MAX_WAIT_IN_SECONDS        10

#define GLOBALS_LOG_FILE_NAME              "AmiModRadio.log"
#define GLOBALS_LOG_SIZE_INCREMENTS        1024

#define GLOBALS_TASK_STACK_SIZE_STR        "65536"
#define GLOBALS_TASK_STACK_SIZE            65536

#define GLOBALS_SEPARATOR_PATHS            "/"
#define GLOBALS_SEPARATOR_PATHS_CHAR       '/'
#define GLOBALS_SEPARATOR_VOLUMES          ":"
#define GLOBALS_SEPARATOR_VOLUMES_CHAR     ':'
#define GLOBALS_SEPARATOR_SIZES            " "
#define GLOBALS_SEPARATOR_SIZES_CHAR       ' '

#endif
