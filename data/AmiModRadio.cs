## Version $VER: AmiModRadio.catalog 1.0 (27.12.2018)
## Languages english german fran�ais italiano
## Codeset english 0
## Codeset german 0
## Codeset fran�ais 0
## Codeset italiano 0
## SimpleCatConfig CharsPerLine 200
## Chunk italiano AUTH Translated by Samir Hawamdeh (http://www.betatesting.it/backforthefuture)
;
MSG_BLACKLISTS_INITBLACKLISTSFILESCANNOTOPENLISTOFBLACKLISTEDDIRECTORIES
init_blacklists_files(), cannot open list of blacklisted directories\n
init_blacklists_files(), Kann die Liste der gesperrten Verzeichnissen nicht �ffnen\n
init_blacklists_files(), n'a pu ouvrir la liste des dossiers sur liste noire\n
init_blacklists_files(), impossibile aprire la lista delle directory nella blacklist\n
;
MSG_BLACKLISTS_INITBLACKLISTSFILESCANNOTOPENLISTOFBLACKLISTEDFILES
init_blacklists_files(), cannot open list of blacklisted files\n
init_blacklists_files(), Kann die Liste der gesperrten Dateien nicht �ffnen\n
init_blacklists_files(), n'a pu ouvrir la liste des fichiers sur liste noire\n
init_blacklists_files(), impossibile aprire la lista dei file nella blacklist\n
;
MSG_BLACKLISTS_BLACKLISTSBLACKLISTDIRECTORYCOULDNOTOPENLISTOFBLACKLISTEDDI
blacklists_blacklist_directory(), could not open list of blacklisted directories\n
blacklists_blacklist_directory(), Konnte die Liste der gesperrten Verzeichnissen nicht �ffnen\n
blacklists_blacklist_directory(), n'a pu ouvrir la liste des dossiers sur liste noire\n
blacklists_blacklist_directory(), impossibile aprire la lista delle directory nella blacklist\n
;
MSG_BLACKLISTS_BLACKLISTSBLACKLISTFILECOULDNOTOPENLISTOFBLACKLISTEDFILES
blacklists_blacklist_file(), could not open list of blacklisted files\n
blacklists_blacklist_file(), Konnte die Liste der gesperrten Dateien nicht �ffnen\n
blacklists_blacklist_file(), n'a pu ouvrir la liste des fichiers sur liste noire\n
blacklists_blacklist_file(), impossibile aprire la lista dei file della blacklist\n
;
MSG_BLACKLISTS_ISBLACKLISTEDELEMENTNOBLACKLISTAVAILABLE
_is_blacklisted_element(), no blacklist available\n
_is_blacklisted_element(), Keine Sperrliste vorhanden\n
_is_blacklisted_element(), aucune liste noire disponible\n
_is_blacklisted_element(), nessuna blacklist disponibile\n
;
MSG_BLACKLISTS_ISBLACKLISTEDELEMENTCOULDNOTALLOCATEFILEPOINTER
_is_blacklisted_element(), could not allocate file pointer\n
_is_blacklisted_element(), Konnte den Dateizeiger nicht zuweisen\n
_is_blacklisted_element(), n'a pu allouer le pointeur de fichier\n
_is_blacklisted_element(), impossibile allocare il file puntatore\n
;
MSG_BLACKLISTS_LISTBLACKLISTEDELEMENTNOBLACKLISTAVAILABLE
_list_blacklisted_element(), no blacklist available\n
_list_blacklisted_element(), Keine Sperrliste vorhanden\n
_list_blacklisted_element(), aucune liste noire disponible\n
_list_blacklisted_element(), nessuna blacklist disponibile\n
;
MSG_BLACKLISTS_LISTBLACKLISTEDELEMENTCOULDNOTALLOCATEFILEPOINTER
_list_blacklisted_element(), could not allocate file pointer\n
_list_blacklisted_element(), Konnte den Dateizeiger nicht zuweisen\n
_list_blacklisted_element(), n'a pu allouer le pointeur de fichier\n
_list_blacklisted_element(), impossibile allocare il file puntatore\n
;
MSG_BLACKLISTS_LISTBLACKLISTEDELEMENTCOULDNOTALLOCATELIST
_list_blacklisted_element(), could not allocate list\n
_list_blacklisted_element(), Konnte die Liste nicht zuweisen\n
_list_blacklisted_element(), n'a pu allouer la liste\n
_list_blacklisted_element(), impossibile allocare la lista\n
;
MSG_BLACKLISTS_BLACKLISTELEMENTNOBLACKLISTAVAILABLE
_blacklist_element(), no blacklist available\n
_blacklist_element(), Keine Sperrliste vorhanden\n
_blacklist_element(), aucune liste noire disponible\n
_blacklist_element(), nessuna blacklist disponibile\n
;
MSG_BLACKLISTS_BLACKLISTELEMENTCOULDNOTALLOCATEFILEPOINTER
_blacklist_element(), could not allocate file pointer\n
_blacklist_element(), Konnte den Dateizeiger nicht zuweisen\n
_blacklist_element(), n'a pu allouer le pointeur de fichier\n
_blacklist_element(), impossibile allocare il file puntatore\n
;
MSG_CONTROLSCLI_CONTROLSCLICOULDNOTOPENINTUITIONLIBRARYV36
controls_cli(), could not open intuition.library v36+\n
controls_cli(), Konnte die intuition.library v36+ nicht �ffnen\n
controls_cli(), n'a pu ouvrir intuition.library v36+\n
controls_cli(), impossibile aprire la libreria intuition.library v36+\n
;
MSG_CONTROLSCLI_CONTROLSCLICOULDNOTGRABCONSOLE
controls_cli(), could not grab Console\n
controls_cli(), Kein Zugriff auf die Konsole\n
controls_cli(), n'a pu s'approprier la Console\n
controls_cli(), impossibile catturare la console\n
;
MSG_CONTROLSCLI_CONTROLSCLICOULDNOTINITIALISECONTROLS
controls_cli(), could not initialise controls\n
controls_cli(), Konnte die Kontrollen nicht initialisieren\n
controls_cli(), n'a pu initialiser les contr�les\n
controls_cli(), impossibile inizializzare i controlli\n
;
MSG_CONTROLSCLI_SOURCE
Source           : %s\n
Quelle           : %s\n
Source           : %s\n
Indirizzo fonte    : %s\n
;
MSG_CONTROLSCLI_ROOTDIRECTORY
Root directory   : %s\n
Hauptverzeichnis : %s\n
Dossier racine   : %s\n
Indice directory   : %s\n
;
MSG_CONTROLSCLI_CURRENTDIRECTORY
Current directory: %s\n
Aktuelles Verzeichnis: %s\n
Dossier courant  : %s\n
Directory corrente : %s\n
;
MSG_CONTROLSCLI_CURRENTFILE
Current file     : %s\n
Aktuelle Datei   : %s\n
Fichier courant  : %s\n
File corrente      : %s\n
;
MSG_CONTROLSCLI_CURRENTMODULES
Current modules  : %s\n
Aktuelle Module  : %s\n
Modules courants : %s\n
Moduli correnti    : %s\n
;
MSG_CONTROLSCLI_LASTMESSAGE
Last message     : %s\n
Letzte Meldung   : %s\n
Dernier message  : %s\n
Ultimo messsaggio  : %s\n
;
MSG_CONTROLSCLI_STATUS
Status           :\x20
Status           :\x20
Status           :\x20
Stato applicazione :\x20
;
MSG_CONTROLSCLI_NOMODULELOADED
No module loaded\n
Keine Module geladen\n
Aucune module charg�
Nessun modulo caricato\n
;
MSG_CONTROLSCLI_NOMODULEPLAYING
No module playing\n
Es wird kein Modul gespielt\n
Aucune module jou�\n
Nessun modulo in esecuzione\n
;
MSG_CONTROLSCLI_MODULEPLAYINGENJOY
Module playing Enjoy!\n
Modul l�uft. Viel Spa�!\n
Module va jouer Profitez-en bien !
Modulo in esecuzione... Buon ascolto!\n
;
MSG_CONTROLSCLI_0QUITAMIMODRADIO
0. Quit AmiModRadio\n
0. Beende AmiModRadio\n
0. Quitter AmiModRadio\n
0. Chiudi AmiModRadio\n
;
MSG_CONTROLSCLI_1ABOUTAMIMODRADIO
1. About AmiModRadio\n
1. �ber AmiModRadio\n
1. Au sujet d'AmiModRadio\n
1. Informazioni su AmiModRadio\n
;
MSG_CONTROLSCLI_2PLAYTHEMODULES
2. Play the modules\n
2. Module abspielen\n
2. Jouer les modules\n
2. Suona i moduli\n
;
MSG_CONTROLSCLI_3REPLAYTHEMODULES
3. Replay the modules\n
3. Module wiederholen\n
3. Rejouer les modules\n
3. Ripeti i moduli\n
;
MSG_CONTROLSCLI_4PAUSETHEMODULES
4. Pause the modules\n
4. Module pausieren\n
4. Interrompre les modules\n
4. Metti in pausa i moduli\n
;
MSG_CONTROLSCLI_5REMOVETHEMODULES
5. Remove the modules\n
5. Module l�schen\n
5. D�charger les modules\n
5. Rimuovi i moduli\n
;
MSG_CONTROLSCLI_6FETCHOTHERMODULES
6. Fetch other modules\n
6. Weitere Module abrufen\n
6. Chercher d'autres modules\n
6. Preleva altri moduli\n
;
MSG_CONTROLSCLI_7LISTBANNEDDIRECTORIES
7. List banned directories\n
7. Liste gesperrte Verzeichnisse auf\n
7. Liste des dossiers bannis\n
7. Mostra directory bannate\n
;
MSG_CONTROLSCLI_8BANTHISDIRECTORY
8. Ban this directory\n
8. Dieses Verzeichnis ausschlie�en\n
8. Bannir ce dossier\n
8. Banna questa directory\n
;
MSG_CONTROLSCLI_9LISTBANNEDFILES
9. List banned files\n
9. Liste ausgeschlossene Dateien auf\n
9. Liste des fichiers bannis\n
9. Mostra file bannati\n\n
;
MSG_CONTROLSCLI_ABANTHISFILE
a. Ban this file\n
a. Diese Datei ausschlie�en\n
a. Bannir ce fichier\n
a. Banna questo file\n
;
MSG_CONTROLSCLI_BSAVETHESEMODULES
b. Save these modules\n
b. Speichere diese Module\n
b. Sauvegarder ces modules\n
b. Salva questi moduli\n
;
MSG_CONTROLSCLI_CSETROOTDIRECTORYASCURRENTDIRECTORY
c. Set root directory as current directory\n
c. Setze das Hauptverzeichnis als aktuelles Verzeichnis\n
c. Choisir le dossier courant comme dossier racine\n
c. Imposta indice directory come directory corrente\n
;
MSG_CONTROLSCLI_DSETRESETROOTDIRECTORYMANUALLY
d. Set/Reset root directory manually\n
d. Setze/Wiederrufe Hauptverzeichnis manuell\n
d. Changer/Remettre � z�ro le dossier racine manuellement\n
d. Imposta/Resetta indice directory manualmente\n
;
MSG_CONTROLSCLI_ECHOOSESOURCE
e. Choose source\n
e. Quelle w�hlen\n
e. Choisir une source\n
e. Seleziona fonte\n
;
MSG_CONTROLSCLI_CONTROLSCLICOULDNOTCREATEMESSAGEPORT
controls_cli(), could not create message port\n
controls_cli(), Konnte Nachrichten-Port nicht erstellen\n
controls_cli(), n'a pu cr�er le port de message\n
controls_cli(), impossibile creare la porta messaggio\n
;
MSG_CONTROLSCLI_CONTROLSCLICOULDNOTCREATEIOREQUEST
controls_cli(), could not create I/O request\n
controls_cli(), Konnte I/O Anfrage nicht stellen\n
controls_cli(), n'a pu cr�er la requ�te E/S\n
controls_cli(), impossibile creare la richiesta I/O\n
;
MSG_CONTROLSCLI_CONTROLSCLICOULDNOTACCESSCONSOLEWINDOW
controls_cli(), could not access Console window\n
controls_cli(), Kein Zugriff auf das Konsole Fenster\n
controls_cli(), n'a pu acc�der � la fen�tre de la Console\n
controls_cli(), impossibile accedere alla finestra console\n
;
MSG_CONTROLSCLI_CONTROLSCLICOULDNOTACCESSCONSOLEERROR
controls_cli(), could not access Console (error %d)\n
controls_cli(), Kein Zugriff auf Konsole (Fehler %d)\n
controls_cli(), n'a pu acc�der � la Console (erreur %d)\n
controls_cli(), impossibile accedere alla console (errore %d)\n
;
MSG_CONTROLSCLI_ENTERNEWROOTDIRECTORYORENTERTORESETTHEROOTDIRECT
Enter new root directory or [ENTER] to reset the root directory:\x20
Gib neues Hauptverzeichnis an oder [ENTER] um das Hauptverzeichnis zur�ckzusetzen:\x20
Entrer un nouveau dossier racine ou [ENTR�E] pour le dossier racine par d�faut :\x20
Inserisci un nuovo indice della directory o premi [INVIO] per resettare l'indice della directory:\x20
;
MSG_CONTROLSCLI_SETCURRENTROOTDIRECTORYMANUALLYCOULDNOTSETCURREN
_set_current_root_directory_manually(), could not set current root directory\n
_set_current_root_directory_manually(), Konnte das aktuelle Hauptverzeichnis nicht setzen\n
_set_current_root_directory_manually(), n'a pu changer le dossier racine\n
_set_current_root_directory_manually(), impossibile impostare il corrente indice della directory\n
;
MSG_CONTROLSCLI_CURRENTROOTDIRECTORYCHANGED
Current root directory changed\n
Aktuelles Hauptverzeichnis ge�ndert\n
Dossier racine chang�\n
L'indice della directory corrente � stato modificato\n
;
MSG_CONTROLSCLI_CURRENTROOTDIRECTORYFORCEDTOMATCHSOURCEROOT
Current root directory forced to match source root\n
Aktuelles Hauptverzeichnis mit Quellverzeichnis abgeglichen\n
Dossier racine forc� d'�tre le dossier racine par d�faut\n
L'indice della directory corrente deve corrispondere all'indice originale\n
;
MSG_CONTROLSCLI_COULDNOTCHANGEDCURRENTROOTDIRECTORY
Could not changed current root directory!\n
Konnte das aktuelles Hauptverzeichnis nicht �ndern!\n
N'a pu changer le dossier racine !\n
Impossibile modificare l'indice della directory corrente!\n
;
MSG_CONTROLSCLI_LISTBLACKLISTEDDIRECTORIESCOULDNOTGETBLACKLISTED
_list_blacklisted_directories(), could not get blacklisted directories\n
_list_blacklisted_directories(), Konnte die gesperrten Verzeichnisse nicht laden\n
_list_blacklisted_directories(), n'a pu obtenir les dossiers sur liste noire\n
_list_blacklisted_directories(), impossibile ottenere la directory della blacklist\n
;
MSG_CONTROLSCLI_BLACKLISTEDDIRECTORIES
Blacklisted directories\n
Gesperrte Verzeichnisse\n
Dossiers bannis\n
Directory delle blacklist\n
;
MSG_CONTROLSCLI_BLACKLISTEDDIRECTORIES_UNDERLINE
-----------------------\n
-----------------------\n
---------------\n
-----------------------\n
;
MSG_CONTROLSCLI_PRESSENTERTOCONTINUE
Press [ENTER] to continue>\x20
Dr�cke [ENTER] um fortzufahren>\x20
Presser [ENTR�E] pour continuer>\x20
Premi [INVIO] per continuare >\x20
;
MSG_CONTROLSCLI_SHOWEDBLACKLISTEDDIRECTORIES
Showed blacklisted directories\n
Gesperrte Verzeichnisse angezeigt\n
A montr� la liste noire des dossiers bannis\n
Directory della blacklist visualizzate\n
;
MSG_CONTROLSCLI_COULDNOTSHOWBLACKLISTEDDIRECTORIES
Could not show blacklisted directories\n
Konnte gesperrte Verzeichnisse nicht angezeigen\n
N'a pu montrer la liste noire des dossiers bannis\n
Impossibile visualizzare le directory della blacklist\n
;
MSG_CONTROLSCLI_LISTBLACKLISTEDFILESCOULDNOTGETBLACKLISTEDFILES
_list_blacklisted_files(), could not get blacklisted files\n
_list_blacklisted_files(), Konnte gesperrte Dateien nicht laden\n
_list_blacklisted_files(), n'a pu obtenir la liste noire des fichiers\n
_list_blacklisted_files(), impossibile ottenere i file della blacklist\n
;
MSG_CONTROLSCLI_BLACKLISTEDFILES
Blacklisted files\n
Gesperrte Dateien\n
Fichiers bannis\n
File nella blacklist\n
;
MSG_CONTROLSCLI_BLACKLISTEDFILES_UNDERLINE
-----------------\n
-----------------\n
---------------\n
-----------------\n
;
MSG_CONTROLSCLI_SHOWEDBLACKLISTEDFILES
Showed blacklisted files\n
Gesperrte Dateien angezeigt\n
A montr� la liste noire des fichiers bannis\n
File della blacklist visualizzati\n
;
MSG_CONTROLSCLI_COULDNOTSHOWBLACKLISTEDFILES
Could not show blacklisted files\n
Konnte gesperrte Dateien nicht angezeigen\n
N'a pu montrer la liste noire des fichiers bannis\n
Impossibile visualizzare i file della blacklist\n
;
MSG_CONTROLSCLI_PLEASEENTERDESTINATIONDIRECTORYORENTERFORTHEDEFA
Please enter destination directory or [ENTER] for the default directory "%s":\x20
Bitte Zielverzeichnis angeben oder [ENTER] f�r das Standardverzeichnis "%s":\x20
Entrer un dossier de destination ou [ENTR�E] pour le dossier par d�faut "%s" :\x20
Per favore inserisci la directory di destinazione o premi [INVIO] per la directory predefinita "%s":\x20
;
MSG_CONTROLSCLI_SAVECURRENTMODULESCOULDNOTREALLOCATEDESTINATION
_save_current_modules(), could not reallocate destination\n
_save_current_modules(), Konnte Ziel nicht neu zuweisen\n
_save_current_modules(), n'a pu r�allouer la destination\n
_save_current_modules(), impossibile riallocare la destinazione\n
;
MSG_CONTROLSCLI_SAVECURRENTMODULESCOULDNOTALLOCATEMEMORYFORTEMPO
_save_current_modules(), could not allocate memory for temporary list of modules\n
_save_current_modules(), Konnte den Speicher nicht f�r die vorl�ufige Modulliste zuweisen\n
_save_current_modules(), n'a pu allouer la m�moire pour la liste temporaire des modules\n
_save_current_modules(), impossibile allocare la memoria per la lista dei moduli temporanei\n
;
MSG_CONTROLSCLI_SAVECURRENTMODULESCOULDNOTCOPY
_save_current_modules(), could not copy "%s"\n"
_save_current_modules(), Konnte nicht kopieren "%s"\n"
_save_current_modules(), n'a pu copier "%s"\n"
_save_current_modules(), impossibile copiare "%s"\n"
;
MSG_CONTROLSCLI_SAVEDCURRENTMODULES
Saved current modules\n
Aktuelle Module gespeichert\n
Modules courants sauvegard�s\n
Moduli correnti salvati\n
;
MSG_CONTROLSCLI_COULDNOTSAVECURRENTMODULES
Could not save current modules!\n
Konnte aktuelle Module nicht speichern\n
N'a pu sauvegarder les modules courants !\n
Impossibile salvare i moduli correnti!\n
;
MSG_CONTROLSCLI_CHOOSESOURCECOULDNOTFINDSOURCESNAMES
_choose_source(), could not find sources names\n
_choose_source(), Konnte Quellnamen nicht finden\n
_choose_source(), n'a pu trouver le noms des sources\n
_choose_source(), impossibile trovare i nomi delle fonti\n
;
MSG_CONTROLSCLI_CHOOSESOURCECOULDNOTFINDATLEASTONESOURCENAME
_choose_source(), could not find at least one source name\n
_choose_source(), Konnte nicht einen einzigen Quellnamen finden\n
_choose_source(), n'a pu trouver le nom d'une source\n
_choose_source(), impossibile trovare una qualsiasa fonte del nome\n
;
MSG_CONTROLSCLI_SOURCESNAMES
Sources Names\n
Quellennamen\n
Noms des sources\n
Nomi fonti\n
;
MSG_CONTROLSCLI_SOURCESNAMES_UNDERLINE
-------------\n
------------\n
----------------\n
-----------------\n
;
MSG_CONTROLSCLI_CHOOSEONESOURCEORENTERFORTHEDEFAULTSOURCE
Choose one source or [ENTER] for the default %s source>\x20
W�hle eine Quelle oder [ENTER] f�r das Standardverzeichnis %s>\x20
Choisir une source ou [ENTR�E] pour la source par d�faut %s>\x20
Seleziona una fonte o premi [INVIO] per la fonte predefinita %s >\x20
;
MSG_CONTROLSCLI_CHOOSESOURCEUNAVAILABLECHOICECN
_choose_source(), unavailable choice: '%c'\n
_choose_source(), Auswahl nicht verf�gbar: '%c'\n
_choose_source(), choix indisponible : '%c'\n
_choose_source(), scelta non disponibile: '%c'\n
;
MSG_CONTROLSCLI_CHOOSESOURCECOULDNOTSETCURRENTSOURCE
_choose_source(), could not set current source\n
_choose_source(), Konnte aktuelle Quelle nicht �bernehmen\n
_choose_source(), n'a pu changer la source courante\n
_choose_source(), impossibile impostare la fonte corrente\n
;
MSG_CONTROLSCLI_CURRENTSOURCECHANGED
Current source changed\n
Aktuelle Quelle ge�ndert\n
Source courante chang�e\n
Fonte corrente modificata\n
;
MSG_CONTROLSCLI_COULDNOTCHANGESOURCE
Could not change source!\n
Konnte die Quelle nicht �ndern!\n
N'a pu changer la source !\n
Impossibile modificare la fonte!\n
;
MSG_CONTROLSCOMMONS_CONTROLSINITCOULDNOTALLOCATEMEMORY
controls_init(), could not allocate memory\n
controls_init(), Konnte Speicher nicht zuweisen\n
controls_init(), n'a pu allouer la m�moire\n
controls_init(), impossibile allocare la memoria\n
;
MSG_CONTROLSCOMMONS_CONTROLSINITCOULDNOTINITIALISEPREFERENCES
controls_init(), could not initialise preferences\n
controls_init(), Konnte Einstellungen nicht laden\n
controls_init(), n'a pu initialiser les pr�f�rences\n
controls_init(), impossibile inizializzare le preferenze\n
;
MSG_CONTROLSCOMMONS_CONTROLSINITCOULDNOTFINDSOURCESNAMES
controls_init(), could not find sources names\n
controls_init(), Konnte Quellenverzeichnisse nicht finden\n
controls_init(), n'a pu trouver les noms des sources\n
controls_init(), impossibile trovare le fonti dei nomi\n
;
MSG_CONTROLSCOMMONS_CONTROLSINITCOULDNOTFINDANYSOURCENAME
controls_init(), could not find any source name\n
controls_init(), Konnte Quellenverzeichnis nicht finden\n
controls_init(), n'a pu trouver le nom d'une source\n
controls_init(), impossibile trovare un qualsiasi nome per la fonte\n
;
MSG_CONTROLSCOMMONS_CONTROLSINITCOULDNOTINITIALISEPLAYERLISTENER
controls_init(), could not initialise player listener\n
controls_init(), Konnte Abspieler nicht initialisieren\n
controls_init(), n'a pu initialiser le contr�leur du joueur\n
controls_init(), impossibile inizializzare un player per l'ascolto\n
;
MSG_CONTROLSCOMMONS_CONTROLSDISPLAYVERSIONCOULDNOTALLOCATEVERSIO
controls_display_version(), could not allocate version string\n
controls_display_version(), Konnte Versionskette nicht zuweisen\n
controls_display_version(), n'a pu allouer la cha�ne de version \n
controls_display_version(), impossibile allocare la stringa di versione\n
;
MSG_CONTROLSCOMMONS_FETCHINGMODULESTOPLAY
Fetching modules to play\n
Lade Module...\n
Cherche des modules � jouer\n
Prelevamento moduli da suonare\n
;
MSG_CONTROLSCOMMONS_MODULESPLAYING
Modules playing\n
Spiele Module\n
Modules jouent\n
Moduli in esecuzione\n
;
MSG_CONTROLSCOMMONS_COULDNOTREALLYFINDANYTHINGTOPLAY
Could not really find anything to play!\n
Konnte absolut nichts zum Abspielen finden!\n
N'a pu vraiment rien trouver � jouer !\n
Non � stato trovato alcun file da riprodurre!\n
;
MSG_CONTROLSCOMMONS_CONTROLSPLAYMODULESCOULDNOTFINDPLAYER
controls_play_modules(), could not find player\n
controls_play_modules(), Konnte den Abspieler nicht finden\n
controls_play_modules(), n'A pu trouver le joueur\n
controls_play_modules(), impossibile trovare il player\n
;
MSG_CONTROLSCOMMONS_COULDNOTPLAYMODULES
Could not play modules!\n
Konnte Module nicht abspielen!\n
N'a pu jouer les modules !\n
Impossibile riprodurre i moduli!\n
;
MSG_CONTROLSCOMMONS_CONTROLSPLAYGIVENMODULESCOULDNOTCONNECTTO
controls_play_given_modules(), could not connect to "%s"\n
controls_play_given_modules(), Habe keinen Zugriff auf "%s"\n
controls_play_given_modules(), n'a pu se connecter � "%s"\n
controls_play_given_modules(), impossibile connettersi a "%s"\n
;
MSG_CONTROLSCOMMONS_CONTROLSPLAYGIVENMODULESCOULDNOTALLOCATEDIR
controls_play_given_modules(), could not allocate memory for current directory\n
controls_play_given_modules(), Konnte keinen Speicher f�r das aktuelle Verzeichnis zuweisen\n
controls_play_given_modules(), n'a pu allouer la m�moire pour le dossier courant \n
controls_play_given_modules(), impossibile allocare la memoria per la directory corrente\n
;
MSG_CONTROLSCOMMONS_CONTROLSPLAYGIVENMODULESCOULDNOTALLOCATEFILE
controls_play_given_modules(), could not allocate memory for current file\n
controls_play_given_modules(), Konnte keinen Speicher f�r die aktuelle Datei zuweisen\n
controls_play_given_modules(), n'a pu allouer la m�moire pour le fichier courant\n
controls_play_given_modules(), impossibile allocare la memoria per il file corrente\n
;
MSG_CONTROLSCOMMONS_CONTROLSPLAYGIVENMODULESCOULDNOTALLOCATEADDR
controls_play_given_modules(), could not allocate memory for address\n
controls_play_given_modules(), Konnte keinen Speicher f�r Adresse zuweisen\n
controls_play_given_modules(), n'a pu allouer la m�moire pour l'adresse \n
controls_play_given_modules(), impossibile allocare la memoria per l'indirizzo\n
;
MSG_CONTROLSCOMMONS_CONTROLSPLAYGIVENMODULESCOULDNOTALLOCATETEMPDIR
controls_play_given_modules(), could not allocate memory for temporay directory\n
controls_play_given_modules(), Konnte keinen Speicher f�r das vorl�ufige Verzeichnis zuweisen\n
controls_play_given_modules(), n'a pu allouer la m�moire pour le dossier temporaire\n
controls_play_given_modules(), impossibile allocare la memoria per la directory temporanea\n
;
MSG_CONTROLSCOMMONS_DOWNLOADING
Downloading "%s"\n
Lade herunter "%s"\n
T�l�charge "%s"\n
Scaricamento di "%s"\n
;
MSG_CONTROLSCOMMONS_CONTROLSPLAYGIVENMODULESCOULDNOTDOWNLOAD
controls_play_given_modules(), could not download "%s"\n
controls_play_given_modules(), Konnte "%s" nicht herunterladen\n
controls_play_given_modules(), n'a pu t�l�charger "%s"\n
controls_play_given_modules(), impossibile scaricare "%s"\n
;
MSG_CONTROLSCOMMONS_OFLENGTH
(Of length %d)\n
(mit der Gr��e %d)\n
(de taille %d) \n
(di lunghezza %d)\n
;
MSG_CONTROLSCOMMONS_CONTROLSPLAYGIVENMODULESCOULDNOTEXTRACTFILES
controls_play_given_modules(), could not extract files from archive "%s"\n
controls_play_given_modules(), Konnte die Dateien nicht aus dem Archiv "%s" entpacken\n
controls_play_given_modules(), n'a pu extraire les fichiers de l'archive "%s"\n
controls_play_given_modules(), impossibile estrarre i file dall'archivio "%s"\n
;
MSG_CONTROLSCOMMONS_CONTROLSPLAYGIVENMODULESCOULDNOTALLOCATETEMPLIST
controls_play_given_modules(), could not allocate memory for temporay list\n
controls_play_given_modules(), Konnte den Speicher f�r die vorl�ufige Liste zuweisen\n
controls_play_given_modules(), n'a pu allouer la m�moire pour la liste temporaire\n
controls_play_given_modules(), impossibile allocare la memoria per la lista temporanea\n
;
MSG_CONTROLSCOMMONS_CONTROLSPLAYGIVENMODULESCOULDNOTCONNECTORSTA
controls_play_given_modules(), could not connect or start player\n
controls_play_given_modules(), Konnte den Abspieler nicht starten\n
controls_play_given_modules(), n'a pu se connecter ou d�marrer un joueur \n
controls_play_given_modules(), impossibile connettersi o avviare il player\n
;
MSG_CONTROLSCOMMONS_CONTROLSPLAYGIVENMODULESCOULDNOTFINDANYMODUL
controls_play_given_modules(), could not find any modules to play in "%s"\n
controls_play_given_modules(), Finde in "%s" nichts zum Abspielen\n
controls_play_given_modules(), n'a pu trouver aucun module � jouer dans "%s"\n
controls_play_given_modules(), non � stato trovato alcun modulo da suonare in "%s"\n
;
MSG_CONTROLSCOMMONS_MODULESSTARTEDPLAYING
Modules started playing\n
Module werden abgespielt\n
Modules commencent � jouer\n
Avvio riproduzione dei moduli\n
;
MSG_CONTROLSCOMMONS_SHOULDCONTINUELOOKINGFORMODULES
Should continue looking for modules!\n
Weiter nach Modulen suchen!\n
Doit continuer � chercher des modules !\n
Ricerca dei moduli in corso!\n
;
MSG_CONTROLSCOMMONS_CONTROLSREPLAYMODULESCOULDNOTFINDPLAYER
controls_replay_modules(), could not find player\n
controls_replay_modules(), Konnte den Abspieler nicht finden\n
controls_replay_modules(), n'a pu trouver un joueur\n
controls_replay_modules(), impossibile trovare il player\n
;
MSG_CONTROLSCOMMONS_MODULESREPLAYING
Modules replaying\n
Spiele Module erneut ab\n
Modules rejouent\n
Ripetizione dei moduli\n
;
MSG_CONTROLSCOMMONS_COULDNOTREPLAYMODULES
Could not replay modules!\n
Konnte Module nicht erneut abspielen!\n
N'a pu rejouer les modules !\n
Impossibile ripetere i moduli!\n
;
MSG_CONTROLSCOMMONS_CONTROLSPAUSEMODULESCOULDNOTFINDPLAYER
controls_pause_modules(), could not find player\n
controls_pause_modules(), Konnte den Abspieler nicht finden\n
controls_pause_modules(), n'a pu trouver un joueur\n
controls_pause_modules(), impossibile trovare il player\n
;
MSG_CONTROLSCOMMONS_MODULESPAUSED
Modules paused\n
Module pausiert\n
Modules en pause\n
Moduli in pausa\n
;
MSG_CONTROLSCOMMONS_COULDNOTPAUSEMODULES
Could not pause modules!\n
Konnte Module nicht unterbrechen!\n
N'a pu mettre en pause les modules !\n
Impossibile mettere in pausa i moduli!\n
;
MSG_CONTROLSCOMMONS_CONTROLSREMOVEMODULESCOULDNOTFINDPLAYER
controls_remove_modules(), could not find player\n
controls_remove_modules(), Konnte Abspieler nicht finden\n
controls_remove_modules(), n'a pu trouver un joueur\n
controls_remove_modules(), impossibile trovare il player\n
;
MSG_CONTROLSCOMMONS_MODULESREMOVED
Modules removed\n
Module entfernt\n
Modules d�charg�s\n
Moduli rimossi\n
;
MSG_CONTROLSCOMMONS_COULDNOTREMOVEMODULES
Could not remove modules!\n
Konnte Module nicht entfernen!\n
N'a pu d�charger les modules !\n
Impossibile rimuovere i moduli!\n
;
MSG_CONTROLSCOMMONS_CONTROLSBANCURRENTDIRECTORYCOULDNOTCONNECTTO
controls_ban_current_directory(), could not connect to blacklists\n
controls_ban_current_directory(), Konnte keine Verbindung zur Sperrliste aufbauen \n
controls_ban_current_directory(), n'a pu se connecter aux listes noires \n
controls_ban_current_directory(), impossibile connettersi alle blacklist\n
;
MSG_CONTROLSCOMMONS_BANNEDCURRENTDIRECTORY
Banned current directory\n
Aktuelles Verzeichnis ausgeschlossen\n
A banni le dossier courant\n
Directory corrente bannata\n
;
MSG_CONTROLSCOMMONS_COULDNOTBANCURRENTDIRECTORY
Could not ban current directory!\n
Konnte aktuelles Verzeichnis nicht ausschlie�en!\n
N'a pu bannir le dossier courant !\n
Impossibile bannare la directory corrente!\n
;
MSG_CONTROLSCOMMONS_CONTROLSBANCURRENTFILECOULDNOTCONNECTTOBLACK
controls_ban_current_file(), could not connect to blacklists\n
controls_ban_current_file(), Konnte keine Verbindung zur Sperrliste aufbauen\n
controls_ban_current_file(), n'a pu se connecter aux listes noires\n
controls_ban_current_file(), impossibile connettersi alle blacklist\n
;
MSG_CONTROLSCOMMONS_BANNEDCURRENTFILE
Banned current file\n
Aktuelle Datei ausgeschlossen\n
A banni le fichier courant\n
File corrente bannato\n
;
MSG_CONTROLSCOMMONS_COULDNOTBANCURRENTFILE
Could not ban current file!\n
Konnte aktuelle Datei nicht ausschlie�en!\n
N'a pu bannir le fichier courant !\n
Impossibile bannare il file corrente!\n
;
MSG_CONTROLSCOMMONS_CONTROLSSETLASTMESSAGECOULDNOTREALLOCATEMESS
controls_set_last_message(), could not reallocate message "%s"\n
controls_set_last_message(), Konnte Meldung nicht neu zuteilen "%s"\n
controls_set_last_message(), n'a pu r�allouer le message "%s"\n
controls_set_last_message(), impossibile riallocare il messaggio "%s"\n
;
MSG_CONTROLSCOMMONS_CONTROLSSETCURRENTSOURCENOTAVALIDINDEXOFSOUR
controls_set_current_source(), not a valid index of source name %d\n
controls_set_current_source(), Kein g�ltiger Index eines Quellnamens %d\n
controls_set_current_source(), pas un index valide pour un nom de source %d\n
controls_set_current_source(), non � un indice valido per il nome della fonte %d\n
;
MSG_CONTROLSCOMMONS_UNAVAILABLE
 (Unavailable)
 (nicht verf�gbar)
 (indisponible)
 (Non disponibile)
;
MSG_CONTROLSCOMMONS_CONTROLSSETCURRENTROOTDIRECTORYFROMSTRINGCOU
controls_set_current_root_directory_from_string(), could not allocate memory\n
controls_set_current_root_directory_from_string(), Konnte Speicher nicht zuweisen\n
controls_set_current_root_directory_from_string(), n'a pu allouer la m�moire\n
controls_set_current_root_directory_from_string(), impossibile allocare la memoria\n
;
MSG_CONTROLSCOMMONS_CONTROLSGETBLACKLISTEDDIRECTORIESLISTCOULDNO
controls_get_blacklisted_directories_list(), could not connect to blacklists\n
controls_get_blacklisted_directories_list(), Konnte nicht zur Sperrliste verbinden\n
controls_get_blacklisted_directories_list(), n'a pu se connecter aux listes noires\n
controls_get_blacklisted_directories_list(), impossibile connettersi alle blacklist\n
;
MSG_CONTROLSCOMMONS_LISTEDBLACKLISTEDDIRECTORIES
Listed blacklisted directories\n
Sperrverzeichnisse aufgelistet\n
A list� les dossiers bannis\n
Directory della blacklist visualizzate\n
;
MSG_CONTROLSCOMMONS_COULDNOTLISTBLACKLISTEDDIRECTORIES
Could not list blacklisted directories!\n
Konnte Sperrverzeichnisse nicht auflisten!\n
N'a pu lister les dossiers bannis\n
Impossibile visualizzare le directory della blacklist!\n
;
MSG_CONTROLSCOMMONS_CONTROLSGETBLACKLISTEDFILESLISTCOULDNOTCONNE
controls_get_blacklisted_files_list(), could not connect to blacklists\n
controls_get_blacklisted_files_list(), Konnte keine Verbindung zur Sperrlisten aufbauen\n
controls_get_blacklisted_files_list(), n'a pu se connecter aux listes noires\n
controls_get_blacklisted_files_list(), impossibile connettersi alle blacklist\n
;
MSG_CONTROLSCOMMONS_LISTEDBLACKLISTEDFILES
Listed blacklisted files\n
Gesperrte Dateien aufgelistet\n
A list� les fichiers bannis\n
File della blacklist visualizzati\n
;
MSG_CONTROLSCOMMONS_COULDNOTLISTBLACKLISTEDFILES
Could not list blacklisted files!\n
Konnte gesperrte Dateien nicht auflisten!\n
N'a pu lister les fichiers bannis\n
Impossibile visualizzare i file della blacklist!\n
;
MSG_CONTROLSCOMMONS_CLEANUPFILESCOULDNOTALLOCATEFILENAME
_cleanup_files(), could not allocate memory for file name\n
_cleanup_files(), Konnte der Datei keinen Speicher zuweisen\n
_cleanup_files(), n'a pu allouer la m�moire pour le nom de fichier\n
_cleanup_files(), impossibile allocare la memoria per il nome del file\n
;
MSG_CONTROLSCOMMONS_CLEANUPFILESCOULDNOTREMOVE
_cleanup_files(), could not remove "%s"\n
_cleanup_files(), Konnte "%s" nicht entfernen\n
_cleanup_files(), n'a pu effacer "%s"\n
_cleanup_files(), impossibile rimuovere "%s"\n
;
MSG_CONTROLSCOMMONS_FETCHMODULESCOULDNOTCONNECTTO
_fetch_modules(), could not connect to "%s"\n
_fetch_modules(), Konnte nicht mit "%s" verbinden\n
_fetch_modules(), n'a pu se connecter � "%s"\n
_fetch_modules(), impossibile connettersi a "%s"\n
;
MSG_CONTROLSCOMMONS_FETCHMODULESCOULDNOTCONNECTTOBLACKLISTS
_fetch_modules(), could not connect to blacklists\n
_fetch_modules(), Keine Verbindung zur Sperrliste\n
_fetch_modules(), n'a pu se connecter aux listes noires\n
_fetch_modules(), impossibile connettersi alle blacklist\n
;
MSG_CONTROLSCOMMONS_FETCHMODULESCOULDNOTCONNECTTORATINGS
_fetch_modules(), could not connect to ratings\n
_fetch_modules(), Konnte die Bewertungen nicht aufrufen\n
_fetch_modules(), n'a pu se connecter aux �valuations\n
_fetch_modules(), impossibile connettersi per votare\n
;
MSG_CONTROLSCOMMONS_FETCHMODULESCOULDNOTCONNECTTOPLAYER
_fetch_modules(), could not connect to player\n
_fetch_modules(), Konnte den Abspieler nicht aufrufen\n
_fetch_modules(), n'a pu se connecter au joueur\n
_fetch_modules(), impossibile connettersi al player\n
;
MSG_CONTROLSCOMMONS_FETCHMODULESFOUNDBLACKLISTEDROOTDIRECTORY
_fetch_modules(), found blacklisted root directory "%s"\n
_fetch_modules(), Gesperrtes Hauptverzechnis "%s"\n
_fetch_modules(), a trouv� le dossier racine banni "%s"\n
_fetch_modules(), trovato indice directory "%s" della blacklist\n
;
MSG_CONTROLSCOMMONS_FETCHMODULESCOULDNOTALLOCATEDIRECTORY
_fetch_modules(), could not allocate directory\n
_fetch_modules(), Konnte Verzeichnis nicht zuweisen\n
_fetch_modules(), n'a pu allouer le dossier\n
_fetch_modules(), impossibile allocare la directory\n
;
MSG_CONTROLSCOMMONS_FETCHMODULESCOULDNOTLISTDIRECTORY
_fetch_modules(), could not list directory "%s"\n
_fetch_modules(), Konnte Verzeichnis "%s" nicht auflisten\n
_fetch_modules(), n'a pu lister le dossier "%s"\n
_fetch_modules(), impossibile visualizzare la directory "%s"\n
;
MSG_CONTROLSCOMMONS_FETCHMODULESCOULDNOTGETRANDOMMODULESARCHIVEF
_fetch_modules(), could not get random modules archive file\n
_fetch_modules(), Konnte Zufallsmodularchivdatei nichgt finden\n
_fetch_modules(), n'a pu obtenir al�atoirement une archive de modules\n
_fetch_modules(), impossibile ottenere dei moduli casuali dall'archivio dei file\n
;
MSG_CONTROLSCOMMONS_FETCHMODULESCOULDNOTFINDADEQUATEFILEAFTERRET
_fetch_modules(), could not find adequate file after %d retries\n
_fetch_modules(), Konnte die passende Datei nach %d Versuchen nicht finden\n
_fetch_modules(), n'a pu trouver un fichier ad�quat apr�s %d essais\n
_fetch_modules(), impossibile trovare un file adeguato dopo %d tentativi!\n
;
MSG_CONTROLSCOMMONS_FETCHMODULESCOULDNOTALLOCATETEMPDIR
_fetch_modules(), could not allocate memory for temporary directory\n
_fetch_modules(), Konnte dem tempor�ren Verzeichnis keinen Speicher zuweisen\n
_fetch_modules(), n'a pu allouer la m�moire pour le dossier temporaire\n
_fetch_modules(), impossibile allocare la memoria per la directory temporanea\n
;
MSG_CONTROLSCOMMONS_FETCHMODULESCOULDNOTALLOCATEADDR
_fetch_modules(), could not allocate memory for address\n
_fetch_modules(), Konnte der Adresse keinen Speicher zuweisen\n
_fetch_modules(), n'a pu allouer la m�moire pour l'adresse \n
_fetch_modules(), impossibile allocare la memoria per l'indirizzo\n
;
MSG_CONTROLSCOMMONS_STUMBLEDUPONABLACKLISTEDDIRECTORY
Stumbled upon a blacklisted directory "%s"\n
Bin �ber das gespertte Verzeichnis "%s" gestolpert\n
Est tomb� sur un dossier banni "%s"\n
Incorso nella directory "%s" della blacklist\n
;
MSG_CONTROLSCOMMONS_FETCHMODULESCOULDNOTLISTSUBDIRECTORY
_fetch_modules(), could not list sub-directory "%s"\n
_fetch_modules(), Konnte das Unterverzeichnis "%s" nicht auflisten\n
_fetch_modules(), n'a pu lister le sous-dossier "%s"\n
_fetch_modules(), impossibile visualizzare la sottodirectory "%s"\n
;
MSG_CONTROLSCOMMONS_FILETOOLARGEWRTPREFERENCES
_fetch_modules(), file too large wrt. user's preferences: "%s"\n
_fetch_modules(), Datei zu gro�. Pr�fe die Einstellungen: "%s"\n
_fetch_modules(), fichier trop gros par rapport aux pr�f�rences usagers: "%s"\n

;
MSG_CONTROLSCOMMONS_STUMBLEDUPONABLACKLISTEDFILE
Stumbled upon a blacklisted file "%s"\n
Bin �ber die gesperrte Date "%s" gestolpert\n
Est tomb� sur un fichier banni "%s"\n
Incorso nel file "%s" della blacklist\n
;
MSG_CONTROLSCOMMONS_STUMBLEDUPONAFILETHATISNEITHERANARCHIVENORAP
Stumbled upon a file that is neither an archive nor a playable file "%s"\n
Bin �ber eine Datei gestolpert, die weder ein Archiv, noch eine abspielbare Datei ist "%s"\n
Est tomb� sur un fichier qui est ni une archive ni un fichier jouable "%s"\n
Incorso in un file che non � n� un archivio n� un file riproducibile "%s"\n
;
MSG_CONTROLSCOMMONS_STUMBLEDUPONAFILETHATHASATOOLOWRATING
Stumbled upon a file that has a too low rating "%s"\n
Bin �ber eine Datei gestolpert, die zu niedrig bewertet wurde "%s"\n
Est tomb� sur un fichier qui a de mauvaises �valuations "%s"\n
Incorso in un file avente una valutazione troppo bassa "%s"\n
;
MSG_CONTROLSCOMMONS_STUMBLEDUPONAFILETHATHASALREADYBEENRATED
Stumbled upon a file that has already been rated "%s"\n
Bin �ber eine Datei gestolpert, die schon bewertet wurde "%s"\n
Est tomb� sur un fichier qui a d�j� �t� �valu� "%s"\n
Incorso in un file che � gi� stato valutato "%s"\n
;
MSG_CONTROLSCOMMONS_FETCHMODULESCOULDNOTDETERMINETHETYPEOFFILE
_fetch_modules(), could not determine the type of file "%s"\n
_fetch_modules(), Konnte den Dateityp von "%s" nicht bestimmen\n
_fetch_modules(), n'a pu d�terminer le type du fichier "%s"\n
_fetch_modules(), impossibile determinare il tipo di file "%s"\n
;
MSG_CONTROLSCOMMONS_MODULESFETCHED
Modules fetched\n
Module geladen\n
Modules trouv�s\n
Moduli prelevati\n
;
MSG_CONTROLSCOMMONS_COULDNOTFETCHANDORPLAYMODULES
Could not fetch and-or play modules!\n
Konnte Module nicht laden bzw. spielen!\n
N'a pu trouver ou jouer des modules !\n
Impossibile prelevare e/o suonare i moduli!\n
;
MSG_CONTROLSCOMMONS_SETCURRENTDIRECTORYTOROOTCOULDNOTALLOCATEDIR
_set_current_directory_to_root(), could not allocate memory\n
_set_current_directory_to_root(), Konnte Speicher nicht zuweisen\n
_set_current_directory_to_root(), n'a pu allouer la m�moire\n
_set_current_directory_to_root(), impossibile allocare la memoria\n
;
MSG_CONTROLSCOMMONS_COMPUTEFILEADDRESSCOULDNOTALLOCATEMEMORY
_compute_file_address(), could not allocate memory\n
_compute_file_address(), Konnte Speicher nicht zuweisen\n
_compute_file_address(), n'a pu allouer la m�moire\n
_compute_file_address(), impossibile allocare la memoria\n
;
MSG_CONTROLSMUI_CONTROLSCLICOULDNOTOPENINTUITIONLIBRARYV36
controls_mui(), could not open intuition.library v36+\n
controls_mui(), Konnte intuition.library v36+ nicht �ffnen\n
controls_mui(), n'a pu ouvrir intuition.library v36+\n
controls_cli(), impossibile aprire la libreria intuition.library v36+\n
;
MSG_CONTROLSMUI_CONTROLSMUICOULDNOTOPENGRAPHICSLIBRARYV0
controls_mui(), could not open graphics.library v0+\n
controls_mui(), Konnte graphics.library v0+ nicht �ffnen\n
controls_mui(), n'a pu ouvrir graphics.library v0+\n
controls_mui(), impossibile aprire la libreria graphics.library v0+\n
;
MSG_CONTROLSMUI_CONTROLSMUICOULDNOTOPENMUIMASTERLIBRARYV19
controls_mui(), could not open muimaster.library v19+\n
controls_mui(), Konnte muimaster.library v19+ nicht �ffnen\n
controls_mui(), n'a pu ouvrir muimaster.library v19+\n
controls_mui(), impossibile aprire la libreria muimaster.library v19+\n
;
MSG_CONTROLSMUI_CONTROLSMUICOULDNOTINITIALISECONTROLS
controls_mui(), could not initialise controls\n
controls_mui(), Konnte Steuerung nicht initialisieren\n
controls_mui(), n'a pu initialiser les contr�les\n
controls_mui(), impossibile inizializzare i controlli\n
;
MSG_CONTROLSMUI_CONTROLSMUICOULDNOTINITIALISEMUIUSERINTERFACE
controls_mui(), could not initialise MUI user-interface\n
controls_mui(), Konnte Benutzeroberfl�che von MUI nicht initialisieren\n
controls_mui(), n'a pu initialiser l'interface usager MUI\n
controls_mui(), impossibile inizializzare l'interfaccia utente di MUI\n
;
MSG_CONTROLSMUI_MUICONTROLSCHANGEDIRECTORYCOULDNOTSAVEROOTDIREC
mui_controls_change_directory(), could not save root directory\n
mui_controls_change_directory(), Konnte Hauptverzeichnis nicht speichern\n
mui_controls_change_directory(), n'a pu sauvegarder le dossier racine\n
mui_controls_change_directory(), impossibile salvare l'indice della directory\n
;
MSG_CONTROLSMUI_DIRECTORYCONTENT
Directory Content
Inhaltsverzeichnis
Contenu du dossier
Contenuto directory
;
MSG_CONTROLSMUI_BANNEDDIRECTORIES
Banned directories
Verzeichnisse gesperrt
Dossiers bannis
Directory bannate
;
MSG_CONTROLSMUI_FILECONTENT
File Content
Dateiinhalt
Contenu du fichier
Contenuto file
;
MSG_CONTROLSMUI_BANNEDFILES
Banned files
Dateien gesperrt
Fichiers bannis
File bannati
;
MSG_CONTROLSMUI_MUICONTROLSMODULESSAVECOULDNOTOPENASLLIBRARY
mui_controls_modules_save(), could not open asl.library\n
mui_controls_modules_save(), Konnte asl.library nicht �ffnen\n
mui_controls_modules_save(), n'a pu ouvrir asl.library\n
mui_controls_modules_save(), impossibile aprire la libreria asl.library\n
;
MSG_CONTROLSMUI_SAVE
Save
Speichern
Sauvegarder
Salva
;
MSG_CONTROLSMUI_SAVEDIRECTORY
Save Directory
Verzeichnis speichern
Dossier de sauvegarde
Salva directory
;
MSG_CONTROLSMUI_MUICONTROLSMODULESSAVECOULDNOTALLOCATEDIRECTORYREQUESTER
mui_controls_modules_save(), could not allocate directory requester\n
mui_controls_modules_save(), Konnte Verzeichnisauswahlfenster nicht bereitstellen\n
mui_controls_modules_save(), n'a pu allouer la boite de s�lection d'un dossier\n
mui_controls_modules_save(), impossibile allocare la finestra di dialogo della directory\n
;
MSG_CONTROLSMUI_MUICONTROLSMODULESSAVECOULDNOTALLOCATEDIRECTORYNAME
mui_controls_modules_save(), could not allocate directory name\n
mui_controls_modules_save(), Konnte Verzeichnisnamen nicht zuweisen\n
mui_controls_modules_save(), n'a pu allouer le nom du dossier\n
mui_controls_modules_save(), impossibile allocare il nome della directory\n
;
MSG_CONTROLSMUI_SAVECURRENTMODULESCOULDNOTCOPYFILE
_save_current_modules(), could not copy file\n
_save_current_modules(), Konnte Datei nicht kopieren\n
_save_current_modules(), n'a pu copier le fichier\n
_save_current_modules(), impossibile copiare il file\n
;
MSG_CONTROLSMUI_MODULESSAVED
Module(s) saved\n
Modul(e) gespeichert\n
Modules sauvegard�s\n
Modulo(i) salvati\n
;
MSG_CONTROLSMUI_MODULESNOTSAVED
Module(s) not saved!\n
Modul(e) nicht gespeichert\n
N'a pu sauvegarder les modules !\n
Modulo(i) non salvati!\n
;
MSG_CONTROLSMUI_MUICONTROLSSETUSERRATINGCOULDNOTCONNECTTORATING
mui_controls_set_user_rating(), could not connect to ratings server\n
mui_controls_set_user_rating(), Konnte nicht mit dem Bewertungsserver verbinden\n
mui_controls_set_user_rating(), n'a pu se connecter au serveur d'�valuations\n
mui_controls_set_user_rating(), impossibile connettersi al server per votare\n
;
MSG_CONTROLSMUI_SAVINGRATING
Saving rating\n
Speichere die Bewertung\n
Sauvegarde votre �valuation\n
Salvataggio voto\n
;
MSG_CONTROLSMUI_RATINGSAVED
Rating saved\n
Bewertung gespeichert\n
�valuation sauvegard�e\n
Voto salvato\n
;
MSG_CONTROLSMUI_MUILOGSAVECOULDNOTOPENASLLIBRARY
mui_log_save(), could not open asl.library\n
mui_log_save(), Konnte asl.library nicht �ffnen\n
mui_log_save(), n'a pu ouvrir asl.library\n
mui_log_save(), impossibile aprire la libreria asl.library\n
;
MSG_CONTROLSMUI_SAVELOG
Save Log
Speichere Log
Sauvegarder journal
Salva log
;
MSG_CONTROLSMUI_MUILOGSAVECOULDNOTALLOCATEDIRECTORYREQUESTER
mui_log_save(), could not allocate directory requester\n
mui_log_save(), Konnte Verzeichnisauswahlfenster nicht bereitstellen\n
mui_log_save(), n'a pu allouer la boite de s�lection d'un dossier\n
mui_log_save(), impossibile allocare la finestra di dialogo della directory\n
;
MSG_CONTROLSMUI_MUILOGSAVECOULDNOTOPENLOGFILE
mui_log_save(), could not open log file\n
mui_log_save(), Konnte Log-Datei nicht �ffnen\n
mui_log_save(), n'a pu ouvrir le journal\n
mui_log_save(), impossibile aprire il file di log\n
;
MSG_CONTROLSMUI_ERRORINDISPLAYCURRENTROOTDIRECTORYCOULDNOTALLOC
_display_current_root_directory(), could not allocate display string\n
_display_current_root_directory(), Konnte den Display String nicht bereitstellen\n
_display_current_root_directory(), n'a pu allouer la cha�ne � afficher \n
Error in _display_current_root_directory(), impossibile allocare le stringhe da visualizzare\n
;
MSG_CONTROLSMUI_GETYOURRATING
Seek your evaluation\n
Suche deine Bewertung\n
Obtient votre �valuation\n
La tua valutazione\n
;
MSG_CONTROLSMUI_DISPLAYCURRENTRATINGSCOULDNOTCONNECTTORATINGSSE
_display_current_ratings(), could not connect to ratings server\n
_display_current_ratings(), Konnte nicht mit dem Bewertungsserver verbinden\n
_display_current_ratings(), n'a pu se connecter au server d'�valuations\n
_display_current_ratings(), impossibile connettersi al server per votare\n
;
MSG_CONTROLSMUI_ERRORINPUSHTOLOGCOULDNOTSAVELOGTEXT
_push_to_log(), could not save log text\n
_push_to_log(), Konnte den Log-Text nicht speichern\n
_push_to_log(), n'a pu sauvegarder le contenu du journal\n
Error in _push_to_log(), impossibile salvare il testo del log\n
;
MSG_CONTROLSMUI_ERRORINPUSHTOLOGCOULDNOTALLOCATEMEMORYFORLOG
_push_to_log(), could not allocate memory for log\n
_push_to_log(), Konnte den Speicher nicht f�r den Log bereitstellen\n
_push_to_log(), n'a pu allouer la m�moire pour le journal\n
Error in _push_to_log(), impossibile allocare la memoria per il log\n
;
MSG_FTP_FTPINITCONNECTIONCOULDNOTOPENBSDSOCKETLIBRARYV4
ftp_init_connection(), could not open bsdsocket.library v4+\n
ftp_init_connection(), Konnte open bsdsocket.library v4+ nicht �ffnen\n
ftp_init_connection(), n'a pu ouvrir bsdsocket.library v4+\n
ftp_init_connection(), impossibile aprire la libreria bsdsocket.library v4+\n
;
MSG_FTP_FTPINITCONNECTIONCOULDNOTCONNECTTOSERVER
ftp_init_connection(), could not connect to server "%s"\n
ftp_init_connection(), Konnte nicht mit Server "%s" verbinden\n
ftp_init_connection(), n'a pu se connecter au serveur "%s"\n
ftp_init_connection(), impossibile connettersi al server "%s"\n
;
MSG_FTP_FTPGETLISTCOULDNOTALLOCATECOMMAND
ftp_get_list(), could not allocate command\n
ftp_get_list(), Konnte Befehl nicht bereitstellen\n
ftp_get_list(), n'a pu allouer la commande\n
ftp_get_list(), impossibile allocare il comando\n
;
MSG_FTP_FTPGETLISTDIDNORECEIVEEXPECTED250RETURNCODE
ftp_get_list(), did no receive expected 250 return code\n
ftp_get_list(), Erhielt nicht erwarteten 250 return Code\n
ftp_get_list(), n'a pas re�u le code de retour 250 attendu\n
ftp_get_list(), non ha ricevuto il previsto codice di ritorno 250\n
;
MSG_FTP_FTPGETLISTCOULDNOTEXECUTECOMMAND
ftp_get_list(), could not execute command "%s"\n
ftp_get_list(), Konnte Befehl "%s" nicht ausf�hren\n
ftp_get_list(), n'a pu ex�cuter la commande "%s"\n
ftp_get_list(), impossibile eseguire il comando "%s"\n
;
MSG_FTP_FTPGETLISTDIDNOTRECEIVEEXPECTED211213RETURNCODE
ftp_get_list(), did not receive expected 211/213 return codes\n
ftp_get_list(), Erhielt nicht die erwarteten 211/213 return Codes\n
ftp_get_list(), n'a pas re�u les code de retour 211/213 attendus\n
ftp_get_list(), non ha ricevuto i previsti codici di ritorno 211/213\n
;
MSG_FTP_FTPGETLISTFROMDATACOULDNOTALLOCATECOMMAND
ftp_get_list_from_data(), could not allocate command\n
ftp_get_list_from_data(), Konnte Befehl nicht bereitstellen\n
ftp_get_list_from_data(), n'a pu allouer la commande\n
ftp_get_list_from_data(), impossibile allocare il comando\n
;
MSG_FTP_FTPGETLISTFROMDATACOULDNOTCONNECTTOSERVER
ftp_get_list_from_data(), could not connect to server "%s"\n
ftp_get_list_from_data(), Konnte nicht zum Server "%s" verbinden\n
ftp_get_list_from_data(), n'a pu se connecter au server "%s"\n
ftp_get_list_from_data(), impossibile connettersi al server "%s"\n
;
MSG_FTP_FTPGETLISTFROMDATACOULDNOTEXECUTECOMMAND
ftp_get_list_from_data(), could not execute command "%s"\n
ftp_get_list_from_data(), Konnte Befehl "%s" nicht ausf�hren\n
ftp_get_list_from_data(), n'a pu ex�cuter la commande "%s"\n
ftp_get_list_from_data(), impossibile eseguire il comando "%s"\n
;
MSG_FTP_FTPGETLISTFROMDATADIDNOTRECEIVEEXPECTED250RETURNCODE
ftp_get_list_from_data(), did not receive expected 250 return code\n
ftp_get_list_from_data(), Erhielt nicht erwarteten 250 return Code\n
ftp_get_list_from_data(), n'a pas re�u le code de retour 250 attendu\n
ftp_get_list_from_data(), non ha ricevuto il previsto codice di ritorno 250\n
;
MSG_FTP_FTPGETLISTFROMDATADIDNOTRECEIVEEXPECTED150RETURNCODE
ftp_get_list_from_data(), did not receive expected 150 return code\n
ftp_get_list_from_data(), Erhielt nicht erwarteten 150 return Code\n
ftp_get_list_from_data(), n'a pas re�u le code de retour 150 attendu\n
ftp_get_list_from_data(), non ha ricevuto il previsto codice di ritorno 150\n
;
MSG_FTP_FTPGETLISTFROMDATADIDNOTRECEIVEEXPECTEDDATA
ftp_get_list_from_data(), did not receive expected data\n
ftp_get_list_from_data(), Erhielt nicht erwartete Daten\n
ftp_get_list_from_data(), n'a pas re�u les donn�es attendues\n
ftp_get_list_from_data(), non sono stati ricevuti i dati attesi\n
;
MSG_FTP_FTPGETLISTFROMDATACOULDNOTREALLOCATEMEMORYFORTHELIST
ftp_get_list_from_data(), could not reallocate memory for the list\n
ftp_get_list_from_data(), Konnte Speicher f�r die Liste nicht neu zuweisen\n
ftp_get_list_from_data(), n'a pu reallouer la m�moire de la liste\n
ftp_get_list_from_data(), impossibile riallocare la memoria per la lista\n
;
MSG_FTP_FTPCHECKFILEEXISTSCOULDNOTALLOCATECOMMAND
ftp_check_file_exists(), could not allocate command\n
ftp_check_file_exists(), Konnte Befehl nicht bereitstellen\n
ftp_check_file_exists(), n'a pu allouer la commande\n
ftp_check_file_exists(), impossibile allocare il comando\n
;
MSG_FTP_FTPCHECKFILEEXISTSCOULDNOTCONNECTTOSERVER
ftp_check_file_exists(), could not connect to server "%s"\n
ftp_check_file_exists(), Konnte nicht mit Server "%s" verbinden\n
ftp_check_file_exists(), n'a pu se connecter au serveur "%s"\n
ftp_check_file_exists(), impossibile connettersi al server "%s"\n
;
MSG_FTP_FTPCHECKFILEEXISTSCOULDNOTEXECUTECOMMAND
ftp_check_file_exists(), could not execute command "%s"\n
ftp_check_file_exists(), Konnte Befehl "%s" nicht ausf�hren\n
ftp_check_file_exists(), n'a pu ex�cuter la commande "%s"\n
ftp_check_file_exists(), impossibile eseguire il comando "%s"\n
;
MSG_FTP_FTPCHECKFILEEXISTSDIDNOTRECEIVEEXPECTED213RETURNCODE
ftp_check_file_exists(), did not receive expected 213 return code\n
ftp_check_file_exists(), Erhielt nicht erwarteten 213 return Code\n
ftp_check_file_exists(), n'a pas re�u le code de retour 213 attendu\n
ftp_check_file_exists(), non ha ricevuto il previsto codice di ritorno 213\n
;
MSG_FTP_FTPGETFILEFROMDATACOULDNOTALLOCATECOMMAND
ftp_get_file_from_data(), could not allocate command\n
ftp_get_file_from_data(), Konnte Befehl nicht bereistellen\n
ftp_get_file_from_data(), n'a pu allouer la commande\n
ftp_get_file_from_data(), impossibile allocare il comando command\n
;
MSG_FTP_FTPGETFILEFROMDATACOULDNOTCONNECTTOSERVER
ftp_get_file_from_data(), could not connect to server "%s"\n
ftp_get_file_from_data(), Konnte nicht mit Server "%s" verbinden\n
ftp_get_file_from_data(), n'a pu se connecter au serveur "%s"\n
ftp_get_file_from_data(), impossibile connettersi al server "%s"\n
;
MSG_FTP_FTPGETFILEFROMDATACOULDNOTEXECUTECOMMAND
ftp_get_file_from_data(), could not execute command "%s"\n
ftp_get_file_from_data(), Konnte Befehl "%s" nicht ausf�hren\n
ftp_get_file_from_data(), n'a pu ex�cuter la commande "%s"\n
ftp_get_file_from_data(), impossibile eseguire il comando "%s"\n
;
MSG_FTP_FTPGETFILEFROMDATADIDNOTRECEIVEEXPECTED200RETURNCODE
ftp_get_file_from_data(), did not receive expected 200 return code\n
ftp_get_file_from_data(), Erhielt nicht erwarteten 200 return Code\n
ftp_get_file_from_data(), n'a pas re�u le code de retour 200 attendu\n
ftp_get_file_from_data(), non ha ricevuto il previsto codice di ritorno 200\n
;
MSG_FTP_FTPGETFILEFROMDATADIDNOTRECEIVEEXPECTED150RETURNCODE
ftp_get_file_from_data(), did not receive expected 150 return code\n
ftp_get_file_from_data(), Erhielt nicht erwarteten 150 return Code\n
ftp_get_file_from_data(), n'a pas re�u le code de retour 150 attendu\n
ftp_get_file_from_data(), non ha ricevuto il previsto codice di ritorno 150\n
;
MSG_FTP_FTPGETFILEFROMDATADIDNOTRECEIVEEXPECTEDDATA
ftp_get_file_from_data(), did not receive expected data\n
ftp_get_file_from_data(), Erhielt nicht die erwarteten Daten\n
ftp_get_file_from_data(), n'a pas re�u les donn�es attendues\n
ftp_get_file_from_data(), non sono stati ricevuti i dati attesi\n
;
MSG_FTP_CONNECTTOSERVERFORCOMMANDSCOULDNOTALLOCATELINE
_connect_to_server_for_commands(), could not allocate line\n
_connect_to_server_for_commands(), Konnte die Zeile nicht bereitstellen\n
_connect_to_server_for_commands(), n'a pu allouer la ligne\n
_connect_to_server_for_commands(), impossibile allocare la linea\n
;
MSG_FTP_CONNECTTOSERVERFORCOMMANDSCOULDNOTALLOCATECOMMAND
_connect_to_server_for_commands(), could not allocate command\n
_connect_to_server_for_commands(), Konnte Befehl nicht bereitstellen\n
_connect_to_server_for_commands(), n'a pu allouer la commande\n
_connect_to_server_for_commands(), impossibile allocare il comando\n
;
MSG_FTP_CONNECTTOSERVERFORCOMMANDSCOULDNOTCONNECTTOSERVER
_connect_to_server_for_commands(), could not connect to server "%s"\n
_connect_to_server_for_commands(), Konnte nicht mit Server "%s" verbinden\n
_connect_to_server_for_commands(), n'a pu se connecter au serveur "%s"\n
_connect_to_server_for_commands(), impossibile connettersi al server "%s"\n
;
MSG_FTP_CONNECTTOSERVERFORCOMMANDSCOULDNOTRECEIVEANYTHINGFROMSE
_connect_to_server_for_commands(), could not receive anything from server\n
_connect_to_server_for_commands(), Hab nichts vom Server empfangen\n
_connect_to_server_for_commands(), n'a rien pu recevoir du serveur\n
_connect_to_server_for_commands(), non � stato ricevuto alcun dato dal server\n
;
MSG_FTP_CONNECTTOSERVERFORCOMMANDSCOULDNOTEXECUTECOMMAND
_connect_to_server_for_commands(), could not execute command "%s"\n
_connect_to_server_for_commands(), Konnte Befehl "%s" nicht ausf�hren\n
_connect_to_server_for_commands(), n'a pu ex�cuter la commande "%s"\n
_connect_to_server_for_commands(), impossibile eseguire il comando "%s"\n
;
MSG_FTP_CONNECTTOSERVERFORCOMMANDSDIDNOTRECEIVEEXPECTED257RETUR
_connect_to_server_for_commands(), did not receive expected 257 return code\n
_connect_to_server_for_commands(), Erhielt nicht den erwarteten 257 return Code\n
_connect_to_server_for_commands(), n'a pas re�u le code de retour 257 attendu\n
_connect_to_server_for_commands(), non ha ricevuto il previsto codice di ritorno 257\n
;
MSG_FTP_CONNECTTOSERVERFORDATACOULDNOTCONNECTTOSERVER
_connect_to_server_for_data(), could not connect to server "%s"\n
_connect_to_server_for_data(), Konnte nicht mit Server "%s" verbinden\n
_connect_to_server_for_data(), n'a pu se connecter au serveur "%s"\n
_connect_to_server_for_data(), impossibile connettersi al server "%s"\n
;
MSG_FTP_GETDATAPORTCOULDNOTALLOCATECOMMAND
_get_data_port(), could not allocate command\n
_get_data_port(), Konnte Befehl nicht bereitstellen\n
_get_data_port(), n'a pu allouer la commande\n
_get_data_port(), impossibile allocare il comando\n
;
MSG_FTP_GETDATAPORTCOULDNOTEXECUTECOMMAND
_get_data_port(), could not execute command "%s"\n
_get_data_port(), Konnte Befehl "%s" nicht ausf�hren\n
_get_data_port(), n'a pu ex�cuter la commande "%s"\n
_get_data_port(), impossibile eseguire il comando "%s"\n
;
MSG_FTP_CONNECTTOSERVERCOULDNOTCREATESOCKET
_connect_to_server(), could not create socket\n
_connect_to_server(), Netzwerkverbindung konnte nicht hergestellt werden\n
_connect_to_server(), n'a pu cr�er la connexion au r�seau\n
_connect_to_server(), impossibile creare il socket\n
;
MSG_FTP_CONNECTTOSERVERCOULDNOTOBTAINIPADDRESSOF
_connect_to_server(), could not obtain IP address of "%s"\n
_connect_to_server(), Konnte IP Adresse von "%s" nicht empfangen\n
_connect_to_server(), n'a pu obtenir l'adresse IP de "%s"\n
_connect_to_server(), impossibile ottenere l'indirizzo IP di "%s"\n
;
MSG_FTP_CONNECTTOSERVERCOULDNOTCONNECTTOSERVER
_connect_to_server(), could not connect to server "%s"\n
_connect_to_server(), Konnte nicht mit Server "%s" verbinden\n
_connect_to_server(), n'a pu se connecter au serveur "%s"\n
_connect_to_server(), impossibile connettersi al server "%s"\n
;
MSG_FTP_EXECUTECOMMANDCOULDNOTSENDCOMMAND
_execute_command(), could not send command\n
_execute_command(), Konnte Befehl nicht senden\n
_execute_command(), n'a pu envoyer la commande\n
_execute_command(), impossibile inviare il comando\n
;
MSG_FTP_READLINESFROMSERVERUNTILCODESERVERDIDNOTRESPONDWITH
_read_lines_from_server_until_code(), server did not respond with "%s" but with "%.3s"\n
_read_lines_from_server_until_code(), Server antwortete nicht auf "%s" aber auf "%.3s"\n
_read_lines_from_server_until_code(), le serveur n'a pas r�pondu avec "%s" mais avec "%.3s"\n
_read_lines_from_server_until_code(), il server non ha risposto con "%s" ma con "%.3s"\n
;
MSG_FTP_READLINESFROMSERVERUNTILCODECOULDNOTALLOCATELINES
_read_lines_from_server_until_code(), could not allocate lines\n
_read_lines_from_server_until_code(), Konnte Zeilen nicht bereitstellen\n
_read_lines_from_server_until_code(), n'a pu allouer les lignes\n
_read_lines_from_server_until_code(), impossibile allocare le linee\n
;
MSG_FTP_READLINESFROMSERVERCOULDNOTALLOCATELINES
_read_lines_from_server(), could not allocate lines\n
_read_lines_from_server(), Konnte Zeilen nicht bereitstellen\n
_read_lines_from_server(), n'a pu allouer les lignes\n
_read_lines_from_server(), impossibile allocare le linee\n
;
MSG_FTP_READLINESFROMSERVERCOULDNOTREALLOCATELINES
_read_lines_from_server(), could not reallocate lines\n
_read_lines_from_server(), Konnte Zeilen nicht neu zuweisen\n
_read_lines_from_server(), n'a pu r�allouer les lignes\n
_read_lines_from_server(), impossibile riallocare le linee\n
;
MSG_FTP_READLINEFROMSERVERCOULDNOTALLOCATETEMPORARYLINE
_read_line_from_server(), could not allocate temporary line\n
_read_line_from_server(), Konnte vorl�ufige Zeile nicht bereitstellen\n
_read_line_from_server(), n'a pu allouer la ligne temporaire\n
_read_line_from_server(), impossibile allocare la linea temporanea\n
;
MSG_FTP_READLINEFROMSERVERRECEIVEDONLYDATA
_read_line_from_server(), received only %d data\n
_read_line_from_server(), Erhielt nur %d Daten\n
_read_line_from_server(), a re�u seulement %d donn�es\n
_read_line_from_server(), ricevuti solo %d dati\n
;
MSG_FTP_READBYTESFROMSERVERCOULDNOTALLOCATETEMPORARYBYTES
_read_bytes_from_server(), could not allocate temporary bytes\n
_read_bytes_from_server(), Konnte vorl�ufige Bytes nicht zuweisen\n
_read_bytes_from_server(), n'a pu allouer les octets temporaires\n
_read_bytes_from_server(), impossibile allocare i byte temporanei\n
;
MSG_FTP_READBYTESFROMSERVERCOULDNOTALLOCATEBUFFER
_read_bytes_from_server(), could not allocate buffer\n
_read_bytes_from_server(), Konnte Puffer nicht zuweisen\n
_read_bytes_from_server(), n'a pu allouer le tampon\n
_read_bytes_from_server(), impossibile allocare il buffer\n
;
MSG_FTP_READBYTESFROMSERVERRECEIVEDUNEXPECTEDDATA
_read_bytes_from_server(), received unexpected data\n
_read_bytes_from_server(), Erhielt unerwartete Daten\n
_read_bytes_from_server(), a re�u des donn�es inattendues\n
_read_bytes_from_server(), ricevuti dati inattesi\n
;
MSG_FTP_READBYTESFROMSERVERCOULDNOTALLOCATEBYTES
_read_bytes_from_server(), could not allocate temporary bytes\n
_read_bytes_from_server(), Konnte vorl�ufige Bytes nicht zuweisen\n
_read_bytes_from_server(), n'a pu allouer les octets\n
_read_bytes_from_server(), impossibile allocare i byte\n
;
MSG_FTP_READBYTESFROMSERVERCOULDNOTREALLOCATETEMPORARYBYTES
_read_bytes_from_server(), could not reallocate temporary bytes\n
_read_bytes_from_server(), Konnte vorl�ufige Bytes nicht neu zuweisen\n
_read_bytes_from_server(), n'a pu r�allouer les octets temporaires\n
_read_bytes_from_server(), impossibile riallocare i byte temporanei\n
;
MSG_HTTP_HTTPINITCONNECTIONCOULDNOTOPENBSDSOCKETLIBRARYV4
ftp_init_connection(), could not open bsdsocket.library v4+\n
ftp_init_connection(), Konnte bsdsocket.library v4+ nicht �ffnen\n
ftp_init_connection(), n'a pu ouvrir bsdsocket.library v4+\n
ftp_init_connection(), impossibile aprire la libreria bsdsocket.library v4+\n
;
MSG_HTTP_HTTPOPTIONSCHARCOULDNOTPARSEURL
http_options(), could not parse URL\n
http_options(), Konnte URL nicht lesen\n
http_options(), n'a pu analyser l'URL\n
http_options(char *), impossibile analizzare l'URL\n
;
MSG_HTTP_HTTPHEADCHARCOULDNOTPARSEURL
_http_head(), could not parse URL\n
_http_head(), Konnte URL nicht lesen\n
_http_head(), n'a pu analyser l'URL\n
_http_head(char *), impossibile analizzare l'URL\n
;
MSG_HTTP_HTTPGETCHARCOULDNOTPARSEURL
_http_get(), could not parse URL\n
_http_get(), Konnte URL nicht lesen\n
_http_get(), n'a pu analyser l'URL\n
_http_get(char *), impossibile analizzare l'URL\n
;
MSG_HTTP_HTTPPOSTCHARCOULDNOTPARSEURL
_http_post(), could not parse URL\n
_http_post(), Konnte URL nicht lesen\n
_http_post(), n'a pu analyser l'URL\n
_http_post(char *), impossibile analizzare l'URL\n
;
MSG_HTTP_HTTPREQUESTCOULDNOTPARSEURL
_http_request(), could not parse URL\n
_http_request(), Konnte URL nicht lesen\n
_http_request(), n'a pu analyser l'URL\n
_http_request(), impossibile analizzare l'URL\n
;
MSG_HTTP_HTTPREQUESTCOULDNOTCREATESOCKET
_http_request(), could not create socket\n
_http_request(), Konnte Netzwerkverbindung nicht herstellen\n
_http_request(), n'a pu cr�er la connexion r�seau (socket)\n
_http_request(), impossibile creare il socket\n
;
MSG_HTTP_HTTPREQUESTCOULDNOTALLOCATESADDR
_http_request(), could not allocate the address\n
_http_request(), Konnte Adresse nicht zuweisen\n
_http_request(), n'a pu allouer l'adresse\n
_http_request(), impossibile allocare s_addr\n
;
MSG_HTTP_HTTPREQUESTCOULDNOTINTERPRETIPADDRESS
_http_request(), could not interpret IP address\n
_http_request(), Konnte IP Adresse nicht lesen\n
_http_request(), n'a pu comprendre l'adresse IP\n
_http_request(), impossibile interpretare l'indirizzo IP\n
;
MSG_HTTP_HTTPREQUESTCOULDNOTCONNECTTOSERVER
_http_request(), could not connect to server\n
_http_request(), Konnte nicht mit dem Server verbinden\n
_http_request(), n'a pu se connecter au serveur\n
_http_request(), impossibile connettersi al server\n
;
MSG_HTTP_HTTPREQUESTCOULDNOTSENDHEADERS
_http_request(), could not send headers\n
_http_request(), Header konnten nicht gesendet werden\n
_http_request(), n'a pu envoyer les en-t�tes\n
_http_request(), impossibile inviare le intestazioni (headers)\n
;
MSG_HTTP_HTTPREQUESTCOULDNOTALLOCATEBUFFER
_http_request(), could not allocate buffer\n
_http_request(), Konnte Puffer nicht zuweisen\n
_http_request(), n'a pu allouer le tampon\n
_http_request(), impossibile allocare il buffer\n
;
MSG_HTTP_HTTPREQUESTCOULDNOTRECEIVEDATAFROMTHESERVER
_http_request(), could not receive data from the server\n
_http_request(), Konnte keine Daten vom Server empfangen\n
_http_request(), n'a pas re�u de donn�es du serveur\n
_http_request(), impossibile ricevere dati dal server\n
;
MSG_HTTP_HTTPREQUESTCOULDNOTALLOCATEHTTPRESPONSE
_http_request(), could not allocate HTTP response\n
_http_request(), Konnte HTTP Antwort nicht zuweisen\n
_http_request(), n'a pu allouer la r�ponse HTTP\n
_http_request(), impossibile allocare la risposta HTTP\n
;
MSG_HTTP_PARSEURLCHARNOTAWELLFORMEDURL
_parse_url(), not a well-formed URL\n
_parse_url(), Falsche URL\n
_parse_url(), pas une URL bien form�e\n
_parse_url(char *), URL non ben formato\n
;
MSG_HTTP_PARSEURLCHARNOTAWELLFORMEDSCHEME
_parse_url(), not a well-formed scheme\n
_parse_url(), Falsches Schema\n
_parse_url(), pas un sch�ma bien form�\n
_parse_url(char *), schema non ben formato\n
;
MSG_HTTP_PARSEURLCHARCOULDNOTALLOCATEMEMORYFORSCHEME
_parse_url(), could not allocate memory for scheme\n
_parse_url(), Konnte keinen Speicher f�r Schema bereitstellen\n
_parse_url(), n'a pu allouer la m�moire pour le sch�ma\n
_parse_url(char *), impossibile allocare la memoria per lo schema\n
;
MSG_HTTP_PARSEURLCHARNOTAWELLFORMEDUSERNAMEPASSWORDPAIR
_parse_url(), not a well-formed username/password pair\n
_parse_url(), Falscher Benutzername/Passwort\n
_parse_url(), pas une paire nom d'usager/mot-de-passe bien form�e\n
 _parse_url(char *), coppia nome utente/password non ben formata\n
;
MSG_HTTP_PARSEURLCHARCOULDNOTALLOCATEMEMORYFORUSERNAMEPASSWORDP
_parse_url(), could not allocate memory for username/password pair\n
_parse_url(), Konnte keinen Speicher f�r Benutzername/Passwort bereitstellen\n
_parse_url(), n'a pu allouer la m�moire pour la paire nom d'usager/mot-de-passe\n
_parse_url(char *), impossibile allocare la memoria per la coppia nome utente/password\n
;
MSG_HTTP_PARSEURLCHARNOTAWELLFORMEDHOST
_parse_url(), not a well-formed host\n
_parse_url(), Falscher Hostname\n
_parse_url(), pas un h�te bien form�\n
 _parse_url(char *), host non ben formato\n
;
MSG_HTTP_PARSEURLCHARCOULDNOTALLOCATEMEMORYFORPORT
_parse_url(), could not allocate memory for port\n
_parse_url(), Konnte keinen Speicher f�r den Port bereitstellen\n
_parse_url(), n'a pu allouer la m�moire pour le port\n
_parse_url(char *), impossibile allocare la memoria per la porta\n
;
MSG_HTTP_PARSEURLCHARNOTAWELLFORMEDDOMAINNAME
_parse_url(), not a well-formed domain name\n
_parse_url(), Falscher Domainname\n
_parse_url(), pas un nom de domaine bien form�\n
_parse_url(char *), nome dominio non ben formato\n
;
MSG_HTTP_PARSEURLCHARCOULDNOTALLOCATEMEMORYFORPATH
_parse_url(), could not allocate memory for path\n
_parse_url(), Konnte keinen Speicher f�r Pfad bereitstellen\n
_parse_url(), n'a pu allouer la m�moire pour le chemin\n
_parse_url(char *), impossibile allocare la memoria per il percorso\n
;
MSG_HTTP_PARSEURLCHARCOULDNOTALLOCATEMEMORYFORQUERY
_parse_url(), could not allocate memory for query\n
_parse_url(), Konnte keinen Speicher f�r Anfrage bereitstellen\n
_parse_url(), n'a pu allouer la m�moire pour la requ�te\n
_parse_url(char *), impossibile allocare la memoria per l'interrogazione\n
;
MSG_HTTP_PARSEURLCHARCOULDNOTALLOCATEMEMORYFORFRAGMENT
_parse_url(), could not allocate memory for fragment\n
_parse_url(), Speicher f�r Fragmentierung kann nicht zugeordnet werden\n
_parse_url(), n'a pu allouer la m�moire pour le fragment\n
_parse_url(char *), impossibile allocare la memoria per la frammentazione\n
;
MSG_KVS_KVSCREATECOULDNOTALLOCATEMEMORY
kvs_create(), could not allocate memory\n
kvs_create(), Konnte Speicher nicht zuweisen\n
kvs_create(), n'a pu allouer la m�moire\n
kvs_create(), impossibile allocare la memoria\n
;
MSG_KVS_KVSRESIZEPAIRSCOULDNOTALLOCATEMEMORY
kvs_resize_pairs(), could not allocate memory\n
kvs_resize_pairs(), Konnte Speicher nicht zuweisen\n
kvs_resize_pairs(), n'a pu allouer la m�moire\n
kvs_resize_pairs(), impossibile allocare la memoria\n
;
MSG_LOG_ERRORINLOGINITOUTPUTSLOGALREADYINITIALISED
log_init_outputs(), log already initialised\n
log_init_outputs(), Protokoll bereits initialisiert\n
log_init_outputs(), le journal est d�j� initialiser\n
Error in log_init_outputs(), log gi� inizializzato\n
;
MSG_LOG_ERRORINLOGINITOUTPUTSCOULDNOTALLOCATEMEMORYFORFORMATTED
log_init_outputs(), could not allocate memory for formatted message\n
log_init_outputs(), Konnte keinen Speicher f�r formatierte Nachricht bereitstellen\n
log_init_outputs(), n'a pu allouer la m�moire pour le message format�\n
Error in log_init_outputs(), impossibile allocare la memoria per il messaggio formattato\n
;
MSG_MUI_CONTROLS
Controls
Steuerungen
Contr�le
Controlli
;
MSG_MUI_PREFS
Prefs
Einstellungen
Pr�f�rences
Preferenze
;
MSG_MUI_LOG
Log
Protokoll
Journal
Log
;
MSG_MUI_DISPLAYPROGRAMINFORMATION
Display program information
Programminformationen anzeigen
Affiche les informations sur le programme
Mostra informazioni sul programma
;
MSG_MUI_PLAYCURRENTMODULEORFETCHANDPLAYNEWMODULE
Play current modules or fetch and play new modules
Spiele aktuelle Module ab oder hole und spiele neue Module ab
Jouer les modules courants ou cherche et joue de nouveaux modules
Suona il modulo corrente o preleva un nuovo modulo da suonare
;
MSG_MUI_REPLAYCURRENTMODULEFROMITSBEGINNING
Replay current modules from the beginning
Spiele das aktuelle Modul erneut ab
Rejouer les modules courants depuis le d�but
Ripeti dall'inizio il modulo corrente
;
MSG_MUI_PAUSETHEPLAYINGOFTHECURRENTMODULE
Pause the playing of the current modules
Stoppe aktuelles Modul
Mettre en pause les modules courants
Metti in pausa il corrente modulo in esecuzione
;
MSG_MUI_STOPTHEPLAYINGOFTHECURRENTMODULE
Stop the playing of the current module
Stoppe aktuelles Modul
Arr�ter de jouer les modules courants
Stoppa il corrente modulo in esecuzione
;
MSG_MUI_FETCHANDPLAYNEWMODULE
Fetch and play new module
Hole und spiele neues Modul ab
Chercher et jouer de nouveaux modules
Preleva e suona il nuovo modulo
;
MSG_MUI_DISPLAYSOURCEANDROOTDIRECTORYINWHICHDIRECTORIESAREFETCH
Display source and root directory in which directories are fetched
Zeige das Quell- und Hauptverzeichnis an aus welchen Module geholt werden sollen
Affiche la source et le dossier racine dans lesquels les dossiers sont cherch�s
Mostra la fonte ed il suo indice dalla quale saranno prelevate le directory
;
MSG_MUI_RESETROOTDIRECTORYTOSOURCESDEFAULT
Reset root directory to source's default
Setze das Hauptverzeichnis als Quellverzeichnis zur�ck
Changer le dossier racine pour sa valeur par d�faut
Resetta indice della directory tornando alla fonte predefinita
;
MSG_MUI_CHANGESOURCETOUSEPREVIOUSSOURCE
Change source to use previous source
Setze die Quelle auf die vorherige zur�ck
Changer la source par la source pr�c�dente
Vai alla fonte precedente
;
MSG_MUI_CHANGESOURCETOUSENEXTSOURCE
Change source to use next source
�ndere Quelle auf die n�chste
Changer la source par la source suivante
Vai alla fonte successiva
;
MSG_MUI_DISPLAYTHEDIRECTORYINWHICHMODULESAREFETCHED
Display the directory in which modules are fetched
Zeige das Verzeichnus an aus welchen Module geholt werden
Affiche le dossier dans lequel les modules sont cherch�s
Mostra la directory dalla quale prelevare i moduli
;
MSG_MUI_SETTHISDIRECTORYASROOTDIRECTORY
Set this directory as root directory
Setze dieses Verzeichnis als Hauptverzeichnis
Choisir ce dossier comme dossier racine
Imposta questa directory come indice della stessa
;
MSG_MUI_LISTTHECONTENTOFTHISDIRECTORY
List the content of this directory
Liste den Inhalt dieses Verzeichnises auf
Afficher le contenu de ce dossier
Mostra il contenuto di questa directory
;
MSG_MUI_BANTHISDIRECTORYTONEVERFETCHMODULESINIT
Ban this directory to never fetch modules in it
Schlie�e dieses Verzeichnis als Quellverzeichnis aus
Bannir ce dossier pour ne plus jamais y chercher des modules
Banna questa directory in modo da non riprodurre mai i moduli da li
;
MSG_MUI_LISTANYBANNEDDIRECTORIES
List any banned directories
Zeige alle gesperrten Verzeichnisse an
Afficher la liste des dossiers bannis
Mostra tutte le directory bannate
;
MSG_MUI_DISPLAYTHEARCHIVEFILEFROMWHICHMODULESAREPLAYED
Display the file from which modules are played
Zeigt das Archiv an, aus dem die Module abgespielt werden
Affiche le fichier de laquelle les modules sont jou�s
Mostra l'archivio dal quale i moduli verranno suonati
;
MSG_MUI_LISTTHECONTENTOFTHISARCHIVEFILE
List the content of this file
Liste den Inhalt dieses Archives auf
Afficher le contenu du fichier
Mostra il contenuto di questo archivio dei file
;
MSG_MUI_BANTHISARCHIVEFILETONEVERPLAYMODULESFROMIT
Ban this archive file to never play modules from it
Dieses Archiv permanent sperren
Bannir ce fichier pour ne plus jamais y chercher des modules
Banna questo archivio in modo da non riprodurre mai i moduli da li
;
MSG_MUI_LISTANYBANNEDFILES
List any banned files
Liste alle gesperrten Dateien auf
Afficher la liste des fichiers bannis
Mostra tutti i file bannati
;
MSG_MUI_DISPLAYTHEMODULESBEINGPLAYED
Display the modules being played
Laufende Module anzeigen
Affiche les modules jou�s
Mostra i moduli in esecuzione
;
MSG_MUI_SAVETODISKTHEMODULESBEINGPLAYED
Save to disk the modules being played
Laufende Module auf Diskette speichern
Sauvegarder sur disque les modules jou�s
Salva su disco i moduli in esecuzione
;
MSG_MUI_SENDANEMAILWITHTHEMODULESBEINGPLAYED
Send an e-mail with the modules being played
Sende eine Mail mit den laufenden Modulen
Envoyer par courriel les modules jou�s
Invia una e-mail con i moduli in esecuzione
;
MSG_MUI_YOURRATING
Your rating
Deine Bewertung
Votre �valuation
Il tuo voto
;
MSG_MUI_RATINGFROMTHECOMMUNITY
Rating from the community
Bewertung der Community
�valuation de la communaut�
Voto espresso dalla comunit�
;
MSG_MUI_NUMBEROFVOTESFROMTHECOMMUNITY
Number of ratings from the community
Anzahl der Bewertungen der Community
Nombre d'�valuation de la communaut�
Numero di voti espressi dalla comunit�
;
MSG_MUI_DISPLAYINFORMATIONMESSAGES
Display information messages
Informationsbenachrichtigungen anzeigen
Affiche les messages d'information
Mostra messaggi di informazione
;
MSG_MUI_PLAYONSTARTUP
Play on startup
Automatisch nach Programmstart abspielen
Jouer au d�marrage
Suona automaticamente all'avvio
;
MSG_MUI_WHETHERORNOTTOPLAYMODULESRIGHTAFTERSTARTING
Whether or not to play modules right after starting
Sollen Module beim Programmstart abgespielt werden?
S'il faut jouer des modules apr�s le d�marrage
Seleziona se riprodurre o meno i moduli all'avvio del programma
;
MSG_MUI_SHOWLOGPAGEONFETCH
Show log page on fetch
Zeige die Protokollseite nach der Abholung
Afficher le journal pendant la recherche
Mostra pagina log al prelevamento
;
MSG_MUI_WHETHERORNOTTOSHOWTHELOGPAGEWHILEFETCHINGNEWMODULES
Whether or not to show the log page while fetching new modules
Soll die Protokollseite beim Aufnehmen neuer Formulare angezeigt werden?
S'il faut afficher le journal pendant la recherche de modules
Seleziona se mostrare o meno la pagina di log durante il prelevamento dei nuovi moduli
;
MSG_MUI_REMEMBERSOURCEBETWEENRUNS
Remember source between runs
Merken Sie sich beim Neustart die Quelle
Se souvenir la source entre ex�cutions
Ricorda fonte al riavvio
;
MSG_MUI_WHETHERORNOTTOREMEMBERTHESOURCEANDROOTDIRECTORYBETWEENR
Whether or not to remember the source and root directory between runs
Soll der Quell- und Hauptverzeichnisindex beim n�chsten Neustart gespeichert werden?
S'il faut se souvenir de la source et du dossier racine entre ex�cutions
Seleziona se ricordare o meno la fonte e l'indice della directory al prossimo riavvio
;
MSG_MUI_CACHEDIRECTORIESMODULESLISTSDURINGRUN
Cache directories/files contents during run
Aktivieren Sie das Caching f�r Verzeichnisse / Module w�hrend der Wiedergabe
Mettre en cache les contenus des dossiers et des fichiers pendant l'ex�cution
Attiva cache per directory/moduli durante la riproduzione
;
MSG_MUI_WHETHERORNOTTOCACHETHEDIRECTORIESANDFILESLISTSINMEMORYM
Whether or not to cache the directories and files contents in-memory (more memory, shorter access times)
Sollen Verzeichnisse und Dateien zwischengespeichert werden (erfordert mehr Speicher, aber weniger Zugriffszeiten)?
S'il faut mettre en cache les contenus des dossiers et des fichiers en m�moire (plus de m�moire, acc�s plus rapides)
Seleziona se inserire o meno nella cache di memoria le directory ed i file (richiede pi� memoria ma minori tempi di accesso)
;
MSG_MUI_CACHEDIRECTORIESMODULESLISTSBETWEENRUNS
Cache directories/files contents between runs
Zwischenspeichern Sie den Inhalt von Ordnern und Dateien zwischen den Programmstarts
Mettre en cache les contenus des dossiers et des fichiers entre ex�cutions
Attiva cache per directory/moduli al riavvio
;
MSG_MUI_WHETHERORNOTTOCACHETHEDIRECTORIESANDFILESLISTSONDISKMOR
Whether or not to cache the directories and files contents on disk (more disk, shorter access times)
Soll der Inhalt von Ordnern und Dateien auf der Festplatte zwischengespeichert werden (ben�tigt mehr Speicher, schnellerer Zugriff)?
S'il faut mettre en cache les contenus des dossiers et des fichiers sur le disque (plus de disque, acc�s plus rapides)
Seleziona se inserire o meno nella cache su disco le directory ed i file (richiede pi� spazio su disco ma minori tempi di accesso)
;
MSG_MUI_SAVEPREFERENCESONEXIT
Save preferences on exit
Einstellungen beim Beenden speichern
Sauvegarder les pr�f�rences � la sortie du programme
Salva preferenze all'uscita
;
MSG_MUI_WHETHERORNOTTOSAVETHEPREFERENCESWHENEXITING
Whether or not to save the preferences when exiting
Sollen die Einstellungen beim Beenden gespeichert werden?
S'il faut sauvegarder les pr�f�rences en quittant le programme
Seleziona se salvare o meno le preferenze all'uscita
;
MSG_MUI_IMAGESSETS
Images sets:
Erscheinungsbild (Skin)
Ensembles d'images :
Skin:
;
MSG_MUI_CHOICEOFSKINSINCLUDINGBUTTONSBACKGROUND
Choice of skins, including buttons, background
W�hlen Sie einen Skin aus, einschlie�lich Schaltfl�chen und Hintergr�nden
Choix de l'apparence du programme, y compris les boutons, l'image de fond
Seleziona una skin, inclusi pulsanti e sfondi
;
MSG_MUI_SKIPMODULESWHENBANNED
Skip modules when banned
Gesperrte Module �berspringen
Sauter les modules si bannis
Tralascia moduli se bannati
;
MSG_MUI_WHETHERORNOTTOSKIPTHEMODULESIFYOUBANTHEM
Whether or not to skip the modules if you ban them
Sollen zuvor gesperrte Module �bersprungen werden?
S'il faut sauter des modules que vous avez bannis
Seleziona se tralasciare o meno i moduli precedentemente bannati
;
MSG_MUI_SKIPMODULESWHENRATINGLOW
Skip modules when rating low
Niedrig bewertete Module �berspringen
Sauter les modules si mal �valu�s
Tralascia moduli con valutazione bassa
;
MSG_MUI_WHETHERORNOTTOSKIPTHEMODULESIFYOURATETHEMLOW
Whether or not to skip the modules if you rate them low
Sollen niedrigbewertete Module �bersprungen werden?
S'il faut sauter les modules que vous avez mal �valu�s
Seleziona se tralasciare o meno i moduli a cui era stata data precedentemente una valutazione bassa
;
MSG_MUI_SKIPMODULESWHENALREADYRATED
Skip modules when already rated
Bereits bewertete Module �berspringen
Sauter les modules d�j� �valu�s
Tralascia moduli se gi� valutati
;
MSG_MUI_WHETHERORNOTTOSKIPTHEMODULESIFTHECOMMUNITYALREADYLISTEN
Whether or not to skip the modules if the community already listened to them, novelties guaranteed!
Sollen von der Community bereits geh�rte Module �bersprungen werden? Nur Neuheiten werden abgespielt!
S'il faut sauter les modules que la communaut� a d�j� �cout�s, nouveaut�s garanties !
Seleziona se tralasciare o meno i moduli che la comunit� di ascoltatatori ha gi� ascoltato, novit� garantite!
;
MSG_MUI_SKIPMODULESWHENALREADYRATEDTOOLOW
Skip modules when already rated too low
Zu niedrig bewertete Module �berspringen
Sauter les modules mal �valu�s
Tralascia moduli con valutazione troppo bassa
;
MSG_MUI_WHETHERORNOTTOSKIPTHEMODULESIFTHECOMMUNITYALREADYRATEDLOW
Whether or not to skip the modules if the community rated them low
Sollen die von der Community niedrig bewerteten Module �bersprungen werden?
S'il faut sauter les modules que la communaut� a mal �valu�s
Seleziona se tralasciare o meno i moduli che la comunit� di ascoltatatori ha giudicato avere una valutatazione bassa
;
MSG_MUI_SKIPMODULESWHICHSIZESAREABOVEMAXIMUMSIZEOF
Skip modules which sizes are above maximum size of
�berspringen Sie Module die gr��er sind als
Sauter les modules dont la taille est sup�rieure �
Tralascia moduli le cui dimensioni siano superiori a
;
MSG_MUI_WHETHERORNOTTOSKIPTHEMODULESIFTHEIRSIZEISABOVETHECHOSEN
Whether or not to skip the modules if their size is above the chosen maximum size
Sollen Module �bersprungen werden, die gr��er sind als die vorher festgelegten Maximalwerte?
S'il faut sauter les modules dont la taille est sup�rieure � la taille maximum
Seleziona se tralasciare o meno i moduli le cui dimensioni siano superiori a quelle massime impostate
;
MSG_MUI_MODULESDIRECTORY
Modules directory:
Modulverzeichnis:
Dossier des modules :
Directory moduli:
;
MSG_MUI_CHOICEOFTHEDIRECTORYWHERETOSAVEMODULESBYDEFAULT
Choice of the directory where to save modules by default
W�hlen Sie ein Verzeichnis aus, in dem die Module standardm��ig gespeichert werden sollen
Choix du dossier par d�faut dans lequel sauvegarder les modules

;
MSG_MUI_CACHEDIRECTORY
Cache directory:
Verzeichnisspeicher:
Dossier du cache :
Directory cache:
;
MSG_MUI_CHOICEOFTHEDIRECTORYWHERETOCACHEMODULESBYDEFAULT
Choice of the directory where put the cache by default
W�hlen Sie ein Cache-Verzeichnis aus, in dem Module standardm��ig gespeichert werden sollen
Choix du dossier par d�faut dans lequel mettre le cache
Seleziona una directory della cache dove salvare i moduli in modo predefinito
;
MSG_MUI_KILOBYTES
kilo bytes
Kilobytes
kilo-octets
kilobyte
;
MSG_MUI_DEFAULT
Default
Standard
D�fauts
Predefinito
;
MSG_MUI_SAVE
Save
Speichern
Sauvegarder
Salva
;
MSG_MUI_RESET
Reset
Zur�cksetzen
Derni�res
Resetta
;
MSG_MUI_MUIINITUICOULDNOTALLOCATEDEFAULTLOGMESSAGE
mui_init_ui(), could not allocate the log message\n
mui_init_ui(), Konnte Protokollnachricht nicht zuweisen\n
mui_init_ui(), n'a pu allouer le message du journal\n
mui_init_ui(), impossibile allocare il messaggio di log predefinito\n
;
MSG_MUI_SHOWTHEACTIVITIESOFAMIMODRADIOANDANYERROR
Show the activities of AmiModRadio and any error
Zeige die Aktivit�ten von AmiModRadio und eventuelle Fehler an
Montre les activit�s d'AmiModRadio et les erreurs
Mostra le attivit� di AmiModRadio e ogni eventuale errore
;
MSG_MUI_CLEAR
Clear
L�schen
Effacer
Cancella
;
MSG_MUI_ALLOFAMINETMODULESATYOURFINGERTIPS
All of Aminet modules at your fingertips
Alle Aminet-Module immer zur Hand
Tous les modules d'Aminet au bout des doigts
Tutti i moduli di Aminet sulla punta delle tue dita
;
MSG_MUI_MUICHILDWINDOWLISTUPDATECOULDNOTALLOCATETEMPORARYSTRING
mui_child_window_list_update(), could not allocate temporary string\n
mui_child_window_list_update(), Konnten vorl�ufige Zeichenkette nicht zuweisen\n
mui_child_window_list_update(), n'a pu allouer la cha�ne temporaire\n
mui_child_window_list_update(), impossibile allocare la stringa temporanea\n
;
MSG_MUI_NOTHINGTODISPLAY
<Nothing to display>
<Nichts anzuzeigen>
<Rien � afficher>
<Nulla da visualizzare>
;
MSG_PLAYERS_PLAYERSINITCONNECTIONCOULDNOTALLOCATEONESIGNAL
players_init_connection(), could not allocate one signal\n
players_init_connection(), Konnte kein Signal zuweisen\n
players_init_connection(), n'a pu allouer le signal\n
players_init_connection(), impossibile allocare un segnale\n
;
MSG_PLAYERS_PLAYERSINITCONNECTIONCOULDNOTOPENREXXSYSLIBLIBRARYV0
players_init_connection(), could not open rexxsyslib.library v0+\n
players_init_connection(), Konnte rexxsyslib.library v0+ nicht �ffnen\n
players_init_connection(), n'a pu ouvrir rexxsyslib.library v0+\n
players_init_connection(), impossibile aprire la libreria rexxsyslib.library v0+\n
;
MSG_PLAYERS_PLAYERSGETPLAYERNAMECOULDNOTACCESSPLAYER
players_get_player_name(), could not access player\n
players_get_player_name(), Konnte Abspieler nicht �ffnen\n
players_get_player_name(), n'a pu acc�der au joueur\n
players_get_player_name(), impossibile accedere al player\n
;
MSG_PLAYERS_PLAYERSCANPLAYMODULECOULDNOTACCESSPLAYER
players_can_play_module(), could not access player\n
players_can_play_module(), Konnte Abspieler nicht �ffnen\n
players_can_play_module(), n'a pu acc�der au joueur\n
players_can_play_module(), impossibile accedere al player\n
;
MSG_PLAYERS_PLAYERSCANPLAYMODULECOULDNOTALLOCATEFILENAME
players_can_play_module(), could not allocate file name\n
players_can_play_module(), Konnte Dateiname nicht zuweisen\n
players_can_play_module(), n'a pu allouer le nom du fichier\n
players_can_play_module(), impossibile allocare il nome del file\n
;
MSG_PLAYERS_PLAYERSCANPLAYMODULECOULDNOTFINDCORRESPONDINGPLAYER
players_can_play_module(), could not find corresponding player\n
players_can_play_module(), Konnte den entsprechenden Abspieler nicht finden\n
players_can_play_module(), n'a pu trouver le joueur correspondant\n
players_can_play_module(), impossibile trovare il player corrispondente\n
;
MSG_PLAYERS_PLAYERSLOADANDPLAYMODULESCOULDNOTACCESSPLAYER
players_load_and_play_modules(), could not access player\n
players_load_and_play_modules(), Konnte Abspieler nicht �ffnen\n
players_load_and_play_modules(), n'a pu acc�der au joueur\n
players_load_and_play_modules(), impossibile accedere al player\n
;
MSG_PLAYERS_PLAYERSLOADANDPLAYMODULESCOULDNOTALLOCATEMEMORYFORPLAYMODCO
players_load_and_play_modules(), could not allocate memory for PLAYMOD command\n
players_load_and_play_modules(), Konnte keinen Speicher f�r den Befehl PLAYMOD bereitstellen\n
players_load_and_play_modules(), n'a pu allouer la m�moire pour la commande PLAYMOD\n
players_load_and_play_modules(), impossibile allocare la memoria per il comando PLAYMOD\n
;
MSG_PLAYERS_PLAYERSLOADANDPLAYMODULESCOULDNOTALLOCATEMEMORYFORLOADMODUL
players_load_and_play_modules(), could not allocate memory for LOADMODULE command\n
players_load_and_play_modules(), Konnte keinen Speicher f�r den Befehl LOADMODULE bereitstellen\n
players_load_and_play_modules(), n'a pu allouer la m�moire pour la commande LOADMODULE\n
players_load_and_play_modules(), impossibile allocare la memoria per il comando LOADMODULE\n
;
MSG_PLAYERS_PLAYERSLOADANDPLAYMODULESCOULDNOTALLOCATEMEMORYFORADDCOMMAN
players_load_and_play_modules(), could not allocate memory for ADD command\n
players_load_and_play_modules(), Konnte keinen Speicher f�r den Befehl ADD bereitstellen\n
players_load_and_play_modules(), n'a pu allouer la m�moire pour la commande ADD\n
players_load_and_play_modules(), impossibile allocare la memoria per il comando ADD\n
;
MSG_PLAYERS_PLAYERSLOADANDPLAYMODULESCOULDNOTALLOCATEMEMORYFORPLAYCOMMA
players_load_and_play_modules(), could not allocate memory for PLAY command\n
players_load_and_play_modules(), Konnte keinen Speicher f�r den Befehl PLAY bereitstellen\n
players_load_and_play_modules(), n'a pu allouer la m�moire pour la commande PLAY\n
players_load_and_play_modules(), impossibile allocare la memoria per il comando PLAY\n
;
MSG_PLAYERS_PLAYERSLOADANDPLAYMODULESCOULDNOTALLOCATEMEMORYFORLOADCOMMA
players_load_and_play_modules(), could not allocate memory for LOAD command\n
players_load_and_play_modules(), Konnte keinen Speicher f�r den Befehl LOAD bereitstellen\n
players_load_and_play_modules(), n'a pu allouer la m�moire pour la commande LOAD\n
players_load_and_play_modules(), impossibile allocare la memoria per il comando LOAD\n
;
MSG_PLAYERS_PLAYERSLOADANDPLAYMODULESCOULDNOTALLOCATEMEMORYFOROPENCOMMA
players_load_and_play_modules(), could not allocate memory for OPEN command\n
players_load_and_play_modules(), Konnte keinen Speicher f�r den Befehl OPEN bereitstellen\n
players_load_and_play_modules(), n'a pu allouer la m�moire pour la commande OPEN\n
players_load_and_play_modules(), impossibile allocare la memoria per il comando OPEN\n
;
MSG_PLAYERS_PLAYERSLOADANDPLAYMODULESCOULDNOTFINDCORRESPONDINGPLAYER
players_load_and_play_modules(), could not find corresponding player\n
players_load_and_play_modules(), Konnte entsprechenden Abspieler nicht �ffnen\n
players_load_and_play_modules(), n'a pu trouver le joueur correspondant\n
players_load_and_play_modules(), impossibile trovare il player corrispondente\n
;
MSG_PLAYERS_PLAYERSLOADANDPLAYMODULESCOULDNOTFINDANYMODULETOPLAY
players_load_and_play_modules(), could not find any module to play\n
players_load_and_play_modules(), Konnte keine Module finden\n
players_load_and_play_modules(), n'a pu trouver aucune module � jouer\n
players_load_and_play_modules(), impossibile trovare un modulo da suonare\n
;
MSG_PLAYERS_PLAYERSPLAYMODULESCOULDNOTACCESSPLAYER
players_play_modules(), could not access player\n
players_play_modules(), Konnte Abspieler nicht �ffnen\n
players_play_modules(), n'a pu acc�der au joueur\n
players_play_modules(), impossibile accedere al player\n
;
MSG_PLAYERS_PLAYERSREPLAYMODULESCOULDNOTACCESSPLAYER
players_replay_modules(), could not access player\n
players_replay_modules(), Konnte Abspieler nicht �ffnen\n
players_replay_modules(), n'a pu acc�der au joueur\n
players_replay_modules(), impossibile accedere al player\n
;
MSG_PLAYERS_PLAYERSPAUSEMODULESCOULDNOTACCESSPLAYER
players_pause_modules(), could not access player\n
players_pause_modules(), Konnte Abspieler nicht �ffnen\n
players_pause_modules(), n'a pu acc�der au joueur\n
players_pause_modules(), impossibile accedere al player\n
;
MSG_PLAYERS_PLAYERSREMOVEMODULESCOULDNOTACCESSPLAYER
players_remove_modules(), could not access player\n
players_remove_modules(), Konnte Abspieler nicht �ffnen\n
players_remove_modules(), n'a pu ac�der au joueur\n
players_remove_modules(), impossibile accedere al player\n
;
MSG_PLAYERS_CONNECTPLAYERCOULDNOTALLOCATEMEMORY
_connect_player(), could not allocate memory\n
_connect_player(), Konnte Speicher nicht zuweisen\n
_connect_player(), n'a pu allouer la m�moire\n
_connect_player(), impossibile allocare la memoria\n
;
MSG_PLAYERS_CONNECTPLAYERCOULDNOTFINDKNOWNAREXXPORTOFAPLAYER
_connect_player(), could not find known ARexx port of a player\n
_connect_player(), Konnten keinen bekannten ARexx Port des Abspielers finden\n
_connect_player(), n'a pu trouver le port ARexx d'un playeur connu\n
_connect_player(), impossibile trovare una porta ARexx conosciuta per il player\n
;
MSG_PLAYERS_EXECUTECOMMANDCANNOTCREATEREPLYPORT
_execute_command(), cannot create reply port\n
_execute_command(), Kann den Reply Port nicht erstellen\n
_execute_command(), n'a pu cr�er le port de r�ponse\n
_execute_command(), impossibile creare la porta per la risposta\n
;
MSG_PLAYERS_EXECUTECOMMANDCANNOTCREATEMESSAGE
_execute_command(), cannot create message\n
_execute_command(), Kann Meldung nicht erstellen\n
_execute_command(), n'a pu cr�er le message\n
_execute_command(), impossibile creare il messaggio\n
;
MSG_PLAYERS_EXECUTECOMMANDCOULDNOTFILLAREXXMESSAGE
_execute_command(), could not fill ARexx message\n
_execute_command(), ARexx-Nachricht konnte nicht abgeschlossen werden\n
_execute_command(), n'a pu remplir le message ARexx\n
_execute_command(), impossibile riempire il messaggio ARexx\n
;
MSG_PLAYERS_EXECUTECOMMANDNOAREXXPORTOFKNOWNPLAYERAVAILABLEANYMORE
_execute_command(), no ARexx port of known player available anymore\n
_execute_command(), ARexx Port der bekannten Abspieler nicht l�nger verf�gbar\n
_execute_command(), pas de port ARexx d'un joueur connu toujours disponible\n
_execute_command(), nessuna porta ARexx disponibile per il player conosciuto\n
;
MSG_PLAYERS_STARTEDDELITRACKERII
Started DeliTracker II\n
DeliTracker II ge�ffnet\n
A d�marr� DeliTracker II\n
Avviato DeliTracker II\n
;
MSG_PLAYERS_STARTEDEAGLEPLAYER
Started EaglePlayer\n
EaglePlayer ge�ffnet\n
A d�marr� EaglePlayer\n
Avviato EaglePlayer\n
;
MSG_PLAYERS_STARTEDHIPPOPLAYER
Started HippoPlayer\n
HippoPlayer ge�ffnet\n
A d�marr� HippoPlayer\n
Avviato HippoPlayer\n
;
MSG_PLAYERS_STARTEDMULTIPLAYER
Started MultiPlayer\n
MultiPlayer ge�ffnet\n
A d�marr� MultiPlayer\n
Avviato MultiPlayer\n
;
MSG_PLAYERS_STARTEDAMIGAAMP
Started AmigaAmp\n
AmigaAmp ge�ffnet\n
A d�marr� AmigaAmp\n
Avviato AmigaAMP\n
;
MSG_PLAYERS_STARTEDTUNENET
Started TuneNet\n
TuneNet ge�ffnet\n
A d�marr� TuneNet\n
Avviato TuneNet\n
;
MSG_PLAYERS_STARTEDFIRSTKNOWNPLAYER
Started first known player\n
Erster bekannter Abspieler ge�ffnet\n
A d�marr� le premier joueur connu\n
Avviato il primo player conosciuto\n
;
MSG_PLAYERS_STARTPLAYERCOULDNOTSTARTANYKNOWNPLAYER
_start_player(), could not start any known player: is one of the supported player in the path?\n
_start_player(), Konnte keinen bekannten Abspieler �ffnen: Ist einer der unterst�tzten Abspieler verf�gbar?\n
_start_player(), n'a pu d�marrer aucun joueur connu : est-ce qu'un des joueurs support� est accessible?\n
_start_player(), impossibile avviare un player conosciuto: assicurati di aver specificato nel percorso un player supportato\n
;
MSG_PLAYERS_PLAYERLISTENERSTARTCOULDNOTCREATEPLAYERLISTENER
_player_listener_start(), could not create player listener\n
_player_listener_start(), Abspieler konnte nicht ge�ffnet werden\n
_player_listener_start(), n'a pu cr�er le superviseur du joueur\n
_player_listener_start(), impossibile creare un player per l'ascolto\n
;
MSG_PLAYERS_ALLOCATESIGNALCOULDNOTALLOCATESIGNAL
_allocate_signal(), could not allocate signal "%s"\n
_allocate_signal(), Konnte Signal "%s" nicht zuordnen\n
_allocate_signal(), n'a pu allouer le signal "%s"\n
_allocate_signal(), impossibile allocare il segnale "%s"\n
;
MSG_PREFS_PREFSINITPREFERENCESCOULDNOTOPENPREFERENCES
prefs_init_preferences(), could not open preferences\n
prefs_init_preferences(), Konnte die Einstellungen nicht �ffnen\n
prefs_init_preferences(), n'a pu ouvrir les pr�f�rences\n
prefs_init_preferences(), impossibile aprire le preferenze\n
;
MSG_PREFS_PREFSSAVEPREFERENCESCOULDNOTOPENPREFERENCES
prefs_save_preferences(), could not open preferences\n
prefs_save_preferences(), Konnte die Einstellungen nicht �ffnen\n
prefs_save_preferences(), n'a pu ouvrir les pr�f�rences\n
prefs_save_preferences(), impossibile aprire le preferenze\n
;
MSG_RATINGS_RATINGSINITCONNECTIONRATINGSSERVERUNAVAILABLEATTHEM
ratings_init_connection(), ratings server unavailable at the moment\n
ratings_init_connection(), Bewertungsserver momentan nicht erreichbar\n
ratings_init_connection(), serveur d'�valuations indisponibles actuellement\n
ratings_init_connection(), il server per le valutazioni � al momento irraggiungibile\n
;
MSG_RATINGS_RATINGSGETUSERRATINGCOULDNOTFINDUSERID
ratings_get_user_rating(), could not find user ID\n
ratings_get_user_rating(), Konnte Benutzer ID nicht finden\n
ratings_get_user_rating(), n'a pu trouver l'identifiant de l'usager\n
ratings_get_user_rating(), impossibile trovare l'ID Utente\n
;
MSG_RATINGS_RATINGSGETUSERRATINGCOULDNOTGETANSWERFROMSERVER
ratings_get_user_rating(), could not get answer from server\n
ratings_get_user_rating(), Konnte keine Antwort vom Server erhalten\n
ratings_get_user_rating(), n'a pu obtenir une r�ponse du serveur\n
ratings_get_user_rating(), impossibile ricevere una risposta dal server\n
;
MSG_RATINGS_RATINGSGETALLRATINGSCOULDNOTGETANSWERFROMSERVER
ratings_get_all_ratings(), could not get answer from server\n
ratings_get_all_ratings(), Konnte keine Antwort vom Server erhalten\n
ratings_get_all_ratings(), n'a pu obtenir une r�ponse du serveur\n
ratings_get_all_ratings(), impossibile ricevere una risposta dal server\n
;
MSG_RATINGS_RATINGSADDUSERRATINGCOULDNOTFINDUSERID
ratings_add_user_rating(), could not find user ID\n
ratings_add_user_rating(), Konnte Benutzer ID nicht finden\n
ratings_add_user_rating(), n'a pu trouver l'identifiant de l'usager\n
ratings_add_user_rating(), impossibile trovare l'ID Utente\n
;
MSG_RATINGS_RATINGSADDUSERRATINGCOULDNOTGETANSWERFROMSERVER
ratings_add_user_rating(), could not get answer from server\n
ratings_add_user_rating(), Konnte keine Antwort vom Server erhalten\n
ratings_add_user_rating(), n'a pu obtenir une r�ponse du serveur\n
ratings_add_user_rating(), impossibile ricevere una risposta dal server\n
;
MSG_RATINGS_RATINGSADDUSERRATINGSAVEDRATINGANDRETURNEDRATINGARE
ratings_add_user_rating(), saved rating and returned rating are different\n
ratings_add_user_rating(), Gespeicherte und zur�ckgegebene Bewertung unterscheiden sich\n
ratings_add_user_rating(), l'�valuation sauvegard�e et l'�valuation du serveur sont diff�rentes\n
ratings_add_user_rating(), la valutazione salvata e il punteggio restituito sono diversi\n
;
MSG_RATINGS_GETUSERIDCOULDNOTGETANSWERFROMSERVER
_get_user_id(), could not get answer from server\n
_get_user_id(), Konnte keine Antwort vom Server erhalten\n
_get_user_id(), n'a pu obtenir une r�ponse du serveur\n
_get_user_id(), impossibile ricevere una risposta dal server\n
;
MSG_RATINGS_GETSERVERANSWERSHOULDNOTTRYTOGETANANSWER
_get_server_answer(), should not try to get an answer\n
_get_server_answer(), Sollte nicht versuchen eine Antwort zu erhlaten\n
_get_server_answer(), ne doit plus attendre une r�ponse \n
_get_server_answer(), non dovrebbe cercare di ottenere una risposta\n
;
MSG_RATINGS_GETSERVERANSWERCOULDNOTALLOCATEURLPREFIX
_get_server_answer(), could not allocate URL prefix\n
_get_server_answer(), URL-Pr�fix (url_prefix) kann nicht zugeordnet werden
_get_server_answer(), n'a pu allouer le pr�fix de l'URL\n
_get_server_answer(), impossibile allocare il prefisso URL (url_prefix)\n
;
MSG_RATINGS_GETSERVERANSWERCOULDNOTALLOCATEURL
_get_server_answer(), could not allocate URL\n
_get_server_answer(), Konnte URL nicht zuordnen\n
_get_server_answer(), n'a pu allouer l'URL\n
_get_server_answer(), impossibile allocare l'URL\n
;
MSG_RATINGS_GETSERVERANSWERCOULDNOTENCODESPACEINURL
_get_server_answer(), could not encode space in URL\n
_get_server_answer(), Kann Freistelle in URL nicht kodieren\n
_get_server_answer(), n'a pu encoder les espaces dans l'URL\n
_get_server_answer(), impossibile codificare lo spazio nell'URL\n
;
MSG_RATINGS_GETSERVERANSWERCOULDNOTENCODEHASHINURL
_get_server_answer(), could not encode hash in URL\n
_get_server_answer(), Kann Rautenzeichen in URL nicht kodieren\n
_get_server_answer(), n'a pu encoder le hachage dans l'URL\n
_get_server_answer(), impossibile codificare l'hash nell'URL\n
;
MSG_RATINGS_GETSERVERANSWERCOULDNOTALLOCATECUSTOMHEADERS
_get_server_answer(), could not allocate custom headers\n
_get_server_answer(), Benutzerdefinierte Header k�nnen nicht zugeordnet werden\n
_get_server_answer(), n'a pu allouer les en-t�tes particuli�res\n
_get_server_answer(), impossibile allocare le intestazioni personalizzate\n
;
MSG_RATINGS_GETSERVERANSWERCOULDNOTGETANANSWER
_get_server_answer(), could not get an answer\n
_get_server_answer(), Keine Antwort erhalten\n
_get_server_answer(), n'a pu obtenir une r�ponse\n
_get_server_answer(), impossibile ottenere una risposta\n
;
MSG_RATINGS_GETSERVERANSWERCOULDNOTALLOCATEANSWER
_get_server_answer(), could not allocate the answer\n
_get_server_answer(), Kann Antwort nicht zuweisen\n
_get_server_answer(), n'a pu allouer la r�ponse\n
_get_server_answer(), impossibile allocare la risposta\n
;
MSG_SOURCES_SOURCESGETNAMESCANNOTALLOCATEARRAYOFSOURCESNAMES
sources_get_names(), could not allocate array of sources names\n
sources_get_names(), Feld der Quellverzeichnisnamen kann nicht zugewiesen werden\n
sources_get_names(), n'a pu allouer le tableau des noms des sources\n
sources_get_names(), impossibile allocare l'array per i nomi delle fonti\n
;
MSG_SOURCES_SOURCESGETNAMESCANNOTALLOCATESOURCENAME
sources_get_names(), could not allocate source name\n
sources_get_names(), Kann Quellname nicht zuweisen\n
sources_get_names(), n'a pu allouer le nom de la source\n
sources_get_names(), impossibile allocare il nome della fonte\n
;
MSG_SOURCES_SOURCESINITCONNECTIONCANNOTALLOCATECURRENTSOURCENAM
sources_init_connection(), could not allocate current source name\n
sources_init_connection(), Kann aktuellen Quellnamen nicht zuweisen\n
sources_init_connection(), n'a pu allouer le nom de la source courante\n
sources_init_connection(), impossibile allocare il corrente nome della fonte\n
;
MSG_SOURCES_SOURCESINITCONNECTIONUNKNOWNSOURCENAME
sources_init_connection(), unknown source name\n
sources_init_connection(), Unbekannter Quellname\n
sources_init_connection(), nom de source inconnu\n
sources_init_connection(), nome della fonte sconosciuto\n
;
MSG_SOURCES_SOURCESGETROOTDIRECTORYNOSOURCEINITIALISEDYET
sources_get_root_directory(), no source initialised yet\n
sources_get_root_directory(), Noch keine Quelle festgelegt\n
sources_get_root_directory(), pas de source initialis�e encore\n
sources_get_root_directory(), fonte non ancora inizializzata\n
;
MSG_SOURCES_SOURCESGETROOTDIRECTORYUNKNOWNSOURCENAME
sources_get_root_directory(), unknown source name\n
sources_get_root_directory(), Unbekannter Quellname\n
sources_get_root_directory(), nom de source inconnu\n
sources_get_root_directory(), nome della fonte sconosciuto\n
;
MSG_SOURCES_SOURCESGETLISTNOSOURCEINITIALISEDYET
sources_get_list(), no source initialised yet\n
sources_get_list(), Noch keine Quelle festgelegt\n
sources_get_list(), pas de source initialis�e encore\n
sources_get_list(), fonte non ancora inizializzata\n
;
MSG_SOURCES_SOURCESGETLISTFOUNDINMEMORYCACHEDLISTINGOF
sources_get_list(), found in-memory cached listing of "%s"\n
sources_get_list(), Im Speicher-Cache die "%s" Liste gefunden\n
sources_get_list(), a trouv� le contenu de "%s" en m�moire\n
sources_get_list(), trovato nella cache di memoria l'elenco di "%s"\n
;
MSG_SOURCES_SOURCESGETLISTFOUNDONDISKCACHEDLISTINGOF
sources_get_list(), found on-disk cached listing of "%s"\n
sources_get_list(), Im Disk-Cache die "%s" Liste gefunden\n
sources_get_list(), a trouv� le contenu de "%s" sur le disque\n
sources_get_list(), trovato nella cache su disco l'elenco di "%s"\n
;
MSG_SOURCES_SOURCESGETLISTUNKNOWNSOURCENAME
sources_get_list(), unknown source name\n
sources_get_list(), Unbekannter Quellname\n
sources_get_list(), nom de source inconnu\n
sources_get_list(), nome della fonte sconosciuto\n
;
MSG_SOURCES_SOURCESGETLISTCACHINGONDISKLISTINGOF
sources_get_list(), caching on-disk listing of "%s"\n
sources_get_list(), Festplatten-Cache gelistet von "%s"\n
sources_get_list(), mis en cache sur disque de "%s"\n
sources_get_list(), cache su disco dell'elenco di "%s"\n
;
MSG_SOURCES_SOURCESGETLISTCACHINGINMEMORYLISTINGOF
sources_get_list(), caching in-memory listing of "%s"\n
sources_get_list(), Im Speicher-Cache gelistet von "%s"\n
sources_get_list(), mis en cache en m�moire de "%s"\n
sources_get_list(), cache in memoria dell'elenco di "%s"\n
;
MSG_SOURCES_SOURCESPICKFILEZEROORNEGATIVENUMBEROFLINES
sources_pick_file(), zero or negative number of lines\n
sources_pick_file(), Null oder negative Anzahl von Zeilen\n
sources_pick_file(), nombre de lignes nul ou n�gatif\n
sources_pick_file(), zero o numero negativo di linee\n
;
MSG_SOURCES_SOURCESPICKFILEUNRECOGNISEDFORMAT
sources_pick_file(...), unrecognised list format, please delete the content of the cache drawer\n
sources_pick_file(...), Falsches Listenformat, bitte l�schen Sie den Inhalt der Cache-Schublade\n
sources_pick_file(...), mauvais format de liste, veuillez effacer le contenu du tiroir cache\n

;
MSG_SOURCES_SOURCESPICKFILECOULDNOTALLOCATEFILE
sources_pick_file(), could not allocate file\n
sources_pick_file(), Konnte Datei nicht zuordnen\n
sources_pick_file(), n'a pu allouer le fichier\n
sources_pick_file(), impossibile allocare il file\n
;
MSG_SOURCES_SOURCESGETFILENOSOURCEINITIALISEDYET
sources_get_file(), no source initialised yet\n
sources_get_file(), Noch keine Quelle festgelegt\n
sources_get_file(), pas de source initialis�e encore\n
sources_get_file(), fonte non ancora inizializzata\n
;
MSG_SOURCES_SOURCESGETFILECOULDNOTALLOCATEMEMORYFORPATH
sources_get_file(), could not allocate memory for path\n
sources_get_file(), Konnte keinen Speicher f�r Pfad zuordnen\n
sources_get_file(), n'a pu allouer la m�moire pour le chemin\n
sources_get_file(), impossibile allocare la memoria per il percorso\n
;
MSG_SOURCES_SOURCESGETFILEUNKNOWNSOURCENAME
sources_get_file(), unknown source name\n
sources_get_file(), Unbekannter Quellname\n
sources_get_file(), nom de la source inconnu\n
sources_get_file(), nome della fonte sconosciuto\n
;
MSG_SOURCES_SOURCESCLOSECONNECTIONNOSOURCEINITIALISEDYET
sources_close_connection(), no source initialised yet\n
sources_close_connection(), Noch keine Quelle festgelegt\n
sources_close_connection(), pas de source initialis�e encore\n
sources_close_connection(), fonte non ancora inizializzata\n
;
MSG_SOURCES_SOURCESCLOSECONNECTIONNOFTPCONNECTIONINITIALISEDYET
sources_close_connection(), no FTP connection initialised yet\n
sources_close_connection(), Noch keine FTP-Verbindung aufgebaut \n
sources_close_connection(), pas de connexion FTP initialis�e encore\n
sources_close_connection(), nessuna connessione FTP ancora inizializzata\n
;
MSG_SOURCES_SOURCESCLOSECONNECTIONUNKNOWNSOURCENAME
sources_close_connection(), unknown source name "%s"\n
sources_close_connection(), Unbekannter Quellname "%s"\n
sources_close_connection(), nom de source "%s" inconnu\n
sources_close_connection(), fonte del nome "%s" sconosciuta\n
;
MSG_SOURCES_CLEANFTPLISTCOULDNOTALLOCATELIST
_clean_ftp_list(), could not allocate list\n
_clean_ftp_list(), Konnte Liste nicht zuordnen\n
_clean_ftp_list(), n'a pu allouer la liste\n
_clean_ftp_list(), impossibile allocare la lista\n
;
MSG_SOURCES_CLEANFTPLISTCOULDNOTALLOCATENEWLIST
_clean_ftp_list(), could not allocate new list\n
_clean_ftp_list(), Konnte keine neue Liste zuordnen\n
_clean_ftp_list(), n'a pu allouer la nouvelle liste\n
_clean_ftp_list(), impossibile allocare la nuova lista\n
;
MSG_SOURCES_CLEANFTPLISTCOULDNOTALLOCATECLEANLIST
_clean_ftp_list(), could not allocate clean list\n
_clean_ftp_list(), Konnte keine leere Liste zuordnen\n
_clean_ftp_list(), n'a pu allouer la liste propre\n
_clean_ftp_list(), impossibile allocare la lista pulita\n
;
MSG_SOURCES_SOURCESGETLISTCOULDNOTALLOCATELIST
sources_get_list(), could not allocate list\n
sources_get_list(), Konnte Liste nicht zuordnen\n
sources_get_list(), n'a pu allouer la liste\n
sources_get_list(), impossibile allocare la lista\n
;
MSG_SOURCES_SOURCESGETLISTCOULDNOTALLOCATELINE
sources_get_list(), could not allocate line\n
sources_get_list(), Konnte Zeile nicht zuordnen\n
sources_get_list(), n'a pu allouer la ligne\n
sources_get_list(), impossibile allocare la linea\n
;
MSG_SOURCES_RETRIEVELISTFROMDISKCOULDNOTENCODEDIRECTORYNAME
_retrieve_list_from_disk(), could not encode directory name\n
_retrieve_list_from_disk(), Konnte Verzeichnisnamen nicht kodieren\n
_retrieve_list_from_disk(), n'a pu encoder le nom du dossier\n
_retrieve_list_from_disk(), impossibile codificare il nome della directory\n
;
MSG_SOURCES_RETRIEVELISTFROMDISKCOULDNOTALLOCATECACHEPATH
_retrieve_list_from_disk(), could not allocate cache path\n
_retrieve_list_from_disk(), Konnte Cache-Pfad nicht zuweisen\n
_retrieve_list_from_disk(), n'a pu allouer le chemin du cache\n
_retrieve_list_from_disk(), impossibile allocare il percorso della cache\n
;
MSG_SOURCES_RETRIEVELISTFROMDISKCOULDNOTOPENCACHEFILE
_retrieve_list_from_disk(), could not open cache "%s"\n
_retrieve_list_from_disk(), Konnte Cache "%s" nicht �ffnen\n
_retrieve_list_from_disk(), n'a pu ouvrir le cache "%s"\n
_retrieve_list_from_disk(), impossibile aprire la cache del file "%s"\n
;
MSG_SOURCES_RETRIEVELISTFROMDISKCOULDNOTALLOCATELIST
_retrieve_list_from_disk(), could not allocate list\n
_retrieve_list_from_disk(), Konnte Liste nicht zuordnen\n
_retrieve_list_from_disk(), n'a pu allouer la liste\n
_retrieve_list_from_disk(), impossibile allocare la lista\n
;
MSG_SOURCES_RETRIEVELISTFROMDISKCOULDNOTREADCACHEFILE
_retrieve_list_from_disk(), could not read cache "%s"\n
_retrieve_list_from_disk(), Konnte Cache "%s" nicht lesen\n
_retrieve_list_from_disk(), n'a pu lire le cache "%s"\n
_retrieve_list_from_disk(), impossibile leggere la cache del file "%s"\n
;
MSG_SOURCES_CACHELISTONDISKCOULDNOTENCODEDIRECTORYNAME
_cache_list_on_disk(), could not encode directory name\n
_cache_list_on_disk(), Konnte Verzeichnisnamen nicht kodieren\n
_cache_list_on_disk(), n'a pu encoder le nom du dossier\n
_cache_list_on_disk(), impossibile codificare il nome della directory\n
;
MSG_SOURCES_CACHELISTONDISKCOULDNOTALLOCATECACHEPATH
_cache_list_on_disk(), could not allocate cache path\n
_cache_list_on_disk(), Konnte Cache-Pfad nicht zuordnen\n
_cache_list_on_disk(), n'a pu allouer le chemin du cache\n
_cache_list_on_disk(), impossibile allocare il percorso della cache\n
;
MSG_SOURCES_CACHELISTONDISKCOULDNOTCREATECACHEFILE
_cache_list_on_disk(), could not create cache "%s"\n
_cache_list_on_disk(), Konnte Cache "%s" nicht erstellen\n
_cache_list_on_disk(), n'a pu cr�er le cache "%s"\n
_cache_list_on_disk(), impossibile creare la cache del file "%s"\n
;
MSG_SOURCES_CACHELISTONDISKCOULDNOTWRITECACHEFILE
_cache_list_on_disk(), could not write cache "%s"\n
_cache_list_on_disk(), Konnte Cache "%s" nicht beschreiben\n
_cache_list_on_disk(), n'a pu �crire le cache "%s"\n
_cache_list_on_disk(), impossibile scrivere la cache del file "%s"\n
;
MSG_SOURCES_RETRIEVELISTFROMMEMORYCOULDNOTENCODEDIRECTORYNAME
_retrieve_list_from_memory(), could not encode directory name\n
_retrieve_list_from_memory(), Konnte Verzeichnisnamen nicht kodieren\n
_retrieve_list_from_memory(), n'a pu encoder le nom du dossier\n
_retrieve_list_from_memory(), impossibile codificare il nome della directory\n
;
MSG_SOURCES_RETRIEVELISTFROMMEMORYCOULDNOTALLOCATECACHEPATH
_retrieve_list_from_memory(), could not allocate cache path\n
_retrieve_list_from_memory(), Konnte Cache-Pfad nicht zuordnen\n
_retrieve_list_from_memory(), n'a pu allouer le chemine du cache\n
_retrieve_list_from_memory(), impossibile allocare il percorso della cache\n
;
MSG_SOURCES_CACHELISTINMEMORYCOULDNOTENCODEDIRECTORYNAME
_cache_list_in_memory(), could not encode directory name\n
_cache_list_in_memory(), Konnte Verzeichnisnamen nicht kodieren\n
_cache_list_in_memory(), n'a pu encoder le nom du dossier\n
_cache_list_in_memory(), impossibile codificare il nome della directory\n
;
MSG_SOURCES_CACHELISTINMEMORYCOULDNOTALLOCATECACHEPATH
_cache_list_in_memory(), could not allocate cache path\n
_cache_list_in_memory(), KOnnte Cache-Pfad nicht zuordnen\n
_cache_list_in_memory(), n'a pu allouer le chemin du cache\n
_cache_list_in_memory(), impossibile allocare il percorso della cache\n
;
MSG_SOURCES_LISTEXISTSONDISKCOULDNOTALLOCATECACHEPATH1
_list_exists_on_disk(), could not allocate cache path 1\n
_list_exists_on_disk(), Cache-Pfad 1 konnte nicht zugewiesen werden\n
_list_exists_on_disk(), n'a pu allouer le chemin du cache 1\n
_list_exists_on_disk(), impossibile allocare il percorso della cache 1\n
;
MSG_SOURCES_LISTEXISTSONDISKCOULDNOTALLOCATECACHEPATH2
_list_exists_on_disk(), could not allocate cache path 2\n
_list_exists_on_disk(), Cache-Pfad 2 konnte nicht zugewiesen werden\n
_list_exists_on_disk(), n'a pu allouer le chemin du cache 2\n
_list_exists_on_disk(), impossibile allocare il percorso della cache 2\n
;
MSG_SOURCES_LISTEXISTSONDISKCOULDNEITHERFINDNORCREATECACHEDIREC
_list_exists_on_disk(), could neither find nor create cache directory "%s"\n
_list_exists_on_disk(), Konnte das Cache-Verzeichnnis "%s" weder finden noch erstellen\n
_list_exists_on_disk(), n'a pu ni trouver ni cr�er le dossier du cache "%s"\n
_list_exists_on_disk(), impossibile trovare o creare la directory della cache "%s"\n
;
MSG_SOURCES_ENCODEDIRECTORYNAMECOULDNOTALLOCATEENCODEDDIRECTORY
_encode_directory_name(), could not allocate encoded directory-name\n
_encode_directory_name(), Konnte kodierten Verzeichnisnamen nicht zuweisen\n
_encode_directory_name(), n'a pu allouer le nom du dossier encod�\n
_encode_directory_name(), impossibile allocare il nome-directory codificato\n
;
MSG_UTILS_IOREADLINECOULDNOTALLOCATEMEMORY
io_read_line(), could not allocate memory\n
io_read_line(), Konnte Speicher nicht zuweisen\n
io_read_line(), n'a pu allouer la m�moire\n
io_read_line(), impossibile allocare la memoria\n
;
MSG_UTILS_IOREADLINECOULDNOTREALLOCATEMEMORY
io_read_line(), could not reallocate memory\n
io_read_line(), Konnte Speicher nicht neu zuweisen\n
io_read_line(), n'a pu r�allouer la m�moire\n
io_read_line(), impossibile riallocare la memoria\n
;
MSG_UTILS_IOCOPYFILECOULDNOTOPENSOURCEFILE
io_copy_file(), could not open source file\n
io_copy_file(), Konnte Quelldatei nicht �ffnen\n
io_copy_file(), n'a pu ouvrir le fichier d'origine\n
io_copy_file(), impossibile aprire la fonte del file\n
;
MSG_UTILS_IOCOPYFILERECEIVEDMALFORMEDSOURCEFILEPATH
io_copy_file(), received malformed source-file path "%s"\n
io_copy_file(), Fehlerhafter Pfad zur Quelldatei "%s" empfangen\n
io_copy_file(), a re�u un chemin du fichier d'origine mal-form� "%s"\n
io_copy_file(), ricevuto percorso della fonte del file malformato "%s"\n
;
MSG_UTILS_IOCOPYFILECOULDNOTALLOCATETARGETFILEPATH
io_copy_file(), could not allocate target-file path\n
io_copy_file(), Konnte Pfad der Zieldatei nicht zuordnen\n
io_copy_file(), n'a pu allouer le chemin du fichier cible\n
io_copy_file(), impossibile allocare il percorso del file target\n
;
MSG_UTILS_IOCOPYFILECOULDNOTOPENDESTINATIONFILE
io_copy_file(), could not open destination file\n
io_copy_file(), Konnte Zieldatei nicht �ffnen\n
io_copy_file(), n'a pu ouvrir le fichier de destination\n
io_copy_file(), impossibile aprire il file di destinazione\n
;
MSG_UTILS_IOCOPYFILECOULDNOTCOPYFILE
io_copy_file(), could not copy file\n
io_copy_file(), Konnte Datei nicht kopieren\n
io_copy_file(), n'a pu copier le fichier\n
io_copy_file(), impossibile copiare il file\n
;
MSG_XAD_EXTRACTARCHIVECOULDNOTALLOCATEMEMORY
extract_archive(), could not allocate memory\n
extract_archive(), Konnte Speicher nicht zuweisen\n
extract_archive(), n'a pu allouer la m�moire\n
extract_archive(), impossibile allocare la memoria\n
;
MSG_XAD_EXTRACTARCHIVECOULDNOTOPENXADMASTERLIBRARYV11
extract_archive(), could not open xadmaster.library v11+\n
extract_archive(), Konnte xadmaster.library v11+ nicht �ffnen\n
extract_archive(), n'a pu ouvrir xadmaster.library v11+\n
extract_archive(), impossibile aprire la libreria xadmaster.library v11+\n
;
MSG_XAD_EXTRACTARCHIVECOULDNOTALLOCATESTRUCTXADARCHIVEINFO
extract_archive(), could not allocate information\n
extract_archive(), Konnte Information nicht zuordnen\n
extract_archive(), n'a pu allouer l'information\n
extract_archive(), impossibile allocare la struttura xadArchiveInfo\n
;
MSG_XAD_EXTRACTARCHIVECOULDNOTCOMPLETETHECALLTOXADGETINFO
extract_archive(), could not obtain information\n
extract_archive(), Konnte keine Information erhalten\n
extract_archive(), n'a pu obtenir l'information\n
extract_archive(), impossibile completare la chiamata a xadGetInfo()\n
;
MSG_XAD_EXTRACTARCHIVECOULDALLOCATEMEMORY
extract_archive(), could allocate memory\n
extract_archive(), Konnte Speicher nicht zuweisen\n
extract_archive(), n'a pu allouer la m�moire\n
extract_archive(), pu� allocare la memoria\n
;
MSG_XAD_EXTRACTARCHIVECOULDNOTCOMPLETETHECALLTOXADFILEUNARC
extract_archive(), could not unarchive the file\n
extract_archive(), Konnte Datei nicht dekomprimieren\n
extract_archive(), n'a pu d�sarchiver le fichier\n
extract_archive(), impossibile completare la chiamata a xadFileUnArc()\n
;
MSG_XAD_EXTRACTARCHIVECOULDREALLOCATEMEMORY
extract_archive(), could reallocate memory\n
extract_archive(), Konnte Speicher neu zuweisen\n
extract_archive(), n'a pu r�allouer la m�moire\n
extract_archive(), pu� riallocare la memoria\n
;
MSG_XAD_EXTRACTARCHIVECOULDNOTTRIMLIST
extract_archive(), could not trim list\n
extract_archive(), Konnte die Liste nicht k�rzen\n
extract_archive(), n'a pu rogner la liste\n
extract_archive(), impossibile rifinire la lista\n
