/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include "controls_rea.h"

#include "globals.h"
#include "controls.h"
#include "controls_workers.h"
#include "globals.h"
#include "gui_rea.h"
#include "locale.h"
#include "log.h"
#include "prefs.h"
#include "utils.h"
#include "utils_tasks.h"
#include "z_fortify.h"

#include <reaction/reaction.h>
#include <reaction/reaction_macros.h>
#include <classes/window.h>
#include <gadgets/button.h>
#include <gadgets/clicktab.h>
#include <gadgets/checkbox.h>
#include <gadgets/chooser.h>
#include <gadgets/fuelgauge.h>
#include <gadgets/listbrowser.h>
#include <gadgets/string.h>
#include <proto/layout.h>
#include <proto/listbrowser.h>

#include <exec/ports.h>
#include <intuition/classusr.h>
#include <proto/alib.h>         // For DoMethod()
#include <proto/asl.h>
#include <proto/exec.h>
#include <proto/intuition.h>
#include <stdio.h>              // For FILE, fprintf()...



/* Constants and declarations */

	   int  controls_rea(IN BPTR);
	   void controls_rea_about(void);
	   void controls_rea_play(void);
	   void controls_rea_replay(void);
	   void controls_rea_pause(void);
	   void controls_rea_stop(void);
	   void controls_rea_next(void);
static void _controls_open_list_directories(void);
static void _controls_open_list_banned_directories(void);
static void _controls_open_list_files(void);
static void _controls_open_list_banned_files(void);
static void _controls_save_modules(void);
static void _controls_email_modules(void);
static void _controls_set_user_rating(IN int);
static void _prefs_save_position(void);
static void _prefs_saved_modules_directory_popdrawer(void);
static void _prefs_cache_directory_popdrawer(void);
static void _prefs_update_all(void);
static void _log_save(void);
static void _log_clear(void);
static void _display_current_root_directory(void);
static void _display_current_directory(void);
static void _display_current_file(void);
static void _display_current_modules(void);
static void _display_current_ratings(void);
static void _display_last_message(void);
static void _update_display_commands(void);
static void _print_debug(IN char *);
static void _print_information(IN char *);
static void _print_warning(IN char *);
static void _print_error(IN char *);
static void _push_to_log(IN char *);
static void _enable_gadgets_upon_playing_modules(IN BOOL);
static void _enable_gadgets_for_rating_modules(IN BOOL);
static void _set_page_gadget_attrs(struct Gadget *,	ULONG, ...);
static void _set_progress_max(IN char *, IN int);
static void _set_progress_val(IN char *, IN int);

// Defined as NULL in main.c
extern struct Library       *AslBase;
extern struct GfxBase       *GfxBase;
extern struct Library       *IconBase;
extern struct IntuitionBase *IntuitionBase;

// Defined as NULL in main.c
extern struct ClassLibrary  *BitMapBase;
extern struct ClassLibrary  *ButtonBase;
extern struct ClassLibrary  *CheckBoxBase;
extern struct ClassLibrary  *ChooserBase;
extern struct ClassLibrary  *ClickTabBase;
extern struct ClassLibrary  *FuelGaugeBase;
extern struct ClassLibrary  *LabelBase;
extern struct ClassLibrary  *ListBrowserBase;
extern struct ClassLibrary  *LayoutBase;
extern struct ClassLibrary  *PageBase;
extern struct ClassLibrary  *StringBase;
extern struct ClassLibrary  *WindowBase;



/* Definitions */

	   struct ClassLibrary *VirtualBase               = NULL;

static struct REA_UI       *_ui_object                = NULL;
static int                  _user_rating              = 0;
static int                  _progress_max             = 0;
static int                  _progress_val             = 0;
static BOOL                 _is_directory_list_opened = FALSE;
static BOOL                 _is_file_list_opened      = FALSE;



int controls_rea(IN BPTR path_list)
{
	struct MsgPort *window_port    = NULL;
	ULONG           port_signals   = 0;
	ULONG           window_signals = 0;
	ULONG           signals	       = 0;
	BOOL            is_closing     = FALSE;

	BOOL            is_selected    = FALSE;
	char           *new_directory  = NULL;
	int             new_value      = 0;
	ULONG           return_code    = 0;

	log_print_debug("controls_rea()\n");

	/*
	 * Initialising base utils.
	 */

	if(log_setup(_print_debug, _print_information, _print_warning, _print_error) == RETURN_ERROR)
	{
		log_print_error("controls_rea(), could not setup log\n");
		goto _RETURN_ERROR;
	}

	if(prefs_setup() == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_CONTROLSCOMMONS_CONTROLSINITCOULDNOTINITIALISEPREFERENCES ) );
		goto _RETURN_ERROR;
	}

	/*
	 * Opening the required libraries and the GUI.
	 */

	if((GfxBase = (struct GfxBase *)OpenLibrary("graphics.library", 0)) == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSMUI_CONTROLSMUICOULDNOTOPENGRAPHICSLIBRARYV0 ) );
		goto _RETURN_ERROR;
	}

	if((BitMapBase = (struct ClassLibrary *)OpenLibrary("images/bitmap.image", 47)) == NULL)
	{
		log_print_error("controls_rea(), could not open BitMap.image v47+\n");
		goto _RETURN_ERROR;
	}

	if((ButtonBase = (struct ClassLibrary *)OpenLibrary("gadgets/button.gadget", 47)) == NULL)
	{
		log_print_error("controls_rea(), could not open Button.gadget v47+\n");
		goto _RETURN_ERROR;
	}

	if((CheckBoxBase = (struct ClassLibrary *)OpenLibrary("gadgets/checkbox.gadget", 47)) == NULL)
	{
		log_print_error("controls_rea(), could not open CheckBox.gadget v47+\n");
		goto _RETURN_ERROR;
	}

	if((ChooserBase = (struct ClassLibrary *)OpenLibrary("gadgets/chooser.gadget", 47)) == NULL)
	{
		log_print_error("controls_rea(), could not open Chooser.gadget v47+\n");
		goto _RETURN_ERROR;
	}

	if((ClickTabBase = (struct ClassLibrary *)OpenLibrary("gadgets/clicktab.gadget", 47)) == NULL)
	{
		log_print_error("controls_rea(), could not open ClickTab.gadget v47+\n");
		goto _RETURN_ERROR;
	}

	if((FuelGaugeBase = (struct ClassLibrary *)OpenLibrary("gadgets/fuelgauge.gadget", 47)) == NULL)
	{
		log_print_error("controls_rea(), could not open FuelGauge.gadget v47+\n");
		goto _RETURN_ERROR;
	}

	if((LabelBase = (struct ClassLibrary *)OpenLibrary("images/label.image", 47)) == NULL)
	{
		log_print_error("controls_rea(), could not open Label.image v47+\n");
		goto _RETURN_ERROR;
	}

	if((ListBrowserBase = (struct ClassLibrary *)OpenLibrary("gadgets/listbrowser.gadget", 47)) == NULL)
	{
		log_print_error("controls_rea(), could not open ListBrowser.gadget v47+\n");
		goto _RETURN_ERROR;
	}

	if((LayoutBase = (struct ClassLibrary *)OpenLibrary("gadgets/layout.gadget", 47)) == NULL)
	{
		log_print_error("controls_rea(), could not open Layout.gadget v47+\n");
		goto _RETURN_ERROR;
	}

	if((PageBase = (struct ClassLibrary *)OpenLibrary("gadgets/page.gadget", 37)) == NULL)
	{
		log_print_error("controls_rea(), could not open Page.gadget v37+\n"); // From ClassAct
		goto _RETURN_ERROR;
	}

	if((StringBase = (struct ClassLibrary *)OpenLibrary("gadgets/string.gadget", 47)) == NULL)
	{
		log_print_error("controls_rea(), could not open String.gadget v47+\n");
		goto _RETURN_ERROR;
	}

	if((VirtualBase = (struct ClassLibrary *)OpenLibrary("gadgets/virtual.gadget", 47)) == NULL)
	{
		log_print_error("controls_rea(), could not open Virtual.gadget v47+\n");
		goto _RETURN_ERROR;
	}

	if((WindowBase = (struct ClassLibrary *)OpenLibrary("window.class", 47)) == NULL)
	{
		log_print_error("controls_rea(), could not open Window.class v47+\n");
		goto _RETURN_ERROR;
	}

	window_port = CreateMsgPort();
	if(window_port == NULL)
	{
		log_print_error("controls_rea(), could not create window port\n");
		goto _RETURN_ERROR;
	}

	if(gui_rea_init_ui(window_port, &_ui_object) == RETURN_ERROR)
	{
		log_print_error("controls_rea(), could not create UI\n");
		goto _RETURN_ERROR;
	}
	
	/*
	 * Initialising the controls.
	 */

	if(controls_setup(
		path_list,
		_set_progress_max,
		_set_progress_val) == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_CONTROLSMUI_CONTROLSMUICOULDNOTINITIALISECONTROLS ) );
		goto _RETURN_ERROR;
	}

	// Make sure that everything
	// is displayed after starting.
	_enable_gadgets_upon_playing_modules(FALSE);
	_enable_gadgets_for_rating_modules(FALSE);
	_display_current_root_directory();
	// Should be like MUI:
	//	_commands_update_current_root_directory();
	//	_commands_update_current_directory();

	/*
	 * The main loop.
	 */

	if(prefs_should_play_on_startup() == TRUE)
	{
		controls_rea_next();
	}

	port_signals = (1L << window_port->mp_SigBit);
	GetAttr(
		WINDOW_SigMask,
		_ui_object->WINDOW_object,
		&window_signals);
	while(!is_closing)
	{
		signals = Wait(SIGBREAKF_CTRL_C |
					   controls_signal_control_playing_stopped() |
					   controls_workers_signal_control_display_last_message() |
					   controls_workers_signal_control_fetching_modules() |
					   controls_workers_signal_control_modules_rated() |
					   controls_workers_signal_control_modules_downloaded() |
					   port_signals |
					   window_signals);
		
		if((signals & SIGBREAKF_CTRL_C) == SIGBREAKF_CTRL_C)
		{
			is_closing = TRUE;
		}

		if((signals & controls_signal_control_playing_stopped()) == controls_signal_control_playing_stopped())
		{
			controls_rea_next();
		}

		if((signals & controls_workers_signal_control_display_last_message()) == controls_workers_signal_control_display_last_message())
		{
			_display_last_message();
		}
	
		if((signals & controls_workers_signal_control_fetching_modules()) == controls_workers_signal_control_fetching_modules())
		{
			_enable_gadgets_upon_playing_modules(FALSE);
			_enable_gadgets_for_rating_modules(FALSE);

			if(prefs_should_show_log_page_on_fetch() == TRUE &&
			   _ui_object->window != NULL)
			{
				SetGadgetAttrs(
					(struct Gadget *)_ui_object->CLICKTAB_object,
					_ui_object->window,
					NULL,
					CLICKTAB_Current,
					2,
					TAG_DONE);
				RefreshGadgets(
					(struct Gadget *)_ui_object->CLICKTAB_object,
					_ui_object->window,
					NULL);
			}

			_update_display_commands();

			utils_tasks_send_back_ack();
		}

		if((signals & controls_workers_signal_control_modules_rated()) == controls_workers_signal_control_modules_rated())
		{
			_enable_gadgets_for_rating_modules(TRUE);
			_display_current_ratings();

			// Stopping current module if appropriate
			if(prefs_should_skip_if_user_rating_low() == TRUE &&
			   _user_rating < LOWEST_PLAYABLE_RATE)
			{
				controls_ban_current_file();
				controls_rea_stop();
				controls_rea_next();
			}
		}

		if((signals & controls_workers_signal_control_modules_downloaded()) == controls_workers_signal_control_modules_downloaded())
		{
			_enable_gadgets_upon_playing_modules(TRUE);
			_enable_gadgets_for_rating_modules(TRUE);

			if(prefs_should_show_log_page_on_fetch() == TRUE &&
			   _ui_object->window != NULL)
			{
				SetGadgetAttrs(
					(struct Gadget *)_ui_object->CLICKTAB_object,
					_ui_object->window,
					NULL,
					CLICKTAB_Current,
					0,
					TAG_DONE);
				RefreshGadgets(
					(struct Gadget *)_ui_object->CLICKTAB_object,
					_ui_object->window,
					NULL);
			}

			_update_display_commands();

			utils_tasks_send_back_ack();
		}

		while((return_code = RA_HandleInput(_ui_object->WINDOW_object, NULL)) != WMHI_LASTMSG)
		{
			switch(return_code & WMHI_CLASSMASK)
			{
			case WMHI_CLOSEWINDOW:
				is_closing = TRUE;
				break;

			case WMHI_CHANGEWINDOW:
				_prefs_save_position();
				break;

			case WMHI_ICONIFY:
				if(_ui_object->window != NULL)
				{
					DoMethod(
						_ui_object->WINDOW_object,
						WM_ICONIFY,
						NULL);
					_ui_object->window = NULL;
				}
				break;

			case WMHI_UNICONIFY:
				_ui_object->window = RA_OpenWindow(_ui_object->WINDOW_object);
				if(_ui_object->window == NULL)
				{
					log_print_error("gui_rea_init_ui(), could not create window");
					goto _RETURN_ERROR;
				}

				GetAttr(
					WINDOW_SigMask,
					_ui_object->WINDOW_object,
					&window_signals);
				_update_display_commands();
				break;

			case WMHI_GADGETUP:
				switch(return_code & WMHI_GADGETMASK)
				{
				case CTRL_ABOUT:
					controls_rea_about();
					break;

				case CTRL_PLAY:
					controls_rea_play();
					break;

				case CTRL_REPLAY:
					controls_rea_replay();
					break;

				case CTRL_PAUSE:
					controls_rea_pause();
					break;

				case CTRL_STOP:
					controls_rea_stop();
					break;

				case CTRL_NEXT:
					controls_rea_next();
					break;

				case CTRL_SOURCE_RESET:
					// The fetcher should step aside and wait
					controls_workers_modules_fetcher_wait();

					controls_set_current_root_directory(NULL);
					_update_display_commands();
					break;

				case CTRL_SOURCE_PREV:
					// The fetcher should step aside and wait
					controls_workers_modules_fetcher_wait();

					controls_set_current_source_index(controls_get_prev_source_index());
					controls_set_current_root_directory(NULL);
					_update_display_commands();
					break;

				case CTRL_SOURCE_NEXT:
					// The fetcher should step aside and wait
					controls_workers_modules_fetcher_wait();

					controls_set_current_source_index(controls_get_next_source_index());
					controls_set_current_root_directory(NULL);
					_update_display_commands();
					break;

				case CTRL_DIRECTORY:
					// The fetcher should step aside and wait
					controls_workers_modules_fetcher_wait();

					new_directory = ((struct StringInfo *)((_ui_object->STR_controls_directory)->SpecialInfo))->Buffer;
					controls_set_current_root_directory(new_directory);
					controls_rea_next();
					_update_display_commands();
					break;

				case CTRL_DIRECTORY_SET:
					// The fetcher should step aside and wait
					controls_workers_modules_fetcher_wait();

					controls_set_current_root_directory(controls_get_current_directory());
					_update_display_commands();
					break;

				case CTRL_DIRECTORY_LIST:
					_controls_open_list_directories();
					break;

				case CTRL_DIRECTORY_BAN:
					// The fetcher should step aside and wait
					controls_workers_modules_fetcher_wait();

					controls_ban_current_directory();
					_update_display_commands();

					if(prefs_should_skip_if_banned())
					{
						controls_workers_modules_fetcher_fetch();
					}
					break;

				case CTRL_DIRECTORY_BAN_LIST:
					_controls_open_list_banned_directories();
					break;

				case CTRL_FILE_LIST:
					_controls_open_list_files();
					break;

				case CTRL_FILE_BAN:
					// The fetcher should step aside and wait
					controls_workers_modules_fetcher_wait();

					controls_ban_current_file();
					_update_display_commands();

					if(prefs_should_skip_if_banned())
					{
						controls_workers_modules_fetcher_fetch();
					}
					break;

				case CTRL_FILE_BAN_LIST:
					_controls_open_list_banned_files();
					break;

				case CTRL_MODULES_SAVE:
					_controls_save_modules();
					break;

				case CTRL_MODULES_MAIL:
					_controls_email_modules();
					break;

				case CTRL_USER_RATING_ONE:
					_controls_set_user_rating(1);
					break;

				case CTRL_USER_RATING_TWO:
					_controls_set_user_rating(2);
					break;

				case CTRL_USER_RATING_THREE:
					_controls_set_user_rating(3);
					break;

				case CTRL_USER_RATING_FOUR:
					_controls_set_user_rating(4);
					break;

				case CTRL_USER_RATING_FIVE:
					_controls_set_user_rating(5);
					break;

				case PREFS_SAVE_PREFERENCES_ON_EXIT:
					is_selected = !prefs_should_save_prefs_on_exit();
					prefs_set_save_prefs_on_exit(is_selected);
					break;

				case PREFS_PLAY_ON_STARTUP:
					is_selected = !prefs_should_play_on_startup();
					prefs_set_play_on_startup(is_selected);
					break;

				case PREFS_SHOW_LOG_PAGE_ON_FETCH:
					is_selected = !prefs_should_show_log_page_on_fetch();
					prefs_set_show_log_page_on_fetch(is_selected);
					break;

				case PREFS_CHOOSE_MAXIMUM_LOG_SIZES:
					GetAttr(
						CHOOSER_Selected,
						_ui_object->CHR_prefs_choose_maximum_log_sizes,
						(ULONG *)&new_value);
					prefs_set_maximum_log_size_index(new_value);

					// Do not forget to allocate the
					// log content with the new size
					new_value = atoi(prefs_get_list_of_maximum_log_sizes()[prefs_get_maximum_log_size_index()]) * BYTES_IN_ONE_KILO_BYTE; /* Flawfinder: ignore */
					// TODO But what does this mean with nodes in a ListBrowser?
					/* From MUI:
					_object->LV_label_log_content = realloc(
						_object->LV_label_log_content,
						length * sizeof(char));
					memset(_object->LV_label_log_content, 0, length * sizeof(char));
					SetAttrs(
						_object->LV_label_log,
						MUIA_Floattext_Text, _object->LV_label_log_content,
						TAG_DONE);
					*/
					break;

				case PREFS_REMEMBER_SOURCE_BETWEEN_RUNS:
					is_selected = !prefs_should_remember_source_between_runs();
					prefs_set_remember_source_between_runs(is_selected);
					break;

				case PREFS_CHOOSE_IMAGES_SET:
					GetAttr(
						CHOOSER_Selected,
						_ui_object->CHR_prefs_choose_images_set,
						(ULONG *)&new_value);
					prefs_set_current_images_set_index(new_value);
					prefs_set_images_directory(prefs_get_list_of_images_sets()[prefs_get_current_images_set_index()]);
					break;

				case PREFS_SAVED_MODULES_DIRECTORY_STRING:
					GetAttr(
						STRINGA_TextVal,
						_ui_object->STR_prefs_saved_module_directory,
						(ULONG *)&new_directory);
					prefs_set_saved_modules_directory(new_directory);
					break;

				case PREFS_SAVED_MODULES_DIRECTORY_POPDRAWER:
					_prefs_saved_modules_directory_popdrawer();
					break;

				case PREFS_CACHE_DIRECTORIES_MODULES_LISTS_DURING_RUN:
					is_selected = !prefs_should_cache_list_during_run();
					prefs_set_cache_list_during_run(is_selected);
					break;

				case PREFS_CACHE_DIRECTORIES_MODULES_LISTS_BETWEEN_RUNS:
					is_selected = !prefs_should_cache_list_between_runs();
					prefs_set_cache_list_between_runs(is_selected);
					break;

				case PREFS_CACHE_DIRECTORY_STRING:
					GetAttr(
						STRINGA_TextVal,
						_ui_object->STR_prefs_cache_directory,
						(ULONG *)&new_directory);
					prefs_set_cache_directory(new_directory);
					break;

				case PREFS_CACHE_DIRECTORY_POPDRAWER:
					_prefs_cache_directory_popdrawer();
					break;

				case PREFS_SKIP_MODULES_WHEN_BANNED:
					is_selected = !prefs_should_skip_if_banned();
					prefs_set_skip_if_banned(is_selected);
					break;

				case PREFS_SKIP_MODULES_WHEN_RATING_LOW:
					is_selected = !prefs_should_skip_if_user_rating_low();
					prefs_set_skip_if_user_rating_low(is_selected);
					break;

				case PREFS_SKIP_MODULES_WHEN_ALREADY_RATED:
					is_selected = !prefs_should_skip_if_all_rating_exists();
					prefs_set_skip_if_all_rating_exists(is_selected);
					break;

				case PREFS_SKIP_MODULES_WHEN_ALREADY_RATED_TOO_LOW:
					is_selected = !prefs_should_skip_if_all_rating_low();
					prefs_set_skip_if_all_rating_low(is_selected);
					break;

				case PREFS_CHOOSE_MAXIMUM_FILE_SIZES:
					GetAttr(
						CHOOSER_Selected,
						_ui_object->CHR_prefs_choose_maximum_file_size,
						(ULONG *)&new_value);
					prefs_set_maximum_file_size_index(new_value);
					break;

				case PREFS_SAVE:
					prefs_save_preferences();
					// Reopen UI?
					log_print_information("Preferences saved\n");
					break;

				case PREFS_RESET:
					prefs_reset_preferences();
					_prefs_update_all();
					log_print_information("Preferences reset\n");
					break;

				case PREFS_DEFAULT:
					prefs_default_preferences();
					_prefs_update_all();
					log_print_information("Preferences reset to default\n");
					break;

				case LOG_SAVE:
					_log_save();
					break;

				case LOG_CLEAR:
					_log_clear();
					break;
				}
				break;
			}
		}
	}

	/*
	 * Closing everything.
	 */

	goto _RETURN_OK;
	_RETURN_OK:
		gui_rea_close_ui(window_port, &_ui_object);
		CloseLibrary((struct Library *)BitMapBase);
		BitMapBase = NULL;
		CloseLibrary((struct Library *)ButtonBase);
		ButtonBase = NULL;
		CloseLibrary((struct Library *)CheckBoxBase);
		CheckBoxBase = NULL;
		CloseLibrary((struct Library *)ChooserBase);
		ChooserBase = NULL;
		CloseLibrary((struct Library *)ClickTabBase);
		ClickTabBase = NULL;
		CloseLibrary((struct Library *)FuelGaugeBase);
		FuelGaugeBase = NULL;
		CloseLibrary((struct Library *)LabelBase);
		LabelBase = NULL;
		CloseLibrary((struct Library *)ListBrowserBase);
		ListBrowserBase = NULL;
		CloseLibrary((struct Library *)LayoutBase);
		LayoutBase = NULL;
		CloseLibrary((struct Library *)PageBase);
		PageBase = NULL;
		CloseLibrary((struct Library *)StringBase);
		StringBase = NULL;
		CloseLibrary((struct Library *)VirtualBase);
		VirtualBase = NULL;
		CloseLibrary((struct Library *)WindowBase);
		WindowBase = NULL;
		CloseLibrary((struct Library *)GfxBase);
		GfxBase = NULL;
		controls_cleanup();
		prefs_cleanup();
		log_cleanup();
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		gui_rea_close_ui(window_port, &_ui_object);
		if(BitMapBase != NULL)
		{
			CloseLibrary((struct Library *)BitMapBase);
			BitMapBase = NULL;
		}
		if(ButtonBase != NULL)
		{
			CloseLibrary((struct Library *)ButtonBase);
			ButtonBase = NULL;
		}
		if(CheckBoxBase != NULL)
		{
			CloseLibrary((struct Library *)CheckBoxBase);
			CheckBoxBase = NULL;
		}
		if(ChooserBase != NULL)
		{
			CloseLibrary((struct Library *)ChooserBase);
			ChooserBase = NULL;
		}
		if(ClickTabBase != NULL)
		{
			CloseLibrary((struct Library *)ClickTabBase);
			ClickTabBase = NULL;
		}
		if(FuelGaugeBase != NULL)
		{
			CloseLibrary((struct Library *)FuelGaugeBase);
			FuelGaugeBase = NULL;
		}
		if(LabelBase != NULL)
		{
			CloseLibrary((struct Library *)LabelBase);
			LabelBase = NULL;
		}
		if(ListBrowserBase != NULL)
		{
			CloseLibrary((struct Library *)ListBrowserBase);
			ListBrowserBase = NULL;
		}
		if(LayoutBase != NULL)
		{
			CloseLibrary((struct Library *)LayoutBase);
			LayoutBase = NULL;
		}
		if(PageBase != NULL)
		{
			CloseLibrary((struct Library *)PageBase);
			PageBase = NULL;
		}
		if(StringBase != NULL)
		{
			CloseLibrary((struct Library *)StringBase);
			StringBase = NULL;
		}
		if(VirtualBase != NULL)
		{
			CloseLibrary((struct Library *)VirtualBase);
			VirtualBase = NULL;
		}
		if(WindowBase != NULL)
		{
			CloseLibrary((struct Library *)WindowBase);
			WindowBase = NULL;
		}
		if(GfxBase != NULL)
		{
			CloseLibrary((struct Library *)GfxBase);
			GfxBase	= NULL;
		}
		controls_cleanup();
		prefs_cleanup();
		log_cleanup();
		return RETURN_ERROR;
}

void controls_rea_about(
	 void)
{
	controls_display_version();
	_update_display_commands();
}

void controls_rea_play(
	 void)
{
	if(controls_are_modules_loaded() == FALSE)
	{
		controls_rea_next();
	}
	else if(controls_are_modules_playing() == FALSE)
	{
		controls_play_modules();
		_update_display_commands();
	}
}

void controls_rea_replay(
	 void)
{
	if(controls_are_modules_loaded() == TRUE)
	{
		controls_replay_modules();
		_update_display_commands();
	}
}

void controls_rea_pause(
	 void)
{
	if(controls_are_modules_loaded()  == TRUE &&
	   controls_are_modules_playing() == TRUE)
	{
		controls_pause_modules();
		_update_display_commands();
	}
}

void controls_rea_stop(
	 void)
{
	// The fetcher should step aside and wait
	controls_workers_modules_fetcher_wait();

	if(controls_are_modules_loaded() == TRUE)
	{
		controls_remove_modules();
		_update_display_commands();

		// Also, some gadgets should be disabled
		_enable_gadgets_upon_playing_modules(FALSE);
		_enable_gadgets_for_rating_modules(FALSE);
	}
}

void controls_rea_next(
	 void)
{
	controls_workers_modules_fetcher_fetch();
}

static void _controls_open_list_directories(
	void)
{
	if(_ui_object->WIN_directories_list == NULL)
	{
		gui_rea_child_window_list_open(
			_ui_object,
			controls_get_current_directory_list(),
			"LBR_directories_list",
			 GetString( MSG_CONTROLSMUI_DIRECTORYCONTENT ) ,
			&_ui_object->LBR_directories_list,
			&_ui_object->WIN_directories_list);
	}
	else
	{
		DoMethod(
			_ui_object->WIN_directories_list,
			WM_CLOSE);
		DoMethod(
			_ui_object->WINDOW_object,
			OM_REMMEMBER,
			_ui_object->WIN_directories_list);
		_ui_object->WIN_directories_list = NULL;
	}
}

static void _controls_open_list_banned_directories(
	void)
{
	char *list = NULL;

	if(_ui_object->WIN_directories_ban_list == NULL)
	{
		controls_get_blacklisted_directories_list(&list);

		gui_rea_child_window_list_open(
			_ui_object,
			list,
			"LBR_directories_ban_list",
			 GetString( MSG_CONTROLSMUI_BANNEDDIRECTORIES ) ,
			&_ui_object->LBR_directories_ban_list,
			&_ui_object->WIN_directories_ban_list);

		free(list);
	}
	else
	{
		DoMethod(
			_ui_object->WIN_directories_ban_list,
			WM_CLOSE);
		DoMethod(
			_ui_object->WINDOW_object,
			OM_REMMEMBER,
			_ui_object->WIN_directories_ban_list);
		_ui_object->WIN_directories_ban_list = NULL;
	}
}

static void _controls_open_list_files(
	void)
{
	if(_ui_object->WIN_files_list == NULL)
	{
		gui_rea_child_window_list_open(
			_ui_object,
			controls_get_current_file_list(),
			"LBR_files_list",
			 GetString( MSG_CONTROLSMUI_FILECONTENT ) ,
			&_ui_object->LBR_files_list,
			&_ui_object->WIN_files_list);
	}
	else
	{
		DoMethod(
			_ui_object->WIN_files_list,
			WM_CLOSE);
		DoMethod(
			_ui_object->WINDOW_object,
			OM_REMMEMBER,
			_ui_object->WIN_files_list);
		_ui_object->WIN_files_list = NULL;
	}
}

static void _controls_open_list_banned_files(
	void)
{
	char *list = NULL;

	if(_ui_object->WIN_files_ban_list == NULL)
	{
		controls_get_blacklisted_files_list(&list);

		gui_rea_child_window_list_open(
			_ui_object,
			list,
			"LBR_files_ban_list",
			 GetString( MSG_CONTROLSMUI_BANNEDFILES ) ,
			&_ui_object->LBR_files_ban_list,
			&_ui_object->WIN_files_ban_list);

		free(list);
	}
	else
	{
		DoMethod(
			_ui_object->WIN_files_ban_list,
			WM_CLOSE);
		DoMethod(
			_ui_object->WINDOW_object,
			OM_REMMEMBER,
			_ui_object->WIN_files_ban_list);
		_ui_object->WIN_files_ban_list = NULL;
	}
}

static void _controls_save_modules(
	void)
{
	int                   length               = 0;
	char                 *default_directory    = NULL;
	struct FileRequester *request              = NULL;
	char                 *current_modules_temp = NULL;
	char                 *token                = NULL;

	if((AslBase = OpenLibrary("asl.library", 37)) == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSMUI_MUICONTROLSMODULESSAVECOULDNOTOPENASLLIBRARY ) );
		goto _RETURN_ERROR;
	}

	if((request = (struct FileRequester *)AllocAslRequestTags(
		ASL_FileRequest,
		ASLFR_DrawersOnly,     TRUE,
		ASLFR_InitialLeftEdge, 20,
		ASLFR_InitialTopEdge,  20,
		ASLFR_InitialWidth,    300,
		ASLFR_InitialHeight,   350,
		ASLFR_InitialDrawer,   prefs_get_saved_modules_directory(),
		ASLFR_PositiveText,    GetString( MSG_CONTROLSMUI_SAVE ) ,
		ASL_Hail,              GetString( MSG_CONTROLSMUI_SAVEDIRECTORY ) ,
		(struct TagItem *)TAG_DONE)) == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSMUI_MUICONTROLSMODULESSAVECOULDNOTALLOCATEDIRECTORYREQUESTER ) );
		goto _RETURN_ERROR;
	}

	if(AslRequest(request, NULL) == TRUE)
	{
		length               = strlen(request->rf_Dir) + 1;
		default_directory    = malloc((length + 1) * sizeof(char));
		if(default_directory == NULL)
		{
			log_print_error( GetString( MSG_CONTROLSMUI_MUICONTROLSMODULESSAVECOULDNOTALLOCATEDIRECTORYNAME ) );
			goto _RETURN_ERROR;
		}
		strcpy(default_directory, request->rf_Dir);
		if(!string_end_with(default_directory, GLOBALS_SEPARATOR_PATHS) &&
		   !string_end_with(default_directory, GLOBALS_SEPARATOR_VOLUMES))
		{
			strncat(default_directory, GLOBALS_SEPARATOR_PATHS, length - strlen(default_directory) - 1);
		}
		prefs_set_saved_modules_directory(default_directory);

		current_modules_temp = string_duplicate(controls_get_current_file_list());
		token = strtok(current_modules_temp, "\n");
		while(token != NULL)
		{
			if(io_file_copy(token, prefs_get_saved_modules_directory()) == RETURN_ERROR)
			{
				log_print_error( GetString( MSG_CONTROLSMUI_SAVECURRENTMODULESCOULDNOTCOPYFILE ) );
				goto _RETURN_ERROR;
			}
			token = strtok(NULL, "\n");
		}
	}

	goto _RETURN_OK;
	_RETURN_OK:
		// Not freeing token because it is freed with current_modules_temp
		free(default_directory);
		default_directory = NULL;
		free(current_modules_temp);
		current_modules_temp = NULL;
		FreeAslRequest(request);
		CloseLibrary(AslBase);
		log_print_information( GetString( MSG_CONTROLSMUI_MODULESSAVED ) );
		return;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		// Not freeing token because it is freed with current_modules_temp
		if(default_directory != NULL)
		{
			free(default_directory);
			default_directory = NULL;
		}
		if(current_modules_temp != NULL)
		{
			free(current_modules_temp);
			current_modules_temp = NULL;
		}
		if(request != NULL)
		{
			FreeAslRequest(request);
		}
		if(AslBase != NULL)
		{
			CloseLibrary(AslBase);
		}
		log_print_information( GetString( MSG_CONTROLSMUI_MODULESNOTSAVED ) );
		return;
}

static void _controls_email_modules(
	void)
{
	log_print_information("Sorry, this feature is not yet implemented in this UI\n");
	// TODO
}

static void _controls_set_user_rating(
	IN int rating)
{
	_enable_gadgets_for_rating_modules(FALSE);

	_user_rating = rating;
	controls_set_current_user_rating(_user_rating);
}

static void _prefs_save_position(
	void)
{
	ULONG x = 0;
	ULONG y = 0;

	GetAttr(
		WA_Left,
		_ui_object->WINDOW_object,
		(ULONG *)&x);
	GetAttr(
		WA_Top,
		_ui_object->WINDOW_object,
		(ULONG *)&y);

	prefs_set_main_window_X(x);
	prefs_set_main_window_Y(y);
}

static void _prefs_saved_modules_directory_popdrawer(
	void)
{
	struct FileRequester *request = NULL;

	if((AslBase = OpenLibrary("asl.library", 37)) == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSMUI_MUILOGSAVECOULDNOTOPENASLLIBRARY ) );
		goto _RETURN_ERROR;
	}

	if((request = (struct FileRequester *)AllocAslRequestTags(
		ASL_FileRequest,
		ASLFR_DrawersOnly,     TRUE,
		ASLFR_InitialLeftEdge, 20,
		ASLFR_InitialTopEdge,  20,
		ASLFR_InitialWidth,    300,
		ASLFR_InitialHeight,   350,
		ASLFR_InitialDrawer,   prefs_get_saved_modules_directory(),
		ASLFR_PositiveText,    GetString( MSG_CONTROLSMUI_SAVE ) ,
		ASL_Hail,              "Saved modules directory",
		TAG_DONE)) == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSMUI_MUILOGSAVECOULDNOTALLOCATEDIRECTORYREQUESTER ) );
		goto _RETURN_ERROR;
	}

	if(AslRequest(request, NULL) == TRUE)
	{
		prefs_set_saved_modules_directory(request->rf_Dir);
		_set_page_gadget_attrs(
			_ui_object->STR_prefs_saved_module_directory,
			STRINGA_TextVal, prefs_get_saved_modules_directory(),
			TAG_DONE);
	}

	goto _RETURN_OK;
	_RETURN_OK:
		FreeAslRequest(request);
		CloseLibrary(AslBase);
		return;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(request != NULL)
		{
			FreeAslRequest(request);
		}
		if(AslBase != NULL)
		{
			CloseLibrary(AslBase);
		}
		return;
}

static void _prefs_cache_directory_popdrawer(
	void)
{
	struct FileRequester *request = NULL;

	if((AslBase = OpenLibrary("asl.library", 37)) == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSMUI_MUILOGSAVECOULDNOTOPENASLLIBRARY ) );
		goto _RETURN_ERROR;
	}

	if((request = (struct FileRequester *)AllocAslRequestTags(
		ASL_FileRequest,
		ASLFR_DrawersOnly,     TRUE,
		ASLFR_InitialLeftEdge, 20,
		ASLFR_InitialTopEdge,  20,
		ASLFR_InitialWidth,    300,
		ASLFR_InitialHeight,   350,
		ASLFR_InitialDrawer,   prefs_get_cache_directory(),
		ASLFR_PositiveText,    GetString( MSG_CONTROLSMUI_SAVE ) ,
		ASL_Hail,              "Cache directory",
		TAG_DONE)) == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSMUI_MUILOGSAVECOULDNOTALLOCATEDIRECTORYREQUESTER ) );
		goto _RETURN_ERROR;
	}

	if(AslRequest(request, NULL) == TRUE)
	{
		prefs_set_cache_directory(request->rf_Dir);
		_set_page_gadget_attrs(
			_ui_object->STR_prefs_cache_directory,
			STRINGA_TextVal, prefs_get_cache_directory(),
			TAG_DONE);
	}

	goto _RETURN_OK;
	_RETURN_OK:
		FreeAslRequest(request);
		CloseLibrary(AslBase);
		return;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(request != NULL)
		{
			FreeAslRequest(request);
		}
		if(AslBase != NULL)
		{
			CloseLibrary(AslBase);
		}
		return;
}

static void _prefs_update_all(void)
{
	// Misc.
	_set_page_gadget_attrs(
		_ui_object->CKB_prefs_save_prefs_on_exit,
		CHECKBOX_Checked, prefs_should_save_prefs_on_exit(),
		TAG_DONE);

	_set_page_gadget_attrs(
		_ui_object->CKB_prefs_play_on_startup,
		CHECKBOX_Checked, prefs_should_play_on_startup(),
		TAG_DONE);

	_set_page_gadget_attrs(
		_ui_object->CKB_prefs_show_log_page_on_fetch,
		CHECKBOX_Checked, prefs_should_show_log_page_on_fetch(),
		TAG_DONE);

	_set_page_gadget_attrs(
		_ui_object->CKB_prefs_remember_source_between_runs,
		CHECKBOX_Checked, prefs_should_remember_source_between_runs(),
		TAG_DONE);

	// Modules directory
	_set_page_gadget_attrs(
		_ui_object->STR_prefs_saved_module_directory,
		STRINGA_TextVal, prefs_get_saved_modules_directory(),
		TAG_DONE);

	// Cache
	_set_page_gadget_attrs(
		_ui_object->CKB_prefs_should_cache_list_during_run,
		CHECKBOX_Checked, prefs_should_cache_list_during_run(),
		TAG_DONE);

	_set_page_gadget_attrs(
		_ui_object->CKB_prefs_should_cache_list_between_runs,
		CHECKBOX_Checked, prefs_should_cache_list_between_runs(),
		TAG_DONE);

	_set_page_gadget_attrs(
		_ui_object->STR_prefs_cache_directory,
		STRINGA_TextVal, prefs_get_cache_directory(),
		TAG_DONE);

	// Images
	_set_page_gadget_attrs(
		_ui_object->CHR_prefs_choose_images_set,
		CHOOSER_Selected, prefs_get_current_images_set_index(),
		TAG_DONE);

	// Ratings
	_set_page_gadget_attrs(
		_ui_object->CKB_prefs_should_skip_if_banned,
		CHECKBOX_Checked, prefs_should_skip_if_banned(),
		TAG_DONE);

	_set_page_gadget_attrs(
		_ui_object->CKB_prefs_should_skip_if_user_rating_low,
		CHECKBOX_Checked, prefs_should_skip_if_user_rating_low(),
		TAG_DONE);

	_set_page_gadget_attrs(
		_ui_object->CKB_prefs_should_skip_if_all_rating_exists,
		CHECKBOX_Checked, prefs_should_skip_if_all_rating_exists(),
		TAG_DONE);

	_set_page_gadget_attrs(
		_ui_object->CKB_prefs_should_skip_if_all_rating_low,
		CHECKBOX_Checked, prefs_should_skip_if_all_rating_low(),
		TAG_DONE);

	// Maximum file size
	_set_page_gadget_attrs(
		_ui_object->CHR_prefs_choose_maximum_file_size,
		CHOOSER_Selected, prefs_get_maximum_file_size_index(),
		TAG_DONE);
}

static void _log_save(
	void)
{
	int                   length    = 0;
	char                 *file_name = NULL;
	struct FileRequester *request   = NULL;
	FILE                 *file      = NULL;
	time_t                raw_time  = 0;
	struct tm            *time_info = NULL;
	struct List          *list      = NULL;
	struct Node          *node      = NULL;
	char                 *message   = NULL;

	if((AslBase = OpenLibrary("asl.library", 37)) == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSMUI_MUILOGSAVECOULDNOTOPENASLLIBRARY ) );
		goto _RETURN_ERROR;
	}

	if((request = (struct FileRequester *)AllocAslRequestTags(
		ASL_FileRequest,
		ASLFR_DrawersOnly, TRUE,
		ASLFR_InitialLeftEdge, 20,
		ASLFR_InitialTopEdge, 20,
		ASLFR_InitialWidth, 300,
		ASLFR_InitialHeight, 350,
		ASLFR_InitialDrawer, "RAM:",
		ASLFR_PositiveText,  GetString( MSG_CONTROLSMUI_SAVE ) ,
		ASL_Hail,  GetString( MSG_CONTROLSMUI_SAVELOG ) ,
		(struct TagItem *)TAG_DONE)) == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSMUI_MUILOGSAVECOULDNOTALLOCATEDIRECTORYREQUESTER ) );
		goto _RETURN_ERROR;
	}

	if(AslRequest(request, NULL))
	{
		length       = strlen(request->rf_Dir) + 1 + strlen(GLOBALS_LOG_FILE_NAME);
		file_name    = malloc((length + 1) * sizeof(char));
		if(file_name == NULL)
		{
			log_print_error("mui_log_save(), could not allocate file name\n");
			goto _RETURN_ERROR;
		}
		strcpy(file_name, request->rf_Dir);
		if(!string_end_with(file_name, GLOBALS_SEPARATOR_PATHS) &&
		   !string_end_with(file_name, GLOBALS_SEPARATOR_VOLUMES))
		{
			strcat(file_name, GLOBALS_SEPARATOR_PATHS);
		}
		strcat(file_name, GLOBALS_LOG_FILE_NAME);

		file = fopen(file_name, "a");     /* Flawfinder: ignore */
		if(file == NULL)
		{
			file = fopen(file_name, "w"); /* Flawfinder: ignore */
			if(file == NULL)
			{
				log_print_error( GetString( MSG_CONTROLSMUI_MUILOGSAVECOULDNOTOPENLOGFILE ) );
				goto _RETURN_ERROR;
			}
		}

		time(&raw_time);
		time_info = localtime(&raw_time);
		fprintf(file, "------------------------\n%s------------------------\n", asctime(time_info));
		GetAttr(
			LISTBROWSER_Labels,
			_ui_object->LBR_log_list,
			(ULONG *)&list);
		for(node = list->lh_Head; node->ln_Succ != NULL ; node = node->ln_Succ)
		{
			GetListBrowserNodeAttrs(
				node,
				LBNCA_Text, &message,
				TAG_END);
			fprintf(file, "%s\n", message);
		}
		fprintf(file, "\n");
	}

	goto _RETURN_OK;
	_RETURN_OK:
		free(file_name);
		fclose(file);
		FreeAslRequest(request);
		CloseLibrary(AslBase);
		return;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(file_name != NULL)
		{
			free(file_name);
		}
		if(file != NULL)
		{
			fclose(file);
		}
		if(request != NULL)
		{
			FreeAslRequest(request);
		}
		if(AslBase != NULL)
		{
			CloseLibrary(AslBase);
		}
		return;
}

static void _log_clear(
	void)
{
	struct List *list      = NULL;
	struct Node *node      = NULL;
	char        *node_text = NULL;

	GetAttr(
		LISTBROWSER_Labels,
		_ui_object->LBR_log_list,
		(ULONG *)&list);

	_set_page_gadget_attrs(
		_ui_object->LBR_log_list,
		LISTBROWSER_Labels, ~0,
		TAG_DONE);

	for(node = list->lh_Head; node->ln_Succ != NULL ; node = node->ln_Succ)
	{
		// Tygre 24/07/07: Freeing messages
		// I don't forget to get the messages
		// from the log and to free them...
		GetListBrowserNodeAttrs(
			node,
			LBNA_Column, 0,
			LBNCA_Text,  &node_text);
		free(node_text);
		Remove(node);
		FreeListBrowserNode(node);
	}
	NewList(list);

	_set_page_gadget_attrs(
		_ui_object->LBR_log_list,
		LISTBROWSER_Labels, list,
		TAG_DONE);
}

static void _display_current_root_directory(void)
{
	int   length = 0;
	char *string = NULL;

	length    = strlen(controls_get_current_source()) + strlen(controls_get_current_root_directory());
	string    = malloc((length + 1) * sizeof(char));
	if(string == NULL)
	{
		log_print_error( GetString( MSG_CONTROLSMUI_ERRORINDISPLAYCURRENTROOTDIRECTORYCOULDNOTALLOC ) );
		return;
	}
	strcpy(string, controls_get_current_source());
	strcat(string, controls_get_current_root_directory());

	_set_page_gadget_attrs(
		_ui_object->STR_controls_root_directory,
		STRINGA_TextVal,
		string,
		TAG_DONE);

	free(string);
	string = NULL;
}

static void _display_current_directory(void)
{
	_set_page_gadget_attrs(
		_ui_object->STR_controls_directory,
		STRINGA_TextVal,
		controls_get_current_directory(),
		TAG_DONE);
}

static void _display_current_file(void)
{
	_set_page_gadget_attrs(
		_ui_object->STR_controls_file,
		STRINGA_TextVal,
		controls_get_current_file(),
		TAG_DONE);
}

static void _display_current_modules(void)
{
	char *current_modules_display = string_replace_all(controls_get_current_file_list(), "\n", ", ");

	if(string_end_with(current_modules_display, ", "))
	{
		current_modules_display[strlen(current_modules_display) - 2] = '\0';
	}

	_set_page_gadget_attrs(
		_ui_object->STR_controls_modules,
		STRINGA_TextVal,
		current_modules_display,
		TAG_DONE);

	free(current_modules_display);
	current_modules_display = NULL;
}

static void _display_current_ratings(void)
{
	int  i        = 0;
	char tally[4] = "NNN";

	// Setting user's rating
	for(i = 0; i < controls_get_current_user_rating(); i++)
	{
		_set_page_gadget_attrs(
			_ui_object->BTN_controls_user_rating[i],
			BUTTON_BackgroundPen, 3,
			TAG_DONE);
	}

	// Clearing remaining user's rating buttons
	for(i = controls_get_current_user_rating(); i < 5; i++)
	{
		_set_page_gadget_attrs(
			_ui_object->BTN_controls_user_rating[i],
			BUTTON_BackgroundPen, 0,
			TAG_DONE);
	}

	// Setting community's rating
	for(i = 0; i < controls_get_current_all_rating(); i++)
	{
		_set_page_gadget_attrs(
			_ui_object->BTN_controls_all_rating[i],
			BUTTON_BackgroundPen, 6,
			TAG_DONE);
	}

	// Clearing remaining community's rating buttons
	for(i = controls_get_current_all_rating(); i < 5; i++)
	{
		_set_page_gadget_attrs(
			_ui_object->BTN_controls_all_rating[i],
			BUTTON_BackgroundPen, 0,
			TAG_DONE);
	}

	// Setting community's tally
	string_snprintf(tally, 4, "%3d", controls_get_current_all_tally());
	_set_page_gadget_attrs(
		_ui_object->BTN_controls_all_tally,
		GA_Text, tally,
		TAG_DONE);
}

static void _display_last_message(void)
{
	char *temp = NULL;

	// Simple optimisation to avoid some flickering.
	GetAttr(
		STRINGA_TextVal,
		_ui_object->STR_controls_last_message,
		(ULONG *)&temp);
	if(temp == NULL || strlen(temp) == 0 ||                      // If there's no previous message
	   string_equal(temp, controls_get_last_message()) == FALSE) // Or the new message is different
	{
		SetAttrs(
			_ui_object->STR_controls_last_message,
			STRINGA_TextVal,
			controls_get_last_message(),
			TAG_DONE);
		if(_ui_object->window != NULL)
		{
			RefreshGadgets(
				_ui_object->STR_controls_last_message,
				_ui_object->window,
				NULL);
		}
	}

	SetAttrs(
		_ui_object->FGA_controls_progress_bar,
		FUELGAUGE_Max,
		_progress_max,
		FUELGAUGE_Level,
		_progress_val,
		TAG_DONE);
	if(_ui_object->window != NULL)
	{
		RefreshGadgets(
			_ui_object->FGA_controls_progress_bar,
			_ui_object->window,
			NULL);
	}
}

static void _update_display_commands(void)
{
	_display_current_root_directory();
	_display_current_directory();
	_display_current_file();
	_display_current_modules();
	_display_current_ratings();
	_display_last_message();
}

static void _print_debug(
	IN char *message)
{
	if(_ui_object == NULL)
	{
		printf(message); /* Flawfinder: ignore */
	}
	else
	{
		_push_to_log(message);
	}
}

static void _print_information(
	IN char *message)
{
	if(_ui_object == NULL)
	{
		printf(message); /* Flawfinder: ignore */
		printf("\n");
	}
	else
	{
		controls_build_last_message(message);
		_push_to_log(message);
	}
}

static void _print_warning(
	IN char *message)
{
	if(_ui_object == NULL)
	{
		printf(message); /* Flawfinder: ignore */
		printf("\n");
	}
	else
	{
		controls_build_last_message(message);
		_push_to_log(message);
	}
}

static void _print_error(
	IN char *message)
{
	if(_ui_object == NULL)
	{
		fprintf(stderr, message); /* Flawfinder: ignore */
		fprintf(stderr, "\n");
	}
	else
	{
		controls_build_last_message(message);
		_push_to_log(message);
	}
}

static void _push_to_log(
	IN char *message)
{
	struct List *list   = NULL;
	struct Node *node   = NULL;
	int          length = 0;
	char        *string = NULL;

	length = strlen(message);
	string = malloc(length * sizeof(char));
	if(string == NULL)
	{
		log_print_error("controls_rea(), could not allocate the temporary message!\n");
		return;
	}
	string[0] = '\0';
	strncat(string, message, length - 1);

	// TODO Honour maximum_log_size = atoi(prefs_get_list_of_maximum_log_sizes()[prefs_get_maximum_log_size_index()]) * BYTES_IN_ONE_KILO_BYTE;

	GetAttr(
		LISTBROWSER_Labels,
		_ui_object->LBR_log_list,
		(ULONG *)&list);

	_set_page_gadget_attrs(
		_ui_object->LBR_log_list,
		LISTBROWSER_Labels, ~0,
		TAG_DONE);

	if((node = (struct Node *)AllocListBrowserNode(
		1,
		LBNA_Column, 0,
		LBNCA_Text,  string,
		TAG_DONE)) != NULL)
	{
		AddTail(list, node);
	}

	_set_page_gadget_attrs(
		_ui_object->LBR_log_list,
		LISTBROWSER_Labels,   list,
		LISTBROWSER_Position, LBP_BOTTOM,
		TAG_DONE);

	// I want to show that last message in the controle page, if displayed
	// I do the showing into the main loop to avoid any complaining about
	// drawing being done in a different task than the main task.
	utils_tasks_send_signal_without_ack(
		utils_tasks_get_main_task(),
		controls_workers_signal_control_display_last_message());

	goto _RETURN_OK;
	_RETURN_OK:
		return;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		return;
}

static void _enable_gadgets_upon_playing_modules(
	IN BOOL enabled)
{
	// Directories
	/*
	// Even if nothing is playing, these should be enabled.
	_set_page_gadget_attrs(
		_ui_object->STR_directory,
		GA_Disabled, !enabled,
		TAG_DONE);
	_set_page_gadget_attrs(
		_ui_object->IM_directory_set_as_root,
		GA_Disabled, !enabled,
		TAG_DONE);
	if(_is_directory_list_opened)
	{
		_set_page_gadget_attrs(
			_ui_object->BTN_controls_directory_list,
			GA_Disabled, TRUE,
			TAG_DONE);
	}
	else
	{
		_set_page_gadget_attrs(
			_ui_object->BTN_controls_directory_list,
			GA_Disabled, !enabled,
			TAG_DONE);
	}
	*/
	_set_page_gadget_attrs(
		_ui_object->BTN_controls_directory_ban,
		GA_Disabled, !enabled,
		TAG_DONE);

	// Files
	/*
	// Even if nothing is playing, these should be enabled.
	if(_is_file_list_opened)
	{
		_set_page_gadget_attrs(
			_ui_object->BTN_controls_file_list,
			GA_Disabled, TRUE,
			TAG_DONE);
	}
	else
	{
		_set_page_gadget_attrs(
			_ui_object->BTN_controls_file_list,
			GA_Disabled, !enabled,
			TAG_DONE);
	}
	*/
	_set_page_gadget_attrs(
		_ui_object->BTN_controls_file_ban,
		GA_Disabled, !enabled,
		TAG_DONE);

	// Modules
	_set_page_gadget_attrs(
		_ui_object->BTN_controls_modules_save,
		GA_Disabled, !enabled,
		TAG_DONE);
	_set_page_gadget_attrs(
		_ui_object->BTN_controls_modules_email,
		GA_Disabled, !enabled,
		TAG_DONE);
}

static void _enable_gadgets_for_rating_modules(
	IN BOOL enabled)
{
	int i = 0;

	for(i = 0; i < 5; i++)
	{
		// Reset
		_set_page_gadget_attrs(
			_ui_object->BTN_controls_user_rating[i],
			BUTTON_BackgroundPen, 0,
			GA_Disabled,   !enabled,
			TAG_DONE);
		_set_page_gadget_attrs(
			_ui_object->BTN_controls_all_rating[i],
			BUTTON_BackgroundPen, 0,
			TAG_DONE);
	}

	_set_page_gadget_attrs(
		_ui_object->BTN_controls_all_tally,
		GA_Text, "   0",
		TAG_DONE);
}

static void _set_page_gadget_attrs(
	struct Gadget *gadget,
	ULONG tag1,
	...)
{
	if(_ui_object->window != NULL &&
	   SetPageGadgetAttrsA(
		gadget,
		_ui_object->CLICKTAB_pages,
		_ui_object->window,
		NULL,
		(struct TagItem *)&tag1) != 0)
	{
		RefreshPageGadget(
			gadget,
			_ui_object->CLICKTAB_pages,
			_ui_object->window,
			NULL);
	}
}

static void _set_progress_max(
	IN char *origin,
	IN int   value)
{
	_progress_val = 0;
	if(value > 0)
	{
		_progress_max = value;
	}
	else
	{
		_progress_max = 400;
	}

	// I want to show that last message in the controle page, if displayed
	// I do the showing into the main loop to avoid MUI complaining about
	// MUI_Redraw() being called in a different task than the main task.
	utils_tasks_send_signal_without_ack(
		utils_tasks_get_main_task(),
		controls_workers_signal_control_display_last_message());
}

static void _set_progress_val(
	IN char *origin,
	IN int   value)
{
	_progress_val += value;

	// I want to show that last message in the controle page, if displayed
	// I do the showing into the main loop to avoid MUI complaining about
	// MUI_Redraw() being called in a different task than the main task.
	utils_tasks_send_signal_without_ack(
		utils_tasks_get_main_task(),
		controls_workers_signal_control_display_last_message());

	if(_progress_val > _progress_max)
	{
		_progress_val = 0;
	}
}


