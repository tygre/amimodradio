/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



#ifndef PLAYERS_H
#define PLAYERS_H 1

#include <dos/dos.h> // For BPTR

#include "utils.h"   // For IN and OUT

int   players_setup(
	  IN BPTR path_list);

int   players_init_connection(
	  void);

char *players_get_player_name(
	  void);

BOOL  players_can_play_module(
	  IN char *file_name);

int   players_load_modules(
	  IN char *files_path,
	  IN char *list);

int   players_play_modules(
	  void);

int   players_replay_modules(
	  void);

int   players_pause_modules(
	  void);

int   players_remove_modules(
	  void);

void  players_close_connection(
	  void);

void  players_cleanup(
	  void);

BOOL  players_check_player_playing(
	  void);

#endif
