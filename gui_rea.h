/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



#ifndef GUI_REA_H
#define GUI_REA_H 1

#include <exec/ports.h>
#include <intuition/classusr.h>

#include "utils.h"

enum
{
	CTRL_ABOUT = 1,
	CTRL_PLAY,
	CTRL_REPLAY,
	CTRL_PAUSE,
	CTRL_STOP,
	CTRL_NEXT,
	CTRL_SOURCE_RESET,
	CTRL_SOURCE_PREV,
	CTRL_SOURCE_NEXT,
	CTRL_DIRECTORY,
	CTRL_DIRECTORY_SET,
	CTRL_DIRECTORY_LIST,
	CTRL_DIRECTORY_BAN,
	CTRL_DIRECTORY_BAN_LIST,
	CTRL_FILE_LIST,
	CTRL_FILE_BAN,
	CTRL_FILE_BAN_LIST,
	CTRL_MODULES_SAVE,
	CTRL_MODULES_MAIL,
	CTRL_USER_RATING_ONE,
	CTRL_USER_RATING_TWO,
	CTRL_USER_RATING_THREE,
	CTRL_USER_RATING_FOUR,
	CTRL_USER_RATING_FIVE,

	PREFS_SAVE_PREFERENCES_ON_EXIT,
	PREFS_PLAY_ON_STARTUP,
	PREFS_SHOW_LOG_PAGE_ON_FETCH,
	PREFS_CHOOSE_MAXIMUM_LOG_SIZES,
	PREFS_REMEMBER_SOURCE_BETWEEN_RUNS,
	PREFS_CHOOSE_IMAGES_SET,
	PREFS_SAVED_MODULES_DIRECTORY_STRING,
	PREFS_SAVED_MODULES_DIRECTORY_POPDRAWER,
	PREFS_CACHE_DIRECTORIES_MODULES_LISTS_DURING_RUN,
	PREFS_CACHE_DIRECTORIES_MODULES_LISTS_BETWEEN_RUNS,
	PREFS_CACHE_DIRECTORY_STRING,
	PREFS_CACHE_DIRECTORY_POPDRAWER,
	PREFS_SKIP_MODULES_WHEN_BANNED,
	PREFS_SKIP_MODULES_WHEN_RATING_LOW,
	PREFS_SKIP_MODULES_WHEN_ALREADY_RATED,
	PREFS_SKIP_MODULES_WHEN_ALREADY_RATED_TOO_LOW,
	PREFS_SKIP_MODULES_IF_ABOVE_MAXIMUM_FILE_SIZES,
	PREFS_CHOOSE_MAXIMUM_FILE_SIZES,
	PREFS_SAVE,
	PREFS_RESET,
	PREFS_DEFAULT,

	LOG_SAVE,
	LOG_CLEAR,

	BUTTON_LAST
};

struct REA_UI
{
	struct Window *window;
	Object        *WINDOW_object;
	Object        *CLICKTAB_object;
	Object        *CLICKTAB_pages;

	struct Gadget *STR_controls_root_directory;
	struct Gadget *STR_controls_directory;
	struct Gadget *BTN_controls_directory_list;
	struct Gadget *BTN_controls_directory_ban;
	struct Gadget *STR_controls_file;
	struct Gadget *BTN_controls_file_list;
	struct Gadget *BTN_controls_file_ban;
	struct Gadget *STR_controls_modules;
	struct Gadget *BTN_controls_modules_save;
	struct Gadget *BTN_controls_modules_email;
	struct Gadget *BTN_controls_user_rating[5];
	struct Gadget *BTN_controls_all_rating[5];
	struct Gadget *BTN_controls_all_tally;
	struct Gadget *STR_controls_last_message;
	struct Gadget *FGA_controls_progress_bar;

	struct Gadget *CKB_prefs_save_prefs_on_exit;
	struct Gadget *CKB_prefs_play_on_startup;
	struct Gadget *CKB_prefs_show_log_page_on_fetch;
	struct Gadget *CHR_prefs_choose_maximum_log_sizes;
	struct Gadget *CKB_prefs_remember_source_between_runs;
	struct Gadget *CHR_prefs_choose_images_set;
	struct Gadget *STR_prefs_saved_module_directory;
	struct Gadget *BTN_prefs_saved_module_directory;
	struct Gadget *CKB_prefs_should_cache_list_during_run;
	struct Gadget *CKB_prefs_should_cache_list_between_runs;
	struct Gadget *STR_prefs_cache_directory;
	struct Gadget *BTN_prefs_cache_directory;
	struct Gadget *CKB_prefs_should_skip_if_banned;
	struct Gadget *CKB_prefs_should_skip_if_user_rating_low;
	struct Gadget *CKB_prefs_should_skip_if_all_rating_exists;
	struct Gadget *CKB_prefs_should_skip_if_all_rating_low;
	struct Gadget *CKB_prefs_should_skip_if_above_maximum_file_size;
	struct Gadget *CHR_prefs_choose_maximum_file_size;
	struct Gadget *LBR_log_list;

	struct Gadget *LBR_directories_list;
	Object        *WIN_directories_list;
	struct Gadget *LBR_directories_ban_list;
	Object        *WIN_directories_ban_list;
	struct Gadget *LBR_files_list;
	Object        *WIN_files_list;
	struct Gadget *LBR_files_ban_list;
	Object        *WIN_files_ban_list;
};

int  gui_rea_init_ui(
	 IN  struct MsgPort  *port,
	 OUT struct REA_UI  **ui_object);

void gui_rea_close_ui(
	 IN struct MsgPort  *port,
	 IN  struct REA_UI **ui_object);

void gui_rea_child_window_list_open(
	 IN  struct REA_UI  *ui_object,
	 IN  char           *list,
	 IN  char           *help,
	 IN  char           *title,
	 OUT struct Gadget **list_object,
	 OUT Object        **window_object);

#endif

