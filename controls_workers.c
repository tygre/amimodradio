/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include "controls_workers.h"

#include "controls.h"
#include "globals.h"
#include "locale.h"
#include "log.h"
#include "prefs.h"
#include "ratings.h"
#include "utils.h"
#include "utils_socketbase.h" // Sockets and such from Roadshow SDK, with task-specific SocketBase and data
#include "utils_tasks.h"
#include "z_fortify.h"

#include <stdio.h>
#include <stdlib.h>
#include <dos/dos.h>          // For RETURN_OK, RETURN_ERROR
#include <dos/dostags.h>      // For the tags for CreateNewProcTags()
#include <proto/dos.h>        // For CreateNewProcTags()
#include <proto/exec.h>       // For OpenLibrary() and CloseLibrary()



/* Constants and declarations */

#define DOWNLOADER_NAME  "AmiModRadio Downloader"
#define FETCHER_NAME     "AmiModRadio Fetcher"
#define RATER_NAME       "AmiModRadio Rater"

	   int   controls_workers_setup(IN BOOL (*)(void), IN void (*)(IN BOOL), IN void (*)(IN char *, IN int), IN void (*)(IN char *, IN int));
	   int   controls_workers_cleanup(void);
	   ULONG controls_workers_signal_control_display_last_message(void);
	   int   controls_workers_modules_fetcher_start(void);
	   void  controls_workers_modules_fetcher_fetch(void);
	   void  controls_workers_modules_fetcher_wait(void);
	   void  controls_workers_modules_fetcher_stop(void);
	   ULONG controls_workers_signal_control_fetching_modules(void);
static void  _modules_fetcher_loop(void);
	   int   controls_workers_modules_rater_start(void);
	   void  controls_workers_modules_rater_stop(void);
	   ULONG controls_workers_signal_control_modules_rated(void);
static void  _modules_rater_loop(void);
	   int   controls_workers_modules_downloader_start(IN char *, IN char *, IN int);
	   void  controls_workers_modules_downloader_stop(void);
	   ULONG controls_workers_signal_control_modules_downloaded(void);
static void  _modules_downloader_loop(void);



/* Definitions */

static BOOL				(*_continue_long_operation_getter_fp)()        = NULL;
static void				(*_continue_long_operation_setter_fp)(IN BOOL) = NULL;
static void				(*_data_size_callback_fp)(
							IN char *origin,
							IN int number_of_bytes)                    = NULL; // Tygre 22/12/19: To update a progress bar when necessary
static void				(*_data_read_callback_fp)(
							IN char *origin,
							IN int number_of_bytes)                    = NULL; // Tygre 22/12/19: To update a progress bar when necessary

static struct Task     *_modules_fetcher_task                          = NULL;
static struct Task     *_modules_rater_task                            = NULL;
static struct Task     *_modules_downloader_task                       = NULL;

// TODO To remove, using appropriate controls_XXX() function
static /*@only@*/ char *_download_directory                            = NULL;
static /*@only@*/ char *_download_file                                 = NULL;
static int              _download_file_size                            = 0;

static ULONG			_SIGNAL_TO_CONTROL_DISPLAY_LAST_MESSAGE        = -1;
static ULONG			_SIGNAL_TO_CONTROL_FETCHING_MODULES            = -1;
static ULONG			_SIGNAL_TO_CONTROL_MODULES_RATED               = -1;
static ULONG			_SIGNAL_TO_CONTROL_MODULES_DOWNLOADED          = -1;
static ULONG            _SIGNALSET_TO_CONTROL_DISPLAY_LAST_MESSAGE     = -1;
static ULONG            _SIGNALSET_TO_CONTROL_FETCHING_MODULES         = -1;
static ULONG            _SIGNALSET_TO_CONTROL_MODULES_RATED            = -1;
static ULONG            _SIGNALSET_TO_CONTROL_MODULES_DOWNLOADED       = -1;



int controls_workers_setup(
	IN BOOL (*continue_long_operation_getter_fp)(),
	IN void (*continue_long_operation_setter_fp)(IN BOOL),
	IN void (*data_size_callback_fp)(IN char *origin, IN int number_of_bytes),
	IN void (*data_read_callback_fp)(IN char *origin, IN int number_of_bytes))
{
	log_print_debug("controls_workers_setup()\n");

	_continue_long_operation_getter_fp = continue_long_operation_getter_fp;
	_continue_long_operation_setter_fp = continue_long_operation_setter_fp;
	_data_size_callback_fp          = data_size_callback_fp;
	_data_read_callback_fp          = data_read_callback_fp;

	if(utils_tasks_setup() == RETURN_ERROR)
	{
		log_print_error("controls_workers_setup(), could not setup tasks\n");
		return RETURN_ERROR;
	}
	
	if(utils_tasks_allocate_signal(&_SIGNAL_TO_CONTROL_DISPLAY_LAST_MESSAGE, &_SIGNALSET_TO_CONTROL_DISPLAY_LAST_MESSAGE, "_SIGNAL_TO_CONTROL_DISPLAY_LAST_MESSAGE") == RETURN_ERROR ||
	   utils_tasks_allocate_signal(&_SIGNAL_TO_CONTROL_FETCHING_MODULES,     &_SIGNALSET_TO_CONTROL_FETCHING_MODULES,     "_SIGNAL_TO_CONTROL_FETCHING_MODULES")     == RETURN_ERROR ||
	   utils_tasks_allocate_signal(&_SIGNAL_TO_CONTROL_MODULES_RATED,        &_SIGNALSET_TO_CONTROL_MODULES_RATED,        "_SIGNAL_TO_CONTROL_MODULES_RATED")        == RETURN_ERROR ||
	   utils_tasks_allocate_signal(&_SIGNAL_TO_CONTROL_MODULES_DOWNLOADED,   &_SIGNALSET_TO_CONTROL_MODULES_DOWNLOADED,   "_SIGNAL_TO_CONTROL_MODULES_DOWNLOADED")   == RETURN_ERROR)
	{
		log_print_error("controls_workers_setup(), could not allocate signals\n");
		return RETURN_ERROR;
	}

	return RETURN_OK;
}

int	controls_workers_cleanup(void)
{
	log_print_debug("controls_workers_cleanup()\n");

	utils_tasks_free_signal(_SIGNAL_TO_CONTROL_DISPLAY_LAST_MESSAGE);
	utils_tasks_free_signal(_SIGNAL_TO_CONTROL_FETCHING_MODULES);
	utils_tasks_free_signal(_SIGNAL_TO_CONTROL_MODULES_RATED);
	utils_tasks_free_signal(_SIGNAL_TO_CONTROL_MODULES_DOWNLOADED);

	utils_tasks_cleanup();

	return RETURN_OK;
}

ULONG controls_workers_signal_control_display_last_message(void)
{
	return _SIGNALSET_TO_CONTROL_DISPLAY_LAST_MESSAGE;
}

int controls_workers_modules_fetcher_start(void)
{
	if(_modules_fetcher_task != NULL)
	{
		log_print_warning("controls_workers_modules_fetcher_start(), modules fetcher already started\n");
		return RETURN_WARN;
	}

	// Allow long operations
	_continue_long_operation_setter_fp(TRUE);

	_modules_fetcher_task = (struct Task *)CreateNewProcTags(
		NP_Entry,       &_modules_fetcher_loop,
		NP_Priority,    1,
		NP_Name,        FETCHER_NAME,
		NP_Output,      Output(),
		NP_CloseOutput, FALSE,
		NP_StackSize,   GLOBALS_TASK_STACK_SIZE,
		NP_CopyVars,    FALSE,
		TAG_DONE);

	if(_modules_fetcher_task == NULL)
	{
		log_print_error("controls_workers_modules_fetcher_start(), could not create modules fetcher\n");
		return RETURN_ERROR;
	}

	return RETURN_OK;
}

void controls_workers_modules_fetcher_fetch(void)
{
	if(_modules_fetcher_task == NULL)
	{
		log_print_debug("controls_workers_modules_fetcher_fetch(), modules fetcher not started\n");
		return;
	}

	// Allow long operations
	_continue_long_operation_setter_fp(TRUE);

	utils_tasks_send_signal_without_ack(
		_modules_fetcher_task,
		utils_tasks_signal_task_to_do());
}

void controls_workers_modules_fetcher_wait(void)
{
	if(_modules_fetcher_task == NULL)
	{
		log_print_debug("controls_workers_modules_fetcher_wait(), modules fetcher not started\n");
		return;
	}

	// Interrupt any long operation
	_continue_long_operation_setter_fp(FALSE);

	utils_tasks_send_signal_without_ack(
		_modules_fetcher_task,
		utils_tasks_signal_task_to_wait());
}

void controls_workers_modules_fetcher_stop(void)
{
	if(_modules_fetcher_task == NULL)
	{
		log_print_debug("controls_workers_modules_fetcher_stop(), modules fetcher not started\n");
		return;
	}
	
	// Interrupt any long operation
	_continue_long_operation_setter_fp(FALSE);

	utils_tasks_send_signal_without_ack(
		_modules_fetcher_task,
		utils_tasks_signal_task_to_stop());

	Wait(utils_tasks_signal_control_task_stopped());

	_modules_fetcher_task = NULL;
}

ULONG controls_workers_signal_control_fetching_modules(void)
{
	return _SIGNALSET_TO_CONTROL_FETCHING_MODULES;
}

static void _modules_fetcher_loop(void)
{
	ULONG received_signals = 0;
	int   result           = RETURN_ERROR;

	while(TRUE)
	{
		received_signals = Wait(
			SIGBREAKF_CTRL_C |
			utils_tasks_signal_task_to_do() |
			utils_tasks_signal_task_to_wait() |
			utils_tasks_signal_task_to_stop());

		if((received_signals & utils_tasks_signal_task_to_do()) == utils_tasks_signal_task_to_do())
		{
			result    = controls_remove_modules();
			if(result == RETURN_OK)
			{
				utils_tasks_send_signal_and_wait_ack(
					utils_tasks_get_main_task(),
					controls_workers_signal_control_fetching_modules(),
					utils_tasks_signal_task_to_do());

				result    = controls_fetch_modules();
				if(result == RETURN_OK)
				{
					utils_tasks_send_signal_and_wait_ack(
						utils_tasks_get_main_task(),
						controls_workers_signal_control_modules_downloaded(),
						utils_tasks_signal_task_to_do());
				}
				else
				{
					// What is there to do?
				}
			}
			else
			{
				// What is there to do?
			}
		}

		if((received_signals & utils_tasks_signal_task_to_wait()) == utils_tasks_signal_task_to_wait())
		{
			// Nothing to do
		}

		if((received_signals & utils_tasks_signal_task_to_stop()) == utils_tasks_signal_task_to_stop() ||
		   (received_signals & SIGBREAKF_CTRL_C)            == SIGBREAKF_CTRL_C)
		{
			// Stop completely
			break;
		}
	}

	utils_tasks_send_signal_without_ack(
		utils_tasks_get_main_task(),
		utils_tasks_signal_control_task_stopped());
}

int controls_workers_modules_rater_start(void)
{
	if(_modules_rater_task != NULL)
	{
		log_print_warning("controls_workers_modules_rater_start(), modules rater already started\n");
		return RETURN_WARN;
	}

	// Allow long operations
	_continue_long_operation_setter_fp(TRUE);

	_modules_rater_task = (struct Task *)CreateNewProcTags(
		NP_Entry,       &_modules_rater_loop,
		NP_Priority,    1,
		NP_Name,        RATER_NAME,
		NP_Output,      Output(),
		NP_CloseOutput, FALSE,
		NP_StackSize,   GLOBALS_TASK_STACK_SIZE,
		TAG_DONE);

	if(_modules_rater_task == NULL)
	{
		log_print_error("controls_workers_modules_rater_start(), could not create modules rater\n");
		return RETURN_ERROR;
	}

	return RETURN_OK;
}

void controls_workers_modules_rater_stop(void)
{
	if(_modules_rater_task == NULL)
	{
		log_print_warning("controls_workers_modules_rater_stop(), modules rater not started\n");
		return;
	}

	// Interrupt any long operation
	_continue_long_operation_setter_fp(FALSE);

	while(_modules_rater_task != NULL)
	{
		// Wait for the rater's loop to finish
	}
}

ULONG controls_workers_signal_control_modules_rated(void)
{
	return _SIGNALSET_TO_CONTROL_MODULES_RATED;
}

static void _modules_rater_loop(void)
{
	int new_all_rating = 0;
	int new_all_tally  = 0;
	
	if(ratings_init_connection() == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_CONTROLSMUI_MUICONTROLSSETUSERRATINGCOULDNOTCONNECTTORATING ) );
		goto _RETURN;
	}

	log_print_information( GetString( MSG_CONTROLSMUI_SAVINGRATING ) );
	if(ratings_add_user_rating(
				controls_get_current_file_address(),
				controls_get_current_user_rating(),
				&new_all_rating,
				&new_all_tally) == RETURN_OK)
	{
		controls_set_current_all_rating(new_all_rating);
		controls_set_current_all_tally(new_all_tally);
		
		utils_tasks_send_signal_without_ack(
			utils_tasks_get_main_task(),
			_SIGNALSET_TO_CONTROL_MODULES_RATED);
	}
	else
	{
		// TODO Have a signal to signal problem during rating
		utils_tasks_send_signal_without_ack(
			utils_tasks_get_main_task(),
			_SIGNALSET_TO_CONTROL_MODULES_RATED);
	}

	goto _RETURN;
	_RETURN:
		ratings_close_connection();
		_modules_rater_task = NULL;
}

int controls_workers_modules_downloader_start(
	IN char *directory,
	IN char *file,
	IN int   file_size)
{
	if(_modules_downloader_task != NULL)
	{
		log_print_warning("controls_workers_modules_downloader_start(), modules downloader already started\n");
		return RETURN_WARN;
	}

	_download_directory = string_duplicate(directory);
	_download_file      = string_duplicate(file);
	_download_file_size = file_size;

	// Allow long operations
	_continue_long_operation_setter_fp(TRUE);

	_modules_downloader_task = (struct Task *)CreateNewProcTags(
		NP_Entry,       &_modules_downloader_loop,
		NP_Priority,    1,
		NP_Name,        DOWNLOADER_NAME,
		NP_Output,      Output(),
		NP_CloseOutput, FALSE,
		NP_StackSize,   GLOBALS_TASK_STACK_SIZE,
		TAG_DONE);

	if(_modules_downloader_task == NULL)
	{
		log_print_error("controls_workers_modules_downloader_start(), could not create modules downloader\n");
		return RETURN_ERROR;
	}

	return RETURN_OK;
}

void controls_workers_modules_downloader_stop(void)
{
	if(_modules_downloader_task == NULL)
	{
		log_print_warning("controls_workers_modules_downloader_stop(), modules downloader not started\n");
		return;
	}

	// Interrupt any long operation
	_continue_long_operation_setter_fp(FALSE);

	while(_modules_downloader_task != NULL)
	{
		// Wait for the _modules_downloader_loop() to finish
	}
}

ULONG controls_workers_signal_control_modules_downloaded(void)
{
	return _SIGNALSET_TO_CONTROL_MODULES_DOWNLOADED;
}

static void _modules_downloader_loop(void)
{
	int result         = RETURN_ERROR;
	char *directory    = NULL;
	char *file         = NULL;
	int   file_size    = 0;
	BOOL  was_skipping = FALSE;

	// Download and play
	result = controls_remove_modules();
	if(result == RETURN_OK)
	{
		// Ignore file size limit!
		was_skipping = prefs_should_skip_if_above_maximum_file_size();
		prefs_set_skip_if_above_maximum_file_size(FALSE);

		result = controls_load_modules(_download_directory, _download_file, _download_file_size);

		// Restore file size limit
		prefs_set_skip_if_above_maximum_file_size(was_skipping);

		if(result == RETURN_OK)
		{
			controls_play_modules();

			utils_tasks_send_signal_and_wait_ack(
				utils_tasks_get_main_task(),
				controls_workers_signal_control_modules_downloaded(),
				utils_tasks_signal_task_to_do());
		}
		else
		{
			// What is there to do?
		}
	}
	else
	{
		// What is there to do?
	}

	free(_download_directory);
	_download_directory = NULL;

	free(_download_file);
	_download_file = NULL;

	_modules_downloader_task = NULL;
}


