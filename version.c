/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include "version.h"
#include "z_fortify.h"



/* Constants and declarations */

#define VERSION_TAG         "\0$VER: AmiModRadio v1.0 (" __DATE__ ")"
#define VERSION_DESCRIPTION "AmiModRadio v1.0 (" __DATE__ ")"
#define VERSION_INFORMATION "AmiModRadio v1.0 (" __DATE__ ")\n"

char *version_get_version(void);
int   version_get_version_length(void);



/* Definitions */

char *version_get_version_description(
	  void)
{
	char *version1 = VERSION_TAG;
	char *version2 = VERSION_DESCRIPTION;
	char *version3 = VERSION_INFORMATION;

	return version2;
}

int   version_get_version_description_length(
	  void)
{
	return strlen(version_get_version_description());
}

char *version_get_version_information(
	  void)
{
	char *version1 = VERSION_TAG;
	char *version2 = VERSION_DESCRIPTION;
	char *version3 = VERSION_INFORMATION;

	return version3;
}

int   version_get_version_information_length(
	  void)
{
	return strlen(version_get_version_information());
}

char *version_get_user_agent(
	  void)
{
	return "User-Agent: " VERSION_DESCRIPTION;
}

int   version_get_user_agent_length(
	  void)
{
	return strlen(version_get_user_agent());
}

