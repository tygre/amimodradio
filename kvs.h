/*
 * http://codereview.stackexchange.com/questions/63493/simple-key-value-store-in-c-take-2
 * CC BY-SA
 *
 * Parts copyright (c) 2015-2024 Tygre <tygre@chingu.asia>
 * These parts are under the same license CC BY-SA as the original source code.
 */



#ifndef KVS_H
#define KVS_H 1

#include "utils.h" // For IN, OUT...

typedef struct KVSstore KVSstore;
typedef struct KVSpair  KVSpair;
typedef void   KVSkey;
typedef void   KVSvalue;
typedef int    KVScompare(IN KVSkey *a, IN KVSkey *b);

KVSstore *kvs_create(
		  IN KVScompare *compare);

void      kvs_destroy(
		  IN KVSstore   *store);

void      kvs_put(
		  IN KVSstore   *store,
		  IN KVSkey     *key,
		  IN KVSvalue   *value);

KVSkey   *kvs_get_key(
		  IN KVSstore   *store,
		  IN size_t      index);

KVSvalue *kvs_get_value(
		  IN KVSstore   *store,
		  IN KVSkey     *key);

void      kvs_remove(
		  IN KVSstore   *store,
		  IN KVSkey     *key);

size_t    kvs_length(
		  IN KVSstore   *store);

int       kvs_compare_pointers(
		  IN KVSkey     *a,
		  IN KVSkey     *b);

int       kvs_compare_unsigned_longs(
		  IN KVSkey     *a,
		  IN KVSkey     *b);

int       kvs_compare_strings(
		  IN KVSkey     *a,
		  IN KVSkey     *b);
#endif
