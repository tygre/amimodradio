/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



#ifndef CONTROLS_WORKER_H
#define CONTROLS_WORKER_H 1

#include "utils.h"

int   controls_workers_setup(
	  IN BOOL (*continue_long_operation_getter_fp)(void),
	  IN void (*continue_long_operation_setter_fp)(IN BOOL should_continue_long_operation),
	  IN void (*data_size_callback_fp)(IN char *origin, IN int number_of_bytes),
	  IN void (*data_read_callback_fp)(IN char *origin, IN int number_of_bytes));

int   controls_workers_cleanup(
	  void);

ULONG controls_workers_signal_control_display_last_message(
	  void);

int   controls_workers_modules_fetcher_start(
	  void);

void  controls_workers_modules_fetcher_fetch(
	  void);

void  controls_workers_modules_fetcher_wait(
	  void);

void  controls_workers_modules_fetcher_stop(
	  void);

ULONG controls_workers_signal_control_fetching_modules(
	  void);

ULONG controls_workers_signal_control_modules_fetched(
	  void);

int   controls_workers_modules_rater_start(
	  void);

void  controls_workers_modules_rater_stop(
	  void);

ULONG controls_workers_signal_control_modules_rated(
	  void);

int   controls_workers_modules_downloader_start(
	  IN char *directory,
	  IN char *file,
	  IN int   file_size);

void  controls_workers_modules_downloader_stop(
	  void);

ULONG controls_workers_signal_control_modules_downloaded(
	  void);

#endif
