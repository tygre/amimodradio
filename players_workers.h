/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



#ifndef PLAYERS_WORKERS_H
#define PLAYERS_WORKERS_H 1

#include "utils.h" // For IN and OUT

int   players_workers_setup(void);

void  players_workers_cleanup(void);

int   players_workers_player_listener_start(void);

void  players_workers_player_listener_listen(void);

void  players_workers_player_listener_wait(void);

void  players_workers_player_listener_stop(void);

ULONG players_signal_control_playing_stopped(void);

ULONG players_workers_signal_control_playing_stopped(void);

#endif
