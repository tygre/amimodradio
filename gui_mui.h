/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



#ifndef GUI_MUI_H
#define GUI_MUI_H 1

#include <exec/types.h>

#include "utils.h"

typedef unsigned long IPTR;
typedef void (*gui_mui_child_window_clicked_callback_fp)(IN char *entry);
typedef void (*gui_mui_child_window_closed_callback_fp)(IN APTR window);

struct MUI_UI
{
	APTR   App;
	APTR   GR_grp_root;

	// Commands
	APTR   WI_label_title;
	APTR   IM_label_logo;

	APTR   IM_label_play;
	APTR   IM_label_replay;
	APTR   IM_label_pause;
	APTR   IM_label_stop;
	APTR   IM_label_next;

	APTR   TX_label_root_directory;
	APTR   IM_label_root_directory_prev;
	APTR   IM_label_root_directory_next;
	APTR   IM_label_root_directory_reset;

	APTR   STR_label_directory;
	APTR   IM_label_directory_set_as_root;
	APTR   IM_label_directory_list;
	APTR   WI_directory_list;
	APTR   LV_directory_list;
	APTR   IM_label_directory_ban;
	APTR   IM_label_directories_ban_list;
	APTR   WI_directories_ban_list;
	APTR   LV_directories_ban_list;

	APTR   STR_label_file;
	APTR   IM_label_file_list;
	APTR   WI_file_list;
	APTR   LV_file_list;
	APTR   IM_label_file_ban;
	APTR   IM_label_files_ban_list;
	APTR   WI_files_ban_list;
	APTR   LV_files_ban_list;

	APTR   TX_label_modules;
	APTR   IM_label_modules_save;
	APTR   IM_label_modules_email;

	APTR   IM_label_user_rating[5];
	APTR   TX_label_all_rating[5];
	APTR   TX_label_all_tally;
	
	APTR   TX_label_last_message;

	APTR   GA_progression_bar;

	// Prefs
	APTR   GR_grp_save_prefs_on_exit;
	APTR   GR_grp_play_on_startup;
	APTR   GR_grp_show_log_page_on_fetch;
	APTR   CY_label_maximum_log_size;
	APTR   GR_grp_remember_source_between_runs;
	APTR   GR_grp_remember_images_set;
	APTR   STR_label_saved_modules_directory;
	APTR   PA_label_saved_modules_directory;

	APTR   GR_grp_cache_list_during_run;
	APTR   GR_grp_cache_list_between_runs;
	APTR   STR_label_cache_directory;
	APTR   PA_label_cache_directory;

	APTR   GR_grp_skip_if_banned;
	APTR   GR_grp_skip_if_user_rating_low;
	APTR   GR_grp_skip_if_all_rating_exists;
	APTR   GR_grp_skip_if_all_rating_low;
	APTR   GR_grp_skip_if_above_maximum_file_size;
	APTR   CY_label_maximum_file_size;

	// Log
	APTR   LV_label_log;
	STRPTR LV_label_log_content;
};

int  gui_mui_init_ui(
	 OUT struct MUI_UI                            **ui_object);

void gui_mui_close_ui(
	 IN  struct MUI_UI                            **ui_object);

void gui_mui_child_window_list_open(
	 IN  struct MUI_UI                             *ui_object,
	 IN  char                                      *list,
	 IN  char                                      *help,
	 IN  char                                      *title,
	 OUT APTR                                      *view,
	 OUT APTR	                                   *window,
	 IN  gui_mui_child_window_clicked_callback_fp   callback_func_clicked,
	 IN  gui_mui_child_window_closed_callback_fp    callback_func_closed);

void gui_mui_child_window_list_update(
	 IN  APTR                                       view,
	 IN  char                                      *list);

#endif
