/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



#ifndef SOURCES_H
#define SOURCES_H 1

#include "utils.h"

typedef enum
{
	DIRECTORY_NAME,
	FILE_NAME,
	OTHER
} file_type_t;

void  sources_get_names(
	  OUT char        **source_names[]);

int   sources_get_number(
	  void);

char *sources_get_root_directory(
	  IN  char         *source_name);

int   sources_setup(
	  IN  BOOL        (*continue_long_operation)(),
	  IN  void        (*data_size_callback_fp)(IN char *origin, IN int number_of_bytes),
	  IN  void        (*data_read_callback_fp)(IN char *origin, IN int number_of_bytes));

void  sources_cleanup(
	  void);

int   sources_init_connection(
	  IN  char         *source_name);

int   sources_close_connection(
	  void);

int   sources_get_list(
	  IN  char         *directory_name,
	  OUT char        **list,
	  OUT int          *list_size);

int   sources_pick_file(
	  IN  char         *list,
	  OUT char        **file,
	  OUT file_type_t  *file_type,
	  OUT int          *file_size);

int   sources_get_file(
	  IN  char         *directory,
	  IN  char         *file,
	  IN  int           file_size,
	  OUT char        **file_bytes,
	  OUT int          *file_size_real);

#endif


