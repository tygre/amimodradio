/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



#ifndef CONTROLS_H
#define CONTROLS_H 1

#include <dos/dos.h> // For BPTR

#include "utils.h"

#define LOWEST_PLAYABLE_RATE 3

// Initialisation and closing

int   controls_setup(
	  IN BPTR     path_list,
	  IN void     (*data_size_callback_fp)(IN char *origin, IN int number_of_bytes),
	  IN void     (*data_read_callback_fp)(IN char *origin, IN int number_of_bytes));

void  controls_cleanup(
	  void);

// Controls

int   controls_fetch_modules(
	  void);

int   controls_play_modules(
	  void);

int   controls_load_modules(
	  IN char    *directory,
	  IN char    *file,
	  IN int      file_size);

int   controls_replay_modules(
	  void);

int   controls_pause_modules(
	  void);

int   controls_remove_modules(
	  void);

BOOL  controls_are_modules_loaded(
	  void);

BOOL  controls_are_modules_playing(
	  void);

// Blacklists

int   controls_ban_current_directory(
	  void);

int   controls_get_blacklisted_directories_list(
	  OUT char  **list);

int   controls_ban_current_file(
	  void);

int   controls_get_blacklisted_files_list(
	  OUT char  **list);

// Source

int   controls_get_sources_names(
	  OUT char ***list);

int   controls_get_sources_number(
	  void);

char *controls_get_current_source(
	  void);

int  controls_get_current_source_index(
	  void);

int  controls_get_prev_source_index(
	  void);

int  controls_get_next_source_index(
	  void);

int   controls_set_current_source_index(
	  IN int source_index);

// Root directory, directory, file, and modules

char *controls_get_current_root_directory(
	  void);

int   controls_set_current_root_directory(
	  IN char    *directory);

char *controls_get_current_directory(
	  void);

int   controls_set_current_directory(
	  IN char    *directory);

char *controls_get_current_directory_list(
	  void);

char *controls_get_current_file(
	  void);

char *controls_get_current_file_address(
	  void);

char *controls_get_current_file_list(
	  void);

int   controls_get_current_user_rating(
	  void);

void  controls_set_current_user_rating(
	  IN int);

int   controls_get_current_all_rating(
	  void);

void  controls_set_current_all_rating(
	  IN int new_all_rating);

int   controls_get_current_all_tally(
	  void);

void  controls_set_current_all_tally(
	  IN int new_all_tally);

// Misc.

int	  controls_display_version(
	  void);

void  controls_build_last_message(
	  IN char *message);

char *controls_get_last_message(
	  void);

ULONG controls_signal_control_playing_stopped(
	  void);

BOOL  controls_continue_long_operation_getter(
	  void);

void  controls_continue_long_operation_setter(
	  IN BOOL should_continue_long_operation);

#endif

