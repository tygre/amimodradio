/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



/* Includes */

#include "ratings.h"

#include "globals.h"
#include "httpx.h"
#include "kvs.h"
#include "locale.h"
#include "log.h"
#include "version.h"
#include "z_fortify.h"

#include <stdarg.h> // For va_start() and va_end()
#include <stdio.h>  // For FILE, fprintf()...



/* Constants and declarations */

#define RATINGS_HOST            "http://www.chingu.asia"
#define GET_USER_RATING         "h:whuimdieagpt%u=\"tp/wwcig.saaiordogtrtn.h?ah\"s&sr\"st/.na/ma/_ipp=\"e%"
#define GET_ALL_RATINGS         "h/hamaeihht/wci.sairdgttnppt=\"t:w.nui/mdi/_ag.?a\"spwgaoorsp%"
#define ADD_USER_RATING         "hwumiap%=t\"twwg.aidorth?\"sr\"aidt/.na/ma/_ipp=\"e%rn%p/cisaoradn.ah&ss&g\":hiddgtu\"="
#define API_IPIFY               "ht:/p.pf.rtp/aiiiyog"
#define IPv4_ADDRESS_MAX_LENGTH (3 + 1 + 3 + 1 + 3 + 1 + 3)

	   int  ratings_setup(IN BOOL (*)(), IN void (*)(IN char *, IN int), IN void (*)(IN char *, IN int));
	   void ratings_cleanup(void);
	   int  ratings_init_connection(void);
	   void	ratings_close_connection(void);
	   int  ratings_get_user_rating(IN char *, OUT int *);
	   int  ratings_get_all_ratings(IN char *, OUT int *, OUT int *);
	   int  ratings_add_user_rating(IN char *, IN int, OUT int *, OUT int *);
static int  _get_user_id(OUT char **);
static int  _get_server_answer(OUT char **, IN char *, IN int, ...);
static int  _retrieve_user_rating_from_memory(IN char *, OUT int *);
static int  _retrieve_all_ratings_from_memory(IN char *, OUT int *, OUT int *);
static void _cache_user_rating_in_memory(IN char *, IN int);
static void _cache_all_ratings_in_memory(IN char *, IN int, IN int);



/* Definitions */

static /*@only@*/ char *_cached_user_id                  = NULL;
static KVSstore        *_cached_user_ratings             = NULL; //
static KVSstore        *_cached_all_ratings              = NULL; // Tygre 19/02/05: To cache the ratings and save some roundtrips to the server
static KVSstore        *_cached_all_tallies              = NULL; //
static BOOL             (*_continue_long_operation_fp)() = NULL; // Tygre 20/08/02: To stop long-running HTTP operations
static void             (*_data_size_callback_fp)(
							IN char *origin,
							IN int number_of_bytes)      = NULL; // Tygre 22/12/19: To update a progress bar when necessary
static void             (*_data_read_callback_fp)(
							IN char *origin,
							IN int number_of_bytes)      = NULL; // Tygre 22/12/19: To update a progress bar when necessary



int ratings_setup(
	IN  BOOL (*continue_long_operation_fp)(),
	IN void  (*data_size_callback_fp)(IN char *origin, IN int number_of_bytes),
	IN void  (*data_read_callback_fp)(IN char *origin, IN int number_of_bytes)
)
{
	log_print_debug("ratings_setup()\n");

	_continue_long_operation_fp = continue_long_operation_fp;
	_data_size_callback_fp      = data_size_callback_fp;
	_data_read_callback_fp      = data_read_callback_fp;

	if(httpx_setup(
		_continue_long_operation_fp,
		_data_size_callback_fp,
		_data_read_callback_fp) == RETURN_ERROR)
	{
		return RETURN_ERROR;
	}
	
	_cached_user_id      = NULL;
	_cached_user_ratings = kvs_create(kvs_compare_strings);
	_cached_all_ratings  = kvs_create(kvs_compare_strings);
	_cached_all_tallies  = kvs_create(kvs_compare_strings);
	if(_cached_user_ratings == NULL ||
	   _cached_all_ratings  == NULL ||
	   _cached_all_tallies  == NULL)
	{
		ratings_cleanup();
		return RETURN_ERROR;
	}

	return RETURN_OK;
}

void ratings_cleanup(
	 void)
{
	log_print_debug("ratings_cleanup()\n");

	kvs_destroy(_cached_user_ratings);
	kvs_destroy(_cached_all_ratings);
	kvs_destroy(_cached_all_tallies);
	
	if(_cached_user_id != NULL)
	{
		free(_cached_user_id);
		_cached_user_id = NULL;
	}

	httpx_cleanup();
}

int ratings_init_connection(
	void)
{
	log_print_debug("ratings_init_connection()\n");

	return httpx_init_connection(HTTP);
}

void ratings_close_connection(
	 void)
{
	log_print_debug("ratings_close_connection()\n");

	httpx_close_connection(HTTP);
}

int ratings_get_user_rating(
	 IN  char *path,
	 OUT int  *rating)
{
	char *answer  = NULL;

	if(_retrieve_user_rating_from_memory(path, rating) == RETURN_OK)
	{
		return RETURN_OK;
	}

	if(_cached_user_id == NULL && _get_user_id(&_cached_user_id) != RETURN_OK)
	{
		log_print_error( GetString( MSG_RATINGS_RATINGSGETUSERRATINGCOULDNOTFINDUSERID ) );
		goto _RETURN_ERROR;
	}

	if(_get_server_answer(&answer, GET_USER_RATING, 3, path, _cached_user_id) != RETURN_OK)
	{
		log_print_error( GetString( MSG_RATINGS_RATINGSGETUSERRATINGCOULDNOTGETANSWERFROMSERVER ) );
		goto _RETURN_ERROR;
	}

	// Answer is of the form "rating"
	*rating = atoi(answer); /* Flawfinder: ignore */

	// Clean up in case...
	*rating = integer_max(integer_min(*rating, 5), 0);

	_cache_user_rating_in_memory(path, *rating);

	goto _RETURN_OK;
	_RETURN_OK:
		free(answer);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		*rating = 0;
		if(answer != NULL)
		{
			free(answer);
		}
		return RETURN_ERROR;
}

int ratings_get_all_ratings(
	 IN  char *path,
	 OUT int  *ratings,
	 OUT int  *tally)
{
	char *answer = NULL;

	if(_retrieve_all_ratings_from_memory(path, ratings, tally) == RETURN_OK)
	{
		return RETURN_OK;
	}

	if(_get_server_answer(&answer, GET_ALL_RATINGS, 4, path) != RETURN_OK)
	{
		log_print_error( GetString( MSG_RATINGS_RATINGSGETALLRATINGSCOULDNOTGETANSWERFROMSERVER ) );
		goto _RETURN_ERROR;
	}

	// Answer is of the form "ratings;tally"
	*ratings  = atoi(strtok(answer, ";\n")); /* Flawfinder: ignore */
	*tally    = atoi(strtok(NULL, ";\n"));   /* Flawfinder: ignore */

	// Clean up in case...
	*tally    = integer_max(*tally, 0);
	if(*tally == 0)
	{
		*ratings = 0;
	}
	else
	{
		*ratings = integer_max(integer_min((*ratings) / (*tally), 5), 0);
	}

	_cache_all_ratings_in_memory(path, *ratings, *tally);

	goto _RETURN_OK;
	_RETURN_OK:
		free(answer);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		*ratings = 0;
		*tally   = 0;
		if(answer != NULL)
		{
			free(answer);
		}
		return RETURN_ERROR;
}

int ratings_add_user_rating(
	IN  char *path,
	IN  int   rating,
	OUT int  *ratings,
	OUT int  *tally)
{
	char *answer     = NULL;
	int   new_rating = -1;

	if(_cached_user_id == NULL && _get_user_id(&_cached_user_id) != RETURN_OK)
	{
		log_print_error( GetString( MSG_RATINGS_RATINGSADDUSERRATINGCOULDNOTFINDUSERID ) );
		goto _RETURN_ERROR;
	}

	if(_get_server_answer(&answer, ADD_USER_RATING, 5, path, _cached_user_id, rating) != RETURN_OK)
	{
		log_print_error( GetString( MSG_RATINGS_RATINGSADDUSERRATINGCOULDNOTGETANSWERFROMSERVER ) );
		goto _RETURN_ERROR;
	}

	// answer is of the form "rating"
	new_rating = atoi(answer); /* Flawfinder: ignore */
	if(new_rating != rating)
	{
		log_print_error( GetString( MSG_RATINGS_RATINGSADDUSERRATINGSAVEDRATINGANDRETURNEDRATINGARE ) );
		goto _RETURN_ERROR;
	}

	// Update the caches, avoiding to call the Web server again...
	_cache_user_rating_in_memory(path, new_rating);

	_retrieve_all_ratings_from_memory(path, ratings, tally);
	*ratings += new_rating;
	*tally   += 1;
	*ratings = integer_max(integer_min((*ratings) / (*tally), 5), 0);
	_cache_all_ratings_in_memory(path, *ratings, *tally);

	goto _RETURN_OK;
	_RETURN_OK:
		free(answer);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(answer != NULL)
		{
			free(answer);
		}
		return RETURN_ERROR;
}

static int _get_user_id(
	OUT char **user_id)
{
	char *answer = NULL;
	int   length = 0;

	if(_get_server_answer(&answer, API_IPIFY, 2) != RETURN_OK)
	{
		log_print_error( GetString( MSG_RATINGS_GETUSERIDCOULDNOTGETANSWERFROMSERVER ) );
		goto _RETURN_ERROR;
	}

	// The answer is of the form "69.164.204.65"
	// This code is not IPv6 ready!
	length      = IPv4_ADDRESS_MAX_LENGTH;
	*user_id    = malloc((length + 1) * sizeof(char));
	if(*user_id == NULL)
	{
		log_print_error("_get_user_id(), could not allocate user ID\n");
		goto _RETURN_ERROR;
	}
	*user_id[0] = '\0';
	strncat(*user_id, answer, length);

	goto _RETURN_OK;
	_RETURN_OK:
		free(answer);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		if(answer != NULL)
		{
			free(answer);
		}
		return RETURN_ERROR;
}

static int _get_server_answer(
	OUT char **answer,
	IN  char  *cipher_url,
	IN  int    cipher_value,
	...)
{
	int             length       = 0;
	char           *url_prefix   = NULL;
	char           *user_id      = NULL;
	char           *url          = NULL;
	va_list         arguments;
	char           *encoded_url1 = NULL;
	char           *encoded_url2 = NULL;
	int		        size         = 0;
	char           *pos          = NULL;
	int             result       = RETURN_ERROR;
	httpx_response *hresp        = NULL;
	char		   *first_EOF    = NULL;

	length        = strlen(cipher_url);
	url_prefix    = malloc((length + 1) * sizeof(char));
	if(url_prefix == NULL)
	{
		log_print_error( GetString( MSG_RATINGS_GETSERVERANSWERCOULDNOTALLOCATEURLPREFIX ) );
		goto _RETURN_ERROR;
	}
	url_prefix[0] = '\0';
	string_decipher(cipher_value, cipher_url, url_prefix);

	length = GLOBALS_MAX_LINE_LENGTH;
	url    = malloc((length + 1) * sizeof(char));
	if(url == NULL)
	{
		log_print_error( GetString( MSG_RATINGS_GETSERVERANSWERCOULDNOTALLOCATEURL ) );
		goto _RETURN_ERROR;
	}
	va_start(arguments, cipher_value);
	vsnprintf(url, GLOBALS_MAX_LINE_LENGTH, url_prefix, arguments); /* Flawfinder: ignore */
	va_end(arguments);

	encoded_url1 = string_replace_all(url, " ", "%20");
	if(encoded_url1 == NULL)
	{
		log_print_error( GetString( MSG_RATINGS_GETSERVERANSWERCOULDNOTENCODESPACEINURL ) );
		goto _RETURN_ERROR;
	}

	encoded_url2 = string_replace_all(encoded_url1, "#", "%23");
	if(encoded_url2 == NULL)
	{
		log_print_error( GetString( MSG_RATINGS_GETSERVERANSWERCOULDNOTENCODEHASHINURL ) );
		goto _RETURN_ERROR;
	}

	if(_data_size_callback_fp != NULL &&			 // Tygre 22/12/19: To update a progress bar when necessary
	   string_equal(cipher_url, API_IPIFY) == FALSE) // Tygre 23/01/05: IPify doesn't support the HEAD method
	{
		result = httpx_head(encoded_url2, NULL, &hresp);
		if(result == RETURN_OK)
		{
			pos = strstr(hresp->response_headers, "Content-Length: ");
			if(pos != NULL)
			{
				pos = pos + 16 * sizeof(char);
				size = atoi(pos); /* Flawfinder: ignore */
			}
			else
			{
				size = 0;
			}
			httpx_free_httpx_response(hresp);
		}
		else
		{
			size = 0;
		}
		_data_size_callback_fp("_get_server_answer() (size)", size);
	}

	result = httpx_get(encoded_url2, NULL, 0, &hresp);
	if(result == RETURN_ERROR)
	{
		log_print_error( GetString( MSG_RATINGS_GETSERVERANSWERCOULDNOTGETANANSWER ) );
		goto _RETURN_ERROR;      
	}

	length     = hresp->body_size_in_bytes;
	*answer	   = malloc((length + 1) * sizeof(char));
	if(*answer == NULL)
	{
		log_print_error( GetString( MSG_RATINGS_GETSERVERANSWERCOULDNOTALLOCATEANSWER ) );
		goto _RETURN_ERROR;
	}
	*answer[0] = '\0';
	strncat(*answer, hresp->body, length);

	// Remove the first line, made of one or more digits and '\n'
	first_EOF = strchr(*answer, '\n');
	if(first_EOF != NULL)
	{
		memmove(*answer, first_EOF + 1, strlen(first_EOF) - 1);
	}

	goto _RETURN_OK;
	_RETURN_OK:
		httpx_free_httpx_response(hresp);
		free(encoded_url2);
		free(encoded_url1);
		free(url);
		free(user_id);
		free(url_prefix);
		return RETURN_OK;

	goto _RETURN_ERROR;
	_RETURN_ERROR:
		httpx_free_httpx_response(hresp);
		if(encoded_url2 != NULL)
		{
			free(encoded_url2);
		}
		if(encoded_url1 != NULL)
		{
			free(encoded_url1);
		}
		if(url != NULL)
		{
			free(url);
		}
		if(user_id != NULL)
		{
			free(user_id);
		}
		if(url_prefix != NULL)
		{
			free(url_prefix);
		}

		return RETURN_ERROR;
}

static int  _retrieve_user_rating_from_memory(
	IN  char *path,
	OUT int  *user_rating)
{
	int *user_rating_temp = NULL;
	
	user_rating_temp = (int *)kvs_get_value(_cached_user_ratings, path);

	if(user_rating_temp == NULL)
	{
		return RETURN_ERROR;
	}

	*user_rating = *user_rating_temp;

	return RETURN_OK;
}

static int  _retrieve_all_ratings_from_memory(
	IN  char *path,
	OUT int  *all_ratings,
	OUT int  *all_tallies)
{
	int *all_ratings_temp = NULL;
	int *all_tallies_temp = NULL;

	all_ratings_temp = (int *)kvs_get_value(_cached_all_ratings, path);
	all_tallies_temp = (int *)kvs_get_value(_cached_all_tallies, path);

	if(all_ratings_temp == NULL || all_tallies_temp == NULL)
	{
		return RETURN_ERROR;
	}

	*all_ratings = *all_ratings_temp;
	*all_tallies = *all_tallies_temp;

	return RETURN_OK;
}

static void _cache_user_rating_in_memory(
	IN char *path,
	IN int   user_rating)
{
	int *user_rating_temp = NULL;

	user_rating_temp    = malloc(1 * sizeof(int));
	if(user_rating_temp == NULL)
	{
		log_print_error("_cache_user_rating_in_memory(), could not allocate user rating\n");
		return;
	}
	*user_rating_temp = user_rating;
	
	kvs_put(_cached_user_ratings, string_duplicate(path), user_rating_temp);
}

static void _cache_all_ratings_in_memory(
	IN char *path,
	IN int   all_ratings,
	IN int   all_tallies)
{
	int *all_ratings_temp = NULL;
	int *all_tallies_temp = NULL;

	all_ratings_temp = malloc(1 * sizeof(int));
	if(all_ratings_temp == NULL)
	{
		log_print_error("_cache_all_ratings_in_memory(), could not allocate all ratings\n");
		return;
	}
	*all_ratings_temp = all_ratings;

	kvs_put(_cached_all_ratings, string_duplicate(path), all_ratings_temp);

	all_tallies_temp = malloc(1 * sizeof(int));
	if(all_tallies_temp == NULL)
	{
		log_print_error("_cache_all_ratings_in_memory(), could not allocate all tallies\n");
		return;
	}
	*all_tallies_temp = all_tallies;

	kvs_put(_cached_all_tallies, string_duplicate(path), all_tallies_temp);
}
