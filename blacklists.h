/*
	AmiModRadio
	All of Aminet modules at your fingertips



	Copyright 2015-2024 Tygre <tygre@chingu.asia>

	This file is part of AmiModRadio.

	AmiModRadio is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	AmiModRadio is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with AmiModRadio. If not, see <http://www.gnu.org/licenses/>.



	Entirely developed on an Amiga!
	(Except for source code versioning...)
	tygre@chingu.asia
*/



#ifndef BLACKLISTS_H
#define BLACKLISTS_H 1

#include "utils.h" // For IN and OUT

#include <stdio.h> // For void

int  blacklists_setup(
	 void);

void blacklists_cleanup(
	 void);

int  blacklists_init_files(
	 void);

void blacklists_close_files(
	 void);

BOOL blacklists_is_blacklisted_directory(
	 IN  char  *directory_name);

BOOL blacklists_is_blacklisted_file(
	 IN  char  *file_name);

int  blacklists_list_blacklisted_directories(
	 OUT char **list);

int  blacklists_list_blacklisted_files(
	 OUT char **list);

int  blacklists_blacklist_directory(
	 IN  char  *directory_name);

int  blacklists_blacklist_file(
	 IN  char  *file_name);

#endif
